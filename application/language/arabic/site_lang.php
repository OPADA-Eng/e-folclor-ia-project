<?php
$lang['update_success']='تم التعديل النجاح';
$lang['update_failed']='حدث مشكلة أثناء التعديل';
$lang['deleted_success']='تم الحذف بنجاح';
$lang['add_balance_success']='تمت إضافة الرصيد بنجاح';
$lang['add_balance_error']='خطأ باسم المستخدم أو كلمة السر';
$lang['buy_success']='تمت عملية الشراء بنجاح';
$lang['buy_error']='فشلت عملية الشراء';
$lang['close_success']='تم ترصيد الفاتورة بنجاح';
$lang['no_enough_balance']='ليس لديك رصيد كاف';
$lang['message_success']='تم إرسال الرسالة بنجاح';
$lang['message_error']='فشل إرسال الرسالة';
$lang['message_success_del']='تم حذف الرسالة بنجاح';
$lang['message_error_del']='فشل بحذف الرسالة';
$lang['no_priv_del']='ليس لديك صلاحية لحذف هذه الرسالة';
$lang['select_sure']='هل أنت متأكد من اختيارك للرسالة الصحيحة';
$lang['no_messages']='لا يوجد رسائل بالصندوق';
?>