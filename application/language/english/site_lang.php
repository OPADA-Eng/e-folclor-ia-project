<?php
$lang['update_success']='update successfully';
$lang['update_failed']='update failed';
$lang['deleted_success']='deleted successfully';
$lang['add_balance_success']='balance added to your account successfully';
$lang['add_balance_error']='error in user name or password';
$lang['buy_success']='buy successfully';
$lang['buy_error']='buy failed';
$lang['close_success']='closed bill successfully';
$lang['no_enough_balance']='no enough balance';
$lang['message_success']='message sent successfully';
$lang['message_error']='send message failed';
$lang['message_success_del']='message deleted successfully';
$lang['message_error_del']='delete message failed';
$lang['no_priv_del']='You Don\'t have privileges to delete this message';
$lang['select_sure']='Oh no :( be sure you are select the right message';
$lang['no_messages']='no messages in this box';


?>