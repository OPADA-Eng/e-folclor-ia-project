<?php

$lang["msg_home"] 				= "Home";
$lang["msg_about"] 				= "About Us";
$lang["msg_products"] 			= "Products";
$lang["msg_contact"] 			= "Contact Us";
$lang["msg_contact_details"]	= "Contact Details";
$lang["msg_welcome"] 			= "Welcome to Monster Company";
$lang["msg_copy_rights"] 		= "All Copy rights reserved";
$lang["msg_designed_by"] 		= "Designed By";
$lang["msg_email"] 				= "Email";
$lang["msg_phone"] 				= "Phone";
$lang["msg_leave_message"] 		= "Please Leave A Message";
$lang["msg_leave_phone"] 		= "Please insert your phone number";
$lang["msg_leave_name"] 		= "Please insert your name";
$lang["msg_leave_email"] 		= "Please insert your email";
$lang['msg_name']				= 'Name';
$lang['msg_email']				= 'Email';
$lang['msg_phone']				= 'Phone';
$lang['msg_quantity']				= 'Quantity';
$lang["msg_send"] = "Send";

$lang['msg_category_name_ar'] 	= 'Arabic Category name';
$lang['msg_category_name_en']	= 'English Category name';
$lang['msg_add'] 				= 'Add';
$lang['msg_edit'] 				= 'Edit';
$lang['msg_delete'] 			= 'Delete';

$lang['msg_success_add'] 		= 'Item added successfully'; 
$lang['msg_success_remove'] 	= 'Item removed successfully';
$lang['msg_success_edit']  		= 'Item Edited successfully';

$lang['msg_add_category'] 		= 'Add new category';
$lang['msg_delete_category'] 	= 'Delete category';
$lang['msg_edit_category'] 		= 'Edit Category';

$lang['msg_manage_category'] 	= 'Manage Categories';
$lang['msg_no_more_cat']		= 'No more categories';
$lang['msg_db_error'] 			= 'An Error occured in database operation';
$lang['msg_delete_failed']		= 'An Error occured while deleteing the item, please try again..';
$lang['msg_chose_cat']			= 'Chose category';

$lang['msg_product_name']		= 'English Product Name';
$lang['msg_product_name_ar']	= 'Arabic Product Name';
$lang['msg_product_desc'] 		= 'English Product description';
$lang['msg_product_desc_ar']	= 'Arabic Product description';
$lang['msg_add_prod_img']		= 'Add product image, Best resolution is : 1000×800';
$lang['msg_add_product'] 		= 'Add new product'; 
$lang['msg_manage_products'] 	= 'Manage Products'; 
$lang['msg_edit_prod_img']		= 'Edit product picture'; 
$lang['msg_order_product']		= 'Order product';
$lang['msg_leave_quantity']		= 'Enter quantity';
$lang['msg_no_data']			= 'No data to be showed';
$lang['msg_no_product']			= 'There is no product in this category';
$lang['msg_note']				= 'Notes';
$lang['msg_send_success']		= 'Message sent successfully';
$lang['msg_error_send']			= 'An Error occurred while sending the message';
