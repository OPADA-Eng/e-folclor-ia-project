<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

// repository
$lang['name_en']				='english name';
$lang['name_ar']				='arabic name';
$lang['capacity']				='capacity';
$lang['add-msg-successful']		='added successfully';
$lang['add-msg-failed']			='adding failed';
$lang['edit-msg-successful']	='edited successfully';
$lang['edit-msg-failed']		='editing failed';
$lang['delete-msg-successful']	='deleted successfully';
$lang['delete-msg-failed']		='deleting failed';