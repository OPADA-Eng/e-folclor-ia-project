<?php
	/**
	 *  get the user's vote then add it to db
	 */
class Product_voting extends CI_Controller
   {		
	function index($product_id)
	{
		$query = $this->product_voting_model->get_product_rate($product_id);
		$data['records'] = $query;
		$this->load->view('votingSystem/vote_a_product',$data);	
	}
	
	function vote()
	{
		$this->form_validation->set_rules('product_id', 'product id', 'required|numeric');
		$this->form_validation->set_rules('rate', 'rate', 'required|numeric');			
		if($this->form_validation->run())
		{
			$this->load->library('ion_auth');
			if($this->ion_auth->logged_in()){
				$row = array(
					'user_id'=> $this->session->userdata('user_id'),
					'pc_id'=> $this->input->post('product_id'),
					'pv_voting'=> $this->input->post('rate')	
				 );	
				 $product_id = $row['pc_id'];	
				 $user_id =$row['user_id'];
				  //add vote 
				 $this->addVote($product_id,$user_id,$row);
				
				 //update voting panel
				 $this->return_votes($product_id,$user_id);
			}
			else{
				echo 'per_error';
			}
		}
		else {
			echo "rate_faild";
		}
		
	}	
		function addVote($product_id,$user_id,$row)
		{
			$result= array();
			$result = $this->product_voting_model->get_product_user_rates($product_id,$user_id);	
			foreach ($result as $data):
 	       		$rate_db[] = $data;
		 	 endforeach;	
			if(@count($rate_db)==0)
	       	{
	     		 $this->product_voting_model->add_rate_for_product($row);
			}
			else
			{
				$this->product_voting_model->update_user_rate($row['pv_voting'],$user_id,$product_id);	
			}
		}
		
		function return_votes($pc_id,$user_id)
		{
			$result= array();
			$result = $this->product_voting_model->get_products_rates($pc_id);	
			foreach ($result as $data):
 	       		$rate_db[] = $data;
	       		$sum_rates[] = $data->pv_voting;
		 	 endforeach;	
			 
	       	if(@count($rate_db))
	       	{
	            $rate_times = count($rate_db);
	            $sum_rates = array_sum($sum_rates);
	            $rate_value = $sum_rates/$rate_times;
	            $rate_bg = (($rate_value)/5)*100;
				$this->product_voting_model->update_product_rate($rate_value,$pc_id);
				$data = array("rate_times"=>$rate_times,"rate_value"=>$rate_value,"rate_bg"=>$rate_bg,"user_id"=>$user_id);
				echo json_encode($data);
	       	}
	       	else
	       	{
	            $rate_times = 0;
	            $rate_value = 0;
	            $rate_bg = 0;	
		   	}
		}
	}
?>