<?php

/**
 * 
 */
class Contact_us extends CI_Controller {
	
	function __construct() {
		parent::__construct(); 
		$this->load->library('recaptcha');
		$this->lang->load('recaptcha');
	}
	
	function index()
	{
		$data = array('view_page' => 'template/contact_us','map'=> $this->configure_map(),
		'message'=>'','recaptcha' =>$this->recaptcha->get_html() );
		$this->load->view('view_controller',$data);
	}
	
	private function configure_map($value='')
	{
		$this->load->library('googlemaps');
		$config = array();
		$marker = array();
		$config['center'] = '33.492336,36.248139';
		$marker['position'] = '33.492336,36.248139';
		$config['zoom'] = '17';
		$config['places'] = TRUE; 
		$this->googlemaps->initialize($config);
		$this->googlemaps->add_marker($marker);
		
		return $this->googlemaps->create_map();
	}
	
	public function send()
	{
		
		$this->form_validation->set_rules('name','الاسم','required|trim');
		$this->form_validation->set_rules('email','البريد الالكتروني','required|trim');
		$this->form_validation->set_rules('phone','الهاتف','required|trim');
		$this->form_validation->set_rules('message','نص الشكوى','required|trim');
		$this->form_validation->set_rules('recaptcha_response_field', 'lang:recaptcha_field_name', 'required|callback_check_captcha');
		if($this->form_validation->run()=== True){
			$name = $this->input->post('name',TRUE);
			$phone = $this->input->post('phone',TRUE);
			$email = $this->input->post('email',TRUE);
			$mes = $this->input->post('message',TRUE);
			$message = 'Name : '.$name."\n".'Phone: '.$phone."\n".'Email : '.$email."\n".$mes;
			//var_dump($message);die();	
			$this->load->library('email');
			$this->email->subject('');
			$this->email->from('visitor@folclor.com');
		 	$this->email->to('m.opada.albosh@gmail.com');
            $this->email->message($message);
            if($this->email->send()){
				$data['message']= 'تم إرسال الرسالة بنجاح شكرا لك';
            }
            else 
            {
            	
                $data['message']= 'لم يتم إرسال الرسالة بسبب حدوث خطأ يرجى المحاولة من جديد';
            }
			
		}
		else {
			$data['message']= validation_errors();
			if (validation_errors() != '')
				$data['message'].='<br>الرجاء تحديث رمز التحقق '; 
			
		}
		$data['recaptcha'] = $this->recaptcha->get_html();
		$data['map'] = $this->configure_map();
		$data['view_page'] = 'template/contact_us';
		$this->load->view('view_controller',$data);
	}
}
