<?php

/**
 * 
 */
class About extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		
	}
	
	function index()
	{
		$data = array('view_page' => 'template/about' );
		$this->load->view('view_controller',$data);
	}
}
