<?php
class LangSwitch extends CI_Controller
{
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
    }
 
    function switchLanguage($language = "",$controller_name='',$func_name='',$parm='') {
        $language = ($language != "") ? $language : "arabic";
		//$this->config->set_item('language',$language);
        $this->session->set_userdata('site_lang', $language);
		
        redirect($controller_name.'/'.$func_name.'/'.$parm);
    }
}