<?php

/**
 * 
 */
class Main extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->library('ion_auth');
		$this->load->model('product_manage_model');
		$this->load->model('foloclor_manage_model');
		$this->load->helper('language');
		$this->lang->load('site',get_current_lang());
	}
	public function index()
	{


		$data['last_folclor']=$this->foloclor_manage_model->get_last_folclor();
		$data['last_added_products']=$this->product_manage_model->get_recently_add_products(4);
		$data['foloclor_articles'] = $this->foloclor_manage_model->get_limit_approved_foloclor_content();//get approved contents.
		$data['popular_f']=$this->foloclor_manage_model->get_popular_foloclors(9);
		$data['recently_f_added']=$this->foloclor_manage_model->get_recently_add_foloclors(3);
		$data['foloclors_tags']=$this->foloclor_manage_model->get_foloclors_tags();
		$data['top_products'] = $this->product_manage_model->get_toplikes_products(4);
        $data['top_commented_f']=$this->foloclor_manage_model->get_topcomments_foloclors(TOP_FOLOCLORS_COMMENTED);
		$data['view_page'] = 'template/home';
		$this->load->view('view_controller',$data);
		
	}
	
}
