<?php
/*
 * 
 */
class Fmg extends CI_Controller {
	
	function __construct() {
		parent::__construct();  
		$this->load->library('ion_auth');  
		$this->load->helper('language');
		$this->lang->load('site',get_current_lang());
		if (!$this->ion_auth->logged_in())
			 redirect(base_url());
		
	}
	public function index()
	{
	}
	
	// get all accepted foloclors.	
	public function load_foloclor_contents()
	{
		$this->data['result'] = "sucess";
		$this->data['foloclor_articles'] = $this->foloclor_manage_model->get_approved_foloclor_content();//get approved contents.
		$this->data['user_id'] = $this->session->userdata('user_id');
		//echo json_encode($this->data['foloclor_articles']);
		
		//here must load page that need to display contents
		$this->load->view('foloclor/browse_all_foloclors', $this->data);
	}

	
	//get foloclor details.
	public function load_foloclor_content_by_id()
	{

		$f_id = $this->input->post('f_id');
		$this->data['foloclor_details'] = $this->foloclor_manage_model->get_foloclor_conten_by_id($f_id);
		$this->load->view('foloclor/browse_a_foloclor', $this->data);
	}
	
	public function load_toplikes_foloclors()
	{
		$this->data['top_foloclors'] = $this->foloclor_manage_model->get_toplikes_foloclors(TOP_FOLOCLORS_RATED);
		echo json_encode($this->data['top_foloclors']);
		//go to target view here
	}
	
	public function load_topcomments_foloclors()
	{
		$this->data['most_commented_foloclors'] = $this->foloclor_manage_model->get_topcomments_foloclors(TOP_FOLOCLORS_COMMENTED);
		echo json_encode($this->data['most_commented_foloclors']);
	}
	
	
	

	public function add_foloclor_content_by_user()
	{
		//if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"add_foloclor")){
			//this to send all categories to view and make user select one to add content relted to it.
			$this->data['foloclor_categories'] = $this->foloclor_manage_model->get_foloclor_gategories();
			// these are for auto complete tags and keys.
			$this->data['tags'] = json_encode($this->foloclor_manage_model->get_tags());
			$this->data['keys'] = json_encode($this->foloclor_manage_model->get_keywords());
			
			 // load target page.
			$this->data['cp_user_page']='cp_user/add_foloclor_by_user_'.get_current_lang_letter();
			$this->data['view_page']='cp_user/cp_user';
			$this->load->view('view_controller', $this->data);	
			 
		//}
		//else {
			//$this->data['result'] = "faild";
		  	/*
			 * 	$this->data['view_page']='cpanel/edit_foloclor';
			 *  $this->load->view('cpanel/cp_view_controller', $this->data);
			 *  here must load page that need to display contents
			 */
		//}
	}

	public function getContentTotag()
	{
		$tag_id = $this->input->post('tag_id');
		$this->data['foloclor_contents'] = $this->foloclor_manage_model->get_foloclor_content_with_tag($tag_id);
		echo json_encode($this->data['foloclor_contents']);
	}
	
	public function add_foloclor_record()
	{
		$this->form_validation->set_rules('fc_name', 'category_name', 'required|numeric');
		$this->form_validation->set_rules('f_title_ar', 'f_title_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_title_en', 'f_title_en', 'required|trim|xss_clean');	
		$this->form_validation->set_rules('f_content_ar', 'f_content_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_content_en', 'f_content_en', 'required|trim|xss_clean');	
		$this->form_validation->set_rules('f_hi_ar', 'f_hi_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_hi_en', 'f_hi_en', 'required|trim|xss_clean');	
		$this->form_validation->set_rules('f_location_ar', 'f_location_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_location_en', 'f_location_en', 'required|trim|xss_clean');			
		if($this->form_validation->run())
		{
			$row = array(
				'user_id'=>$this->session->userdata('user_id'),
				'f_cat_id'=> $this->input->post('fc_name'),
				'f_title_ar'=> $this->input->post('f_title_ar'),
				'f_title_en'=> $this->input->post('f_title_en'),
				'f_content_ar'=> $this->input->post('f_content_ar'),
				'f_content_en'=> $this->input->post('f_content_en'),
				'f_historical_importance_ar'=> $this->input->post('f_hi_ar'),
				'f_historical_importance_en'=> $this->input->post('f_hi_en'),
				'f_location_ar'=>  $this->input->post('f_location_ar'),
				'f_location_en'=> $this->input->post('f_location_en'),
				'publish_state' => 7
				
			 );

			$last_add = $this->foloclor_manage_model->create_new_foloclor_content($row);
			foreach ($this->input->post('tags') as $tag) {
				$this->foloclor_manage_model->add_tags_to_content($tag,$last_add);
			}
			
			foreach ($this->input->post('keys') as $key) {
				$this->foloclor_manage_model->add_keywords_to_content($key,$last_add);
			}	
			echo $last_add;
		}
	}
// after add set multimedia fields
	public function update_mm_foloclor_record()
	{
		$this->form_validation->set_rules('f_image', 'f_image', 'xss_clean');	
		$this->form_validation->set_rules('f_video', 'f_video', 'xss_clean');	
		$this->form_validation->set_rules('f_sound', 'f_sound', 'xss_clean');	
		if($this->form_validation->run())
		{
			$data = array('f_image' => $this->input->post('f_image') ,
						 'f_video' => $this->input->post('f_video') ,
						 'f_sound' => $this->input->post('f_sound'));
			$f_id = $this->input->post('f_id');
			$this->foloclor_manage_model->add_mm_to_content($data,$f_id);
		}
	}
}
?>