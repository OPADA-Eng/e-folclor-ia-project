<?php 
class Foloclor extends CI_Controller
{
	private $data = array();
	function __construct() {
		parent::__construct();  
		$this->load->library('ion_auth');  
		$this->load->model('product_manage_model');
		$this->load->model('foloclor_manage_model');
	}
	public function index($view_page='foloclor/foloclors')
	{
		$this->data['view_page'] = $view_page;
		if($view_page == 'foloclor/foloclors'){
			$this->data['foloclors'] = $this->foloclor_manage_model->get_folclors();
		}
		$this->data['foloclors_tags']=$this->foloclor_manage_model->get_foloclors_tags();
		$this->data['top_products'] = $this->product_manage_model->get_toplikes_products(TOP_PRODUCTS_RATED);
		$this->data['p_categories'] = $this->product_manage_model->get_product_categories();
		$this->data['popular_f']=$this->foloclor_manage_model->get_popular_foloclors(9);
		$this->data['recently_f_added']=$this->foloclor_manage_model->get_recently_add_foloclors(3);
	    $this->data['top_commented_f']=$this->foloclor_manage_model->get_topcomments_foloclors(TOP_FOLOCLORS_COMMENTED);
	    $this->data['foloclors_tags']=$this->foloclor_manage_model->get_foloclors_tags();
		
		$this->data['f_categories'] = $this->foloclor_manage_model->get_foloclor_categories();
		$this->load->view('view_controller',$this->data);
	}
	
	public function category($id='')
	{
		if($id ==''){
			$this->index();
		}
		else {
			if(is_numeric($id)){
				$this->load->library('pagination');
				$this->data['per_page']  = $this->config->item('per_page');
				$this->data['count'] 	 = $this->foloclor_manage_model->count_foloclors_by_cat($id);
				$this->data['foloclors'] = $this->foloclor_manage_model->get_folclors_by_category($id,$this->uri->segment(4),$this->config->item('per_page'));
				$this->index('foloclor/foloclors_category');
			}
			else {
				$this->index();
			}
		}
	}
	
	public function view_foloclor($id='')
	{
		$this->data['fol'] = $this->foloclor_manage_model->get_foloclor_conten_by_id($id);
		$this->data['comments'] = $this->foloclor_manage_model->get_foloclor_comments($id);
		$this->index('foloclor/view_foloclor');
	}
	
	public function add_comment()
	{
		if($this->ion_auth->logged_in()){
			$this->form_validation->set_rules('f_id', 'foloclor id', 'required|numeric');
			$this->form_validation->set_rules('f_comment', 'Comment', 'required|trim|xss_clean');
			if($this->form_validation->run()== true){
				$data = array('user_id' => $this->session->userdata('user_id') ,
							  'foloclor_id'=>$this->input->post('f_id'),
							  'comment'=>$this->input->post('f_comment'),
							  'publish_state'=>'pending',
							  'date_time'=>time(0)
				 );
				 
				 if($this->foloclor_manage_model->add_foloclor_comment($data)){
				 	echo "success";
				 }
				 else {
					 echo "err_db";
				 }
				 
			}
			else {
				echo "err_valid";
			}
		}
		else {
			echo "access_denied";
		}
	}
}
?>