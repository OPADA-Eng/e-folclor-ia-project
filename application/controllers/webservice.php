<?php
/**
 * 
 */
class Webservice extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('product_manage_model');
		$this->load->model('foloclor_manage_model');
	}
	
	public function latest_folclors()
	{
		$last = $this->foloclor_manage_model->get_last_folclor();
		echo json_encode($last);
	}
	
	public function get_folclor($id='')
	{
		$fol = $this->foloclor_manage_model->get_foloclor_conten_by_id($id);
		$comments = $this->foloclor_manage_model->get_foloclor_comments($id);
		echo json_encode(array('folclor'=>$fol,'comments'=>$comments));
	}
	
	public function add_comment()
	{
		$this->form_validation->set_rules('f_id', 'foloclor id', 'required|numeric');
		$this->form_validation->set_rules('u_id', 'user id', 'required|numeric');
		$this->form_validation->set_rules('f_comment', 'Comment', 'required|trim|xss_clean');
		if($this->form_validation->run()== true){
			$data = array('user_id' =>  $this->input->post('u_id'),
						  'foloclor_id'=>$this->input->post('f_id'),
						  'comment'=>$this->input->post('f_comment'),
						  'publish_state'=>'pending',
						  'date_time'=>time(0)
			 );
			 
			 if($this->foloclor_manage_model->add_foloclor_comment($data)){
			 	echo json_encode(array("result" =>"success"));
			 }
			 else {
				 echo json_encode(array("result" =>"err_db"));
			 }
			 
		}
		else {
			echo json_encode(array("result" =>"err_valid"));
		}
	}
	
	public function add_folclor_rate()
	{
		$this->form_validation->set_rules('foloclor_id', 'foloclor id', 'required|numeric');
		$this->form_validation->set_rules('user_id', 'foloclor id', 'required|numeric');
		$this->form_validation->set_rules('rate', 'rate', 'required|numeric');			
		if($this->form_validation->run()==true)
		{
			$row = array(
			'user_id'=>$this->input->post('user_id'),
			'f_id'=> $this->input->post('foloclor_id'),
			'fv_voting'=> $this->input->post('rate')	
			 );
			 $foloclor_id = $row["f_id"];	
			 $user_id = $row["user_id"];
			 //add vote 
			 $this->addVote($foloclor_id,$user_id,$row);
			
			 //update voting panel
			 $this->return_votes($foloclor_id);
		}
		else {
			echo json_encode(array("result" =>"rate_faild")); 
		}
	}
	function addVote($foloclor_id,$user_id,$row)
	{
		$result= array();
		$result = $this->foloclor_voting_model->get_foloclor_user_rates($foloclor_id,$user_id);	
		
		foreach ($result as $data):
       		$rate_db[] = $data;
	 	 endforeach;	
		 
		if(@count($rate_db)==0)
       	{
     		 $this->foloclor_voting_model->add_rate_for_foloclor($row);
		}
		else
		{
			$this->foloclor_voting_model->update_user_rate($row['fv_voting'],$user_id,$foloclor_id);	
		}
	}
	function return_votes($foloclor_id)
	{
		$result= array();
		$result = $this->foloclor_voting_model->get_foloclor_rates($foloclor_id);	
		foreach ($result as $data):
       		$rate_db[] = $data;
       		$sum_rates[] = $data->fv_voting;
	 	 endforeach;	
		 
       	if(@count($rate_db))
       	{
            $rate_times = count($rate_db);
            $sum_rates = array_sum($sum_rates);
            $rate_value = $sum_rates/$rate_times;
            $rate_bg = (($rate_value)/5)*100;
			// new rating value
			$this->foloclor_voting_model->update_foloclor_rate($rate_value,$foloclor_id);
			$data = array("rate_times"=>$rate_times,"rate_value"=>$rate_value,"rate_bg"=>$rate_bg);
			echo json_encode($data);
       	}
       	else
       	{
            $rate_times = 0;
            $rate_value = 0;
            $rate_bg = 0;	
	   	}
	}
	
	public function products_categories()
	{
		$cat = $this->product_manage_model->get_product_categories();
		echo json_encode($cat);
	}
	
	public function get_product_by_category()
	{
		//$this->form_validation->set_rules('cat_id', 'cat id', 'required|numeric');
		//$this->form_validation->set_rules('page_num', 'page number', 'required|numeric');		
		//if($this->form_validation->run()==true)
		//{
			$id = 1;//$this->input->post('cat_id');
			$page = 3;//$this->input->post('page_num');
			$this->load->library('pagination');
			$products = $this->product_manage_model->get_category_products($id,$page,$this->config->item('per_page'));
			echo json_encode($products);
		//}
		// else {
			// echo json_encode(array("result" =>"err_valid"));
		// }
	}
	
	public function get_product()
	{
		$this->form_validation->set_rules('id', 'product id', 'required|numeric');	
		if($this->form_validation->run()==true)
		{
			$id = $this->input->post('id');
			$prod = $this->product_manage_model->get_product_details($id);
			echo json_encode($prod);
		}
		else {
			echo json_encode(array("result" =>"err_valid"));
		}
	}
	
	
	
	function add_product_rate()
	{
		$this->form_validation->set_rules('product_id', 'product id', 'required|numeric');
		$this->form_validation->set_rules('user_id', 'user id', 'required|numeric');
		$this->form_validation->set_rules('rate', 'rate', 'required|numeric');			
		if($this->form_validation->run())
		{
			$this->load->library('ion_auth');
			if($this->ion_auth->logged_in()){
				$row = array(
					'user_id'=> $this->input->post('user_id'),
					'pc_id'=> $this->input->post('product_id'),
					'pv_voting'=> $this->input->post('rate')	
				 );	
				 $product_id = $row['pc_id'];	
				 $user_id =$row['user_id'];
				  //add vote 
				 $this->addProductVote($product_id,$user_id,$row);
				
				 //update voting panel
				 $this->return_product_votes($product_id,$user_id);
			}
			else{
				echo 'per_error';
			}
		}
		else {
			echo "rate_faild";
		}
		
	}	
	function addProductVote($product_id,$user_id,$row)
	{
		$result= array();
		$result = $this->product_voting_model->get_product_user_rates($product_id,$user_id);	
		foreach ($result as $data):
       		$rate_db[] = $data;
	 	 endforeach;	
		if(@count($rate_db)==0)
       	{
     		 $this->product_voting_model->add_rate_for_product($row);
		}
		else
		{
			$this->product_voting_model->update_user_rate($row['pv_voting'],$user_id,$product_id);	
		}
	}
		
	function return_product_votes($pc_id,$user_id)
	{
		$result= array();
		$result = $this->product_voting_model->get_products_rates($pc_id);	
		foreach ($result as $data):
       		$rate_db[] = $data;
       		$sum_rates[] = $data->pv_voting;
	 	 endforeach;	
		 
       	if(@count($rate_db))
       	{
            $rate_times = count($rate_db);
            $sum_rates = array_sum($sum_rates);
            $rate_value = $sum_rates/$rate_times;
            $rate_bg = (($rate_value)/5)*100;
			$this->product_voting_model->update_product_rate($rate_value,$pc_id);
			$data = array("rate_times"=>$rate_times,"rate_value"=>$rate_value,"rate_bg"=>$rate_bg,"user_id"=>$user_id);
			echo json_encode($data);
       	}
       	else
       	{
            $rate_times = 0;
            $rate_value = 0;
            $rate_bg = 0;	
	   	}
	}
	
	public function get_top_commented_folclors()
	{
		$most_commented_foloclors = $this->foloclor_manage_model->get_topcomments_foloclors(TOP_FOLOCLORS_COMMENTED);
		echo json_encode($most_commented_foloclors);
	}
	
	public function get_top_rated_products()
	{
		$this->data['top_products'] = $this->product_manage_model->get_toplikes_products(TOP_PRODUCTS_RATED);
		echo json_encode($this->data['top_products']);
	}
	
	public function get_top_rated_folclors()
	{
		$this->data['top_foloclors'] = $this->foloclor_manage_model->get_toplikes_foloclors(TOP_FOLOCLORS_RATED);
		echo json_encode($this->data['top_foloclors']);
	}
	
	
}

?>