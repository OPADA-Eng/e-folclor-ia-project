<?php
	/**
	 *  get the user's vote then add it to db
	 */
	class Foloclor_voting extends CI_Controller
	   {
		
	function index()
	{
		$foloclor_id = 3;
		$query = $this->foloclor_voting_model->get_foloclor_rate($foloclor_id);
		$data['records'] = $query;			
		$this->load->view('votingSystem/vote_a_foloclor',$data);	
	}
	
	function vote()
	{
		
		$this->form_validation->set_rules('foloclor_id', 'foloclor id', 'required|numeric');
		$this->form_validation->set_rules('rate', 'rate', 'required|numeric');			
		if($this->form_validation->run()==true)
		{
			$this->load->library('ion_auth');
			if($this->ion_auth->logged_in()){
				$row = array(
				'user_id'=>$this->session->userdata('user_id'),
				'f_id'=> $this->input->post('foloclor_id'),
				'fv_voting'=> $this->input->post('rate')	
				 );
				 $foloclor_id = $row["f_id"];	
				 $user_id = $row["user_id"];
				 //add vote 
				 $this->addVote($foloclor_id,$user_id,$row);
				
				 //update voting panel
				 $this->return_votes($foloclor_id);
			}
			else{
				echo 'per_error';
			}
		}
		else {
			echo "rate_faild";
		}
	}
		function addVote($foloclor_id,$user_id,$row)
		{
			$result= array();
			$result = $this->foloclor_voting_model->get_foloclor_user_rates($foloclor_id,$user_id);	
			
			foreach ($result as $data):
 	       		$rate_db[] = $data;
		 	 endforeach;	
			 
			if(@count($rate_db)==0)
	       	{
	     		 $this->foloclor_voting_model->add_rate_for_foloclor($row);
			}
			else
			{
				$this->foloclor_voting_model->update_user_rate($row['fv_voting'],$user_id,$foloclor_id);	
			}
		}
		function return_votes($foloclor_id)
		{
			$result= array();
			$result = $this->foloclor_voting_model->get_foloclor_rates($foloclor_id);	
			foreach ($result as $data):
 	       		$rate_db[] = $data;
	       		$sum_rates[] = $data->fv_voting;
		 	 endforeach;	
			 
	       	if(@count($rate_db))
	       	{
	            $rate_times = count($rate_db);
	            $sum_rates = array_sum($sum_rates);
	            $rate_value = $sum_rates/$rate_times;
	            $rate_bg = (($rate_value)/5)*100;
				// new rating value
				$this->foloclor_voting_model->update_foloclor_rate($rate_value,$foloclor_id);
				$data = array("rate_times"=>$rate_times,"rate_value"=>$rate_value,"rate_bg"=>$rate_bg);
				echo json_encode($data);
	       	}
	       	else
	       	{
	            $rate_times = 0;
	            $rate_value = 0;
	            $rate_bg = 0;	
		   	}
		}
	}
?>