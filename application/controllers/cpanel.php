<?php
/**
 * 
 */
class Cpanel extends CI_Controller {
	
   function __construct() {
		parent::__construct();  
		$this->load->library('ion_auth');  
		$this->load->model('cpanel_model');
		$this->load->model('Product_model');
		$this->load->helper('language');
	}
	public function index()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else {
			redirect('auth');
		}
		$this->load->library('validation');
	}	
	
	public function accept_comments_foloclor(){
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"accept_comment_foloclor")){
			$this->data['result'] = "sucess";
			$this->data['active'] = "faild";
			$this->data['inactive'] = "faild";
			$this->data['foloclor_content'] = $this->cpanel_model->get_foloclor_content();
			$this->data['view_page']='cpanel/accept_comments_foloclor';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}else{
			$this->data['result'] = "faild";
			$this->data['active'] = "faild";
			$this->data['inactive'] = "faild";
			$this->data['view_page']='cpanel/accept_comments_foloclor';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}
		
	}
	public function manage_foloclor()
	{
		$this->data['view_page']='cpanel/manage_foloclor';
	    $this->load->view('cpanel/cp_view_controller', $this->data);
		
	}
	
	public function add_category()
	{
		$this->form_validation->set_rules('fc_name_ar', 'fc_name_ar', 'required|trim|xss_clean');
		$this->form_validation->set_rules('fc_name_en', 'fc_name_en', 'required|trim|xss_clean');
		if ($this->form_validation->run()) 
		{
			$data = array('fc_name_ar' => $this->input->post('fc_name_ar') , 
						  'fc_name_en' => $this->input->post('fc_name_en'));
			$cat= $this->cpanel_model->add_new_category($data);
			$data1 = array('fc_id' => $cat,
						  'fc_name_ar' => $this->input->post('fc_name_ar'),
						  'fc_name_en' => $this->input->post('fc_name_en'));
			echo json_encode($data1);
		}
		else {
			echo "failed";
		}
	}
	
	public function add_keyword()
	{
		$this->form_validation->set_rules('fk_ar', 'fk_ar', 'required|trim|xss_clean');
		$this->form_validation->set_rules('fk_ar', 'fk_ar', 'required|trim|xss_clean');
		if ($this->form_validation->run()) 
		{	
			$data = array('fk_keyword_ar' => $this->input->post('fk_ar') , 
						  'fk_keyword_en' => $this->input->post('fk_en'));
			$cat= $this->cpanel_model->add_new_keyword($data);
			$data1 = array('fk_id' => $cat,
						  'fk_keyword_ar' => $this->input->post('fk_ar') , 
						  'fk_keyword_en' => $this->input->post('fk_en'));
			echo json_encode($data1);
		}
		else {
			echo "failed";
		}
	}
	
	public function add_tag()
	{
		$this->form_validation->set_rules('ft_ar', 'ft_ar', 'required|trim|xss_clean');
		$this->form_validation->set_rules('ft_ar', 'ft_ar', 'required|trim|xss_clean');
		if ($this->form_validation->run()) 
		{	
			$data = array('f_tag_ar' => $this->input->post('ft_ar') , 
						  'f_tag_en' => $this->input->post('ft_en'));
			$cat= $this->cpanel_model->add_new_tag($data);
			$data1 = array('ft_id' => $cat,
						  'f_tag_ar' => $this->input->post('ft_ar') , 
						  'f_tag_en' => $this->input->post('ft_en'));
			echo json_encode($data1);
		}
		else {
			echo "failed";
		}
	}	
	public function add_foloclor()
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"add_foloclor")){
				
			$this->data['result'] = "sucess";
			$this->data['view_page'] = 'cpanel/add_foloclor';
			$this->data['user_id'] = $this->session->userdata('user_id');
			if($this->ion_auth->is_admin())
			{
				$this->data['user_type'] = 1;
			}
			else 
			{
				$this->data['user_type'] = 0;	
			}
			//this to send all categories to view and make user select one to add content relted to it.
			$this->data['foloclor_categories'] = $this->cpanel_model->get_foloclor_gategories();
			$this->data['tags'] = json_encode($this->cpanel_model->get_tags());
			$this->data['keys'] = json_encode($this->cpanel_model->get_keywords());	
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}else{
			$this->data['result'] = "faild";
			$this->data['view_page']='cpanel/add_foloclor';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}	
	}
	
	public function edit_foloclor()
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"edit_foloclor")){
				
			$this->data['result'] = "sucess";
			$this->data['foloclor_categories'] = json_encode($this->cpanel_model->get_foloclor_gategories());
			$this->data['tags'] = json_encode($this->cpanel_model->get_tags());
			$this->data['keys'] = json_encode($this->cpanel_model->get_keywords());	
			$this->data['view_page'] = 'cpanel/edit_foloclor';
			if($this->ion_auth->is_admin())
			{
				$this->data['user_type'] = 1;
				$this->data['foloclor_articles'] = $this->cpanel_model->get_checked_foloclor_content(); // get all contents rejected or approved by checker
			}
			else 
			{
				$this->data['user_type'] = 0;	
				$this->data['foloclor_articles'] = $this->cpanel_model->get_all_pending_foloclor_content();//get all contents that still pendiig
			}
			//echo json_encode(	$this->data['foloclor_articles']);
			$this->data['user_id'] = $this->session->userdata('user_id');
			$this->load->view('cpanel/cp_view_controller', $this->data);
		}else{
		
			$this->data['result'] = "faild";
			$this->data['view_page']='cpanel/edit_foloclor';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}	
	}
	
	public function delete_foloclor()
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"edit_foloclor")){
				
			$this->data['result'] = "sucess";
			$this->data['foloclor_articles'] = $this->cpanel_model->monitor_foloclor_content(); 				
			$this->data['view_page'] = 'cpanel/monitor_voting';
			$this->load->view('cpanel/cp_view_controller', $this->data);
		}
		else{
		
			$this->data['result'] = "faild";
			$this->data['view_page']='cpanel/monitor_voting';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}	
	}
	public function approve_content()
	{
		$this->form_validation->set_rules('content_id', 'Foloclor', 'required|numeric');
		$this->form_validation->set_rules('user_id', 'User', 'required|numeric');
		if($this->form_validation->run())
		{
			$content_id = $this->input->post('content_id');
			$user_id = $this->input->post('user_id');
			$state = 0;
			if($this->ion_auth->is_admin())
			{
				$state = 4;
			}
			else
			{
				$state = 5;	
			}
			$this->cpanel_model->update_content_status_to_approve($content_id,$state,$user_id);
		}
		else
			echo "failed";
	}

	public function approve_product()
	{
		$this->form_validation->set_rules('content_id', 'Foloclor', 'required|numeric');
		$this->form_validation->set_rules('user_id', 'User', 'required|numeric');
		if($this->form_validation->run())
		{
			$product_id = $this->input->post('content_id');
			$user_id = $this->input->post('user_id');
			$state = 0;
			if($this->ion_auth->is_admin())
			{
				$state = 4;
			}
			else
			{
				$state = 6;	
			}
			$this->Product_model->update_product_status_to_approve($product_id,$state,$user_id);
		}
		else
			echo "failed";
	}
	
	public function reject_product()
	{
		$this->form_validation->set_rules('content_id', 'product_id', 'required|numeric');
		$this->form_validation->set_rules('checker_opinion', 'checker_opinion', 'required|trim|xss_clean');
		$this->form_validation->set_rules('user_id', 'User', 'required|numeric');
		if($this->form_validation->run())
		{
			$product_id = $this->input->post('content_id');
			$checker_opinion = $this->input->post('checker_opinion');
			$user_id = $this->input->post('user_id');
			$state = 0;
			if($this->ion_auth->is_admin())
			{
				$state = 1;
			}
			else
			{
				$state = 3;	
			}
			$this->Product_model->update_product_status_to_reject($product_id,$checker_opinion,$state,$user_id);
		}
		else 
		{
			echo "failed";
		}
	}
	
	public function	get_rating_history()
	{
		$this->form_validation->set_rules('f_id', 'Foloclor', 'required|numeric');
		if($this->form_validation->run())
		{
			$f_id = $this->input->post('f_id');
			$data = $this->cpanel_model->get_foloclor_rate_history($f_id);
			echo json_encode($data);
		}
	else {
			echo "failed";
	}
	}
	public function reject_content()
	{
		$this->form_validation->set_rules('content_id', 'Foloclor', 'required|numeric');
		$this->form_validation->set_rules('checker_opinion', 'checker_opinion', 'trim|xss_clean');
		$this->form_validation->set_rules('user_id', 'User', 'required|numeric');
		if($this->form_validation->run())
		{
			$content_id = $this->input->post('content_id');
			$checker_opinion = $this->input->post('checker_opinion');
			$user_id = $this->input->post('user_id');
			$state = 0;
			if($this->ion_auth->is_admin())
			{
				$state = 1;
			}
			else
			{
				$state = 2;	
			}
			$this->cpanel_model->update_content_status_to_reject($content_id,$checker_opinion,$state,$user_id);
		}
		else {
			echo "failed";
		}
	}
	public function deactivate_content()
	{
		$this->form_validation->set_rules('f_id', 'Foloclor', 'required|numeric');
		if($this->form_validation->run())
		{
			$f_id = $this->input->post('f_id');
			$this->cpanel_model->update_content_status_to_deactivate($f_id);
		}
	}
	public function activate_content()
	{
		$this->form_validation->set_rules('f_id', 'Foloclor', 'required|numeric');
		if($this->form_validation->run())
		{
			$f_id = $this->input->post('f_id');
			$this->cpanel_model->update_content_status_to_activate($f_id);
		}
	}
	public function update_content()
	{
		$this->form_validation->set_rules('f_id', 'Foloclor', 'required|numeric');
		$this->form_validation->set_rules('fc_name', 'category_name', 'required|numeric');
		$this->form_validation->set_rules('f_title_ar', 'f_title_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_title_en', 'f_title_en', 'required|trim|xss_clean');	
		$this->form_validation->set_rules('f_content_ar', 'f_content_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_content_en', 'f_content_en', 'required|trim|xss_clean');	
		$this->form_validation->set_rules('f_hi_ar', 'f_hi_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_hi_en', 'f_hi_en', 'required|trim|xss_clean');	
		$this->form_validation->set_rules('f_location_ar', 'f_location_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_location_en', 'f_location_en', 'required|trim|xss_clean');			
		if($this->form_validation->run())
		{
			$f_id = $this->input->post('f_id');
			if($this->ion_auth->is_admin())
			{
				 $row = array(
					'f_cat_id'=> $this->input->post('fc_name'),
					'f_title_ar'=>$this->input->post('f_title_ar'),
					'f_title_en'=>$this->input->post('f_title_en'),
					'f_content_ar'=>$this->input->post('f_content_ar'),
					'f_content_en'=>$this->input->post('f_content_en'),
					'f_historical_importance_ar'=> $this->input->post('f_hi_ar'),
					'f_historical_importance_en'=> $this->input->post('f_hi_en'),
					'f_location_ar'=>  $this->input->post('f_location_ar'),
					'f_location_en'=> $this->input->post('f_location_en'),
					'is_active' => '1',
					'admin_id'=> $this->session->userdata('user_id'),
					'publish_state' => 4
			 	);
		 	}
			else
			{
				$row = array(
					'f_cat_id'=> $this->input->post('fc_name'),
					'f_title_ar'=> $this->input->post('f_title_ar'),
					'f_title_en'=> $this->input->post('f_title_en'),
					'f_content_ar'=> $this->input->post('f_content_ar'),
					'f_content_en'=> $this->input->post('f_content_en'),
					'f_historical_importance_ar'=> $this->input->post('f_hi_ar'),
					'f_historical_importance_en'=> $this->input->post('f_hi_en'),
					'f_location_ar'=>  $this->input->post('f_location_ar'),
					'f_location_en'=> $this->input->post('f_location_en'),
					'checker_id'=> $this->session->userdata('user_id'),
					'publish_state' => 5
				 );
				 
			}
			 $tags = $this->input->post('tags');
			 $keys = $this->input->post('keys');
			$this->cpanel_model->update_foloclor_content($f_id,$row);
			if(count($tags) > 0)
			{
				for ($i=0; $i < count($tags) ; $i++) { 
				$this->cpanel_model->add_tags_to_content($tags[$i],$f_id);
				}
			}
			if(count($keys) > 0)
			{
			    for ($j=0; $j < count($keys) ; $j++) { 
				$this->cpanel_model->add_keywords_to_content($keys[$j],$f_id);
			    }
			}
			echo "success";	
		}
	}
	public function add_foloclor_record()
	{
		$this->form_validation->set_rules('fc_name', 'category_name', 'required|numeric');
		$this->form_validation->set_rules('f_title_ar', 'f_title_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_title_en', 'f_title_en', 'required|trim|xss_clean');	
		$this->form_validation->set_rules('f_content_ar', 'f_content_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_content_en', 'f_content_en', 'required|trim|xss_clean');	
		$this->form_validation->set_rules('f_hi_ar', 'f_hi_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_hi_en', 'f_hi_en', 'required|trim|xss_clean');	
		$this->form_validation->set_rules('f_location_ar', 'f_location_ar', 'required|trim|xss_clean');		
		$this->form_validation->set_rules('f_location_en', 'f_location_en', 'required|trim|xss_clean');			
		if($this->form_validation->run())
		{
			if($this->ion_auth->is_admin())
			{
				 $row = array(
					'user_id'=>$this->session->userdata('user_id'),
					'f_cat_id'=> $this->input->post('fc_name'),
					'f_title_ar'=> $this->input->post('f_title_ar'),
					'f_title_en'=> $this->input->post('f_title_en'),
					'f_content_ar'=> $this->input->post('f_content_ar'),
					'f_content_en'=> $this->input->post('f_content_en'),
					'f_historical_importance_ar'=> $this->input->post('f_hi_ar'),
					'f_historical_importance_en'=> $this->input->post('f_hi_en'),
					'f_location_ar'=>  $this->input->post('f_location_ar'),
					'f_location_en'=> $this->input->post('f_location_en'),
					'admin_id'=> $this->session->userdata('user_id'),
					'publish_state' => 4,
					'is_active' => 1
				 );
			 }
			else
			{
				$row = array(
					'user_id'=>$this->session->userdata('user_id'),
					'f_cat_id'=> $this->input->post('fc_name'),
					'f_title_ar'=> $this->input->post('f_title_ar'),
					'f_title_en'=> $this->input->post('f_title_en'),
					'f_content_ar'=> $this->input->post('f_content_ar'),
					'f_content_en'=> $this->input->post('f_content_en'),
					'f_historical_importance_ar'=> $this->input->post('f_hi_ar'),
					'f_historical_importance_en'=> $this->input->post('f_hi_en'),
					'f_location_ar'=>  $this->input->post('f_location_ar'),
					'f_location_en'=> $this->input->post('f_location_en'),
					'checker_id'=> $this->session->userdata('user_id'),
					'publish_state' => 5
				 );
			}

			$last_add = $this->cpanel_model->create_new_foloclor_content($row);
			foreach ($this->input->post('tags') as $tag) {
				$this->cpanel_model->add_tags_to_content($tag,$last_add);
			}
			
			foreach ($this->input->post('keys') as $key) {
					$this->cpanel_model->add_keywords_to_content($key,$last_add);
			}
			
			echo $last_add;
	}
}
	public function update_mm_foloclor_record()
	{
		$this->form_validation->set_rules('f_image', 'f_image', 'xss_clean');	
		$this->form_validation->set_rules('f_video', 'f_video', 'xss_clean');	
		$this->form_validation->set_rules('f_sound', 'f_sound', 'xss_clean');	
		if($this->form_validation->run())
		{
			$data = array('f_image' => $this->input->post('f_image') ,
						 'f_video' => $this->input->post('f_video') ,
						 'f_sound' => $this->input->post('f_sound'));
			$f_id = $this->input->post('f_id');
			$this->cpanel_model->add_mm_to_content($data,$f_id);
		}
	}
	
	public function manage_product($value='')
	{
		
		$this->data['view_page']='cpanel/manage_product';
	    $this->load->view('cpanel/cp_view_controller', $this->data);
	}

    public function update_im_product_record()
	{
		$this->form_validation->set_rules('image', 'image', 'xss_clean');	
		if($this->form_validation->run())
		{
			$data = array('image' => $this->input->post('f_image'));
			$pc_id =  $this->input->post('pc_id');
			$this->Product_model->add_mm_to_content($data,$pc_id);
		}
	}
	
	public function add_product_record()
	{
		$this->form_validation->set_rules('cat_en', 'cat_en', 'required|numeric');
		$this->form_validation->set_rules('type_en', 'type_en', 'required|numeric');
		$this->form_validation->set_rules('product_name_ar', 'product_name_ar', 'required|trim|xss_clean');
		$this->form_validation->set_rules('product_name_en', 'product_name_en', 'required|trim|xss_clean');
		$this->form_validation->set_rules('color', 'color', 'trim|xss_clean');
		$this->form_validation->set_rules('details_ar', 'details_ar', 'required|trim|xss_clean');
		$this->form_validation->set_rules('details_en', 'details_en', 'required|trim|xss_clean');
		$this->form_validation->set_rules('min_price', 'min_price', 'required|numeric');
		$this->form_validation->set_rules('max_price', 'max_price', 'required|numeric');
		$this->form_validation->set_rules('weight', 'weight', 'numeric');
		$this->form_validation->set_rules('weight_type', 'weight_type', 'trim|xss_clean');
		$this->form_validation->set_rules('volum', 'volum', 'numeric');
		$this->form_validation->set_rules('width', 'width', 'numeric');
		$this->form_validation->set_rules('height', 'height', 'numeric');	
		$this->form_validation->set_rules('max_quantity', 'max_quantity', 'required|numeric');
		$this->form_validation->set_rules('min_quantity', 'min_quantity', 'required|numeric');
		
		if($this->form_validation->run())
		{
			if($this->ion_auth->is_admin())
			{
				 $row = array(
					'user_id'=>$this->session->userdata('user_id'),
					'pcat_id'=> $this->input->post('cat_en'),
					'pt_id'=> $this->input->post('type_en'),
					'product_name_ar' =>$this->input->post('product_name_ar'),
					'product_name_en' =>$this->input->post('product_name_en'),
					'min_price'=> $this->input->post('min_price'),
					'max_price'=> $this->input->post('max_price'),
					'color'=> $this->input->post('color'),
					'weight'=> $this->input->post('weight'),
					'weight_type'=> $this->input->post('weight_type'),
					'volum'=>  $this->input->post('volum'),
					'height'=> $this->input->post('height'),
					'width'=> $this->input->post('width'),
					'max_quantity'=> $this->input->post('max_quantity'),
					'min_quantity'=> $this->input->post('min_quantity'),
					'details_ar'=> $this->input->post('details_ar'),
					'details_en'=> $this->input->post('details_en'),
					'admin_id'=> $this->session->userdata('user_id'),
					'publish_state' => 4
				 );
			}
			else
			{
					$row = array(
					'user_id'=>$this->session->userdata('user_id'),
					'pcat_id'=> $this->input->post('cat_en'),
					'pt_id'=> $this->input->post('type_en'),
					'product_name_ar' =>$this->input->post('product_name_ar'),
					'product_name_en' =>$this->input->post('product_name_en'),
					'min_price'=> $this->input->post('min_price'),
					'max_price'=> $this->input->post('max_price'),
					'color'=> $this->input->post('color'),
					'weight'=> $this->input->post('weight'),
					'weight_type'=> $this->input->post('weight_type'),
					'volum'=>  $this->input->post('volum'),
					'height'=> $this->input->post('height'),
					'width'=> $this->input->post('width'),
					'max_quantity'=> $this->input->post('max_quantity'),
					'min_quantity'=> $this->input->post('min_quantity'),
					'details_ar'=> $this->input->post('details_ar'),
					'details_en'=> $this->input->post('details_en'),
					'marketing_respons_id'=> $this->session->userdata('user_id'),
					'publish_state' => 6
				 );
			}
			
			$last_add = $this->Product_model->create_new_product_content($row);		
			foreach ($this->input->post('keys') as $key) {
					$this->Product_model->add_keywords_to_content($key,$last_add);
			}
			
			echo $last_add;
		}
		else {
			echo "failed";
		}
	}
	
	public function update_product()
	{
		$this->form_validation->set_rules('pc_id', 'pc_id', 'required|numeric');
		$this->form_validation->set_rules('cat_en', 'cat_en', 'required|numeric');
		$this->form_validation->set_rules('type_en', 'type_en', 'required|numeric');
		$this->form_validation->set_rules('product_name_ar', 'product_name_ar', 'required|trim|xss_clean');
		$this->form_validation->set_rules('product_name_en', 'product_name_en', 'required|trim|xss_clean');
		$this->form_validation->set_rules('color', 'color', 'trim|xss_clean');
		$this->form_validation->set_rules('details_ar', 'details_ar', 'required|trim|xss_clean');
		$this->form_validation->set_rules('details_en', 'details_en', 'required|trim|xss_clean');
		$this->form_validation->set_rules('min_price', 'min_price', 'required|numeric');
		$this->form_validation->set_rules('max_price', 'max_price', 'required|numeric');
		$this->form_validation->set_rules('weight', 'weight', 'numeric');
		$this->form_validation->set_rules('weight_type', 'weight_type', 'trim|xss_clean');
		$this->form_validation->set_rules('volum', 'volum', 'numeric');
		$this->form_validation->set_rules('width', 'width', 'numeric');
		$this->form_validation->set_rules('height', 'height', 'numeric');	
		$this->form_validation->set_rules('max_quantity', 'max_quantity', 'required|numeric');
		$this->form_validation->set_rules('min_quantity', 'min_quantity', 'required|numeric');	
		if($this->form_validation->run())
		{
			$pc_id = $this->input->post('pc_id');
		    if($this->ion_auth->is_admin())
			{
			     $row = array(
					'pcat_id'=> $this->input->post('cat_en'),
					'pt_id'=> $this->input->post('type_en'),
					'product_name_ar' =>$this->input->post('product_name_ar'),
					'product_name_en' =>$this->input->post('product_name_en'),
					'min_price'=> $this->input->post('min_price'),
					'max_price'=> $this->input->post('max_price'),
					'color'=> $this->input->post('color'),
					'weight'=> $this->input->post('weight'),
					'weight_type'=> $this->input->post('weight_type'),
					'volum'=>  $this->input->post('volum'),
					'height'=> $this->input->post('height'),
					'width'=> $this->input->post('width'),
					'max_quantity'=> $this->input->post('max_quantity'),
					'min_quantity'=> $this->input->post('min_quantity'),
					'details_ar'=> $this->input->post('details_ar'),
					'details_en'=> $this->input->post('details_en'),
					'admin_id'=> $this->session->userdata('user_id'),
					'publish_state' => 4
				 );
			}
			else
			{
					$row = array(
					'pcat_id'=> $this->input->post('cat_en'),
					'pt_id'=> $this->input->post('type_en'),
					'product_name_ar' =>$this->input->post('product_name_ar'),
					'product_name_en' =>$this->input->post('product_name_en'),
					'min_price'=> $this->input->post('min_price'),
					'max_price'=> $this->input->post('max_price'),
					'color'=> $this->input->post('color'),
					'weight'=> $this->input->post('weight'),
					'weight_type'=> $this->input->post('weight_type'),
					'volum'=>  $this->input->post('volum'),
					'height'=> $this->input->post('height'),
					'width'=> $this->input->post('width'),
					'max_quantity'=> $this->input->post('max_quantity'),
					'min_quantity'=> $this->input->post('min_quantity'),
					'details_ar'=> $this->input->post('details_ar'),
					'details_en'=> $this->input->post('details_en'),
					'marketing_respons_id'=> $this->session->userdata('user_id'),
					'publish_state' => 6
				 );
			}
			$keys = $this->input->post('keys');
			$this->Product_model->update_product_content($pc_id,$row,$keys);
			echo "1";	
		}
		else
			echo "failed";
	}
	
    public function add_product()
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"add_product")){
			$this->data['result'] = "sucess";
			
			$this->data['keys'] = json_encode($this->Product_model->get_keywords());	
			$this->data['user_id'] = $this->session->userdata('user_id');
			if($this->ion_auth->is_respons_market())
			{
				$this->data['user_type'] = 0;
			}
			else 
			{
				$this->data['user_type'] = 1;	
			}
			$this->data['product_category']=$this->Product_model->show_categories();
			$this->data['product_type']=$this->Product_model->show_types();
			$this->data['view_page']='cpanel/add_product';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}else{
			$this->data['result'] = "faild";
			$this->data['view_page']='cpanel/add_product';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}
	}
	
	public function add_p_category()
	{
		$this->form_validation->set_rules('p_cat_ar', 'p_cat_ar', 'required|trim|xss_clean');
		$this->form_validation->set_rules('p_cat_en', 'p_cat_en', 'required|trim|xss_clean');
		if ($this->form_validation->run()) 
		{
			$data = array('cat_ar' => $this->input->post('p_cat_ar') , 
						  'cat_en' => $this->input->post('p_cat_en'));
			$cat= $this->Product_model->add_new_category($data);
			$data1 = array('pcat_id' => $cat,
						  'cat_ar' => $this->input->post('p_cat_ar'),
						  'cat_en' => $this->input->post('p_cat_en'));
			echo json_encode($data1);
		}
	}
	
	public function edit_product()
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"edit_product"))
		{	
			$this->data['result'] = "sucess";
			$this->data['product_categories'] = json_encode($this->Product_model->show_categories());
			$this->data['product_types']= json_encode($this->Product_model->show_types());
			
			$this->data['keys'] = json_encode($this->cpanel_model->get_keywords());	
			$this->data['view_page']='cpanel/edit_product';
			if($this->ion_auth->is_admin())
			{
				$this->data['user_type'] = 1;
				$this->data['products'] = $this->Product_model->get_checked_products(); // get all contents rejected or approved by checker
			}
			else // is marketing manager
			{
				$this->data['user_type'] = 0;	
				$this->data['products'] = $this->Product_model->get_pending_products();//get all contents that still pendiig
			}
			$this->data['user_id'] = $this->session->userdata('user_id');
			$this->load->view('cpanel/cp_view_controller', $this->data);
		}
		else
		{
			$this->data['result'] = "faild";
			$this->data['view_page']='cpanel/edit_product';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}	
	}
	
	public function active_comment($fcc_id)
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"accept_comment_foloclor")){
			$this->data['result'] = "sucess";
			$this->data['foloclor_content'] = $this->cpanel_model->get_foloclor_content();
			if($this->cpanel_model->active_comment($fcc_id)){
				$this->data['active'] = "success";
				$this->data['inactive'] = "faild";
				$this->data['view_page']='cpanel/accept_comments_foloclor';
	  	    	$this->load->view('cpanel/cp_view_controller', $this->data);
			}else{
				$this->data['result'] = "faild";
				$this->data['active'] = "faild";
				$this->data['inactive'] = "faild";
				$this->data['view_page']='cpanel/accept_comments_foloclor';
		  	    $this->load->view('cpanel/cp_view_controller', $this->data);
			}
		}else{
			$this->data['result'] = "faild";
			$this->data['active'] = "faild";
			$this->data['inactive'] = "faild";
			$this->data['view_page']='cpanel/accept_comments_foloclor';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}
	}
	public function inactive_comment($fcc_id)
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"accept_comment_foloclor")){
			$this->data['result'] = "sucess";
			$this->data['foloclor_content'] = $this->cpanel_model->get_foloclor_content();
			if($this->cpanel_model->inactive_comment($fcc_id)){
				$this->data['inactive'] = "success";
				$this->data['active'] = "faild";
				$this->data['view_page']='cpanel/accept_comments_foloclor';
	  	    	$this->load->view('cpanel/cp_view_controller', $this->data);
			}else{
				$this->data['result'] = "faild";
				$this->data['inactive'] = "faild";
				$this->data['active'] = "faild";
				$this->data['view_page']='cpanel/accept_comments_foloclor';
		  	    $this->load->view('cpanel/cp_view_controller', $this->data);
			}
		}else{
			$this->data['result'] = "faild";
			$this->data['inactive'] = "faild";
			$this->data['active'] = "faild";
			$this->data['view_page']='cpanel/accept_comments_foloclor';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}
	}
	private function get_product_categories_news()
	{
		$cats = $this->cpanel_model->get_product_categories();
		$categories = array();
		$i = 0;
		foreach ($cats as $key => $cat) {
			$products = $this->cpanel_model->get_product_by_category($cat->pcat_id);
			$categories[$i]['category'] = $cat;
			$categories[$i]['products'] = $products;
			$i++;
		}
		return $categories;
	}
	private function get_foloclor_categories_news()
	{
		$cats = $this->cpanel_model->get_foloclor_categories();
		$categories = array();
		$i = 0;
		foreach ($cats as $key => $cat) {
			$foloclors = $this->cpanel_model->get_foloclor_by_category($cat->fc_id);
			$categories[$i]['category'] = $cat;
			$categories[$i]['foloclors'] = $foloclors;
			$i++;
		}
		return $categories;
	}
	public function manage_newsbar()
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"manage_newsbar")){
			$this->data['result'] = "sucess";
			$data['mode'] = '';
			$data['error'] = '';
			$this->data['product_categories'] = $this->get_product_categories_news();
			$this->data['foloclor_categories'] = $this->get_foloclor_categories_news();
			$this->data['foloclor_current_news'] = $this->cpanel_model->get_foloclor_current_news();
			$this->data['product_current_news'] = $this->cpanel_model->get_product_current_news();
			$this->data['view_page']='cpanel/manage_newsbar';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}else{
			$this->data['result'] = "faild";
			$data['mode'] = '';
			$data['error'] = '';
			$this->data['view_page']='cpanel/manage_newsbar';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}
	}
	public function manage_news_bar()
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"manage_news_bar")){
			$this->data['result'] = "success";
			$this->data['mode'] = '';
			$this->data['error'] = '';
			$this->data['news'] = $this->cpanel_model->get_news();
			$this->data['current_news'] = $this->cpanel_model->get_current_news();
			$this->data['view_page']='cpanel/manage_news_bar';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}else{
			$this->data['result'] = "faild";
			$this->data['mode'] = '';
			$this->data['error'] = '';
			$this->data['view_page']='cpanel/manage_news_bar';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}
	}
	public function add_news_to_newsbar()
	{
			$this->data['error'] = "";
			if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"manage_news_bar")){
				
				if($this->input->post('add')){
					
					$this->data['result'] = "success";
					$this->form_validation->set_rules('n_id','News ID','required|trim|numeric');
					if ($this->form_validation->run() === TRUE) {
						
						$n_id = $this->input->post('n_id');
						if(!$this->cpanel_model->get_news_by_id($n_id)){
							$this->data['mode'] = 'error';
							$this->data['error'] = 'لم يتم العثور على الخبر ليتم إضافته ';
						}
						else {
							if($this->cpanel_model->check_news_exist_in_newsbar($n_id)){
								$this->data['mode'] = 'error';
								$this->data['error'] = 'العنصر الذي تحاول إضافته موجود بالفعل في الشريط الإخباري';
							}
							else {
								
								if ($this->cpanel_model->set_to_news_bar($n_id)){
									$this->data['mode'] = 'success_adding';
								}else{
									$this->data['mode'] = 'error';
									$this->data['error'] = 'حذث خطأ أثناء إضافة الخبر';
								}
								
							}
						}
						$this->data['news'] = $this->cpanel_model->get_news();
						$this->data['current_news'] = $this->cpanel_model->get_current_news();
						$this->data['view_page']='cpanel/manage_news_bar';
				  	    $this->load->view('cpanel/cp_view_controller', $this->data);
					}
					else {
						$this->data['mode'] = 'error';
						$this->data['error'] = '';
					}
				}else {
					redirect('cpanel/manage_news_bar');
				}
			}
			else {
				$this->data['result'] = "faild";
				$this->data['mode'] = '';
				$this->data['error'] = '';
				$this->data['view_page']='cpanel/manage_news_bar';
		  	    $this->load->view('cpanel/cp_view_controller', $this->data);
			}
			
			
		}
	public function hide_news_from_newsbar()
	{
		if(isset($_POST['hide'])){
			$this->data['result'] = "success";
			if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"manage_news_bar")){
				$this->form_validation->set_rules('n_id','News Id','required|trim|numeric');
				if ($this->form_validation->run() === TRUE) {
					$n_id = $this->input->post('n_id');
					if(!$this->cpanel_model->get_news_by_id($n_id)){
						$this->data['mode'] = 'error';
						$this->data['error'] = 'لم يتم العثور على الخبر ليتم إخفاؤه ';
					}
					else {
						$this->cpanel_model->hide_from_news_bar($n_id);
						$this->data['mode'] = 'success_removing';
					}
				}
				else {
					$this->data['mode'] = 'error';
					$this->data['error'] = '';
				}
			}
			else
			{
				$this->data['result'] = "faild";
				$this->data['mode'] = '';
				$this->data['error'] = '';
				$this->data['view_page']='cpanel/manage_news_bar';
		  	    $this->load->view('cpanel/cp_view_controller', $this->data);
			}
			$this->data['news'] = $this->cpanel_model->get_news();
			$this->data['current_news'] = $this->cpanel_model->get_current_news();
			$this->data['view_page']='cpanel/manage_news_bar';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}
		else {
			redirect('cpanel/manage_newsbar');
		}

	}
	public function choice_option_foloclor()
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"manage_newsbar")){
			$this->data['result'] = "sucess";
			$this->form_validation->set_rules('f_id','Foloclor ID','required|trim|numeric');
			if($this->form_validation->run()===TRUE){
				$f_id = $this->input->post('f_id');
				if(is_numeric($f_id)){
					if($this->cpanel_model->add_foloclor_news($f_id)){
						$data['mode'] = 'success';
					}
					else {
						$data['mode'] = 'error';
						$data['error'] = 'لم يتم العثور على العنصر';
					}
				}else {
					$data['mode'] = 'error';
					$data['error'] = 'خطأ في الرابط';
				}
			}else{
				$this->data['product_categories'] = $this->get_product_categories_news();
				$this->data['foloclor_categories'] = $this->get_foloclor_categories_news();
				$this->data['foloclor_current_news'] = $this->cpanel_model->get_foloclor_current_news();
				$this->data['product_current_news'] = $this->cpanel_model->get_product_current_news();
				$data['mode'] = '';
				$data['error'] = '';
				$this->data['view_page']='cpanel/manage_newsbar';
		  	    $this->load->view('cpanel/cp_view_controller', $this->data);
			}
			
		}else{
			$this->data['result'] = "faild";
			$this->data['view_page']='cpanel/manage_newsbar';
	  	    $this->load->view('cpanel/cp_view_controller', $this->data);
		}
	}

	public function add_news()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else {
			if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"add_news")){
				$this->data['result'] = "success";
				$this->data['mode']= 'add';
				$this->data['view_page']='cpanel/add_news';
		  	    $this->load->view('cpanel/cp_view_controller', $this->data);
			}else{
				$this->data['result'] = "faild";
				$this->data['mode']= 'add';
				$this->data['view_page']='cpanel/add_news';
		  	    $this->load->view('cpanel/cp_view_controller', $this->data);
			}
		}
		
	}
	public function insert_news()
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		else {
			if($this->ion_auth->has_privilage($this->session->userdata('user_id'),"add_news")){
				$this->data['result'] = "success";
				$this->data['mode']= 'add';
				if(isset($_POST['send'])){
					$this->form_validation->set_rules('title_ar','Arabic Title','required|trim');
					$this->form_validation->set_rules('title_en','English Title','required|trim');
					$this->form_validation->set_rules('news_bar','display on news bar','required|trim|numeric');
					$this->form_validation->set_rules('text_ar','Arabic Details','required|trim');
					$this->form_validation->set_rules('text_en','English Details','required|trim');
					if($this->form_validation->run()=== True){
						$this->data['mode'] = 'success';
						$info['title_ar'] = $this->input->post('title_ar');
						$info['title_en'] = $this->input->post('title_en');
						$info['details_ar'] = $this->input->post('text_ar');
						$info['details_en'] = $this->input->post('text_en');
						$info['date_time'] = time();
						$info['user_id'] = $this->session->userdata('user_id');
						if ($this->input->post('news_bar') == 1)
							$info['display_on_newsbar'] = 'Yes';
						else 
							$info['display_on_newsbar'] = 'No';
					    $n_id = $this->cpanel_model->insert_news($info);
						if ($this->input->post('news_bar') != 0)
							$this->cpanel_model->set_to_news_bar($n_id);
						
					}else {
						$data['mode'] = 'error';
						$data['error'] = '';
					}
					$this->data['view_page']='cpanel/add_news';
		  	   		$this->load->view('cpanel/cp_view_controller', $this->data);
				}else{
					$this->data['view_page']='cpanel/add_news';
		  	    	$this->load->view('cpanel/cp_view_controller', $this->data);
				}
				//$this->data['view_page']='cpanel/add_news';
		  	    //$this->load->view('cpanel/cp_view_controller', $this->data);
			}else{
				$this->data['result'] = "faild";
				$this->data['mode']= 'add';
				$this->data['view_page']='cpanel/add_news';
		  	    $this->load->view('cpanel/cp_view_controller', $this->data);
			}
		}
	}
	public function upload_video()
	{
			$error = "";
			$msg = "";
			$file_element_name = 'videoUserfile';
			if (!$error) {
			    $config['upload_path'] = './uploads/videos/';
			    $config['allowed_types'] = 'mp4|flv|mpg|mpeg|avi|mov';
			    $config['max_size'] = '500000';
			    $config['max_width']  = '';
			    $config['max_height']  = '';
			
			    $this->load->library('upload', $config);
			
			    if (!$this->upload->do_upload($file_element_name)) {
			      $error = $this->upload->display_errors('', '');
			    } 
			    else 
			    {
			    	$data = $this->upload->data();
			      	$msg = $data['file_name'];
			    }   
			}
			echo "{";
			echo "error: '" . $error . "',\n";
			echo "msg: '" . $msg . "'\n";
			echo "}";
		}

	public function upload_image()
	{
			$error = "";
			$msg = "";
			$file_element_name = 'userfile';
			if (!$error) {
			    $config['upload_path'] = './uploads/images/';
			    $config['allowed_types'] = 'gif|jpg|png|jpeg|bmp';
			    $config['max_size'] = 1024 * 3;
			    $config['max_width']  = '1280';
			    $config['max_height']  = '1024';
			    $this->load->library('upload', $config);
			    if (!$this->upload->do_upload($file_element_name)) {
			      $error = $this->upload->display_errors('', '');
			    }
				else {
			    	$image_data= $this->upload->data();
				
					$this->load->library('image_lib');
				    $this->image_resize($image_data['full_path'],'./uploads/images/rotator/',$image_data['file_name'],480,335);
				    $this->image_resize($image_data['full_path'],'./uploads/images/91_55/',$image_data['file_name'],91,55);
					$this->image_resize($image_data['full_path'],'./uploads/images/225_136/',$image_data['file_name'],225,136);				  
				    $this->image_resize($image_data['full_path'],'./uploads/images/270_171/',$image_data['file_name'],270,171);
				   	$this->image_resize($image_data['full_path'],'./uploads/images/371_177/',$image_data['file_name'],371,177);
				   	$this->image_resize($image_data['full_path'],'./uploads/images/770_320/',$image_data['file_name'],770,320);
					$this->image_resize($image_data['full_path'],'./uploads/images/160_120/',$image_data['file_name'],160,120);
					$delete = $image_data['full_path'];
			    	$msg = $image_data['file_name'];
					unlink($delete);
				}   
			}
			echo "{";
			echo "error: '" . $error . "',\n";
			echo "msg: '" . $msg . "'\n";
			echo "}";
		}
	private function image_resize($path,$save_path,$image_name,$width,$height)
    {
        $config['image_library'] = 'gd2'; 
        $config['source_image'] = $path;
        $config['new_image'] = $save_path.$image_name; 
        $config['create_thumb'] = FALSE; 
        $config['maintain_ratio'] = FALSE; 
        $config['width'] = $width; 
        $config['height'] = $height; 
        
        $this->image_lib->initialize($config); 
        
       
         
        // handle if there is any problem 
        if ( ! $this->image_lib->resize())
        {
             return $this->image_lib->display_errors(); 
        }
        $this->image_lib->clear();
        return 1;
    }
	public function upload_soundFile()
	{
			$error = "";
			$msg = "";
			$file_element_name = 'soundUserfile';
			if (!$error) {
			    $config['upload_path'] = './uploads/sounds/';
			    $config['allowed_types'] = 'mp3|wav';
			    $config['max_size'] = 100000000;
			    $this->load->library('upload', $config);
			    if (!$this->upload->do_upload($file_element_name)) {
			      $error = $this->upload->display_errors('', '');
			    }
				else {
			    	$sound_data= $this->upload->data();
			        $msg = $sound_data['file_name'];
			    }   
			}
			echo "{";
			echo "error: '" . $error . "',\n";
			echo "msg: '" . $msg . "'\n";
			echo "}";
	}
	public function getContentTotag()
	{
		$tag_id = $this->input->post('tag_id');
		$this->data['foloclor_contents'] = $this->cpanel_model->get_foloclor_content_with_tag($tag_id);
		echo json_encode($this->data['foloclor_contents']) ;
	}
	public function getActivatedcontents()
	{
		
	}
}
?>