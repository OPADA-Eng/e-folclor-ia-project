<?php
/**
 * 
 */
class MobileWebService extends CI_Controller {
	
	function __construct() {
		parent::__construct();
		$this->load->model('product_manage_model');
		$this->load->model('foloclor_manage_model');
		//$this->load->model('webServiceModel');
		$this->load->model('bonds_model');
		$this->load->model('bill_model');
	}
	public function buyCard()
	{
		$data_json = json_decode(trim(file_get_contents('php://input')), true);
		// here you can do what you want on data 
		//data contains products ids with wanted quantity
		$products=$data_json;
		$user_id=$products[0]->user_id;
		unset($products[0]);
		$bill_type=1;		
		$bill_kind='retail';
		$bond_type='later';
		$total=0;
		$i=1;
		foreach ($products as $key => $prod) {
			$prod=$this->bill_model->get_product($products[$i]->p_id);
			if($prod){
				$total	+=	$products[$i]->p_quantity	*	$prod->max_price;
				$prods[]=array( "id" => $products[$i]->p_id ,
								"price" => $prod->max_price ,
								"qty" => $products[$i]->p_quantity);
			}
			$i++;
				
		}
		$data=array('b_user_id' => $user_id ,
					'b_total' => $total,
					'b_date' => time(),
					'is_aproved' =>0,
					'bt_id' => $bill_type,
					'bill_kind' =>$bill_kind,
					'bond_type' => $bond_type,
					);
		$bill_id=$this->bill_model->insert($data);
		if($bill_id)
			{
				foreach ($prods as $item) {
						$data=array('b_id' => $bill_id,
									'pc_id' => $item['id'],
									'bp_price' =>$item['price'],
									'bill_type' => $bill_type,
									'count' => $item['qty']);
						$this->bill_model->insert_bill_part($data);
					}
					echo json_encode(array('data'=>$data_json,"success"=>1));	
			}
			else {
					echo json_encode(array('data'=>$data_json,"success"=>1));	
				}			
		
		//data will be like this
		//{"data":[{"p_id":"1","p_quantity":"33"},{"p_id":"2","p_quantity":"32"},{"p_id":"3","p_quantity":"31"}],"success":1}
	}
	
	public function getFolclorCats()
	{
		$cats = $this->foloclor_manage_model->get_foloclor_gategories();
		echo json_encode(array('cats'=>$cats,'success'=>1));
	}
	public function getProductCats()
	{
		$cats = $this->product_manage_model->get_product_categories();
		echo json_encode(array('cats'=>$cats,'success'=>1));	
	}
	public function getActiveProductsWithCat()
	{
		$id = $this->input->post('pcat_id');
		$products = $this->product_manage_model->get_mob_category_products($id);
		echo json_encode(array('products'=>$products,"success"=>1));
	}
	public function getActiveFolclorsWithCat()
	{
		$id = $this->input->post('fc_id');
		$folclors = $this->foloclor_manage_model->get_mob_category_folclors($id);
		echo json_encode(array('folclors'=>$folclors,"success"=>1));
	}
	public function getActiveProductDetails()
	{
		$p_id = $this->input->post('pc_id');	
		$product = $this->product_manage_model->get_mob_product_details($p_id);
		echo json_encode(array('product'=>$product,"success"=>1));
	}
	public function getActiveFolclorDetails()
	{
		$f_id = $this->input->post('f_id');	
		$folclor = $this->foloclor_manage_model->get_mob_folclor_details($f_id);
		echo json_encode($folclor);
	}
	
	public function addFolclorRate()
	{
		$row = array(
			'user_id'=>$this->input->post('user_id'),
			'f_id'=> $this->input->post('f_id'),
			'total_voting'=> $this->input->post('rate')	
			 );
			  $foloclor_id = $row["f_id"];	
			  $user_id = $row["user_id"];
			  $this->addVote($foloclor_id,$user_id,$row);
			  $this->return_votes($foloclor_id);		
	}
	
	function addVote($foloclor_id,$user_id,$row)
	{
		$result= array();
		$result = $this->foloclor_voting_model->get_foloclor_user_rates($foloclor_id,$user_id);	
		
		foreach ($result as $data):
       		$rate_db[] = $data;
	 	 endforeach;	
		 
		if(@count($rate_db)==0)
       	{
     		 $this->foloclor_voting_model->add_rate_for_foloclor($row);
		}
		else
		{
			$this->foloclor_voting_model->update_user_rate($row['total_voting'],$user_id,$foloclor_id);	
		}
	}
	
	function return_votes($foloclor_id)
	{
		$result= array();
		$result = $this->foloclor_voting_model->get_foloclor_rates($foloclor_id);	
		foreach ($result as $data):
       		$rate_db[] = $data;
       		$sum_rates[] = $data->fv_voting;
	 	 endforeach;	
		 
       	if(@count($rate_db))
       	{
            $rate_times = count($rate_db);
            $sum_rates = array_sum($sum_rates);
            $rate_value = $sum_rates/$rate_times;
            $rate_bg = (($rate_value)/5)*100;
			// new rating value
			$this->foloclor_voting_model->update_foloclor_rate($rate_value,$foloclor_id);
			$data = array("total_voting"=>$rate_value,"success"=>1);
			echo json_encode($data);
       	}
       	else
       	{
            $rate_times = 0;
            $rate_value = 0;
            $rate_bg = 0;	
	   	}
	}
	
	public function add_comment()
	{
		$this->form_validation->set_rules('f_id', 'foloclor id', 'required|numeric');
		$this->form_validation->set_rules('user_id', 'user id', 'required|numeric');
		$this->form_validation->set_rules('comment', 'Comment', 'required|trim|xss_clean');
		if($this->form_validation->run()== true){
			$data = array('user_id' =>  $this->input->post('user_id'),
						  'foloclor_id'=>$this->input->post('f_id'),
						  'comment'=>$this->input->post('comment'),
						  'publish_state'=>'pending',
						  'date_time'=>time(0)
			 );
			 
			 if($this->foloclor_manage_model->add_foloclor_comment($data)){
			 	echo json_encode(array("success" =>"1"));
			 }
			 else {
				 echo json_encode(array("result" =>"err_db"));
			 }
			 
		}
		else {
			echo json_encode(array("result" =>"err_valid"));
		}
	}
	
	public function get_top_rated_products()
	{
		$products = $this->product_manage_model->get_toplikes_mob_products(TOP_PRODUCTS_RATED);
		echo json_encode(array('products'=>$products,'success'=>'1'));
	}
	
	public function get_top_rated_folclors()
	{
		$folclors = $this->foloclor_manage_model->get_toplikes_mob_folclors(TOP_FOLOCLORS_RATED);
		echo json_encode(array('folclors'=>$folclors,'success'=>'1'));
	}
	/*
	public function buy()
	{
		$data='[{"user_id":"1"},{"p_id":"8","p_quantity":"12"},{"p_id":"7","p_quantity":"2"}]';
		$products=json_decode($data);
		$user_id=$products[0]->user_id;
		unset($products[0]);
		$bill_type=1;		
		$bill_kind='retail';
		$bond_type='later';
		$total=0;
		$i=1;
		foreach ($products as $key => $prod) {
			$prod=$this->bill_model->get_product($products[$i]->p_id);
			if($prod){
				$total	+=	$products[$i]->p_quantity	*	$prod->max_price;
				$prods[]=array( "id" => $products[$i]->p_id ,
								"price" => $prod->max_price ,
								"qty" => $products[$i]->p_quantity);
			}
			$i++;
				
		}
		$data=array('b_user_id' => $user_id ,
					'b_total' => $total,
					'b_date' => time(),
					'is_aproved' =>0,
					'bt_id' => $bill_type,
					'bill_kind' =>$bill_kind,
					'bond_type' => $bond_type,
					);
		$bill_id=$this->bill_model->insert($data);
		if($bill_id)
			{
				foreach ($prods as $item) {
						$data=array('b_id' => $bill_id,
									'pc_id' => $item['id'],
									'bp_price' =>$item['price'],
									'bill_type' => $bill_type,
									'count' => $item['qty']);
						$this->bill_model->insert_bill_part($data);
					}
					echo json_encode(array("success"=>1));
			}
			else {
					echo json_encode(array("success"=>0));
				}								
	}
	 */
}
?>
