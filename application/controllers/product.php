<?php
/**
 * 
 */
class Product extends CI_Controller {
	private $data = array();
	function __construct() {
		parent::__construct();  
		$this->load->library('ion_auth');  
		$this->load->model('product_manage_model');
	}
	public function index($view_page='product/products')
	{
		$this->data['view_page'] = $view_page;
		if($view_page == 'product/products'){
			$this->data['products'] = $this->product_manage_model->get_products_on_cats();
		}
		
		$this->load->view('view_controller',$this->data);
	}
	
	public function category($id='')
	{
		if($id ==''){
			$this->index();
		}
		else {
			if(is_numeric($id)){
				$this->load->library('pagination');
				$this->data['per_page']  = $this->config->item('per_page');
				$this->data['count'] 	 = $this->product_manage_model->count_products_by_cat($id);
				$this->data['products'] = $this->product_manage_model->get_category_products($id,$this->uri->segment(4),$this->config->item('per_page'));
				$this->index('product/products_category');
			}
			else {
				$this->index();
			}
		}
	}
	
	public function view_product($id='')
	{
		if($id != '' && is_numeric($id)){
			$this->data['prod'] = $this->product_manage_model->get_product_details($id);
			$this->data['related_fol'] =  $this->product_manage_model->get_product_related_foloclors($id);
			$this->index('product/view_product');
		}
		else {
			$this->index();
		}
		
	}
	
	
	
	
	
	public function get_relted_foloclors()
	{
		$p_id = 1;//$this->input->post('p_id');
		$data = $this->product_manage_model->get_foloclors($p_id);
		echo json_encode($data);	
	}
}
