<?php class Accounting extends CI_Controller
{
	
	public function __construct()
	{
		parent::__construct();
		$this->load->library('ion_auth');
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login');
		}
		
		$this->load->library('cart');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$this->load->helper('language');
		$this->lang->load('site',get_current_lang());
		$this->load->model('repository_model');
		$this->load->model('account_points_model');
		$this->load->model('bill_model');
		$this->load->model('bonds_model');
		$this->load->model('ion_auth_model');
	}
	
	//repository
	
	public function manage_repository()
	{
		if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),"manage_repository"))
		{
			$data['repositories']= $this->repository_model->get_all();
			$data['view_page']='repository/show_all';
			$this->load->view('cpanel/cp_view_controller', $data);
		}	
	}
	public function add_repository()
	{		
		if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),"manage_repository"))
		{
			$this->form_validation->set_rules('name_en','name_en','required|trim');
			$this->form_validation->set_rules('name_ar','name_ar','required|trim');
			$this->form_validation->set_rules('capacity' , 'capcity','required|trim');
			if ($this->form_validation->run()) 
			{
				$data=array( 'r_name_en' => $this->input->post('name_en'),
							 'r_name_ar' => $this->input->post('name_ar'),
							 'r_capacity' => $this->input->post('capacity'));
				if ($this->repository_model->insert($data))
				{
					//echo 'repository '.get_string('msg-successful');

					$data['message']='repository added successfully';
					$data['view_page']='repository/add';
					$data['mode']='sucessful';
					$this->load->view('cpanel/cp_view_controller', $data);	
				}else{
					$data['message']='add repository failed';
					$data['view_page']='repository/add';
					$data['mode']='failed';
					$this->load->view('cpanel/cp_view_controller', $data);
				}
			}
			else {
				$data['mode']='failed';
				$data['message']='';
				$data['view_page']='repository/add';
				$this->load->view('cpanel/cp_view_controller', $data);
			}
			
		}else{
			redirect(base_url());
		}
		
		
	}
	
	public function edit_repository($id)
	{
		
		if ($id=='' || !is_numeric($id) )
			redirect('auth');
		else if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),"manage_repository"))
			 {
			 	$this->form_validation->set_rules('name_en','name_en','required|trim');
				$this->form_validation->set_rules('name_ar','name_ar','required|trim');
				$this->form_validation->set_rules('capacity' ,'capcity','required|trim');
				if ($this->form_validation->run()) 
				{
					
					$data=array( 'r_name_en' => $this->input->post('name_en'),
								 'r_name_ar' => $this->input->post('name_ar'),
								 'r_capacity' => $this->input->post('capacity'));
					if ($this->repository_model->edit($id,$data))
					{
						//echo 'repository '.get_string('edit_msg-successful');
						$data['message']='repository edited msg-successful';
						$data['view_page']='repository/edit';
						$data['mode']='sucessful';
						$this->load->view('cpanel/cp_view_controller', $data);	
					}else{
						//echo get_string('edit_msg-failed');
						$data['message']='edit repository failed';
						$data['view_page']='repository/edit';
						$data['mode']='failed';
						$repository=$this->repository_model->get_by_id($id);
						$data['id']=$repository->r_id;
						$data['name_en']=$repository->r_name_en;
						$data['name_ar']=$repository->r_name_ar;
						$data['capacity']=$repository->r_capacity;
						$this->load->view('cpanel/cp_view_controller', $data);	
					}
				}
				else
					{
					//echo validation_errors();
						$data['message']=validation_errors();
						$data['view_page']='repository/edit';
						$data['mode']='failed';
						
						$repository=$this->repository_model->get_by_id($id);
						$data['id']=$repository->r_id;
						$data['name_en']=$repository->r_name_en;
						$data['name_ar']=$repository->r_name_ar;
						$data['capacity']=$repository->r_capacity;
						$this->load->view('cpanel/cp_view_controller', $data);	
					}
			}else{
				redirect(base_url());
			}
	}
	
	public function delete_repository($id='')
	{
		if ($id=='' || !is_numeric($id) )
			redirect('auth');
		else if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),"manage_repository"))
			 {
			 	if($this->repository_model->delete($id))
					//echo get_string('delete-msg-successful');
					//echo 'delete-msg-successful';
					$this->manage_repository();
				else 
					//echo get_string('delete-msg-failed');
					//echo 'delete-msg-failed';
					$this->manage_repository();
			}
			else {
				redirect('auth');
		}
	}
	
	//cart 
	
	public function in_cart($id='')
	{
		foreach ($this->cart->contents() as $items)
			if ($id == $items['id'])
					return $items;
			
		return FALSE;
	}
	
	public function update_item($id='',$qty='1')
	{
		//$this->cart->destroy();die();
		//var_dump($this->cart->contents());die();
		if ($id=='' || !is_numeric($id) )
			redirect('auth');
		else{
			//check if the product in cart	
			//$lang=get_current_lang_letter();		
			$item=$this->in_cart($id);
			if (!$item){
				//to_fix
				
				$product=$this->bill_model->get_product($id);
				
				$data = array(
		               'id'      => $id,
		               'qty'     => 1,
		               'price'   =>$product->max_price,
		               'name'    =>$product->product_name_en,
		              // ,'options' => array('Size' => 'L', 'Color' => 'Red'               
		           	 );	
				if ($this->cart->insert($data))
					
					echo $this->lang->line('update_success');
				else
					echo $this->lang->line('update_failed');
			}
			else{
				$item['qty']=$qty;
				$this->cart->update($item);
			}
			
		}
	}


	public function update_cart()
	{
		$in_cart=$this->input->get('in_cart');
		foreach ($in_cart as $key => $value) {
			$this->update_item($value[0],$value[1]);
		}
		echo $this->lang->line('update_success');
	}
	
	public function cart()
	{
		$html=$this->load->view('show_cart_'.get_current_lang_letter(),'',True);
		echo $html;
	}

	public function delete_cart()
	{
			$this->cart->destroy();
			echo 	$this->lang->line('deleted_success');
	}
	
	//balance
	
	public function add_balance()
	{
		$this->form_validation->set_rules('username','username','required|trim');
		$this->form_validation->set_rules('password','password','required|trim');
		if ($this->form_validation->run()) 
		{
			
			$username=$this->input->post('username');
			$password=$this->input->post('password');
			
			$account_points=$this->account_points_model->get_by_username($username,$password);
			
			if ($account_points)
			{
				$data['message']=$this->lang->line('add_balance_success');
				$this->account_points_model->add_balance_to_user($this->session->userdata('user_id'),$account_points->ap_ammount,$account_points->ap_id);
				$data['mode']='success';
			}else{	
				$data['message']=$this->lang->line('add_balance_error');
				$data['mode']='failed';	
			}
		}
		else {
			$data['message']='';
			$data['mode']='failed';	
		}
		$data['cp_user_page']='cp_user/add_balance_'.get_current_lang_letter();
		$data['view_page']='cp_user/cp_user';
		$this->load->view('view_controller', $data);
		
	}
	
	public  function test_blanace ($user_id,$ammount)
	{
		$user_balance=$this->account_points_model->get_balance($user_id);
		if ( $user_balance >= $ammount)
			return true;
		else
			return false;
	}
	
	
	//billing 
	
	public function get_buy_form()
	{
		$data['bill_types']=$this->bill_model->get_bill_types();
		echo $this->load->view('show_buy_'.get_current_lang_letter(),$data,TRUE);
	}
	
	public function buy()
	{
		$bill_type=1;		
		$bill_kind='retail';
		$bond_type=$this->input->get('bond');
		$data=array('b_user_id' => $this->session->userdata('user_id') ,
					'b_total' => $this->cart->total(),
					'b_date' => time(),
					'is_aproved' =>0,
					'bt_id' => $bill_type,
					'bill_kind' =>$bill_kind,
					'bond_type' => $bond_type,
					);
		$bill_id=$this->bill_model->insert($data);
		if($bill_id)
			{
				foreach ($this->cart->contents() as $item) {
						$data=array('b_id' => $bill_id,
									'pc_id' => $item['id'],
									'bp_price' =>$item['price'],
									'bill_type' => $bill_type,
									'count' => $item['qty']);
						$this->bill_model->insert_bill_part($data);
					}
					$this->cart->destroy();
					echo $this->lang->line('buy_success');
			}
			else {
					echo $this->lang->line('buy_error');
				}								
	}

	public function approve_bill($id='')
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),'accept_bill'))
		{
			if($id =='' || !is_numeric($id))
				redirect('auth');
			else{
				$bill=$this->bill_model->get_by_id($id);
				$is_cash=FALSE; 
				$cash_ok=FALSE;
				$bill_type=$bill->bt_id;		
				$bill_kind=$bill->bill_kind;
				$bond_type=$bill->bond_type;
				if ($bond_type=='cash')
				{
					$is_cash=TRUE;
					if ($this->test_blanace($bill->b_user_id,$bill->b_total))
						$cash_ok=TRUE;
					else
						echo 'not enougth balance';
				}
				
				if ($cash_ok || !$is_cash)
				{
						$bill_parts=$this->bill_model->get_user_bill_product($bill->b_user_id,$id);
						$repository_enough=$this->test_repository_enough($bill_parts);
						if ($repository_enough)
						{
							foreach ($bill_parts as $item) {
										$data=array( 'r_id' => $repository_enough[$item->bp_id]->r_id,
													 'pc_is_approved' => 1);
											 	
										$this->bill_model->close_bill_part($item->bp_id,$data);
										$this->repository_model->minus_count_product_in_rep($repository_enough[$item->bp_id]->pr_id,$item->count);
										$this->repository_model->minus_count_repository($repository_enough[$item->bp_id]->r_id,$item->count);
										
									}
									//$this->bill_model->approve_bill($id);
									$this->bill_model->edit($id,array('is_aproved' => 1,'accountant_id' => $this->session->userdata('user_id')));
									
									if ($cash_ok)
									{
										$data=array('bds_debtor' => $bill->b_user_id,
													'bds_creditor' =>$this->session->userdata('user_id'),
													'bond_type' => $bond_type,
													'bds_date' => time(),
													'bill_id' =>$id,
													'paid_ammount' => $bill->b_total);
										if ($this->bonds_model->insert($data))
										{
											
											$this->account_points_model->minus_balance($bill->b_user_id,$bill->b_total);
											$this->bill_model->close_bill($id);
										}
									}
									echo "success";
								} else
									 echo "no products enough";
							
						}
						else {
									echo 'not enougth balance';
							}								
			
			}
	}else 
		redirect('auth');
	}

	public function accept_bill()
	{
		if($this->ion_auth->has_privilage($this->session->userdata('user_id'),'accept_bill'))
		{
		$data['bills']=$this->bill_model->get_not_approved_bill();
		$data['view_page']='billing/show_not_approved';
		//var_dump($data);die();
		$this->load->view('cpanel/cp_view_controller',$data);
		}else
			redirect('auth');
	}
	/*
	public function test_repository_enough()
	{
		$enough=TRUE;
		$product_rep=array();
		foreach ($this->cart->contents() as $item) {
			$repository_id=$this->repository_model->get_by_product_id($item['id'],$item['qty']);
			if ($repository_id)
			{
				$product_rep[$item['rowid']]=$repository_id;
			}else 
				return FALSE;
		}
		return $product_rep;
	}
	*/
	
	public function test_repository_enough($products_part)
	{
		$enough=TRUE;
		$product_rep=array();
		foreach ($products_part as $item) {
			$repository_id=$this->repository_model->get_by_product_id($item->pc_id,$item->count);
			if ($repository_id)
			{
				$product_rep[$item->bp_id]=$repository_id;
			}else 
				return FALSE;
		}
		
		return $product_rep;
	}
	
	public function bill_not_paid()
	{
		$bills=$this->bill_model->get_approved_not_closed_bill($this->session->userdata('user_id'));
		$data['bills']=$bills;
		$data['view_page']='cp_user/cp_user';
		$data['cp_user_page']='cp_user/show_not_closed_'.get_current_lang_letter();
		$this->load->view('view_controller',$data);
		
	}
	
	public function close_bill($id='')
	{
		if( $id=='' || !is_numeric($id))
			redirect('auth');
		else {
			$bill=$this->bill_model->get_by_id($id);
			if ($this->test_blanace($this->session->userdata('user_id'), $bill->b_total))
				{
					$this->bill_model->closed_bill($id);
					$this->account_points_model->minus_balance($this->session->userdata('user_id'),$bill->b_total);
					$data=array('bds_debtor' => $this->session->userdata('user_id'),
								'bds_creditor' =>1,
								'bond_type' => 'later',
								'bds_date' => time(),
								'bill_id' =>$id,
								'paid_ammount' => $bill->b_total);
					$this->bonds_model->insert($data);
					echo $this->lang->line('close_success');
				}else
					echo $this->lang->line('no_enough_balance');
			
		}
	}
	
	public function back_bill($id='')
	{
		if( $id=='' || !is_numeric($id))
			redirect('auth');
		else {
			$bill=$this->bill_model->get_by_id($id);
			$bill_type=3;
			$data=array('b_user_id' => $this->session->userdata('user_id') ,
						'b_total' =>$bill->b_total,
						'b_date' => time(),
						'is_aproved' =>1,
						'bt_id' => $bill_type,
						'bill_kind' =>$bill->bill_kind,
						'b_is_closed' => 1,
						'bond_type' => 'cash');
			$bill_id=$this->bill_model->insert($data);
			$bill_parts=$this->bill_model->get_user_bill_product($bill->b_user_id,$id);
			foreach ($bill_parts as $item) {
				$data=array( 'pc_is_approved' => 1); 	
				$this->bill_model->close_bill_part($item->bp_id,$data);
				$this->repository_model->add_products_repository($item->r_id,$item->pc_id,$item->count);
			}
			$data=array('bds_debtor' => $this->ion_auth_model->get_accountant_user_id(),
						'bds_creditor' =>$bill->b_user_id,
						'bond_type' => 'cash',
						'bds_date' => time(),
						'bill_id' =>$id,
						'paid_ammount' => $bill->b_total,);
			if ($this->bonds_model->insert($data))
			{
				
				$this->account_points_model->add_balance($bill->b_user_id,$bill->b_total);
				$this->bill_model->close_bill($id);
				echo 'bill added successfully';
			}else
				echo 'add bill failed';
			
	}

	}			

	public function buying_bill()
	{
			if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),"buying_bill"))
			{
			$this->form_validation->set_rules('prod_id','product ','required');
			$this->form_validation->set_rules('rep_id','repository ','required');
			$this->form_validation->set_rules('qty' , 'qountity','required');
			$this->form_validation->set_rules('price' , 'bill type','required');
			$this->form_validation->set_rules('bill_kind' , 'bill kind','required');
			$this->form_validation->set_rules('bond_type' , 'bond type','required');
			//var_dump($this->input->post());
			if ($this->form_validation->run() ) 
			   {
				$data=array('b_user_id' => 1 ,
								'b_total' =>  ($this->input->post('price') * $this->input->post('qty')), //qty*price;
								'b_date' => time(),
								'is_aproved' =>1,
								'b_is_closed' =>1,
								'bt_id' => 2,
								'bill_kind' =>$this->input->post('bill_kind'),
								'bond_type' => $this->input->post('bond_type'),
								'accountant_id' =>$this->session->userdata('user_id'));
				$bill_id=$this->bill_model->insert($data);
				if ($bill_id)
				{
					$data=array('b_id' => $bill_id,
								'pc_id' => $this->input->post('prod_id'),
								'bp_price' =>$this->input->post('price'),
								'bill_type' =>2,
								'r_id' => $this->input->post('rep_id'),
								'count' =>$this->input->post('qty'),
								'pc_is_approved' => 1);
								
					if ($this->bill_model->insert_bill_part($data))
					{
						$this->repository_model->add_products_repository($this->input->post('rep_id'),$this->input->post('prod_id'),$this->input->post('qty'));
						$data=array('bds_debtor' => $this->session->userdata('user_id'),
									'bds_creditor' =>0,
									'bond_type' => $this->input->post('bond_type'),
									'bds_date' => time(),
									'bill_id' =>$bill_id,
									'paid_ammount' => ($this->input->post('price') * $this->input->post('qty')));
						if ($this->bonds_model->insert($data))
						{
							$this->account_points_model->minus_balance($this->session->userdata('user_id'),$this->input->post('price') * $this->input->post('qty'));							
						}
										
						$data['message']='bill added successfully';
						$data['mode']='sucessful';
						$data['view_page']='billing/buying';
						$this->load->view('cpanel/cp_view_controller', $data);
					}	
				}else{
					$data['repositories']=$this->repository_model->get_all();
					//$data['products']=$this->products_model->get_all();
					$data['products']=$this->bill_model->get_products();
					$data['bill_types']=$this->bill_model->get_bill_types();
					$data['mode']='failed';
					$data['message']='add bill failed';
					$data['view_page']='billing/buying';
					$this->load->view('cpanel/cp_view_controller', $data);
				}
			}
			else {
				$data['repositories']=$this->repository_model->get_all();
				//$data['products']=$this->products_model->get_all();
				$data['products']=$this->bill_model->get_products();
				$data['bill_types']=$this->bill_model->get_bill_types();
				$data['mode']='failed';
				$data['message']='';
				$data['view_page']='billing/buying';
				$this->load->view('cpanel/cp_view_controller', $data);
			}
			
		}else{
			redirect(base_url());
		}
	}
	
	
	//reporting
	
	public function repository_report($id='')
	{
		if($id =='' || !is_numeric($id))
			{
				$data['repositories']=$this->repository_model->get_all();
				$data['message']='not yet';
				$data['view_page']='report/repository';
				$this->load->view('cpanel/cp_view_controller',$data);
			}
		else if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),'repository_report'))
				{
					$repository=$this->repository_model->get_products_by_id($id);
					$data['repository']=$repository;
					$data['message']='ok';
					$data['view_page']='report/repository';
					$this->load->view('cpanel/cp_view_controller',$data);
				}
	}
	
	public function repository_report_csv($id)
	{
		if($id =='' || !is_numeric($id))
			{
				$data['repositories']=$this->repository_model->get_all();
				$data['message']='not yet';
				$data['view_page']='report/repository';
				$this->load->view('cpanel/cp_view_controller',$data);
			}
		else if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),'repository_report'))
				{
					$repository=$this->repository_model->get_products_by_id($id);	
					$data[]=array('product','qountity');
					foreach ($repository as $key => $value) {
						$data[]=array($value->product_name_en,$value->pr_count);
					}
					$this->load->helper('csv');
					array_to_csv($data, 'repository-'.$id.'.csv');
				}

	}

	public function repository_report_pdf($id)
	{
		if($id =='' || !is_numeric($id))
			{
				$data['repositories']=$this->repository_model->get_all();
				$data['message']='not yet';
				$data['view_page']='report/repository';
				$this->load->view('cpanel/cp_view_controller',$data);
			}
		else if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),'repository_report'))
				{
					$filename=md5(time());
					// As PDF creation takes a bit of memory, we're saving the created file in reports/
					$pdfFilePath = FCPATH."reports\\$filename.pdf";	 
					if (file_exists($pdfFilePath) == FALSE)
					{
					    ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
					  
					   // $html='<h1>  hello hello hello hello </h1>';
					  	$repository=$this->repository_model->get_products_by_id($id);
						$data['repository']=$repository;
						$html = $this->load->view('report/repository_pdf', $data, true); // render the view into HTML
						  
					    $this->load->library('pdf');
					    $pdf = $this->pdf->load();
					    //$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
					    $pdf->WriteHTML($html); // write the HTML into the PDF
					    $pdf->Output($pdfFilePath, 'F'); // save to file because we can
						//download
						$this->load->helper('download');
						$data = file_get_contents(FCPATH."reports\\$filename.pdf"); // Read the file's contents
						$name = 'repository report.pdf';
						force_download($name, $data);
						unlink(FCPATH."reports\\$filename.pdf");
					}
				}
	}

	public function product_report($id='')
	{
		if($id =='' || !is_numeric($id))
			{
				$data['products']=$this->bill_model->get_products();
				$data['view_page']='report/product';
				$data['message']='not yet';
				$this->load->view('cpanel/cp_view_controller',$data);
			}
		else if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),'product_report'))
				{
					$product=$this->bill_model->get_by_product_id($id);
					$data['product']=$product;
					$data['message']='ok';
					$data['view_page']='report/product';
					$this->load->view('cpanel/cp_view_controller',$data);
				}
	}

	public function product_report_csv($id='')
	{
		if($id =='' || !is_numeric($id))
			{
				$data['products']=$this->bill_model->get_products();
				$data['view_page']='report/product';
				$data['message']='not yet';
				$this->load->view('cpanel/cp_view_controller',$data);
			}
		else if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),'product_report'))
				{
					$product=$this->bill_model->get_by_product_id($id);
					$data[]=array('bill type','repository','qountity','price');
					foreach ($product as $key => $value) {
						$data[]=array($value->bt_name_en,$value->r_name_en,$value->count,$value->bp_price);
					}
			
					$this->load->helper('csv');
					array_to_csv($data, 'product-'.$id.'.csv');
				}
	}
	
	public function product_report_pdf($id)
	{
		if($id =='' || !is_numeric($id))
			{
				$data['products']=$this->bill_model->get_products();
				$data['view_page']='report/product';
				$data['message']='not yet';
				$this->load->view('cpanel/cp_view_controller',$data);
			}
		else if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),'product_report'))
				{
					$filename=md5(time());
					// As PDF creation takes a bit of memory, we're saving the created file in reports/
					$pdfFilePath = FCPATH."reports\\$filename.pdf";	 
					if (file_exists($pdfFilePath) == FALSE)
					{
					    ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
						$product=$this->bill_model->get_by_product_id($id);
						$data['product']=$product;
						// render the view into HTML
						$html=$this->load->view('report/product_pdf',$data,TRUE);
						
					    $this->load->library('pdf');
					    $pdf = $this->pdf->load();
					    //$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
					    $pdf->WriteHTML($html); // write the HTML into the PDF
					    $pdf->Output($pdfFilePath, 'F'); // save to file because we can
						//download
						$this->load->helper('download');
						$data = file_get_contents(FCPATH."reports\\$filename.pdf"); // Read the file's contents
						$name = 'product report.pdf';
						force_download($name, $data);
						unlink(FCPATH."reports\\$filename.pdf");
						
					}
				}
	}

	/*
	public function account_book()
	{
		if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),"teacher_book"))
		{
			$users=$this->bill_model->get_users();
			$i=0;
			foreach ($users as $key => $value) {
				$total_not_paid=0;
				$not_closed_bills=$this->bill_model->get_user_bill_not_closed($value->b_user_id);	
				foreach ($not_closed_bills as $key => $value) {
					if (!$value->b_is_closed)
						$total_not_paid +=$value->b_total;
				}
				$bills=$this->bill_model->get_user_bill_not_closed($value->b_user_id);
				$data['users_bills'][$i]=array('user_id' => $value->b_user_id , 'bills' => $bills , 'total' => $total_not_paid);
			}
			var_dump($bills);die();
			$data['view_page']='report/account_book';
			$this->load->view('cpanel/cp_view_controller',$data);
		}
	}
	*/
	
	public function account_book()
	{
		if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),"teacher_book"))
		{
			$data['bonds']=$this->bonds_model->get_all();	
			$data['view_page']='report/account_book';
			$this->load->view('cpanel/cp_view_controller',$data);
		}
	}

	public function account_book_csv()
	{
		if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),"teacher_book"))
		{
			$data[]=array('debtor','creditor','ammount','paid type');
			$bonds=$this->bonds_model->get_all();	
			foreach ($bonds as $key => $value) {
				$data[]=array($value->bds_debtor,$value->bds_creditor,$value->paid_ammount,$value->bond_type);
			}
			$this->load->helper('csv');
			array_to_csv($data,'account_book.csv');
		}
	}

	public function account_book_pdf()
	{
		if ($this->ion_auth->has_privilage($this->session->userdata('user_id'),"teacher_book"))
		{
			$filename=md5(time());
			// As PDF creation takes a bit of memory, we're saving the created file in reports/
			$pdfFilePath = FCPATH."reports\\$filename.pdf";	 
			if (file_exists($pdfFilePath) == FALSE)
			{
			    ini_set('memory_limit','32M'); // boost the memory limit if it's low <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
				$data['bonds']=$this->bonds_model->get_all();	
				// render the view into HTML
				$html=$this->load->view('report/account_book_pdf',$data,TRUE);
			    $this->load->library('pdf');
			    $pdf = $this->pdf->load();
			    //$pdf->SetFooter($_SERVER['HTTP_HOST'].'|{PAGENO}|'.date(DATE_RFC822)); // Add a footer for good measure <img src="https://davidsimpson.me/wp-includes/images/smilies/icon_wink.gif" alt=";)" class="wp-smiley">
			    $pdf->WriteHTML($html); // write the HTML into the PDF
			    $pdf->Output($pdfFilePath, 'F'); // save to file because we can
				//download
				$this->load->helper('download');
				$data = file_get_contents(FCPATH."reports\\$filename.pdf"); // Read the file's contents
				$name = 'account book.pdf';
				force_download($name, $data);
				unlink(FCPATH."reports\\$filename.pdf");
				
			}
				
		}
	}

}

?>

