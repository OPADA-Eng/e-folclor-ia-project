<?php

class Pmg extends CI_Controller {
	
	
	
	function __construct() {
		parent::__construct();  
		$this->load->library('ion_auth');  
		$this->load->helper('language');
		$this->lang->load('site',get_current_lang());
		if (!$this->ion_auth->logged_in())
			 redirect(base_url());
	}
	
	public function index()
	{
		
	}
	
	public function load_categories_products()
	{
		$this->data['result'] = "sucess";
		$this->data['user_id'] = $this->session->userdata('user_id');
		$this->data['categories_products'] = $this->product_manage_model->get_products_on_cats();		
		$this->load->view('product/browse_cats_prods', $this->data);	 
	}
	
	public function load_product_details_by_id()
	{
		$this->form_validation->set_rules('pc_id', 'User', 'required|numeric');
		if($this->form_validation->run())
		{
			$pc_id =$this->input->post('pc_id');
	
			$this->data['product_details'] = $this->product_manage_model->get_product_details($pc_id);
			$this->data['related_foloclors'] = $this->get_product_related_foloclors->get_product_related_foloclors($pc_id);
			$this->load->view('product/browse_prod_details', $this->data);
		}
	}
	
	public function load_toplikes_product()
	{
		$this->data['top_products'] = $this->product_manage_model->get_toplikes_products(TOP_PRODUCTS_RATED);
		echo json_encode($this->data['top_products']);
		//$this->load->view('product/browse_prod_details', $this->data);
	}
	
	public function load_topcomments_product()
	{
		$this->data['most_commented_products'] = $this->product_manage_model->get_topcomments_products(TOP_PRODUCTS_COMMENTED);
		echo json_encode($this->data['most_commented_products']);
		//$this->load->view('product/browse_prod_details', $this->data);
	}
	
	public function load_recently_added_products()
	{
		$this->data['recently_products'] = $this->foloclor_manage_model->get_recently_add_products(RECENTLY_PRODUCTS_ADDED);
		echo json_encode($this->data['recently_products']);
		//load target view
	}
	
	public function add_product_record()
	{
			$this->form_validation->set_rules('cat_en', 'cat_en', 'required|numeric');
			$this->form_validation->set_rules('type_en', 'type_en', 'required|numeric');
			$this->form_validation->set_rules('product_name_ar', 'product_name_ar', 'required|trim|xss_clean');
			$this->form_validation->set_rules('product_name_en', 'product_name_en', 'required|trim|xss_clean');
			$this->form_validation->set_rules('color', 'color', 'trim|xss_clean');
			$this->form_validation->set_rules('details_ar', 'details_ar', 'required|trim|xss_clean');
			$this->form_validation->set_rules('details_en', 'details_en', 'required|trim|xss_clean');
			$this->form_validation->set_rules('min_price', 'min_price', 'required|numeric');
			$this->form_validation->set_rules('max_price', 'max_price', 'required|numeric');
			$this->form_validation->set_rules('weight', 'weight', 'numeric');
			$this->form_validation->set_rules('weight_type', 'weight_type', 'trim|xss_clean');
			$this->form_validation->set_rules('volum', 'volum', 'numeric');
			$this->form_validation->set_rules('width', 'width', 'numeric');
			$this->form_validation->set_rules('height', 'height', 'numeric');	
			$this->form_validation->set_rules('max_quantity', 'max_quantity', 'required|numeric');
			$this->form_validation->set_rules('min_quantity', 'min_quantity', 'required|numeric');	
			
			if($this->form_validation->run())
			{
				 $row = array(
					'user_id'=>$this->session->userdata('user_id'),
					'pcat_id'=> $this->input->post('cat_en'),
					'pt_id'=> $this->input->post('type_en'),
					'product_name_ar' =>$this->input->post('product_name_ar'),
					'product_name_en' =>$this->input->post('product_name_en'),
					'min_price'=> $this->input->post('min_price'),
					'max_price'=> $this->input->post('max_price'),
					'color'=> $this->input->post('color'),
					'weight'=> $this->input->post('weight'),
					'weight_type'=> $this->input->post('weight_type'),
					'volum'=>  $this->input->post('volum'),
					'height'=> $this->input->post('height'),
					'width'=> $this->input->post('width'),
					'max_quantity'=> $this->input->post('max_quantity'),
					'min_quantity'=> $this->input->post('min_quantity'),
					'details_ar'=> $this->input->post('details_ar'),
					'details_en'=> $this->input->post('details_en'),
					'publish_state' => 7
				 );
			
				$last_add = $this->Product_model->create_new_product_content($row);		
				foreach ($this->input->post('keys') as $key) {
					$this->Product_model->add_keywords_to_content($key,$last_add);
				}
				echo $last_add;
			}
			else
				echo "failed";

	}

	public function update_im_product_record()
	{
		if ($this->ion_auth->logged_in())
		{
			$this->form_validation->set_rules('image', 'image', 'xss_clean');	
			if($this->form_validation->run())
			{
				$data = array('image' => $this->input->post('f_image'));
				$pc_id =  $this->input->post('pc_id');
				$this->Product_model->add_mm_to_content($data,$pc_id);
			}
		}else{
			redirect(base_uel());
	}
	}
	
	public function add_product()
	{

			$this->data['result'] = "sucess";
			$this->data['user_id'] = $this->session->userdata('user_id');
			$this->data['product_category']=$this->Product_model->show_categories();
			$this->data['product_type']=$this->Product_model->show_types();
			$this->data['keys'] = json_encode($this->Product_model->get_keywords());	
	  	    $this->data['cp_user_page']='cp_user/add_product_by_user_'.get_current_lang_letter();
			$this->data['view_page']='cp_user/cp_user';
			$this->load->view('view_controller', $this->data);	
		 
	}
	
}
?>