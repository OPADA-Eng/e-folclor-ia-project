<?php

/**
 * 
 */
class Messages extends CI_Controller {
	
	private $userId;
	function __construct() {
		parent::__construct();
		$this->load->model('messages_model');
		$this->load->library('ion_auth');
		$this->userId = $this->session->userdata('user_id');
		$this->load->helper('language');
		$this->lang->load('site',get_current_lang());
	}
	
	public function index($page_num='')
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login/messages');
		}
		$data = array('cp_user_page' =>'msg/message_'.get_current_lang_letter(),
					  'view_page'  => 'cp_user/cp_user',
					  'inbox_messages'=>$this->messages_model->get_messages($page_num,$this->userId,'in'),
					  'outbox_messages'=>$this->messages_model->get_messages($page_num,$this->userId,'out'),
					  'available_users' => json_encode($this->messages_model->get_all_users())
					  );
		$this->load->view('view_controller',$data);
	}
	public function inbox_messages($page_num='')
	{
		if (!$this->ion_auth->logged_in())
		{
			echo "access_denied";
		}
		echo json_encode($this->messages_model->get_messages($page_num,$this->userId,'in'));
	}
	public function outbox_messages($page_num='')
	{
		if (!$this->ion_auth->logged_in())
		{
			redirect('auth/login/messages');
		}
		echo json_encode($this->messages_model->get_messages($page_num,$this->userId,'out'));
	}
	public function get_available_users()
	{
		if (!$this->ion_auth->logged_in())
		{
			echo 'access_denied';
		}
		else {
			echo json_encode($this->messages_model->get_all_users());
		}
	}
	public function send_message()
	{
		if (!$this->ion_auth->logged_in())
		{
			echo "access denied";
		}
		$this->form_validation->set_rules('to_user_id', 'To User', 'required');
		$this->form_validation->set_rules('message', 'Message', 'required|trim|xss_clean');
		if($this->form_validation->run() == true){
			// add the message
			$message = $this->input->post('message');
			$to_user = $this->input->post('to_user_id');
			$msg_arr = array('msg_text'=>$message,
							 'msg_from'=>$this->userId,
							 'msg_date'=>time(0),
							 'msg_sender_status'=>'Sent');
			
			if($this->messages_model->add_message($msg_arr,$to_user)){
				echo "success";
			}
			else {
				echo $this->lang->line('message_error');
			}
		}
		else {
			echo $this->lang->line('message_error');
		}
	}
	
	public function delete_message_in()
	{
		if (!$this->ion_auth->logged_in())
		{
			echo "access denied";
		}
		$this->form_validation->set_rules('message_id', 'Message', 'required|trim|numeric|xss_clean');
		if($this->form_validation->run() == true){
			$id = $this->input->post('message_id');
			if($this->messages_model->has_message($this->userId,$id)){
				if($this->messages_model->delete_recived_message($id)){
					echo $this->lang->line('message_success_del');
				}
				else {
					echo $this->lang->line('message_error_del');
				}
			}
			else{
				echo $this->lang->line('no_priv_del');
			}
		}
		else {
			echo $this->lang->line('message_error_del');
		}
	}
	
	public function delete_message_out()
	{
		if (!$this->ion_auth->logged_in())
		{
			echo "access denied";
		}
		$this->form_validation->set_rules('message_id', 'Message', 'required|trim|numeric|xss_clean');
		if($this->form_validation->run() == true){
			$id = $this->input->post('message_id');
			if($this->messages_model->has_message($this->userId,$id)){
				if($this->messages_model->delete_sent_message($id)){
				echo $this->lang->line('message_success_del');
				}
				else {
					echo $this->lang->line('message_error_del');
				}
			}
			else{
				echo $this->lang->line('no_priv_del');
			}
		}
		else {
			echo $this->lang->line('select_sure');
		}
	}
	
}
