<?php 
/**
 * 
 */
class Search extends CI_Controller {
	
	function __construct() {
		parent::__construct(); 
		$this->load->model('search_model');
		$this->load->helper('language');
	}
	public function index()
	{
		if($this->input->post('submit')) {
			$keyword = $this->input->post('keyword');
			if($keyword != ''){
				$this->session->set_userdata($keyword);
				if(get_current_lang_letter() == 'en'){
					$result_foloclor_by_title_en = $this->search_model->get_search_foloclor_by_title_en($keyword,$this->config->item('per_page'),$this->uri->segment(3));
					$result_product_by_title_en = $this->search_model->get_search_product_by_title_en($keyword,$this->config->item('per_page'),$this->uri->segment(3));
					$found_foloclor_result = array();
					$found_product_result = array();
					if($result_foloclor_by_title_en){
						for ($i=0; $i < sizeof($result_foloclor_by_title_en); $i++) { 
							$found_foloclor_result[$i]['foloclor_contents'] = $result_foloclor_by_title_en[$i];
							$found_foloclor_result[$i]['user_publisher'] = $this->search_model->get_user_publisher_foloclor($result_foloclor_by_title_en[$i]->f_id);
							$found_foloclor_result[$i]['tags'] = $this->search_model->get_tag_foloclor($result_foloclor_by_title_en[$i]->f_id);
							$found_foloclor_result[$i]['comments'] = $this->search_model->get_comment_foloclor($result_foloclor_by_title_en[$i]->f_id);
						}
						$data['search_foloclor']=$found_foloclor_result;
						$data['mode_foloclor']='search_foloclor';
						$data['type']= 'Foloclor';
					}else{
						$result_foloclor_by_content_en = $this->search_model->get_search_foloclor_by_content_en($keyword,$this->config->item('per_page'),$this->uri->segment(3));
						$found_foloclor_result = array();
						if($result_foloclor_by_content_en){
							for ($i=0; $i < sizeof($result_foloclor_by_content_en); $i++) { 
								$found_foloclor_result[$i]['foloclor_contents'] = $result_foloclor_by_content_en[$i];
								$found_foloclor_result[$i]['user_publisher'] = $this->search_model->get_user_publisher_foloclor($result_foloclor_by_content_en[$i]->f_id);
								$found_foloclor_result[$i]['tags'] = $this->search_model->get_tag_foloclor($result_foloclor_by_content_en[$i]->f_id);
								$found_foloclor_result[$i]['comments'] = $this->search_model->get_comment_foloclor($result_foloclor_by_content_en[$i]->f_id);
							}
							$data['search_foloclor']=$found_foloclor_result;
							$data['mode_foloclor']='search_foloclor';	
							$data['type']= 'Foloclor';
						}else{
							$data['mode']='error';
						}
					}
					////////////////////////////////////////////
					if($result_product_by_title_en){
						for ($i=0; $i < sizeof($result_product_by_title_en); $i++) { 
							$found_product_result[$i]['product_contents'] = $result_product_by_title_en[$i];
							$found_product_result[$i]['user_publisher'] = $this->search_model->get_user_publisher_product($result_product_by_title_en[$i]->pc_id);
						}
						$data['search_product']=$found_product_result;
						$data['mode_product']='search_product';
						$data['type']= 'Product';
					}else{
						$result_product_by_content_en = $this->search_model->get_search_product_by_content_en($keyword,$this->config->item('per_page'),$this->uri->segment(3));
					
						$found_product_result = array();
						if($result_product_by_content_en){
							for ($i=0; $i < sizeof($result_product_by_content_en); $i++) { 
								$found_product_result[$i]['product_contents'] = $result_product_by_content_en[$i];
								$found_product_result[$i]['user_publisher'] = $this->search_model->get_user_publisher_product($result_product_by_content_en[$i]->pc_id);
							}
							$data['search_product']=$found_product_result;
							$data['mode_product']='search_product';	
							$data['type']= 'Product';
						}else{
							$data['mode']='error';
						}
					}
					$data['view_page']='search';
					$this->load->view('view_controller',$data);
			}else{
					
					$result_foloclor_by_title_ar = $this->search_model->get_search_foloclor_by_title_ar($keyword,$this->config->item('per_page'),$this->uri->segment(3));
					$result_product_by_title_ar = $this->search_model->get_search_product_by_title_ar($keyword,$this->config->item('per_page'),$this->uri->segment(3));
					$found_foloclor_result = array();
					$found_product_result = array();
					if($result_foloclor_by_title_ar){
						for ($i=0; $i < sizeof($result_foloclor_by_title_ar); $i++) { 
							$found_foloclor_result[$i]['foloclor_contents'] = $result_foloclor_by_title_ar[$i];
							$found_foloclor_result[$i]['user_publisher'] = $this->search_model->get_user_publisher_foloclor($result_foloclor_by_title_ar[$i]->f_id);
							$found_foloclor_result[$i]['tags'] = $this->search_model->get_tag_foloclor($result_foloclor_by_title_ar[$i]->f_id);
							$found_foloclor_result[$i]['comments'] = $this->search_model->get_comment_foloclor($result_foloclor_by_title_ar[$i]->f_id);
						}
						$data['search_foloclor']=$found_foloclor_result;
						$data['mode']='search_foloclor_by_title_ar';
						$data['type']= 'فلوكلور';
					}else{
						$result_foloclor_by_content_ar = $this->search_model->get_search_foloclor_by_content_ar($keyword,$this->config->item('per_page'),$this->uri->segment(3));
						$found_foloclor_result = array();
						if($result_foloclor_by_content_ar){
							for ($i=0; $i < sizeof($result_foloclor_by_content_ar); $i++) { 
								$found_foloclor_result[$i]['foloclor_contents'] = $result_foloclor_by_content_ar[$i];
								$found_foloclor_result[$i]['user_publisher'] = $this->search_model->get_user_publisher_foloclor($result_foloclor_by_content_ar[$i]->f_id);
								$found_foloclor_result[$i]['tags'] = $this->search_model->get_tag_foloclor($result_foloclor_by_content_ar[$i]->f_id);
								$found_foloclor_result[$i]['comments'] = $this->search_model->get_comment_foloclor($result_foloclor_by_content_ar[$i]->f_id);
							}
							$data['search_foloclor']=$found_foloclor_result;
							$data['mode_foloclor']='search_foloclor';	
							$data['type']= 'Foloclor';
						}else{
							$data['mode']='error';
						}
					}
					////////////////////////////////////////////
					if($result_product_by_title_ar){
						for ($i=0; $i < sizeof($result_product_by_title_ar); $i++) { 
							$found_product_result[$i]['product_contents'] = $result_product_by_title_ar[$i];
							$found_product_result[$i]['user_publisher'] = $this->search_model->get_user_publisher_product($result_product_by_title_ar[$i]->pc_id);
						}
						$data['search_product']=$found_product_result;
						$data['mode_product']='search_product';
						$data['type']= 'Product';
					}else{
						$result_product_by_content_ar = $this->search_model->get_search_product_by_content_ar($keyword,$this->config->item('per_page'),$this->uri->segment(3));
					
						$found_product_result = array();
						if($result_product_by_content_ar){
							for ($i=0; $i < sizeof($result_product_by_content_ar); $i++) { 
								$found_product_result[$i]['product_contents'] = $result_product_by_content_ar[$i];
								$found_product_result[$i]['user_publisher'] = $this->search_model->get_user_publisher_product($result_product_by_content_ar[$i]->pc_id);
							}
							$data['search_product']=$found_product_result;
							$data['mode_product']='search_product';	
							$data['type']= 'Product';
						}else{
							$data['mode']='error';
						}
					}
					$data['view_page']='search';
					$this->load->view('view_controller',$data);
			}
			
		}
	}else{
		$data['view_page']='template/home';
		$this->load->view('view_controller',$data);
	}
}
}