<?php
function get_nessacery_header_content()
{
	$ci =& get_instance();
	$ci->load->model('product_manage_model');
	$ci->load->model('foloclor_manage_model');
	$data['latest_newsbar']	= $ci->product_manage_model->get_latest_news();
	$data['foloclor_articles'] 	= $ci->foloclor_manage_model->get_limit_approved_foloclor_content();//get approved contents.
	$data['p_categories'] 		= $ci->product_manage_model->get_product_categories();
	$data['f_categories'] 		= $ci->foloclor_manage_model->get_foloclor_categories();
	return $data;
}
function get_nessacery_footer_content()
{
	$ci =& get_instance();
	$ci->load->model('product_manage_model');
	$ci->load->model('foloclor_manage_model');
	
	$data['foloclors_tags']		= $ci->foloclor_manage_model->get_foloclors_tags();
	$data['top_products'] 		= $ci->product_manage_model->get_toplikes_products(TOP_PRODUCTS_RATED);
	
	return $data;
}

?>