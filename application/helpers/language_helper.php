<?php

function get_string($value='')
{
	$ci =& get_instance();
	return $ci->lang->line($value);
}
function lang($value='')
{
	$ci =& get_instance();
	return $ci->lang->line($value);
}
function get_current_lang()
{
	$ci =& get_instance();
	return $ci->session->userdata('site_lang');
}
function get_current_lang_letter()
{
	$ci =& get_instance();
	if($ci->session->userdata('site_lang')== 'arabic'){
		return 'ar';
	}
	else {
		return 'en';
	}
}