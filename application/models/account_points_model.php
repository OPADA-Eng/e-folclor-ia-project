<?php

class Account_points_model extends CI_Model 
{
	private $tabel_name='account_points';
	private $tabel_id='ap_id';
	
	public function __construct ()
	{
		
	}
	
	public function get_all()
	{
		$query=$this->db->get($this->tabel_name);
		$results=$query->result();
		if ($results)
			return $results;
		else 
			return FALSE;
	}
	
	public function get_by_id($id)
	{
		$this->db->where($this->tabel_id,$id);
		$query=$this->db->get($this->tabel_name);
		$results=$query->row();
		if ($results)
			return $results;
		else 
			return FALSE;
	}
	public function get_by_username($username,$password)
	{
		$this->db->where('ap_username',$username);
		$this->db->where('ap_password',$password);
		$this->db->where('ap_is_active',1);
		$query=$this->db->get($this->tabel_name);
		$results=$query->row();
		if ($results)
			return $results;
		else 
			return FALSE;
	}
	
	public function add_balance_to_user($user_id,$ammount,$ap_id)
	{
		$this->db->where('id',$user_id);
		$query=$this->db->get('users');
		$result=$query->row();
		$this->db->where('id',$user_id);
		$this->db->update('users',array('balance' =>$ammount + $result->balance) );
		if ( $this->db->affected_rows() >0)
		{
			$this->disactive_account($ap_id);
			return TRUE;
		}
		else
			return FALSE;
	}
	
	public function add_balance($user_id,$ammount)
	{
		$this->db->where('id',$user_id);
		$query=$this->db->get('users');
		$result=$query->row();
		$this->db->where('id',$user_id);
		$this->db->update('users',array('balance' =>$ammount + $result->balance) );
		if ( $this->db->affected_rows() >0)
		{	
			return TRUE;
		}
		else
			return FALSE;
	}
	
	
	 // public function add_balance_to_user($user_id,$ammount,$ap_id)
	// {
		// $this->db->where('user_id',$user_id);
		// $query=$this->db->get('user_bill_account');
		// $result=$query->row();
		// if ($result)
		// {
			// $this->db->insert('user_bill_account',array('user_id' => $user_id , 'uba_ammount' => $ammount));
			// if ( $this->db->affected_rows() >0)
			// {
				// $this->disactive_account($ap_id);
				// return TRUE;
			// }
			// else
				// return FALSE;
		// }else{
			// $this->db->where('user_id',$user_id);
			// $this->db->update('user_bill_account',array('uba_ammount' =>$ammount + $result->uba_ammount) );
			// if ( $this->db->affected_rows() >0)
			// {
				// $this->disactive_account($ap_id);
				// return TRUE;
			// }
			// else
				// return FALSE;
		// }
	// }
	 
	public function get_balance($user_id)
	{
		$this->db->where('id',$user_id);
		$query=$this->db->get('users');
		if ($query )
		{
			return $query->row()->balance;
		}
		else
			return FALSE;
		
	}
	
	public function disactive_account($id='')
	{
		$this->db->where($this->tabel_id,$id);
		$this->db->update($this->tabel_name,array('ap_is_active'=>0));
		if ($this->db->affected_rows() >0 )
			return TRUE;
		else
			return FALSE;
	}
	
	public function minus_balance($user_id,$ammount)
	{
		$this->db->where('id',$user_id);
		$query=$this->db->get('users');
		$this->db->update('users',array('balance' => ($query->row()->balance - $ammount)));
		if ($this->db->affected_rows() >0 )
		{
			return TRUE;
		}
		else
			return FALSE;
		
	}

	/*
	public function get_id_by_user_id($id='')
	{
		$this->db->where('user_id' , $id);
		$query=$this->db->get('user_bill_account');
		if ($query)
		 return $query->row()->uba_id;
		else
			return FALSE;
	}
	*/
	/*
	public function get_sys_id()
	{
		$this->db->where('user_id' , 0);
		$query=$this->db->get('user_bill_account');
		if ($query)
		 return $query->row()->uba_id;
		else
			return FALSE;
	}
	 * */
	public  function insert($date)
	{
		$this->db->insert($this->tabel_name,$date);
		if ($this->db->affected_rows() > 0)
			return $this->db->insert_id();
		else 
			return FALSE;
	}
	
	public function edit ($id,$data)
	{
		$this->db->where($this->tabel_id,$id);
		$this->db->update($this->tabel_name,$data);
		if ($this->db->affected_rows() >0 )
			return TRUE;
		else 
			return FALSE;
	}
	
	public function delete ($id)
	{
		$this->db->where($this->tabel_id,$id);
		if ($this->db->delete($this->tabel_name) )
			return TRUE;
		else 
			return FALSE;
	}
	
	
	
}
