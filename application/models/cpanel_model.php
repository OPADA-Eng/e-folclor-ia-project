<?php
/**
 * 
 */
class Cpanel_model extends CI_Model {
	
	public function get_pending_comments_foloclor($pending)
	{
		$result= mysql_query("SELECT foloclor_content_comment.fcc_id,foloclor_content_comment.foloclor_id,foloclor_content.f_id,foloclor_content_comment.comment, foloclor_content_comment.date,foloclor_content.f_title_ar,foloclor_content.f_title_en,foloclor_content.f_historical_importance_ar,foloclor_content.f_historical_importance_en,users.username
							  FROM foloclor_content_comment INNER JOIN foloclor_content ON foloclor_content_comment.publish_state = '".$pending."'AND foloclor_content_comment.foloclor_id=foloclor_content.f_id
							  INNER JOIN users ON foloclor_content_comment.user_id = users.id ");
		if(mysql_num_rows($result) > 0 ){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	
	public function get_foloclor_gategories()
	{
		$query = $this->db->get('foloclor_category');
		return $query->result();
	}
	
	public function get_tags()
	{
		$q = $this->db->get('foloclor_tag');
		if($q->num_rows()> 0){
			foreach ($q->result() as $key => $value) {
				$data[] = $value; 
			}
			return $data;
		}else{
			return FALSE;
		}
		
	}
	
	public function get_keywords()
	{
		$q = $this->db->get('foloclor_keywords');
		if($q->num_rows()> 0){
			foreach ($q->result() as $key => $value) {
				$data[] = $value; 
			}
			return $data;
		}else{
			return FALSE;
		}
		
	}
	public function add_new_category($data)
	{
		$this->db->insert('foloclor_category',$data);
		$insert_id = $this->db->insert_id();
   		$this->db->trans_complete();
   		return  $insert_id;
	}
	
	public function add_new_keyword($data)
	{
		$this->db->insert('foloclor_keywords',$data);
		$insert_id = $this->db->insert_id();
   		$this->db->trans_complete();
   		return  $insert_id;
	}
	public function add_tags_to_content($tag_id,$f_id)
	{
		$data = array('f_id' => $f_id ,'ft_id'=> $tag_id);
		$this->db->insert('foloclor_content_tag',$data);
	}

	public function add_keywords_to_content($keyword_id,$f_id)
	{
		$data = array('f_id' => $f_id ,'fk_id'=>$keyword_id);
		$this->db->insert('foloclor_content_keyword',$data);
	}

	
	public function add_mm_to_content($data,$f_id)
	{
		$this->db->where('f_id',$f_id);
		$this->db->update('foloclor_content',$data);
	}

	public function add_new_tag($data)
	{
		$this->db->insert('foloclor_tag',$data);
		$insert_id = $this->db->insert_id();
   		$this->db->trans_complete();
   		return  $insert_id;
	}
	
	public function create_new_foloclor_content($data)
	{
		$this->db->insert('foloclor_content',$data);
		$insert_id = $this->db->insert_id();
   		$this->db->trans_complete();
   		return  $insert_id;
	}	
	public function get_foloclor_content_rates_times($f_id)
	{
		$this->db->select('*');
		$this->db->from('foloclor_voting');
		$this->db->where('f_id',$f_id);
		$query = $this->db->get();
		$rowcount = $query->num_rows();
		return $rowcount;
	}
	public function monitor_foloclor_content()
	{
		$this->db->select('*');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['f_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['rate_times'] = $this->get_foloclor_content_rates_times($item['f_id']);
			$final_res[$i]['item_tags'] = $this->get_foloclor_content_tags($item['f_id']);
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
		return $final_res;
	}
	public function get_foloclor_rate_history($f_id)
	{
		$this->db->select('*');
		$this->db->from('foloclor_voting');
		$this->db->join('users','foloclor_voting.user_id = users.id');
		$this->db->where('f_id',$f_id);
		$query = $this->db->get();
		return $query->result();
	}
	public function	get_active_foloclor_contents()
	{
		$this->db->select('*');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('publish_state',4); // approved by admin
		$this->db->where('is_active',1); // is active
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['f_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_tags'] = $this->get_foloclor_content_tags($item['f_id']);
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
		return $final_res;
	}
	public function get_foloclor_content_with_tag($tag_id)
	{
		$this->db->select('foloclor_content.f_id,
						   f_title_ar,f_title_en,f_content_ar,f_content_en,
						   f_historical_importance_ar,f_historical_importance_en,
					       f_location_ar,f_location_en,
						   f_image,f_video,f_sound,total_voting,
						   foloclor_category.fc_id,foloclor_category.fc_name_ar,foloclor_category.fc_name_en,
						  ');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('foloclor_content_tag','foloclor_content.f_id = foloclor_content_tag.f_id');
		$this->db->join('foloclor_tag','foloclor_tag.ft_id = foloclor_content_tag.ft_id');
		$this->db->where('foloclor_tag.ft_id',$tag_id);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
		return $final_res;
	}
	
	public function get_checked_foloclor_content()
	{
		$this->db->select('*');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('publish_state',5);
		$this->db->or_where('publish_state',2);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['f_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_tags'] = $this->get_foloclor_content_tags($item['f_id']);
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
		return $final_res;
	}
	
	public function get_all_pending_foloclor_content()
	{
		$this->db->select('*');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('publish_state',7);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['f_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_tags'] = $this->get_foloclor_content_tags($item['f_id']);
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
		return $final_res;
	}
	
	public function get_foloclor_content_tags($fc_id)
	{
		$this->db->select('*');
		$this->db->from('foloclor_tag');
		$this->db->join('foloclor_content_tag','foloclor_tag.ft_id = foloclor_content_tag.ft_id');
		$this->db->where('foloclor_content_tag.f_id',$fc_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_foloclor_content_keywords($fc_id)
	{
		$this->db->select('*');
		$this->db->from('foloclor_keywords');
		$this->db->join('foloclor_content_keyword','foloclor_keywords.fk_id = foloclor_content_keyword.fk_id');
		$this->db->where('foloclor_content_keyword.f_id',$fc_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function update_content_status_to_approve($content_id,$state,$user_id)
	{
		if($state == 4)
		{
			$data = array('publish_state' => "$state",
			     		  'admin_id'=> "$user_id",
						  'is_active' => 1);
		}
		else
		{
			$data = array('publish_state' => "$state",
				    	  'checker_id'=> "$user_id");
		}
		$this->db->where('f_id',$content_id);
		$this->db->update('foloclor_content',$data);
	}
	
	public function update_content_status_to_deactivate($f_id)
	{
		$data = array('is_active' => 0);
		$this->db->where('f_id',$f_id);
		$this->db->update('foloclor_content',$data);	
	}
	
	public function update_content_status_to_activate($f_id)
	{
		$data = array('is_active' => 1);
		$this->db->where('f_id',$f_id);
		$this->db->update('foloclor_content',$data);	
	}
	
	public function update_foloclor_content($content_id,$data)
	{
		$this->db->where('f_id',$content_id);
		$this->db->update('foloclor_content',$data);
		/*
		 * 1, delete all previous tags and keyword of f_id
		 * 2, add new tags and keywords  
		 */
		$this->cpanel_model->delete_previous_tags($content_id);
		$this->cpanel_model->delete_previous_keys($content_id);
		
	}
	public function delete_previous_tags($f_id)
	{
		$this->db->where('f_id',$f_id);
		$this->db->delete('foloclor_content_tag');
	}
	
	public function delete_previous_keys($f_id)
	{
		$this->db->where('f_id',$f_id);
		$this->db->delete('foloclor_content_keyword');
	}
	
	public function update_content_status_to_reject($content_id,$checker_opinion,$state,$user_id)
	{
		if($state == 2)
		{
			$data = array('publish_state' => "$state",
					      'chacker_opinion' => "$checker_opinion",
					      'checker_id'=> "$user_id",
						  'is_active' => 0);
		}
		else 
		{
			$data = array('publish_state' => "$state",
						  'admin_id'=> "$user_id");
		}		  
		$this->db->where('f_id',$content_id);
		$this->db->update('foloclor_content',$data);
	}
	
	
	public function get_active_comments_foloclor($active)
	{
		$result= mysql_query("SELECT foloclor_content_comment.comment, foloclor_content_comment.date,foloclor_content.f_title_ar,foloclor_content.f_title_en,foloclor_content.f_historical_importance_ar,foloclor_content.f_historical_importance_en,users.username
							  FROM foloclor_content_comment INNER JOIN foloclor_content ON foloclor_content_comment.publish_state = '".$active."'AND foloclor_content_comment.foloclor_id=foloclor_content.f_id
							  INNER JOIN users ON foloclor_content_comment.user_id = users.id ");
		if(mysql_num_rows($result) > 0 ){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	public function get_inactive_comments_foloclor($inactive)
	{
		$result= mysql_query("SELECT foloclor_content_comment.comment, foloclor_content_comment.date,foloclor_content.f_title_ar,foloclor_content.f_title_en,foloclor_content.f_historical_importance_ar,foloclor_content.f_historical_importance_en,users.username
							  FROM foloclor_content_comment INNER JOIN foloclor_content ON foloclor_content_comment.publish_state = '".$inactive."'AND foloclor_content_comment.foloclor_id=foloclor_content.f_id
							  INNER JOIN users ON foloclor_content_comment.user_id = users.id ");
		if(mysql_num_rows($result) > 0 ){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	
	public function get_pending_foloclor_content1()
	{
		$pending = "pending";
		$result = mysql_query("SELECT foloclor_content.f_id,foloclor_content.f_title_ar,foloclor_content.f_title_en,foloclor_content.f_historical_importance_ar,foloclor_content.f_historical_importance_en
							  FROM foloclor_content_comment INNER JOIN foloclor_content 
							  ON foloclor_content_comment.publish_state = '".$pending."' 
							  AND foloclor_content_comment.foloclor_id = foloclor_content.f_id ");
		if(mysql_num_rows($result) > 0 ){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	
	public function get_foloclor_content()
	{
		$res = $this->db->get('foloclor_content');
		if($res->num_rows() > 0){
			return $res->result();
		}else{
			return FALSE;
		}
	}
	public function get_pending_foloclor_comments($foloclor_id)
	{
		$pending = "pending";
		$result= mysql_query("SELECT foloclor_content_comment.fcc_id, foloclor_content_comment.comment, foloclor_content_comment.date_time, users.username
							  FROM foloclor_content_comment INNER JOIN users ON foloclor_content_comment.foloclor_id = '".$foloclor_id."' AND foloclor_content_comment.publish_state='".$pending."' 
							  AND foloclor_content_comment.user_id = users.id ");
		if(mysql_num_rows($result) > 0 ){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	public function get_inactive_foloclor_comments($foloclor_id)
	{
		$pending = "inactive";
		$result= mysql_query("SELECT foloclor_content_comment.fcc_id, foloclor_content_comment.comment, foloclor_content_comment.date_time, users.username
							  FROM foloclor_content_comment INNER JOIN users ON foloclor_content_comment.foloclor_id = '".$foloclor_id."' AND foloclor_content_comment.publish_state='".$pending."' 
							  AND foloclor_content_comment.user_id = users.id ");
		if(mysql_num_rows($result) > 0 ){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	public function active_comment($fcc_id)
	{
		$data['publish_state'] = "active";
		$this->db->where('fcc_id',$fcc_id);
		$this->db->update('foloclor_content_comment',$data);
		if($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function inactive_comment($fcc_id)
	{
		$data['publish_state'] = "inactive";
		$this->db->where('fcc_id',$fcc_id);
		$this->db->update('foloclor_content_comment',$data);
		if($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function is_there_pending_comments()
	{
		$res = $this->db->get_where('foloclor_content_comment',array('publish_state'=>'pending'));
		if($res->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function is_there_inactive_comments()
	{
		$res = $this->db->get_where('foloclor_content_comment',array('publish_state'=>'inactive'));
		if($res->num_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function get_product_categories()
	{
		$res = $this->db->get('product_category');
		if($res->num_rows() > 0){
			return $res->result();
		}else{
			return FALSE;
		}
	}
	public function get_foloclor_categories()
	{
		$res = $this->db->get('foloclor_category');
		if($res->num_rows() > 0){
			return $res->result();
		}else{
			return FALSE;
		}
	}
	public function get_product_by_category($pcat_id)
	{
		$this->db->where('pcat_id',$pcat_id);
		$this->db->order_by('date','desc');
		$q = $this->db->get_where('product_content',array('is_active !=' =>'0'));
		if($q->num_rows() > 0){
			return $q->result();
		}
		else {
			return FALSE;
		}
		
	}
	public function get_foloclor_by_category($fc_id)
	{
		$this->db->where('f_cat_id',$fc_id);
		$this->db->order_by('date','desc');
		$q = $this->db->get_where('foloclor_content',array('is_active !=' =>'0'));
		if($q->num_rows() > 0){
			return $q->result();
		}
		else {
			return FALSE;
		}
		
	}
	public function get_foloclor_current_news()
	{
		$foloclor = "foloclor";
		$result= mysql_query("SELECT latest_news.type,latest_news.ln_id,foloclor_content.f_title_ar,foloclor_content.f_title_en
							  FROM latest_news INNER JOIN foloclor_content 
							  ON latest_news.type = '".$foloclor."' AND latest_news.fc_id=foloclor_content.f_id");
		if(mysql_num_rows($result) > 0 ){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	public function get_product_current_news()
	{
		$product = "product";
		$result= mysql_query("SELECT latest_news.type,latest_news.ln_id,product_content.title_ar,product_content.title_en
							  FROM latest_news INNER JOIN product_content 
							  ON latest_news.type = '".$product."' AND latest_news.pc_id=product_content.pc_id");
		if(mysql_num_rows($result) > 0 ){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	public function add_foloclor_news($f_id)
	{
		$data['fc_id'] = $f_id;
		$data['type'] = 'foloclor';
		$this->db->insert('latest_news',$data);
		if($this->db->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}

	}
	public function insert_news($info)
	{
		$this->db->insert('news',$info);
		return $this->db->insert_id();
	}
	
	public function get_news()
	{
		$res = $this->db->get('news');
		if($res->num_rows() > 0){
			return $res->result();
		}else{
			return FALSE;
		}
	}
	public function get_current_news()
	{
		$result= mysql_query("SELECT news.n_id,news.title_en
							  FROM news INNER JOIN news_bar 
							  ON news_bar.news_id = news.n_id ");
		if(mysql_num_rows($result) > 0 ){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	public function get_news_by_id($id)
	{
		$q = $this->db->get_where('news',array('n_id' =>$id));
		if($q->num_rows() > 0){
			return $q->result();
		}
		else
			return FALSE;
	}
	public function check_news_exist_in_newsbar($id)
	{
		$this->db->where('news_id',$id);
		$q = $this->db->get('news_bar');
		if ($q->num_rows()>0) {
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function set_to_news_bar($n_id)
	{
		$q = $this->db->insert('news_bar',array('news_id'=> $n_id));
		if($this->db->affected_rows() > 0){
			return TRUE;
		}else{
			return FALSE;
		}
	}
	public function hide_from_news_bar($n_id)
	{
		$this->db->where('news_id',$n_id);
		$this->db->delete('news_bar');
	}
}
