<?php
class product_voting_model extends CI_Model
{
	// select all product's rates
	function get_products_rates($product_id)
	{
		$this->db->where('pc_id' , $product_id);
		$query = $this->db->get('product_voting');
		return $query->result();
	}
	
	function get_product_user_rates($product_id,$user_id)
	{
		$array = array('user_id' => $user_id, 'pc_id' => $product_id);	
		$this->db->where($array);
		$query = $this->db->get('product_voting');
		return $query->result();
	}
	
	// insert new rate
	function add_rate_for_product($data)
	{
		$this->db->insert('product_voting',$data);
	}
	
	// update product content table
	function update_product_rate($rate, $product_id)
	{
		$data = array('pc_rate'=> $rate);
		$this->db->where('pc_id',$product_id);
		$this->db->update('product_content',$data);
	}
	
	// if user has rated product in past, just update record with new rating
	function update_user_rate($rate,$user,$product_id)
	{
		$array = array('user_id' => $user, 'pc_id' => $product_id);
		$data =  array('pv_voting' => $rate ); 
		$this->db->where($array);
		$this->db->update('product_voting',$data);
	}
	
	//read product's rate from product content table
	function get_product_rate($product_id)
	{
		$this->db->where('pc_id',$product_id);
		$query = $this->db->get('product_content');
		return $query->result();
	}
}
?>