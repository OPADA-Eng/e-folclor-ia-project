<?php
 /**
 * 
 */
class Product_manage_model extends CI_Model {
	
	public function get_product_categories()
	{
		$q = $this->db->get('product_category');
		return $q->result();
	}
	
	public function get_latest_news()
	{
		$this->db->join('news','news.n_id = news_bar.news_id');
		$q = $this->db->get('news_bar');
		return $q->result();
	}
	
	
	public function count_products_by_cat($cat_id='')
	{
		$this->db->where('pcat_id',$cat_id);
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$q = $this->db->get('product_content');
		return $q->num_rows();
	}
	
	public function get_toplikes_products($count)
	{
		$this->db->select('*');
		$this->db->from('product_content');
		$this->db->join('product_category','product_content.pcat_id = product_category.pcat_id');
		$this->db->join('product_type','product_content.pt_id = product_type.pt_id');
		$this->db->join('users','product_content.user_id = users.id');
		$this->db->order_by('product_content.pc_rate', 'DESC');
		$this->db->limit($count);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['pc_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_keywords'] = $this->Product_model->get_product_content_keywords($item['pc_id']);
			$i++;
		}
		
		return $final_res;
	}
	
	public function get_toplikes_mob_products($count)
	{
		$this->db->select('*');
		$this->db->from('product_content');
		$this->db->order_by('product_content.pc_rate', 'DESC');
		$this->db->limit($count);
		$query = $this->db->get();
		return $query->result();
	}
		
	public function get_topcomments_products($count)
	{
		$this->db->select('*');
		$this->db->from('product_content');
		$this->db->join('product_category','product_content.pcat_id = product_category.pcat_id');
		$this->db->join('product_type','product_content.pt_id = product_type.pt_id');
		$this->db->join('users','product_content.user_id = users.id');
		$this->db->order_by('product_content.pc_comments_count', 'DESC');
		$this->db->limit($count);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['pc_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_keywords'] = $this->Product_model->get_product_content_keywords($item['pc_id']);
			$i++;
		}
		return $final_res;
	}
	
	public function get_recently_add_products($count)
	{
		$this->db->select('*');
		$this->db->from('product_content');
		$this->db->join('product_category','product_content.pcat_id = product_category.pcat_id');
		$this->db->join('product_type','product_content.pt_id = product_type.pt_id');
		$this->db->join('users','product_content.user_id = users.id');
		$this->db->order_by('product_content.date', 'DESC');
		$this->db->limit($count);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['pc_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_keywords'] = $this->Product_model->get_product_content_keywords($item['pc_id']);
			$i++;
		}
		return $final_res;
	}
	
	public function get_products_on_cats()
	{
		$this->db->select('*');
		$this->db->from('product_category');
		$query = $this->db->get();
		$res = $query->result_array();
		$final_res = array();
		$i=0;
		foreach ($res as $item){
			$final_res[$i]['category'] = $item;
			$final_res[$i]['products'] = $this->get_category_products($item['pcat_id'],1,8); //: 8 is number of products showed in a category
			$i++;
		}
		return $final_res;
	}
	public function get_mob_category_products($cat_id)
	{
		$this->db->select('pc_id,max_price,product_name_en,image');
		$this->db->from('product_content');
		$this->db->where('product_content.pcat_id',$cat_id);
		$this->db->where('product_content.publish_state',4);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_category_products($cat_id,$start=1,$per_page)
	{
		//$this->db->select('pc_id,pc_rate,product_name_ar,product_name_en,image');
		$this->db->where('product_content.pcat_id',$cat_id);
		$this->db->join('product_category','product_content.pcat_id = product_category.pcat_id');
		$this->db->join('product_type','product_content.pt_id = product_type.pt_id');
		$this->db->join('users','product_content.user_id = users.id');
		$query = $this->db->get('product_content',$per_page,$start);
		return $query->result();
	}
	
	public function get_product_related_foloclors($pc_id)
	{
		$result = $this->product_manage_model->get_related_foloclors_ids($pc_id);
		
		$final_res= array();
		foreach ($result as $row) {
			$final_res[] = $this->get_foloclor_details($row->f_id);
			
		}
		// $this->db->select('*');
		// $this->db->from('product_content');
		// $this->db->join('product_category','product_content.pcat_id = product_category.pcat_id');
		// $this->db->join('product_type','product_content.pt_id = product_type.pt_id');
		// $this->db->join('users','product_content.user_id = users.id');
		// $this->db->where('pc_id',$pc_id);
		// $query = $this->db->get();
		// $res = $query->first_row();	
		// $final_res = array();
		// $final_res['item_data'] = $res;
		// $final_res['item_keywords'] = $this->Product_model->get_product_content_keywords($res->pc_id);
		return $final_res;
	}

	public function get_related_foloclors_ids($pc_id)
	{
		$this->db->select('foloclor_content.f_id,product_content.pc_id');
		$this->db->join('product_keyword','product_content.pc_id = product_keyword.pc_id');
		$this->db->join('foloclor_content_keyword','foloclor_content_keyword.fk_id = product_keyword.fk_id');
		$this->db->join('foloclor_content','foloclor_content_keyword.f_id = foloclor_content.f_id');
		$this->db->where('product_content.pc_id',$pc_id);
		$this->db->distinct();
		$query = $this->db->get('product_content');
		return $query->result();
		// var_dump($query->result());
		// die();
	}
	public function get_product_details($p_id)
	{
		$this->db->from('product_content');
		$this->db->join('product_category','product_content.pcat_id = product_category.pcat_id');
		$this->db->join('users','product_content.user_id = users.id');
		$this->db->where('pc_id',$p_id);
		$query = $this->db->get();
		$res = $query->first_row();	
		$final_res['item_data'] = $res;
		$final_res['item_keywords'] = $this->get_foloclor_content_keywords($res->pc_id);
		return $final_res;
	}

	public function get_mob_product_details($p_id)
	{
		$this->db->from('product_content');
		$this->db->where('pc_id',$p_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	private function get_foloclor_details($f_id)
	{
		$this->db->from('foloclor_content');
		$this->db->where('f_id',$f_id);
		$query = $this->db->get();
		$res = $query->first_row();
		return $res;
	}
	public function get_foloclor_content_tags($fc_id)
	{
		$this->db->select('*');
		$this->db->from('foloclor_tag');
		$this->db->join('foloclor_content_tag','foloclor_tag.ft_id = foloclor_content_tag.ft_id');
		$this->db->where('foloclor_content_tag.f_id',$fc_id);
		$query = $this->db->get();
		return $query->result();
	}
	public function get_foloclor_content_keywords($fc_id)
	{
		$this->db->select('*');
		$this->db->from('foloclor_keywords');
		$this->db->join('foloclor_content_keyword','foloclor_keywords.fk_id = foloclor_content_keyword.fk_id');
		$this->db->where('foloclor_content_keyword.f_id',$fc_id);
		$query = $this->db->get();
		return $query->result();
	}
			
}
?>