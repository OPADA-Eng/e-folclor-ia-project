<?php

/**
 * 
 */
class Messages_model extends CI_Model {
	
	public function get_messages($p,$u_id,$messages_type,$per_page = 6)
	{
		
		$this->db->order_by('msg_date','desc');
		
		
		if($p ==''){
			if($messages_type == 'in'){
				$this->db->select('msg_id,msg_text,msg_date,msg_reciver_status,first_name,last_name');
				$this->db->join('users','users.id = message.msg_from');
				$this->db->join('message_touser','message_touser.mg_message_id = message.msg_id');
				$this->db->where('msg_reciver_status !=','Deleted');
				$q = $this->db->get_where('message',array('mg_to_user_id'=>$u_id));
			}
			else{
				$this->db->select('msg_id,msg_text,msg_date,msg_sender_status,first_name,last_name');
				$this->db->join('message_touser','message_touser.mg_message_id = message.msg_id');
				$this->db->join('users','users.id = message_touser.mg_to_user_id');
				$this->db->where('msg_sender_status !=','Deleted');
				$q = $this->db->get_where('message',array('msg_from'=>$u_id));
			}
		}
		else {
			if($messages_type == 'in'){
				$this->db->select('msg_id,msg_text,msg_date,msg_reciver_status,first_name,last_name');
				$this->db->join('users','users.id = message.msg_from');
				$this->db->join('message_touser','message_touser.mg_message_id = message.msg_id');
				$this->db->where('msg_reciver_status !=','Deleted');
				$q = $this->db->get_where('message',array('msg_to'=>$u_id,),$per_page,$per_page*($p-1));
			}
			else{
				$this->db->select('msg_id,msg_text,msg_date,msg_sender_status,first_name,last_name');
				$this->db->join('message_touser','message_touser.mg_message_id = message.msg_id');
				$this->db->join('users','users.id = message_touser.mg_to_user_id');
				$this->db->where('msg_sender_status !=','Deleted');
				$q = $this->db->get_where('message',array('msg_from'=>$u_id),$per_page,$per_page*($p-1));
			}
		}
		if($q->num_rows() >0){
			return $q->result();
		}
		else {
			return FALSE;
		}
	}
	public function get_all_users()
	{
		$this->db->select('id,first_name,last_name');	
		$q = $this->db->get('users');
		return $q->result();
	}
	public function add_message($data='',$users)
	{
		
		$this->db->insert('message',$data);
		$insert_id = $this->db->insert_id();
		foreach ($users as $key => $user) {
			$this->db->insert('message_touser',array('mg_message_id'=>$insert_id, 'mg_to_user_id'=>$user));
		}
		if($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	
	public function delete_recived_message($id='')
	{
		$this->db->where('mg_message_id',$id);
		$this->db->update('message_touser',array('msg_reciver_status'=>'Deleted'));
		if($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	
	public function has_message($u_id,$msg_id,$box='in')
	{
		if($box == 'in'){
			$this->db->where('mg_to_user_id',$u_id);
			$this->db->where('mg_message_id',$msg_id);
			$q = $this->db->get('message_touser');
			if($q->num_rows() >0){
				return TRUE;
			}
			else {
				return FALSE;
			}
		}
		
	}
	
	public function delete_sent_message($id='')
	{
		$this->db->where('msg_id',$id);
		$this->db->update('message',array('msg_sender_status'=>'Deleted'));
		if($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
}
