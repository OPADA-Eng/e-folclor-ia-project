<?php
 /**
 * 
 */
class Foloclor_manage_model extends CI_Model {
	
	public function add_foloclor_comment($data='')
	{
		
		$this->db->insert('foloclor_content_comment',$data);
		if($this->db->affected_rows() > 0){
			return TRUE;
		}
		else {
			return FALSE;
		}
	}
	public function get_foloclor_comments($id='')
	{
		$this->db->where('foloclor_id',$id);
		//$this->db->where('publish_state',4);
		$this->db->join('users','users.id=foloclor_content_comment.user_id');
		$q = $this->db->get('foloclor_content_comment');
		return $q->result();
	}
	public function get_foloclor_categories($id='')
	{
		if ($id != ''){
			$this->db->where('fc_id',$id);
		}
		$query = $this->db->get('foloclor_category');
		return $query->result();
	}
		
	public function get_foloclor_gategories()
	{
		$query = $this->db->get('foloclor_category');
		return $query->result();
	}
	
	
	public function get_topcomments_foloclors($count)
	{
		$this->db->select('foloclor_content.f_id,
						   f_title_ar,f_title_en,f_content_ar,f_content_en,
						   f_historical_importance_ar,f_historical_importance_en,
					       f_location_ar,f_location_en,
						   f_image,f_video,f_sound,total_voting,
						   foloclor_category.fc_id,foloclor_category.fc_name_ar,foloclor_category.fc_name_en,
						   users.username,users.first_name,users.last_name
						  ');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$this->db->order_by('foloclor_content.fc_comments_count', 'DESC');
		$this->db->limit($count);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['f_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_tags'] = $this->get_foloclor_content_tags($item['f_id']);
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
		return $final_res;
	}
	
	public function count_foloclors_by_cat($cat_id='')
	{
		$this->db->where('f_cat_id',$cat_id);
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$q = $this->db->get('foloclor_content');
		return $q->num_rows();
	}
	
	public function get_folclors_by_category($cat_id='',$start=1,$per_page)
	{
		$this->db->where('f_cat_id',$cat_id);
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$this->db->order_by('date',"desc");
		$this->db->join('foloclor_category','foloclor_category.fc_id = foloclor_content.f_cat_id');
		$this->db->join('users','users.id = foloclor_content.user_id');
		$q = $this->db->get('foloclor_content',$per_page,$start);
		return $q->result();
	}
	public function get_mob_category_folclors($cat_id)
	{
		$this->db->where('f_cat_id',$cat_id);
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$this->db->order_by('date',"desc");
		$q = $this->db->get('foloclor_content');
		return $q->result();
	}
	public function get_folclors()
	{
		$q = $this->db->get('foloclor_category');
		$cats = $q->result();
		$result = array();
		$i = 0;
		foreach ($cats as $key => $cat) {
			$f = $this->get_folclors_by_category($cat->fc_id,1,8);
			$result[$i]['category'] = $cat;
			$result[$i]['folclors'] = $f;
			$i++;
		}
		return $result;
	}
	
	public function get_tags()
	{
		$q = $this->db->get('foloclor_tag');
		foreach ($q->result() as $key => $value) {
			$data[] = $value; 
		}
		return $data;
	}
	
	public function get_keywords()
	{
		$q = $this->db->get('foloclor_keywords');
		foreach ($q->result() as $key => $value) {
			$data[] = $value; 
		}
		return $data;
	}
	public function add_tags_to_content($tag_id,$f_id)
	{
		$data = array('f_id' => $f_id ,'ft_id'=>$tag_id);
		$this->db->insert('foloclor_content_tag',$data);
	}

	public function add_keywords_to_content($keyword_id,$f_id)
	{
		$data = array('f_id' => $f_id ,'fk_id'=>$keyword_id);
		$this->db->insert('foloclor_content_keyword',$data);
	}

	public function add_mm_to_content($data,$f_id)
	{
		$this->db->where('f_id',$f_id);
		$this->db->update('foloclor_content',$data);
	}
	public function create_new_foloclor_content($data)
	{
		$this->db->insert('foloclor_content',$data);
		$insert_id = $this->db->insert_id();
   		$this->db->trans_complete();
   		return  $insert_id;
	}	
	public function get_approved_foloclor_content()
	{
		$this->db->select('foloclor_content.f_id,
						   f_title_ar,f_title_en,f_content_ar,f_content_en,
						   f_historical_importance_ar,f_historical_importance_en,
					       f_location_ar,f_location_en,
						   f_image,f_video,f_sound,total_voting,
						   foloclor_category.fc_id,foloclor_category.fc_name_ar,foloclor_category.fc_name_en,
						   users.username
						  ');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['f_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_tags'] = $this->get_foloclor_content_tags($item['f_id']);
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
        
		return $final_res;
	}
	
	public function get_limit_approved_foloclor_content()
	{
		
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$this->db->limit(3);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['f_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_tags'] = $this->get_foloclor_content_tags($item['f_id']);
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
         // var_dump($final_res);
		 // die();
		return $final_res;
	}

public function get_limit_Intellectual_heritage_approved_foloclor_content()
	{
		$this->db->select('foloclor_content.f_id,
						   f_title_ar,f_title_en,f_content_ar,f_content_en,
						   f_historical_importance_ar,f_historical_importance_en,
					       f_location_ar,f_location_en,date,
						   f_image,f_video,f_sound,total_voting,
						   foloclor_category.fc_id,foloclor_category.fc_name_ar,foloclor_category.fc_name_en,
						   users.username
						  ');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$this->db->where('f_cat_id',7);
		$this->db->limit(2);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['f_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_tags'] = $this->get_foloclor_content_tags($item['f_id']);
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
         // var_dump($final_res);
		 // die();
		return $final_res;
	}

   public function get_last_folclor()
	{
		$query = $this->db->query('SELECT f_title_en, f_historical_importance_en ,date,f_image
                                     FROM foloclor_content
                                     WHERE f_id = (
                                     SELECT max( f_id )
                                     FROM foloclor_content ) ');
            return $query->result();
	}
	
	public function get_foloclor_conten_by_id($f_id)
	{
		$this->db->select('foloclor_content.f_id,f_title_ar,f_title_en,f_content_ar,f_content_en,
						   f_historical_importance_ar,f_historical_importance_en,
					       f_location_ar,f_location_en,
						   f_image,f_video,f_sound,total_voting,
						   foloclor_category.fc_id,foloclor_category.fc_name_ar,foloclor_category.fc_name_en,
						   users.first_name,users.last_name,fc_comments_count,date
						   ');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('f_id',$f_id);
		$query = $this->db->get();
		$res = $query->first_row();	
		$final_res = array();
		$final_res['ID'] = $res->f_id;
		$final_res['item_data'] =  $res;
		$final_res['item_tags'] = $this->get_foloclor_content_tags( $res->f_id);
		$final_res['item_keywords'] = $this->get_foloclor_content_keywords( $res->f_id);
		return $final_res;
	}
public function get_mob_folclor_details($f_id)
{
	$this->db->select('*');
	$this->db->from('foloclor_content');
	$this->db->where('f_id',$f_id);
	$query = $this->db->get();
	$res = $query->result();	
	$comments = $this->get_foloclor_comments($f_id);
	
	$final_res = array();
	$final_res['success'] = "1";
	$final_res['folclor'] =  $res;
	$final_res['folclors_comments'] = $comments;
	return $final_res;
}
	public function get_foloclor_content_tags($fc_id)
	{
		$this->db->select('*');
		$this->db->from('foloclor_tag');
		$this->db->join('foloclor_content_tag','foloclor_tag.ft_id = foloclor_content_tag.ft_id');
		$this->db->where('foloclor_content_tag.f_id',$fc_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_foloclors_tags()
	{
		
		$query = $this->db->query('SELECT * FROM (
                                        SELECT * FROM foloclor_tag ORDER BY ft_id DESC LIMIT 5
                                                 ) sub
                                           ORDER BY ft_id ASC');
            return $query->result();
	}
	

	
	public function get_foloclor_content_keywords($fc_id)
	{
		$this->db->select('*');
		$this->db->from('foloclor_keywords');
		$this->db->join('foloclor_content_keyword','foloclor_keywords.fk_id = foloclor_content_keyword.fk_id');
		$this->db->where('foloclor_content_keyword.f_id',$fc_id);
		$query = $this->db->get();
		return $query->result();
	}
	
	public function get_popular_foloclors($count)
	{
		$this->db->select('foloclor_content.f_id,
						   f_title_ar,f_title_en,f_content_ar,f_content_en,
						   f_historical_importance_ar,f_historical_importance_en,
					       f_location_ar,f_location_en,
						   f_image,f_video,f_sound,total_voting,
						   foloclor_category.fc_id,foloclor_category.fc_name_ar,foloclor_category.fc_name_en,
						   users.username,users.first_name,users.last_name
						  ');
						  
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$this->db->order_by('foloclor_content.total_voting', 'DESC');
		$this->db->limit($count);
		$query = $this->db->get();
		$res = $query->result_array();
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['f_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_tags'] = $this->get_foloclor_content_tags($item['f_id']);
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
		return $final_res;
	}
	
	
	public function get_recently_add_foloclors($count)
	{
		$this->db->select('foloclor_content.f_id,
						   f_title_ar,f_title_en,f_content_ar,f_content_en,
						   f_historical_importance_ar,f_historical_importance_en,
					       f_location_ar,f_location_en,
						   f_image,f_video,f_sound,total_voting,
						   foloclor_category.fc_id,foloclor_category.fc_name_ar,foloclor_category.fc_name_en,
						   users.username,users.first_name,users.last_name
						  ');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('users','foloclor_content.user_id = users.id');
		$this->db->where('publish_state',4);
		$this->db->where('is_active',1);
		$this->db->order_by('foloclor_content.date', 'DESC');
		$this->db->limit($count);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['f_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_tags'] = $this->get_foloclor_content_tags($item['f_id']);
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
		return $final_res;
	}
	public function get_foloclor_content_with_tag($tag_id)
	{
		$this->db->select('foloclor_content.f_id,
						   f_title_ar,f_title_en,f_content_ar,f_content_en,
						   f_historical_importance_ar,f_historical_importance_en,
					       f_location_ar,f_location_en,
						   f_image,f_video,f_sound,total_voting,
						   foloclor_category.fc_id,foloclor_category.fc_name_ar,foloclor_category.fc_name_en,
						  ');
		$this->db->from('foloclor_content');
		$this->db->join('foloclor_category','foloclor_content.f_cat_id = foloclor_category.fc_id');
		$this->db->join('foloclor_content_tag','foloclor_content.f_id = foloclor_content_tag.f_id');
		$this->db->join('foloclor_tag','foloclor_tag.ft_id = foloclor_content_tag.ft_id');
		$this->db->where('foloclor_tag.ft_id',$tag_id);
		$this->db->where('foloclor_content.publish_state',4);
		$this->db->where('foloclor_content.is_active',1);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_keywords'] = $this->get_foloclor_content_keywords($item['f_id']);
			$i++;
		}
		return $final_res;
	}
	
	public function get_toplikes_mob_folclors($count)
	{
		$this->db->select('*');
		$this->db->from('foloclor_content');
		$this->db->order_by('foloclor_content.total_voting', 'DESC');
		$this->db->limit($count);
		$query = $this->db->get();
		return $query->result();
	}
}
?>