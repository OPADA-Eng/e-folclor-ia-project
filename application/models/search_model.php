<?php

class Search_model extends  CI_Model
{
	public function get_search_foloclor_by_title_en($search,$per_page,$start)
	{
		$this->db->where('is_active','1');
		$this->db->where("`f_title_en` LIKE  '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by('date', 'desc');
		$q = $this->db->get('foloclor_content',$per_page,$start);
		$data = $q->result();
		if($q->num_rows() > 0)
			return $data;
		else
			return FALSE;
	}
	public function get_search_product_by_title_en($search,$per_page,$start)
	{
		$this->db->where('is_active','1');
		$this->db->where("`product_name_en` LIKE  '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by('date', 'desc');
		$q = $this->db->get('product_content',$per_page,$start);
		$data = $q->result();
		if($q->num_rows() > 0)
			return $data;
		else
			return FALSE;
	}
	public function get_search_product_by_title_ar($search,$per_page,$start)
	{
		$this->db->where('is_active','1');
		$this->db->where("`product_name_ar` LIKE  '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by('date', 'desc');
		$q = $this->db->get('product_content',$per_page,$start);
		$data = $q->result();
		if($q->num_rows() > 0)
			return $data;
		else
			return FALSE;
	}
	public function get_tag_foloclor($f_id)
	{
		$result = mysql_query("SELECT foloclor_tag.ft_id,foloclor_tag.f_tag_en,foloclor_tag.f_tag_ar
							   FROM foloclor_content_tag INNER JOIN foloclor_tag WHERE foloclor_content_tag.f_id = '".$f_id."' AND foloclor_content_tag.ft_id = foloclor_tag.ft_id");
		if(mysql_num_rows($result) > 0){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	public function get_comment_foloclor($f_id)
	{
		$active = "active";
		$result = mysql_query("SELECT foloclor_content_comment.fcc_id,foloclor_content_comment.comment,foloclor_content_comment.date,users.username
							   FROM foloclor_content_comment INNER JOIN users WHERE foloclor_content_comment.foloclor_id = '".$f_id."' AND foloclor_content_comment.publish_state = '".$active."'
							   AND foloclor_content_comment.user_id = users.id");
		if(mysql_num_rows($result) > 0){
			while ($row = mysql_fetch_assoc($result)) {
				$data[]= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
		
		$this->db->where('foloclor_id',$f_id);
		$this->db->where('publish_state','active');
		$q = $this->db->get('foloclor_content_comment');
		$data = $q->result();
		if($q->num_rows() > 0)
			return $data;
		else
			return FALSE;
	}
	public function get_user_publisher_foloclor($f_id)
	{
		$result = mysql_query("SELECT users.id,users.username
							   FROM users INNER JOIN foloclor_content WHERE foloclor_content.f_id = '".$f_id."' AND foloclor_content.user_id = users.id");
		if(mysql_num_rows($result) > 0){
			while ($row = mysql_fetch_assoc($result)) {
				$data= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	public function get_user_publisher_product($pc_id)
	{
		$result = mysql_query("SELECT users.id,users.username
							   FROM users INNER JOIN product_content WHERE product_content.pc_id = '".$pc_id."' AND product_content.user_id = users.id");
		if(mysql_num_rows($result) > 0){
			while ($row = mysql_fetch_assoc($result)) {
				$data= $row;
			}
			return $data;
		}else{
			return FALSE;
		} 
	}
	public function get_search_foloclor_by_content_en($search,$per_page,$start)
	{
		$this->db->where('is_active','1');
		$this->db->where("`f_content_en` LIKE  '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by('date', 'desc');
		$q = $this->db->get('foloclor_content',$per_page,$start);
		$data = $q->result();
		if($q->num_rows() > 0)
			return $data;
		else
			return FALSE;
	}
	public function get_search_foloclor_by_content_ar($search,$per_page,$start)
	{
		$this->db->where('is_active','1');
		$this->db->where("`f_content_ar` LIKE  '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by('date', 'desc');
		$q = $this->db->get('foloclor_content',$per_page,$start);
		$data = $q->result();
		if($q->num_rows() > 0)
			return $data;
		else
			return FALSE;
	}
	public function get_search_product_by_content_en($search,$per_page,$start)
	{
		$this->db->where('is_active','1');
		$this->db->where("`details_en` LIKE  '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by('date', 'desc');
		$q = $this->db->get('product_content',$per_page,$start);
		$data = $q->result();
		if($q->num_rows() > 0)
			return $data;
		else
			return FALSE;
	}
	public function get_search_product_by_content_ar($search,$per_page,$start)
	{
		$this->db->where('is_active','1');
		$this->db->where("`details_ar` LIKE  '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by('date', 'desc');
		$q = $this->db->get('product_content',$per_page,$start);
		$data = $q->result();
		if($q->num_rows() > 0)
			return $data;
		else
			return FALSE;
	}
	public function get_search_foloclor_by_title_ar($search,$per_page,$start)
	{
		$this->db->where('is_active','1');
		$this->db->where("`f_title_ar` LIKE  '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by('date', 'desc');
		$q = $this->db->get('foloclor_content',$per_page,$start);
		$data = $q->result();
		if($q->num_rows() > 0)
			return $data;
		else
			return FALSE;
	}
	public function get_search_foloclor_count($search)
	{
		$this->db->where('is_active','1');
		$this->db->where("`Title` LIKE  '%".$this->db->escape_like_str($search)."%'");
		$this->db->order_by('Title', 'RANDOM');
		$q = $this->db->get('news');
		$data = $q->result();
		
		if($q->num_rows() > 0)
			return $q->num_rows() ;
		else
			return FALSE;
	}
}
?>