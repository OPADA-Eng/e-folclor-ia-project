<?php

class Bill_model extends  CI_Model
{
	private $tabel_name='bill';
	private $tabel_id='b_id';
	
	public function __construct ()
	{
		
	}
	
	public function get_all()
	{
		$q=$this->db->get($this->tabel_name);
		$results=$q->result();
		if ($results)
			return $results;
		else 
			return FALSE;
	}
	
	public function get_by_id($id)
	{
		$this->db->where($this->tabel_id , $id);
		$q=$this->db->get($this->tabel_name);
		$result=$q->row();
		if ($result)
			return $result;
		else 
			return FALSE;
	}
	public  function insert($date)
	{
		$this->db->insert($this->tabel_name,$date);
		if ($this->db->affected_rows() > 0)
			return $this->db->insert_id();
		else 
			return FALSE;
	}
	
	public function edit ($id,$data)
	{
		$this->db->where($this->tabel_id,$id);
		$this->db->update($this->tabel_name,$data);
		if ($this->db->affected_rows() >0 )
			return TRUE;
		else 
			return FALSE;
	}
	
	public function delete ($id)
	{
		$this->db->where($this->tabel_id,$id);
		if ($this->db->delete($this->tabel_name) )
			return TRUE;
		else 
			return FALSE;
	}
	
	
	//bill_part
	public  function insert_bill_part($date)
	{
		$this->db->insert('bill_product',$date);
		if ($this->db->affected_rows() > 0)
			return $this->db->insert_id();
		else 
			return FALSE;
	}
	/*
	public function get_bill_not_paid($user_id)
	{
		$this->db->where('b_is_closed',0 );
		$this->db->where('b_user_id',$user_id);
		$q=$this->db->get($this->tabel_name);
		if ($q)
			return $q->result();
		else 
			return FALSE;
	}
	*/
	public function closed_bill($id)
	{
		$this->db->where($this->tabel_id,$id);
		$this->db->update($this->tabel_name,array('b_is_closed' => 1));
		if ($this->db->affected_rows() >0 )
			return TRUE;
		else 
			return FALSE;	
	}
	
	public function get_by_product_id($id)
	{
		$this->db->select('bill_type.bt_name_en ,repository.r_name_en ,bill_product.count,bill_product.bp_price');
		$this->db->where('bill_product.pc_id',$id);
		$this->db->join('bill_type','bill_type.bt_id = bill_product.bill_type');
		$this->db->join('repository','repository.r_id = bill_product.r_id');
		$q=$this->db->get('bill_product');
		if ($q->num_rows() > 0)
			return $q->result();
		else
			return FALSE;
	}
	
	public function get_user_bill_not_closed($id)
	{
		$this->db->where('b_is_closed',0);
		$this->db->where('b_user_id',$id);
		$q=$this->db->get('bill');
		if ($q->num_rows() > 0 )
			return $q->result();
		else
			return FALSE;
	}
	
	public function get_user_bill($id)
	{
		$this->db->where('b_user_id',$id);
		$q=$this->db->get('bill');
		if ($q->num_rows() > 0 )
			return $q->result();
		else
			return FALSE;
	}
	

	public function get_user_bill_product($user_id,$bill_id)
	{	
		$this->db->where('b_user_id',$user_id);
		$this->db->where('bill_product.b_id',$bill_id);
		$this->db->join('bill_product','bill_product.b_id = bill.b_id');
		$q=$this->db->get('bill');
		if ($q->num_rows() > 0 )
			return $q->result();
		else
			return FALSE;
	}

	public function get_users()
	{
		$this->db->select('b_user_id');
		$this->db->distinct('b_user_id');
		$q=$this->db->get('bill');
		if ($q->num_rows() > 0)
			return $q->result();
		else
			return FALSE;
	}
	
	public function get_bill_types()
	{
		$q=$this->db->get('bill_type');
		if ($q->num_rows() > 0)
			return $q->result();
		else
			return;
	}
	public function get_products()
	{
		$q=$this->db->get('product_content');
		if ($q->num_rows() > 0)
			return $q->result();
		else
			return;	
	}
	
	public function get_product($id)
	{
		$this->db->where('pc_id',$id);
		$q=$this->db->get('product_content');
		if ($q->num_rows() > 0)
			return $q->row();
		else
			return FALSE;	
	}
	
	public function get_not_approved_bill()
	{
		$this->db->select('bill.b_id,users.username,bill_type.bt_name_en,bill.b_total,bill.bill_kind');
		$this->db->where('is_aproved',0);
		$this->db->join('users','users.id = bill.b_user_id');
		$this->db->join('bill_type','bill_type.bt_id = bill.bt_id');
		$q=$this->db->get('bill');
		if ( $q->num_rows() > 0 )
			return $q->result();
		else 
			return FALSE;
	}
	public function close_bill_part($id,$data)
	{
		$this->db->where('bp_id',$id);
		$this->db->update('bill_product',$data);
		
		if($this->db->affected_rows() > 0 )
			return TRUE;
		else 
			return FALSE;	
	}
	
	public function approve_bill($id)
	{
		$this->db->where('b_id',$id);
		$this->db->update('bill',array('is_aproved' => 1));
		
		if($this->db->affected_rows() > 0 )
			return TRUE;
		else 
			return FALSE;	
	}
	public function close_bill($id)
	{
		$this->db->where('b_id',$id);
		$this->db->update('bill',array('b_is_closed' => 1));
		
		if($this->db->affected_rows() > 0 )
			return TRUE;
		else 
			return FALSE;	
	}

	public function get_approved_not_closed_bill($user_id)
	{
		$this->db->where(array('b_user_id' => $user_id,
								'is_aproved' =>1,
								'b_is_closed' =>0));
		$this->db->join('bill_type','bill_type.bt_id = bill.bt_id');
		$q=$this->db->get('bill');
		if ( $q->num_rows() > 0 )
			return $q->result();
		else 
			return FALSE;
	}
}

?>