<?php

class Bonds_model extends  CI_Model
{
	private $tabel_name='bonds';
	private $tabel_id='bds_id';
	
	public function __construct ()
	{
		
	}
	
	public function get_all()
	{
		$query=$this->db->get($this->tabel_name);
		$results=$query->result();
		if ($results)
			return $results;
		else 
			return FALSE;
	}
	
	public function get_by_id($id)
	{
		$this->db->where($this->tabel_id , $id);
		$query=$this->db->get($this->tabel_name);
		$result=$query->result();
		if ($result)
			return $result;
		else 
			return FALSE;
	}
	
	public function get_by_product_id($id)
	{
		$this->db->select('bonds.bond_type,bonds.bds_debtor,bonds.bds_creditor,bonds.bds_date');
		$this->db->where('product_id' , $id);
		$query=$this->db->get($this->tabel_name);
		$result=$query->result();
		if ($result)
			return $result;
		else 
			return FALSE;
	}
	
	public  function insert($date)
	{
		$this->db->insert($this->tabel_name,$date);
		if ($this->db->affected_rows() > 0)
			return $this->db->insert_id();
		else 
			return FALSE;
	}
	
	
}

?>