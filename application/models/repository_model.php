<?php

class Repository_model extends  CI_Model
{
	private $tabel_name='repository';
	private $tabel_id='r_id';
	
	public function __construct ()
	{
		
	}
	
	public function get_all()
	{
		$query=$this->db->get($this->tabel_name);
		$results=$query->result();
		if ($results)
			return $results;
		else 
			return FALSE;
	}
	
	public function get_by_id($id)
	{
		$this->db->where($this->tabel_id,$id);
		$query=$this->db->get($this->tabel_name);
		$results=$query->row();
		if ($results)
			return $results;
		else 
			return FALSE;
	}
	
	public function get_products_by_id($id='')
	{
		$this->db->select('product_content.product_name_en ,product_repository.pr_count');
		$this->db->where('product_repository.r_id',$id);
		$this->db->join('product_content','product_content.pc_id = product_repository.pc_id');
		$query=$this->db->get('product_repository');
		if($query)
			return $query->result();
		else
			return FALSE;
	}
	
	public function get_by_product_id($product_id,$qty)
	{
		$this->db->select('repository.r_id ,product_repository.pr_id');
		$this->db->where('product_repository.pc_id',$product_id);
		$this->db->where('product_repository.pr_count >= ',$qty);
		$this->db->join('repository','repository.r_id = product_repository.r_id');
		$query=$this->db->get('product_repository');
		if($query)
			return $query->row();
		else
			return FALSE;
	}
	
	public function minus_count_repository($id,$qty)
	{
		$this->db->where($this->tabel_id,$id);
		$query=$this->db->get($this->tabel_name);
		$results=$query->row();
		$this->db->where($this->tabel_id,$id);
		$this->db->update($this->tabel_name,array('r_capacity' => $results->r_capacity - $qty));
		if ($this->db->affected_rows() >0 )
			return TRUE;
		else 
			return FALSE;
	}

	public function minus_count_product_in_rep($id,$qty)
	{
		$this->db->where('pr_id',$id);
		$query=$this->db->get('product_repository');
		$results=$query->row();
		$this->db->where('pr_id',$id);
		$this->db->update('product_repository',array('pr_count' => $results->pr_count - $qty));
		if ($this->db->affected_rows() >0 )
			return TRUE;
		else 
			return FALSE;
	}
	public  function insert($date)
	{
		$this->db->insert($this->tabel_name,$date);
		if ($this->db->affected_rows() > 0)
			return $this->db->insert_id();
		else 
			return FALSE;
	}
	
	public function edit ($id,$data)
	{
		$this->db->where($this->tabel_id,$id);
		$this->db->update($this->tabel_name,$data);
		if ($this->db->affected_rows() >0 )
			return TRUE;
		else 
			return FALSE;
	}
	
	public function delete ($id)
	{
		$this->db->where($this->tabel_id,$id);
		if ($this->db->delete($this->tabel_name) )
			return TRUE;
		else 
			return FALSE;
	}
	
	public function add_products_repository($repository_id,$product_id,$ammount)
	{
		$pr=$this->is_there_product_in_repository($repository_id,$product_id);
		if($pr)
		{
			$this->db->where('pr_id',$pr->pr_id);
			$this->db->update('product_repository',array('pr_count' => $pr->pr_count + $ammount));	
		}
		else{
			$data=array('pc_id' => $product_id , 'r_id' => $repository_id , 'pr_count' => $ammount);
			$this->db->insert('product_repository',$data);
		}
		if($this->update_repository($repository_id,$ammount))
			return TRUE;
		else
			return FALSE;
	}
	
	public function is_there_product_in_repository($repository_id,$product_id)
	{
		$this->db->where('pc_id',$product_id);
		$this->db->where('r_id',$repository_id);
		$q=$this->db->get('product_repository');
		if($q->num_rows() > 0)
			return $q->row();
		else
			return FALSE;
		
	}
	
	public function update_repository($repository_id ,$ammount)
	{
		$this->db->where('r_id',$repository_id);
		$q=$this->db->get('repository');
		$rep=$q->row();
		$this->db->where('r_id',$rep->r_id);
		$this->db->update('repository',array('r_capacity' => $rep->r_capacity + $ammount));
		if ($this->db->affected_rows() >0)
			return TRUE;
		else
			return FALSE;
	}
}

?>