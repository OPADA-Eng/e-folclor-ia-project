<?php
class foloclor_voting_model extends CI_Model
{
	function get_foloclor_rates($foloclor_id)
	{
		$this->db->where('f_id',$foloclor_id);
		$query = $this->db->get('foloclor_voting');
		return $query->result();
	}
	
	function get_foloclor_user_rates($foloclor_id,$user_id)
	{
		$array = array('user_id' => $user_id, 'f_id' => $foloclor_id);	
		$this->db->where($array);
		$query = $this->db->get('foloclor_voting');
		return $query->result();
	}
	
	function add_rate_for_foloclor($data)
	{
		$this->db->insert('foloclor_voting',$data);
	}
	
	// update foloclor content table
	function update_foloclor_rate($rate, $foloclor_id)
	{
		$data = array('total_voting'=> $rate);
		$this->db->where('f_id',$foloclor_id);
		$this->db->update('foloclor_content',$data);
	}
	
	// if user has rated foloclor in past, just update record with new rating
	function update_user_rate($rate,$user,$foloclor_id)
	{
		$array = array('user_id' => $user, 'f_id' => $foloclor_id);
		$data =  array('fv_voting' => $rate ); 
		$this->db->where($array);
		$this->db->update('foloclor_voting',$data);
	}
	
	//read foloclor's rate from product content table
	function get_foloclor_rate($foloclor_id)
	{
		$this->db->where('f_id',$foloclor_id);
		$query = $this->db->get('foloclor_content');
		return $query->result();
	}
}
?>