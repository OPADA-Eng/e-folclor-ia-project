<?php

class Product_model extends CI_Model
{
	
	function getmembers($var)
	{
		if($var ==1)
		{
			$query = $this->db->query('SELECT pcat_id,cat_ar,cat_en FROM product_category');
            return $query->result();
	 	
		}
		else {
			$query = $this->db->query('SELECT pt_id,pt_name_ar,pt_name_en FROM product_type');
            return $query->result();
		   	}
	}
	
	function add_product($content)
	{

		$this->db->insert('product_content',$content);
		return;
		
	}
	
	function show_categories()
	{
	$query = $this->db->get('product_category');
		return $query->result();
	}
	
	function show_types()
	{
		$query = $this->db->get('product_type');
		return $query->result();
	}
	
	public function create_new_product_content($data)
	{
		$this->db->insert('product_content',$data);
		$insert_id = $this->db->insert_id();
   		$this->db->trans_complete();
   		return  $insert_id;
	}	
	
	public function add_mm_to_content($data,$pc_id)
	{
		$this->db->where('pc_id',$pc_id);
		$this->db->update('product_content',$data);
	}
	
	public function add_new_category($data)
	{
		$this->db->insert('product_category',$data);
		$insert_id = $this->db->insert_id();
   		$this->db->trans_complete();
   		return  $insert_id;
	}
	
	public function add_new_keyword($data)
	{
		$this->db->insert('foloclor_keywords',$data);
		$insert_id = $this->db->insert_id();
   		$this->db->trans_complete();
   		return  $insert_id;
	}
	
	public function get_keywords()
	{
		$q = $this->db->get('foloclor_keywords');
		if($q->num_rows()> 0){
			foreach ($q->result() as $key => $value) {
				$data[] = $value; 
			}
			return $data;
		}else{
			return FALSE;
		}
		
	}
	
	public function show_products()
	{
		$query = $this->db->get('product_content');
        $query_result = $query->result();
        return $query_result;
	}

public function show_products_details($data)
{
	    $this->db->select('*');
        $this->db->from('product_content');
        $this->db->where('pc_id', $data);
        $query = $this->db->get();
        $result = $query->result();
        return $result; 
	
}

function update_product_id1($id,$data){
     $this->db->where('pc_id', $id);
     $this->db->update('product_content', $data);  
	 echo "donnn";
    }

 function delete_product_id($id,$data)
 {
 	  $this->db->where('pc_id', $id);
      $this->db->update('product_content', $data);
	  echo "don";  
 	
 }
 
 public function get_product_content_keywords($pc_id)
 {
        $this->db->select('*');
		$this->db->from('foloclor_keywords');
		$this->db->join('product_keyword','foloclor_keywords.fk_id = product_keyword.fk_id');
		$this->db->where('product_keyword.pc_id',$pc_id);
		$query = $this->db->get();
		return $query->result();
 }
 
 public function get_checked_products()
 {
        $this->db->select('*');
		$this->db->from('product_content');
		$this->db->join('product_category','product_content.pcat_id = product_category.pcat_id');
		$this->db->join('product_type','product_content.pt_id = product_type.pt_id');
		$this->db->join('users','product_content.user_id = users.id');
		$this->db->where('publish_state',6);
		$this->db->or_where('publish_state',3);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['pc_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_keywords'] = $this->get_product_content_keywords($item['pc_id']);
			$i++;
		}
		return $final_res;
 }
 
 public function get_pending_products()
 {
        $this->db->select('*');
		$this->db->from('product_content');
		$this->db->join('product_category','product_content.pcat_id = product_category.pcat_id');
		$this->db->join('product_type','product_content.pt_id = product_type.pt_id');
		$this->db->join('users','product_content.user_id = users.id');
		$this->db->where('publish_state',7);
		$query = $this->db->get();
		$res = $query->result_array();	
		$final_res = array();
		$i = 0;
		foreach ($res as $item){
			$final_res[$i]['ID'] = $item['pc_id'];
			$final_res[$i]['item_data'] = $item;
			$final_res[$i]['item_keywords'] = $this->get_product_content_keywords($item['pc_id']);
			$i++;
		}
		return $final_res;
 }
 
 	public function update_product_status_to_approve($product_id,$state,$user_id)
	{
		if($state == 4)
		{
			$data = array('publish_state' => "$state",
			     		  'admin_id'=> "$user_id",
						  );
		}
		else
		{
			$data = array('publish_state' => "$state",
				    	  'marketing_respons_id'=> "$user_id");
		}
		$this->db->where('pc_id',$product_id);
		$this->db->update('product_content',$data);
	}
	
	public function get_last_product()
	{
		$query = $this->db->query('SELECT product_name_en, details_en ,date,image
                                     FROM product_content
                                     WHERE pc_id = (
                                     SELECT max( pc_id )
                                     FROM product_content ) ');
            return $query->result();
	}

public function get_first_foloclors()
	{
		$query = $this->db->query('SELECT f_title_en, f_historical_importance_en ,date,f_image
                                     FROM foloclor_content
                                     WHERE f_id > ((
                                     SELECT max( f_id )
                                     FROM foloclor_content )-2) ANd  f_id < (
                                     SELECT max( f_id )
                                     FROM foloclor_content )');
            return $query->result();
	}
	public function get_last_foloclor()
	{
		$query = $this->db->query('SELECT f_title_en, f_historical_importance_en ,date,f_image
                                     FROM foloclor_content
                                     WHERE f_id > (
                                     SELECT max( f_id )
                                     FROM foloclor_content ) ');
            return $query->result();
	}
	public function update_product_status_to_reject($product_id,$checker_opinion,$state,$user_id)
	{
		if($state == 3)
		{
			$data = array('publish_state' => "$state",
					      'marketing_respons_opinion' => "$checker_opinion",
					      'marketing_respons_id'=> "$user_id",
						  );
		}
		else 
		{
			$data = array('publish_state' => "$state",
						  'admin_id'=> "$user_id");
		}		  
		$this->db->where('pc_id',$product_id);
		$this->db->update('product_content',$data);
	}
	
	public function update_product_content($product_id,$data,$keys)
	{
		$this->db->where('pc_id',$product_id);
		$this->db->update('product_content',$data);
		/*
		 * 1, delete all previous  keyword of product_id
		 * 2, add new keywords  
		 */
		 $this->Product_model->delete_previous_keys($product_id);
		
		for ($j=0; $j < count($keys) ; $j++) { 
			$this->Product_model->add_keywords_to_content($keys[$j],$product_id);
		}
	}
	public function add_keywords_to_content($keyword_id,$product_id)
	{
		$data = array('pc_id' => $product_id ,'fk_id'=>$keyword_id);
		$this->db->insert('product_keyword',$data);
	}
	
	public function delete_previous_keys($product_id)
	{
		$this->db->where('pc_id',$product_id);
		$this->db->delete('product_keyword');
	}
 
}




?>