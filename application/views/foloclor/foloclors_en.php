<script>
	jQuery(document).ready(function() {
	    jQuery("a[rel^='prettyPhoto']").prettyPhoto({
			social_tools : false,
			show_title : false
	    });
	});
</script>


<div id="content" class="container">
	
	<?php
	
		foreach ($foloclors as $key => $fol) {
			if(!empty($fol['folclors'])){
				echo'<div id="main" class="row-fluid">
						<header class="entry-header">
							<h1 class="entry-title"><span>'.anchor('foloclor/category/'.$fol['category']->fc_id,$fol['category']->fc_name_en).'</span></h1>
						</header><!-- .entry-header -->
						<div class="row-fluid">';
						if(sizeof($fol['folclors'])> 4){
							$max = 4;
						}
						else {
							$max = sizeof($fol['folclors']);
						}
						for ($i=0; $i < $max; $i++) { 
							
							echo '<div class="kontengal4 span3">
									<article class="galleries">'.anchor('foloclor/view_foloclor/'.$fol['folclors'][$i]->f_id,$fol['folclors'][$i]->f_title_en);
										
											if($fol['folclors'][$i]->f_image != "NULL"){
												echo '<img style="width:570px; hieght:360px;" src="'.base_url().'uploads/images/270_171/'.$fol['folclors'][$i]->f_image.'" alt="shutterstock_58382248" />';
											}
											else if($fol['folclors'][$i]->f_video != "NULL"){
												echo '<video width="570" height="	" controls>
														  <source src="'.base_url().'uploads/videos/'.$fol['folclors'][$i]->f_video.'" type="video/mp4">
														  Your browser does not support the video tag.
													  </video>';
											}
											else {
												echo '<img width="570" height="360" src="'.base_url().'images/sound_thumb.png" alt="shutterstock_58382248" />';
											}
										echo '</a>
										<h3 class="gal-title">'.substr($fol['folclors'][$i]->f_historical_importance_en, 0,60) .'</h3>
									</article>
								</div>';
						}
				echo '</div>
					<div class="clearfix"></div>
				</div><!-- #main -->';
			}
		}
	?>

</div><!-- #content -->
