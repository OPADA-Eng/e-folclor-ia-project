<div id="content" class="container">
		<div id="main" class="row-fluid">
			<div id="main-left" class="span8">
				<?php 
				foreach ($foloclors as $key => $fol) {
					echo '<article class="post">
					<h2 class="entry-title">
						'.anchor('foloclor/view_foloclor/'.$fol->f_id,$fol->f_title_en).'
						<span class="entry-cat">'.anchor('foloclor/category/'.$fol->fc_id,$fol->fc_name_en).'</span>
					</h2>
					<div class="entry-meta row-fluid">
						<ul class="clearfix">
							<li><img src="'.base_url().'images/dummy/gravatar.png" height="15" width="15" /><a href="#" title="Posts by Admin" rel="author">'.$fol->first_name.' '.$fol->last_name.'</a></li>
							<li><img src="'.base_url().'images/time.png" alt="">'.$fol->date.'</li>
							<li><img src="'.base_url().'images/view-bg.png" alt="">'.$fol->total_voting.'</li>
							<li><img src="'.base_url().'images/komen.png" alt=""><a href="#" title="Comment on Lectus non rutrum pulvinar urna leo dignissim lorem">'.$fol->fc_comments_count.' Comment</a></li>
							
						</ul>
					</div>
					<div class="entry-content">
						<a href="#" title="Permalink to Lectus non rutrum pulvinar urna leo dignissim lorem" rel="bookmark">
						';
						if($fol->f_image != "NULL"){
							echo '<img style="width:774px; hieght:320px;" src="'.base_url().'uploads/images/770_320/'.$fol->f_image.'" alt="shutterstock_58382248" />';
						}
						else if($fol->f_video != "NULL"){
							echo '<video width="774" height="320" controls>
									  <source src="'.base_url().'uploads/videos/'.$fol->f_video.'" type="video/mp4">
									  Your browser does not support the video tag.
								  </video>';
						}
						else {
							echo '<img width="774" height="320" src="'.base_url().'images/sound_thumb.png" alt="shutterstock_58382248" />';
						}
						
						echo '
						</a>
						<p>'.$fol->f_historical_importance_en.'</p>
						<p class="moretag">'.anchor('foloclor/view_foloclor/'.$fol->f_id,'Read more').'</p>
					</div>
				</article>';
				}

				//pagenation
				 if($count > $per_page): 
					$url = site_url().'/';							 
					$url .= $this->uri->segment(1).'/';
					$url .= $this->uri->segment(2).'/';
					$url .= $this->uri->segment(3).'/';
					
					//$url = uri_string();
					$config['base_url'] = $url;
					$config['uri_segment'] = 4;
					$config['total_rows'] = $count;
					$config['per_page'] = $per_page;
					$config['num_links'] = 10;
					
					$config['full_tag_open'] = '<div class="pagination magz-pagination">';
					$config['full_tag_close'] = '</div>';
					
					// $config['first_link'] = 'الأولى';
					// $config['first_tag_open'] = '<div>';			
					// $config['first_tag_close'] = '</div>';
					
					// $config['last_link'] = 'الأخيرة';
					// $config['last_tag_open'] = '<div>';
					// $config['last_tag_close'] = '</div>';
					
					$config['next_link'] = '<span class="next page-numbers"> Next </span>';
					$config['next_tag_open'] = '';
					$config['next_tag_close'] = '';
					
					$config['prev_link'] = '<span class="prev page-numbers"> Prev </span>';
					$config['prev_tag_open'] = '';
					$config['prev_tag_close'] = '';
					
					$config['cur_tag_open'] = '<span class="page-numbers current">';
					$config['cur_tag_close'] = '</span>';
					
					$config['num_tag_open'] = '<span class="page-numbers">';
					$config['num_tag_close'] = '</span>';

					$this->pagination->initialize($config);
					echo $this->pagination->create_links();
				endif; 
				 ?>
		
	
			</div><!-- #main-left -->

		<div id="sidebar" class="span4">

			<div id="tabwidget" class="widget tab-container"> 
				<ul id="tabnav" class="clearfix"> 
					<li><h3><a href="#tab1" class="selected"><img src="<?php echo base_url(); ?>images/view-white-bg.png" alt="Popular">Popular</a></h3></li>
					<li><h3><a href="#tab2"><img src="<?php echo base_url(); ?>images/time-white.png" alt="Recent">Recent</a></h3></li>
				    <li><h3><a href="#tab3"><img src="<?php echo base_url(); ?>images/komen-putih.png" alt="Comments">Comments</a></h3></li>
				</ul> 

			<div id="tab-content">
			
	 		<div id="tab1" style="display: block; ">
				<ul id="itemContainer" class="recent-tab">
					<?php
						  for($i=0;$i<8;$i++)
				          {
				          	
				          	if(isset($popular_f[$i]['item_data']))
							{
								echo '<li>';
								if($popular_f[$i]['item_data']['f_image'] != "NULL"){
								         echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/91_55/'.$popular_f[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
								         ';       	
													                                }
								else if($popular_f[$i]['item_data']['f_video'] != "NULL"){
									 echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/movies-icon.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';     
								                                                    }
								else if($popular_f[$i]['item_data']['f_sound'] != "NULL"){
									echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/sound_thumb.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';
								}
				             	echo '
							<h4 class="post-title"><a href="#">'.$popular_f[$i]['item_data']['f_title_en'].'</a></h4>
							<p>'.substr($popular_f[$i]['item_data']['f_historical_importance_en'],0,60).'....</p>
							<div class="clearfix"></div>				
						    </li>';
							}
						  }?>								
					<script type="text/javascript">
						jQuery(document).ready(function($){

							/* initiate the plugin */
							$("div.holder").jPages({
							containerID  : "itemContainer",
							perPage      : 3,
							startPage    : 1,
							startRange   : 1,
							links		   : "blank"
							});
						});		
					</script>

				</ul>
				
				<div class="holder clearfix"></div>
				<div class="clear"></div>

			<!-- End most viewed post -->		  

			</div><!-- /#tab1 -->
 
			<div id="tab2" style="display: none;">	
				<ul id="itemContainer2" class="recent-tab">
					<?php for($i=0;$i<3;$i++)
				                  {
				                  	
				                  	if(isset($recently_f_added[$i]['item_data']))
									{
										echo '<li>';
										if($recently_f_added[$i]['item_data']['f_image'] != "NULL"){
								         echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/91_55/'.$recently_f_added[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
								         ';       	
													                                }
								         else if($recently_f_added[$i]['item_data']['f_video'] != "NULL"){
									      echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/movies-icon.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';     
								                                                    }
								        else if($recently_f_added[$i]['item_data']['f_sound'] != "NULL"){
									     echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/sound_thumb.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';
								                                                     }
						
						               echo'
										<h4 class="post-title"><a href="http://magazine.themedesigner.in/lectus-non-rutrum-pulvinar-urna-leo-dignissim-lorem-8/">'.$recently_f_added[$i]['item_data']['f_title_en'].'</a></h4>
										<p>'.substr($recently_f_added[$i]['item_data']['f_historical_importance_en'], 0,60).'....</p>
										<div class="clearfix"></div>	
									       </li>';
									}
								  }
					  ?>
					</ul> 	 
			</div><!-- /#tab2 --> 

			<div id="tab3" style="display: none; ">
				<ul id="itemContainer2" class="recent-tab">
					<?php for($i=0;$i<3;$i++)
				                  {
				                  	if(isset($top_commented_f[$i]['item_data']))
									{
										echo '<li>';
										if($top_commented_f[$i]['item_data']['f_image'] != "NULL"){
								         echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/91_55/'.$top_commented_f[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
								         ';       	
													                                }
								         else if($top_commented_f[$i]['item_data']['f_video'] != "NULL"){
									      echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/movies-icon.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';     
								                                                    }
								        else if($top_commented_f[$i]['item_data']['f_sound'] != "NULL"){
									     echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/sound_thumb.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';
								                                                     }
						
						               echo'
										<h4 class="post-title"><a href="http://magazine.themedesigner.in/lectus-non-rutrum-pulvinar-urna-leo-dignissim-lorem-8/">'.$top_commented_f[$i]['item_data']['f_title_en'].'</a></h4>
										<p>'.substr($top_commented_f[$i]['item_data']['f_historical_importance_en'], 0,50).'...</p>
										<div class="clearfix"></div>	
									       </li>';
									       }
								  }
					  ?>
					</ul>
			</div><!-- /#tab2 --> 
	
			</div><!-- /#tab-content -->

			</div><!-- /#tab-widget --> 

			<div class="widget widget_latestpost">
					</div>

			
						
		</div><!-- sidebar -->
		
		<div class="clearfix"></div>

		</div><!-- #main -->

		</div><!-- #content -->
