<style>
	.moretag a {
  color:#fff;
  background:#767b7e;
  padding:5px 25px;
  text-align:center;
  transition:all 0.8s linear 0s;
  clear: both;
}
.home .moretag, .page-template-magazine-php .moretag, .page-template-magazine-home-php .moretag {
  float: right;
  margin: 10px 0;
  padding: 5px 10px;
  width: auto;
}

.moretag a:hover,
.moretag a:focus {
  color:#fff;
  background:#555;
  text-decoration:none;
}
.tagcloud_f:before,
.tagcloud_f:after {
  content:"";
  display:table;
  line-height:0;
}

.tagcloud_f:after {
  clear:both;
}

.tagcloud_f a {
  float:right;
  background-color:#ccc;
  -webkit-transition:all 0.8s linear 0s;
  -moz-transition:all 0.8s linear 0s;
  -o-transition:all 0.8s linear 0s;
  transition:all 0.8s linear 0s;
  color:#555E6B;
  font-size:15px !important;
  margin:0 5px 5px 0;
  padding:3px 9px;
}

.tagcloud_f a:hover {
  background-color:#0088CC;
  color:#FFF;
  text-decoration:none;
}
</style>
<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel_style.css">
		<script src="<?php echo base_url();?>js/template/jquery-1.10.2.min.js" type="text/javascript" ></script>
<div id="content" class="container">

		<div id="main" class="row-fluid">
			<div id="main-left" class="span8">
			
				<?php
				echo '<article class="post">
					<h2 class="entry-title">
						'.anchor('foloclor/view_foloclor/'.$fol['item_data']->f_id,$fol['item_data']->f_title_ar).'
						<span class="entry-cat">'.anchor('foloclor/category/'.$fol['item_data']->fc_id,$fol['item_data']->fc_name_ar).'</span>
					</h2>
					<div class="entry-meta row-fluid">
					<div class="table-responsive">
							<table   class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
									<tr class="pad_table">
										<th><img src="'.base_url().'images/dummy/gravatar.png" height="15" width="15" /><a href="#" title="Posts by Admin" rel="author">'.$fol['item_data']->first_name.' '.$fol['item_data']->last_name.'</a></th>
										<th><img src="'.base_url().'images/time.png" alt="">'.$fol['item_data']->date.'</th>
										<th><img src="'.base_url().'images/view-bg.png" alt="">'.$fol['item_data']->total_voting.'</th>
										<th><img src="'.base_url().'images/komen.png" alt=""><a href="#" title="Comment on Lectus non rutrum pulvinar urna leo dignissim lorem">'.$fol['item_data']->fc_comments_count.' Comment</a></th>
										<th><div class="rate-result-cnt">
						    			 	<div class="rate-bg" id="avg" style="width:'.$rate_bg=(($fol['item_data']->total_voting/5)*100);  echo $rate_bg.'%"></div>
						   				 <div class="rate-stars"></div>
										</div></th>
									</tr>
								</thead>
							</table>
						</div>
						
					</div>
					<div class="entry-content">
						<a href="#" title="Permalink to Lectus non rutrum pulvinar urna leo dignissim lorem" rel="bookmark">
						';
						if($fol['item_data']->f_image != "NULL"){
							echo '<img style="width:774px; hieght:320px;" src="'.base_url().'uploads/images/thumbs/'.$fol['item_data']->f_image.'" alt="shutterstock_58382248" />';
						}
						else if($fol['item_data']->f_video != "NULL"){
							echo '<video width="774" height="320" controls>
									  <source src="'.base_url().'uploads/videos/'.$fol['item_data']->f_video.'" type="video/mp4">
									  Your browser does not support the video tag.
								  </video>';
						}
						else if($fol['item_data']->f_sound != "NULL"){
							echo '<img width="774" height="320" src="'.base_url().'images/sound_thumb.png" alt="shutterstock_58382248" />';
							echo '<audio controls><source src="'.base_url().'uploads/sounds/'.$fol['item_data']->f_sound.'" type="audio/mpeg">
									Your browser does not support the audio element.
								  </audio>'	;	
						}
						else {
							echo 'item has no media file';
						}
						echo '
						</a>
						<h2>Historical importance</h2>
						<p>'.$fol['item_data']->f_historical_importance_en.'</p>
						
						<h2>Description</h2>
						<p>'.$fol['item_data']->f_content_en.'</p>
						
						<h2>Location</h2>
						<p>'.$fol['item_data']->f_location_en.'</p>
						
						<h2>Keywords</h2>';
						 foreach($fol['item_keywords'] as $keyword) : 	?>
							<h5> <?php echo $keyword->fk_keyword_ar;?>  , <?php echo $keyword->fk_keyword_en;?></h5>
						<?php  endforeach;?>
						
						<h2>Tags</h2>
						<?php foreach($fol['item_tags'] as $tag) : 	?>
						<input class="tagcloud_f" id="tag_ar" type="button" onclick=getTags(<?php echo $tag->ft_id;?>);  value="<?php echo $tag->f_tag_ar;?>" />
						<input class="tagcloud_f" id="tag_en" type="button" value="<?php echo $tag->f_tag_en;?>" /> 
						<br>
						<br>
						<?php  endforeach;?>
						<br>
						
						<div class="rate-ex1-cnt">
					        <div id="1" class="rate-btn-1 rate-btn"></div>
					        <div id="2" class="rate-btn-2 rate-btn"></div>
					        <div id="3" class="rate-btn-3 rate-btn"></div>
					        <div id="4" class="rate-btn-4 rate-btn"></div>
					        <div id="5" class="rate-btn-5 rate-btn"></div>
			        	</div>
			        	<br />
			        	 
			        	<hr/>
			        	<br />
			        	<br />
			        	<h2>Recent comments</h2>
			        	<form class="addcomm" method="post">
			        		<input name="f_id" value="<?php echo $fol['item_data']->f_id; ?>" type="hidden" />
			        		<input id="comment_id" name="f_comment" type="text" placeholder="أضف تعليقا"/>
			        		<input type="submit" value="أضف" />
			        	</form>
			        	<?php 
			        		foreach ($comments as $key => $comm) {
								echo $comm->comment;
							}
			        	?>
			        	<?php $foloclor_id =  $fol['item_data']->f_id; ?>
					</div>
				</article>
			</div><!-- #main-left -->
<script>
        // rating script
        $(function(){ 
            $('.rate-btn').hover(function(){
                $('.rate-btn').removeClass('rate-btn-hover');
                var therate = $(this).attr('id');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-hover');
                };
            });
                            
            $('.rate-btn').click(function(){    
                var therate = $(this).attr('id');
                var dataRate = {foloclor_id:<?php echo $foloclor_id; ?>,rate:therate}; //
                $('.rate-btn').removeClass('rate-btn-active');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-active');
                };
                $.ajax({
                    type : "POST",
                    url : "<?php echo base_url(); ?>index.php/foloclor_voting/vote",
                    data: dataRate,
                    success:function(response){
                    	if(response == 'per_error'){
                    		alert("You don\'t have permisions to do this operation");
                    		window.location.replace("<?php echo base_url(); ?>index.php/auth/login/foloclor/view_foloclor/<?php echo $foloclor_id; ?>");
                    	}
                    	else if(response == 'rate_faild'){
                    		alert('Error in validation you are wasting your time :P ');
                    	}
                    	else
                    	{
                    		var parsed = JSON.parse(response);
						//	$("#times").html(parsed.rate_times);
							console.log(parsed);
							$("#voted").html(parsed.rate_times);
							//document.getElementById('avg').style.width=parsed.rate_bg+'%';
							//$("#avg").html(parsed.rate_bg);
                    	}
						
					}
                });
            });
        });
    </script>
    <script>
$(document).ready(function () {
 $('form.addcomm').on('submit', function() {
  var obj = $(this), // (*) references the current object/form each time
   url =  "<?php echo base_url();?>/index.php/foloclor/add_comment",
   method = obj.attr('method'),
   data = {};
   
  obj.find('[name]').each(function(index, value) {
   // console.log(value);
   var obj = $(this),
    name = obj.attr('name'),
    value = obj.val();
    data[name] = value;
  });

  $.ajax({
   // see the (*)
   url: url,
   type: method,
   data: data,
   success: function(response) {
    if(response =="success"){
    	alert("Comment added and waiting for admin aproving");
    	$('#comment_id').val('');
    }
    else 
    	alert("Comment  not added there is some error");
    //location.reload();
    // $("#feedback").html(data);
   }
  });
  return false; //disable refresh
 });
}); 
</script>
		<div id="sidebar" class="span4">

			<div id="tabwidget" class="widget tab-container"> 
				<ul id="tabnav" class="clearfix"> 
					<li><h3><a href="#tab1" class="selected"><img src="<?php echo base_url();?>images/view-white-bg.png" alt="Popular">الأشهر</a></h3></li>
					<li><h3><a href="#tab2"><img src="<?php echo base_url();?>images/time-white.png" alt="Recent">الأخيرة</a></h3></li>
				    <li><h3><a href="#tab3"><img src="<?php echo base_url();?>images/komen-putih.png" alt="Comments">التعليقات</a></h3></li>
				</ul> 

			<div id="tab-content">
			
	 		<div id="tab1" style="display: block; ">
				<ul id="itemContainer" class="recent-tab">
					<?php
						  for($i=0;$i<8;$i++)
				          {
				          	
				          	if(isset($popular_f[$i]['item_data']))
							{
								echo '<li>';
								if($popular_f[$i]['item_data']['f_image'] != "NULL"){
								         echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/91_55/'.$popular_f[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
								         ';       	
													                                }
								else if($popular_f[$i]['item_data']['f_video'] != "NULL"){
									 echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/movies-icon.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';     
								                                                    }
								else if($popular_f[$i]['item_data']['f_sound'] != "NULL"){
									echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/sound_thumb.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';
								}
				             	echo '
							<h4 class="post-title"><a href="#">'.$popular_f[$i]['item_data']['f_title_ar'].'</a></h4>
							<p>'.substr($popular_f[$i]['item_data']['f_historical_importance_ar'],0,60).'....</p>
							<div class="clearfix"></div>				
						    </li>';
							}
						  }?>
								
					<script type="text/javascript">
						jQuery(document).ready(function($){

							/* initiate the plugin */
							$("div.holder").jPages({
							containerID  : "itemContainer",
							perPage      : 3,
							startPage    : 1,
							startRange   : 1,
							links		   : "blank"
							});
						});		
					</script>

				</ul>
				
				<div class="holder clearfix"></div>
				<div class="clear"></div>

			<!-- End most viewed post -->		  

			</div><!-- /#tab1 -->
 
			<div id="tab2" style="display: none;">	
				<ul id="itemContainer2" class="recent-tab">
					<?php for($i=0;$i<3;$i++)
				                  {
				                  	
				                  	if(isset($recently_f_added[$i]['item_data']))
									{
										echo '<li>';
										if($recently_f_added[$i]['item_data']['f_image'] != "NULL"){
								         echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/91_55/'.$recently_f_added[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
								         ';       	
													                                }
								         else if($recently_f_added[$i]['item_data']['f_video'] != "NULL"){
									      echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/movies-icon.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';     
								                                                    }
								        else if($recently_f_added[$i]['item_data']['f_sound'] != "NULL"){
									     echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/sound_thumb.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';
								                                                     }
						
						               echo'
										<h4 class="post-title"><a href="http://magazine.themedesigner.in/lectus-non-rutrum-pulvinar-urna-leo-dignissim-lorem-8/">'.$recently_f_added[$i]['item_data']['f_title_ar'].'</a></h4>
										<p>'.substr($recently_f_added[$i]['item_data']['f_historical_importance_ar'], 0,60).'....</p>
										<div class="clearfix"></div>	
									       </li>';
									}
								  }
					  ?>
					
				</ul> 	 
			</div><!-- /#tab2 --> 

			<div id="tab3" style="display: none; ">
				<ul id="itemContainer2" class="recent-tab">
					<?php for($i=0;$i<3;$i++)
				                  {
				                  	
				                  	if(isset($top_commented_f[$i]['item_data']))
									{
										echo '<li>';
										if($top_commented_f[$i]['item_data']['f_image'] != "NULL"){
								         echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/91_55/'.$top_commented_f[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
								         ';       	
													                                }
								         else if($top_commented_f[$i]['item_data']['f_video'] != "NULL"){
									      echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/movies-icon.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';     
								                                                    }
								        else if($top_commented_f[$i]['item_data']['f_sound'] != "NULL"){
									     echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/sound_thumb.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';
								                                                     }
						
						               echo'
										<h4 class="post-title"><a href="http://magazine.themedesigner.in/lectus-non-rutrum-pulvinar-urna-leo-dignissim-lorem-8/">'.$top_commented_f[$i]['item_data']['f_title_ar'].'</a></h4>
										<p>'.substr($top_commented_f[$i]['item_data']['f_historical_importance_ar'], 0,50).'...</p>
										<div class="clearfix"></div>	
									       </li>';
									}
								  }
					  ?>	</ul>
			</div><!-- /#tab2 --> 
	
			</div><!-- /#tab-content -->

			</div><!-- /#tab-widget --> 
		</div>
	</div>
</div>