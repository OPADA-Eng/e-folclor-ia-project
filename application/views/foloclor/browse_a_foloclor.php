<html>
	<head>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel_style.css">
		<link href="<?php echo base_url();?>css/cp/bootstrap.css" rel="stylesheet">
		<link href="<?php echo base_url();?>css/cp/bootstrap-dialog.css" rel="stylesheet">
		<script src="<?php echo base_url();?>js/template/jquery-1.10.2.min.js" type="text/javascript" ></script>
	    <script src="<?php echo base_url();?>js/bootstrap.js"></script>
    	<script src="<?php echo base_url();?>js//cp/bootstrap-dialog.js"></script>
		<script src="<?php echo base_url();?>js//cp/bootstrap-dialog.js"></script>
		
	</head>
	<body>
		<article >
				English title
				<h2> <?php echo $foloclor_details[0]['item_data']['f_title_en']; ?></h2>
				English category name
				<br>
				<h4> <?php echo $foloclor_details[0]['item_data']['fc_name_en']; ?></h3>
		
				English content 
				<br>
				<p> <?php  echo $foloclor_details[0]['item_data']['f_content_en']; ?></p>
		
				English historical importance
				<br>
				<p> <?php  echo $foloclor_details[0]['item_data']['f_historical_importance_en']; ?></p>
				
				English location
				<br>
				<h4> <?php echo $foloclor_details[0]['item_data']['f_location_en']; ?></h4>
	
			
				<?php if($foloclor_details[0]['item_data']['f_image']!="NULL"){ ?>
				Image <br>
				<img width="150" height="100" src="<?php echo base_url();?>uploads/images/160_120/<?php echo $foloclor_details[0]['item_data']['f_image'];?>"/>
				<br>
				<?php }?>
				<?php if($foloclor_details[0]['item_data']['f_video']!="NULL"){ ?>
				Video <br>
				<video id="myVideo" controls>
					  <source id="mp4_src" src="<?php echo base_url();?>uploads/videos/<?php echo $foloclor_details[0]['item_data']['f_video'];?>" type="video/mp4">
					  Your browser does not support HTML5 video.
				</video>
				<br>
				<?php }?>
				<?php if($foloclor_details[0]['item_data']['f_sound']!="NULL"){ ?>
				Audio <br>
				<audio controls>
				   <source src="<?php echo base_url();?>uploads/sounds/<?php echo $foloclor_details[0]['item_data']['f_sound'];?>" type="audio/mpeg">
				   Your browser does not support the audio element.
				</audio>
				<br>
				<?php }?>
	
				Keywords:
				<?php foreach($foloclor_details[0]['item_keywords'] as $keyword) : 	?>
					<h5> <?php echo $keyword->fk_keyword_ar;?>  , <?php echo $keyword->fk_keyword_en;?></h5>
				<?php  endforeach;?>	
				<br>
				Tags:<br>
				<?php foreach($foloclor_details[0]['item_tags'] as $tag) : 	?>
					<input id="tag_ar" type="button" onclick=getTags(<?php echo $tag->ft_id;?>);  value="<?php echo $tag->f_tag_ar;?>" />
					<input id="tag_en" type="button" onclick=getTags(<?php echo $tag->ft_id;?>);  value="<?php echo $tag->f_tag_en;?>" /> 
					<br>
					<br>
				<?php  endforeach;?>
				<br>
				
				Added by
				<h4> <?php echo $foloclor_details[0]['item_data']['username']; ?></h4>	
			</article>
			<div>
	            <h3>The average rating is at <strong id="values"><?php echo $foloclor_details[0]['item_data']['total_voting']; ?></strong></h3>
	            <div class="rate-result-cnt">
    			 <div class="rate-bg" id="avg" style="width:<?php $rate_bg=(($foloclor_details[0]['item_data']['total_voting']/5)*100);  echo $rate_bg; ?>%"></div>
   				 <div class="rate-stars"></div>
				</div>    
	    	</div>
	    	<div class="rate-ex1-cnt">
		        <div id="1" class="rate-btn-1 rate-btn"></div>
		        <div id="2" class="rate-btn-2 rate-btn"></div>
		        <div id="3" class="rate-btn-3 rate-btn"></div>
		        <div id="4" class="rate-btn-4 rate-btn"></div>
		        <div id="5" class="rate-btn-5 rate-btn"></div>
        	</div>
	</body>
	<?php $foloclor_id =  $foloclor_details[0]['item_data']['f_id']; ?>
	 <script>
        // rating script
        $(function(){ 
            $('.rate-btn').hover(function(){
                $('.rate-btn').removeClass('rate-btn-hover');
                var therate = $(this).attr('id');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-hover');
                };
            });
                            
            $('.rate-btn').click(function(){    
                var therate = $(this).attr('id');
                var dataRate = 'act=rate&foloclor_id=<?php echo $foloclor_id; ?>&rate='+therate; //
                $('.rate-btn').removeClass('rate-btn-active');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-active');
                };
                $.ajax({
                    type : "POST",
                    url : "<?php echo base_url(); ?>index.php/foloclor_voting/vote",
                    data: dataRate,
                    success:function(responce){
					//	alert(responce);
						var parsed = JSON.parse(responce);
					//	$("#times").html(parsed.rate_times);
						$("#values").html(parsed.rate_value);
						document.getElementById('avg').style.width=parsed.rate_bg+'%';
						//$("#avg").html(parsed.rate_bg);
					}
                });
            });
        });
        
        function getTags(tagId)
		{
			var dataString = 'tag_id='+tagId;
			$.ajax({
			type: 'POST',
			url: "<?php echo base_url();?>index.php/fmg/getContentTotag",
			data: dataString,
			cache: false,
			success: function(result){
				$("#divy").html(result);
				var parsed = JSON.parse(result);
				console.log(parsed);
				displayTags(parsed);
			}
		});		
		}
		function displayTags(parsed)
		{
		if(parsed.length == 0)
		{
				$modal_body = "There is no related contents yet";
		}
		else
		{
			$modal_body = "";
		for (var i=0; i < parsed.length; i++) {
			$src = "<?php echo base_url();?>";	
			$image="";
			if(parsed[i].item_data.f_image != 'null' && parsed[i].item_data.f_image != 'NULL' &&  parsed[i].item_data.f_image != null)
			{
				$image = 'Image <br><img width="150" height="100" src="'+$src+'uploads/images/160_120/'+parsed[i].item_data.f_image+'" />';
				console.log($image);
			}
			$keys = "";
			for (var j=0; j < parsed[i].item_keywords.length; j++) {
			  $keys+= parsed[i].item_keywords[j].fk_keyword_ar+','+parsed[i].item_keywords[j].fk_keyword_ar+'<br>';
			};
			$modal_body += 
						   '<article>'+
						   '<details>'+
						   '<summary>'+
						   '<br>English title:'+parsed[i].item_data.f_title_en+'<br>'+
						   '</summary>'+
						   '<br>arabic title: '+parsed[i].item_data.f_title_ar+
						   '<br>Arabic category name :'+parsed[i].item_data.fc_name_ar+
						   '<br>English category name :'+parsed[i].item_data.fc_name_en+
						   '<br>Arabic location :'+ parsed[i].item_data.f_location_ar+
						   '<br>English location :'+ parsed[i].item_data.f_location_en+
						   '<details>'+
						   '<summary>'+
						   '<br>Content Details <br>'+
						   '</summary>'+
						   '<br>Arabic content<br><p>'+parsed[i].item_data.f_content_ar+
						   '<br>English content<br><p>'+parsed[i].item_data.f_content_en+'</p>'+
						   '<br>Arabic historical importance<br><p>'+parsed[i].item_data.f_historical_importance_ar+'</p>'+
						   '<br>English historical importance<br><p>'+parsed[i].item_data.f_historical_importance_en+'</p>'+
						   'Keywords: '+
						    $keys+
						   '</details>'+
						   '</details>'+
						   '</article><br>'+
						   $image+'<hr>';				   			
		};
		}
		BootstrapDialog.show({
            title: 'all content related',
            message: 
            		$modal_body,
            buttons: [{
                label: 'Close',
                action: function(dialogItself) {
                    dialogItself.close();
                }
            }
            ]
        });
	}
    </script>
</html>