<html>
	<head>
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel_style.css">	
		<script src="<?php echo base_url();?>js/template/jquery-1.10.2.min.js" type="text/javascript" ></script>
	</head>
	<body>
		<article >
					<details open="true">
						<summary>
							General information
						</summary>
						English name
						<h2> <?php echo $product_details[0]['item_data']['product_name_en']; ?></h2>
						Arabic name <br>
						<h2> <?php echo $product_details[0]['item_data']['product_name_ar']; ?></h2>
						Category name
						<br>
						<h4> <?php echo $product_details[0]['item_data']['cat_en']; ?></h4>
						Product type 
						<br>
						<h4> <?php echo $product_details[0]['item_data']['pt_name_en']; ?></h4>
					</details>
					<details>
						<summary>
							Product proberities
						</summary>
						Color
						<br>
						<h4> <?php echo $product_details[0]['item_data']['color']; ?></h4>
						Min price = <?php echo $product_details[0]['item_data']['min_price']; ?><br>
						Max price = <?php echo $product_details[0]['item_data']['max_price']; ?><br>
						unit weight = <?php echo $product_details[0]['item_data']['weight']; ?> <?php echo $product_details[0]['item_data']['weight_type']; ?><br>
						volume = <?php echo $product_details[0]['item_data']['volum']; ?><br>
						Height = <?php echo $product_details[0]['item_data']['height']; ?><br>
						Width = <?php echo $product_details[0]['item_data']['width']; ?><br>
					</details>
					<details>
						<summary>
							Extra details
						</summary>
						Min repository quantity = <?php echo $product_details[0]['item_data']['min_quantity']; ?><br>
						Max repository quntity = <?php echo $product_details[0]['item_data']['max_quantity']; ?><br>
						Arabic culture detail:<br>
						<p> <?php echo $product_details[0]['item_data']['details_ar']; ?> </p>
						English culture detail:<br>
						<p> <?php echo $product_details[0]['item_data']['details_en']; ?> </p>
						Keywords:
						<?php foreach($product_details[0]['item_keywords'] as $keyword) : 	?>
							<h5> <?php echo $keyword->fk_keyword_ar;?>  , <?php echo $keyword->fk_keyword_en;?></h5>
						<?php  endforeach;?>	
						Added by : <?php echo $product_details[0]['item_data']['username']; ?>
					</details>
					<?php if($product_details[0]['item_data']['image']!="NULL"){ ?>
					Image <br>
					<img width="150" height="100" src="<?php echo base_url();?>uploads/images/thumbs/<?php echo $product_details[0]['item_data']['image'];?>"/>
					<br>
					<?php } ?>
					<div>
			            <h3>The average rating is at <strong id="values"><?php echo $product_details[0]['item_data']['pc_rate']; ?></strong></h3>
			            <div class="rate-result-cnt">
		    			 	<div class="rate-bg" id="avg" style="width:<?php $rate_bg=(($product_details[0]['item_data']['pc_rate']/5)*100);  echo $rate_bg; ?>%"></div>
		   				 	<div class="rate-stars"></div>
						</div>    
	    			</div>
			    	<div class="rate-ex1-cnt">
				        <div id="1" class="rate-btn-1 rate-btn"></div>
				        <div id="2" class="rate-btn-2 rate-btn"></div>
				        <div id="3" class="rate-btn-3 rate-btn"></div>
				        <div id="4" class="rate-btn-4 rate-btn"></div>
				        <div id="5" class="rate-btn-5 rate-btn"></div>
		        	</div>
		        </article>
		   </body>
<?php $product_id =  $product_details[0]['item_data']['pc_id']; ?>
	 <script>
        // rating script
        $(function(){ 
            $('.rate-btn').hover(function(){
                $('.rate-btn').removeClass('rate-btn-hover');
                var therate = $(this).attr('id');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-hover');
                };
            });
                            
            $('.rate-btn').click(function(){    
                var therate = $(this).attr('id');
                var dataRate = 'act=rate&product_id=<?php echo $product_id; ?>&rate='+therate; //
                $('.rate-btn').removeClass('rate-btn-active');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-active');
                };
                $.ajax({
                    type : "POST",
                    url : "<?php echo base_url(); ?>index.php/product_voting/vote",
                    data: dataRate,
                    success:function(responce){
					//	alert(responce);
						var parsed = JSON.parse(responce);
					//	$("#times").html(parsed.rate_times);
						$("#values").html(parsed.rate_value);
						document.getElementById('avg').style.width=parsed.rate_bg+'%';
						alert(parsed.user_id);
						//$("#avg").html(parsed.rate_bg);
					}
                });
            });
        });
    </script>
</html>