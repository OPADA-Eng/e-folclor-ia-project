<div id="content" class="container">
		<div id="main" class="row-fluid">
			<div id="main-left" class="span8">
				<?php 
				foreach ($products as $key => $prod) {
					echo '<article class="post">
					<h2 class="entry-title">
						'.anchor('product/view_product/'.$prod->pc_id,$prod->product_name_en).'
						<span class="entry-cat">'.anchor('product/category/'.$prod->pc_id,$prod->cat_en).'</span>
					</h2>
					<div class="entry-meta row-fluid">
						<ul class="clearfix">
							<li><img src="'.base_url().'images/dummy/gravatar.png" height="15" width="15" /><a href="#" title="Posts by Admin" rel="author">'.$prod->first_name.' '.$prod->last_name.'</a></li>
							<li><img src="'.base_url().'images/time.png" alt="">'.$prod->date.'</li>
							<li><img src="'.base_url().'images/view-bg.png" alt="">'.$prod->pc_rate.'</li>
							<li><img src="'.base_url().'images/komen.png" alt=""><a id="add_to_cart" name="'.$prod->pc_id.'" href="#" >add to cart</a></li>
						</ul>
					</div>
					<div class="entry-content">
						<a href="#"  rel="bookmark">
						';
						if($prod->image != "NULL"){
							echo '<img style="width:774px; hieght:320px;" src="'.base_url().'uploads/images/770_320/'.$prod->image.'" />';
						}
						
						echo '
						</a>
						<p>'.$prod->details_en.'</p>
						<p class="moretag">'.anchor('product/view_product/'.$prod->pc_id,'Read more').'</p>
					</div>
				</article>';
				}

				//pagenation
				 if($count > $per_page): 
					$url = site_url().'/';							 
					$url .= $this->uri->segment(1).'/';
					$url .= $this->uri->segment(2).'/';
					$url .= $this->uri->segment(3).'/';
					
					//$url = uri_string();
					$config['base_url'] = $url;
					$config['uri_segment'] = 4;
					$config['total_rows'] = $count;
					$config['per_page'] = $per_page;
					$config['num_links'] = 10;
					
					$config['full_tag_open'] = '<div class="pagination magz-pagination">';
					$config['full_tag_close'] = '</div>';
					
					
					
					$config['next_link'] = '<span class="next page-numbers"> Next </span>';
					$config['next_tag_open'] = '';
					$config['next_tag_close'] = '';
					
					$config['prev_link'] = '<span class="prev page-numbers"> Prev </span>';
					$config['prev_tag_open'] = '';
					$config['prev_tag_close'] = '';
					
					$config['cur_tag_open'] = '<span class="page-numbers current">';
					$config['cur_tag_close'] = '</span>';
					
					$config['num_tag_open'] = '<span class="page-numbers">';
					$config['num_tag_close'] = '</span>';

					$this->pagination->initialize($config);
					echo $this->pagination->create_links();
				endif; 
				 ?>
		
	
			</div><!-- #main-left -->

		<div id="sidebar" class="span4">

			<div id="tabwidget" class="widget tab-container"> 
				<ul id="tabnav" class="clearfix"> 
					<li><h3><a href="#tab1" class="selected"><img src="<?php echo base_url(); ?>images/view-white-bg.png" alt="Popular">Popular</a></h3></li>
					<li><h3><a href="#tab2"><img src="<?php echo base_url(); ?>images/time-white.png" alt="Recent">Recent</a></h3></li>
				    <li><h3><a href="#tab3"><img src="<?php echo base_url(); ?>images/komen-putih.png" alt="Comments">Comments</a></h3></li>
				</ul> 

			<div id="tab-content">
			
	 		<div id="tab1" style="display: block; ">
				<ul id="itemContainer" class="recent-tab">
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_134257640-225x136.jpg" class="thumb" alt="shutterstock_134257640" /></a>
						<h4 class="post-title"><a href="#">Lectus non rutrum pulvinar urna...</a></h4>
						<p>Nam nibh arcu, tristique eget pretium sed, porta id quam. Praesent dig...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/photodune-2043745-college-student-s-225x136.jpg" class="thumb" alt="photodune-2043745-college-student-s" /></a>
						<h4 class="post-title"><a href="#">Donec consectetuer ligula vulpu...</a></h4>
						<p>Nam nibh arcu, tristique eget pretium sed, porta id quam. Praesent dig...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/photodune-3517559-idea-s-225x136.jpg" class="thumb" alt="photodune-3517559-idea-s" /></a>
						<h4 class="post-title"><a href="#">Nam nibh arcu tristique eget pr...</a></h4>
						<p>Nam nibh arcu, tristique eget pretium sed, porta id quam. Praesent dig...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_123603871-225x136.jpg" class="thumb" alt="gallery-1" /></a>
						<h4 class="post-title"><a href="#">Quisque sodales viverra ornare ...</a></h4>
						<p>Quisque sodales viverra ornare. Aenean posuere lacus sed facilisis gra...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/photodune-3834701-laughing-girl-xs-225x136.jpg" class="thumb" alt="photodune-3834701-laughing-girl-xs" /></a>
						<h4 class="post-title"><a href="#">Morbi est est lectus non rutrum...</a></h4>
						<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_2114081-225x136.jpg" class="thumb" alt="shutterstock_2114081" /></a>
						<h4 class="post-title"><a href="#">Aliquam quam lectus pulvinar ur...</a></h4>
						<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_70184773-225x136.jpg" class="thumb" alt="shutterstock_70184773" /></a>
						<h4 class="post-title"><a href="#">Suspen disse auctor dapibus neq...</a></h4>
						<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_70787161-225x136.jpg" class="thumb" alt="shutterstock_70787161" /></a>
						<h4 class="post-title"><a href="#">Porta lorem ipsum dolor sit ame...</a></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer place...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_58382248-225x136.jpg" class="thumb" alt="shutterstock_58382248" /></a>
						<h4 class="post-title"><a href="#">Integer vitae libero ac risus e...</a></h4>
						<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing...</p>
						<div class="clearfix"></div>				
					</li>
								
					<script type="text/javascript">
						jQuery(document).ready(function($){

							/* initiate the plugin */
							$("div.holder").jPages({
							containerID  : "itemContainer",
							perPage      : 3,
							startPage    : 1,
							startRange   : 1,
							links		   : "blank"
							});
						});		
					</script>

				</ul>
				
				<div class="holder clearfix"></div>
				<div class="clear"></div>

			<!-- End most viewed post -->		  

			</div><!-- /#tab1 -->
 
			<div id="tab2" style="display: none;">	
				<ul id="itemContainer2" class="recent-tab">
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_134257640-225x136.jpg" class="thumb" alt="shutterstock_134257640" /></a>
						<h4 class="post-title"><a href="http://magazine.themedesigner.in/lectus-non-rutrum-pulvinar-urna-leo-dignissim-lorem-8/">Lectus non rutrum pulvinar urna...</a></h4>
						<p>Nam nibh arcu, tristique eget pretium sed, porta id quam. Praesent dig...</p>
						<div class="clearfix"></div>	
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_70184773-225x136.jpg" class="thumb" alt="shutterstock_70184773" /></a>
						<h4 class="post-title"><a href="#">Suspen disse auctor dapibus neq...</a></h4>
						<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing...</p>
						<div class="clearfix"></div>	
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_70787161-225x136.jpg" class="thumb" alt="shutterstock_70787161" /></a>
						<h4 class="post-title"><a href="#">Porta lorem ipsum dolor sit ame...</a></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer place...</p>
						<div class="clearfix"></div>	
					</li>
				</ul> 	 
			</div><!-- /#tab2 --> 

			<div id="tab3" style="display: none; ">
				<ul>
					<li><span class="author">Magazine Author</span> on <a href="#" title=" View comment ">Lorem Ipsum is simply dummy te</a></li>
					<li><span class="author">Magazine Author</span> on <a href="#" title=" View comment ">Lorem Ipsum is simply dummy te</a></li>
					<li><span class="author">magazine</span> on <a href="#" title=" View comment ">This is threaded comment level</a></li>
					<li><span class="author">magazine</span> on <a href="#" title=" View comment ">This is threaded comment syste</a></li>
				</ul>
			</div><!-- /#tab2 --> 
	
			</div><!-- /#tab-content -->

			</div><!-- /#tab-widget --> 

			<div class="widget widget_latestpost">
				<h3 class="title"><span>Recent Posts</span></h3>
				<div class="latest-posts widget">
					<div class="latest-post clearfix">
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_134257640-225x136.jpg" class="thumb fl" alt="shutterstock_134257640" title="" /></a>
						<h4><a href="#" rel="bookmark" title="Lectus non rutrum pulvinar urna leo dignissim lorem">Lectus non rutrum pulvinar urna leo</a></h4>
						<div class="post-time">August 12, 2013</div>
						<div class="ratings" style="float: none">
							<input class="star" type="radio" name="recent-post-1" value="1" disabled="disabled"/>
							<input class="star" type="radio" name="recent-post-1" value="2" disabled="disabled"/>
							<input class="star" type="radio" name="recent-post-1" value="3" disabled="disabled"/>
							<input class="star" type="radio" name="recent-post-1" value="4" disabled="disabled" checked="checked"/>
							<input class="star" type="radio" name="recent-post-1" value="5" disabled="disabled"/>
						</div>
					</div>
					
					<div class="latest-post clearfix">
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_70184773-225x136.jpg" class="thumb fl" alt="shutterstock_70184773" title="" /></a>
						<h4><a href="#" rel="bookmark" title="Suspen disse auctor dapibus neque pulvinar urna leo">Suspen disse auctor dapibus neque</a></h4>
						<div class="post-time">August 11, 2013</div>
						<div class="ratings" style="float: none">
							<input class="star" type="radio" name="recent-post-2" value="1" disabled="disabled"/>
							<input class="star" type="radio" name="recent-post-2" value="2" disabled="disabled"/>
							<input class="star" type="radio" name="recent-post-2" value="3" disabled="disabled"/>
							<input class="star" type="radio" name="recent-post-2" value="4" disabled="disabled" checked="checked"/>
							<input class="star" type="radio" name="recent-post-2" value="5" disabled="disabled"/>
						</div>
					</div>

					<div class="latest-post clearfix">
						<a href="#"><img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_70787161-225x136.jpg" class="thumb fl" alt="shutterstock_70787161" title="" /></a>
						<h4><a href="#" rel="bookmark" title="Porta lorem ipsum dolor sit amet, consectetur adipiscing risus">Porta lorem ipsum dolor sit amet</a></h4>
						<div class="post-time">August 2, 2013</div>
						<div class="ratings" style="float: none">
							<input class="star" type="radio" name="recent-post-3" value="1" disabled="disabled"/>
							<input class="star" type="radio" name="recent-post-3" value="2" disabled="disabled"/>
							<input class="star" type="radio" name="recent-post-3" value="3" disabled="disabled"/>
							<input class="star" type="radio" name="recent-post-3" value="4" disabled="disabled" checked="checked"/>
							<input class="star" type="radio" name="recent-post-3" value="5" disabled="disabled"/>
						</div>
					</div>

				</div>
			</div>

			<div class="widget widget_latestpost"><h3 class="title"><span>Technology News</span></h3>
				<div class="latest-posts">
					<article class="post">
						<a class="image_thumb_zoom" href="#" title="Permalink to Porta lorem ipsum dolor sit amet, consectetur adipiscing risus" rel="bookmark">
						<img width="371" height="177" src="<?php echo base_url(); ?>images/dummy/shutterstock_70787161-371x177.jpg" class="attachment-post-first wp-post-image" alt="shutterstock_70787161" />
						</a>
						<h4 class="post-title">
						<a href="#" title="Permalink to Porta lorem ipsum dolor sit amet, consectetur adipiscing risus" rel="bookmark">Porta lorem ipsum dolor sit amet, c...</a>
						<span class="date">August 2, 2013</span>
						</h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer placerat id augue non dapibus. Morbi ut ipsum cond...</p>
					</article>
				
					<article class="post">
						<div class="entry clearfix">
							<a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
							<img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/photodune-2043745-college-student-s-225x136.jpg" class="thumb" alt="photodune-2043745-college-student-s" />
							<h4 class="post-title">Donec consectetuer ligula vulputate...</h4>
							</a>
							<p>Nam nibh arcu, tristique eget pretium se...</p>
							<div class="meta">
								<span class="date">July 11, 2013</span>
							</div>
						</div>
					</article>

					<article class="post">
						<div class="entry clearfix">
							<a href="#" title="Permalink to Quisque sodales viverra ornare vitae libero ac risus" rel="bookmark">
							<img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_123603871-225x136.jpg" class="thumb" alt="gallery-1" />
							<h4 class="post-title">Quisque sodales viverra ornare vita...</h4></a>
							<p>Quisque sodales viverra ornare. Aenean p...</p>
							<div class="meta">
								<span class="date">July 2, 2013</span>
							</div>
						</div>
					</article>
				</div>
			</div>
			
			<div class="video-box widget row-fluid">
				<h3 class="title"><span style="background-color: #;color: #;">Videos Gallery</span></h3>		
				<iframe width="369" height="188" src="http://www.youtube.com/embed/pkRB5xC1Cw8" frameborder="0" allowfullscreen></iframe>
				<ul>
					<li>
					<a href="#" title="Permalink to Lectus non rutrum pulvinar urna leo dignissim lorem" rel="bookmark">
					<img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_134257640-225x136.jpg" alt="shutterstock_134257640" />
					</a>
					</li>
						
					<li>
					<a href="#" title="Permalink to Vestibulum volutpat tortor libero sodales leo mauris ut lectus" rel="bookmark">
					<img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/photodune-3517559-idea-s-225x136.jpg" alt="photodune-3517559-idea-s" />
					</a>
					</li>
						
					<li>
					<a href="#" title="Permalink to Vivamus ultrices luctus nunc sem sit amet interdum consectetuer" rel="bookmark">
					<img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/photodune-2043745-college-student-s-225x136.jpg" alt="photodune-2043745-college-student-s" />
					</a>
					</li>
					
					<li>
					<a href="#" title="Permalink to Phasellus scelerisque massa molestie iaculis lectus pulvinar" rel="bookmark">
					<img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_109209743-225x136.jpg" alt="shutterstock_109209743" />
					</a>
					</li>
				</ul>
        	</div>
        				
		</div><!-- sidebar -->
		
		<div class="clearfix"></div>

		</div><!-- #main -->

		</div><!-- #content -->


<script type='text/javascript' src="<?php echo base_url(); ?>js/template/jquery-1.10.2.min.js"></script>
<script>
	 $('#add_to_cart').click(function(){
     	console.log('clicked');
     	id=$(this).attr('name');
     	//alert(id);
     	 $.ajax({
        	url:"<?php echo base_url();?>index.php/accounting/update_item/"+id,
        	method:'GET',
        	data:'',
        	success:function(data){
        		//show_cart();
        		showMessage(data);
        	},
        	error :function(xhr,textStatus,errorThrown){
        		alert('request failed: '+ errorThrown);
        	}	
        });
     });
     
     function closeAllDialoge () {
	      $.each(BootstrapDialog.dialogs, function(id, dialog){
	           dialog.close();
	       });
	      }
</script>