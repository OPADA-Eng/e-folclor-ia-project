<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel.css">
		<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel_style.css">
		<script src="<?php echo base_url();?>js/template/jquery-1.10.2.min.js" type="text/javascript" ></script>
<div id="content" class="container">

		<div id="main" class="row-fluid">
			<div id="main-left" class="span8">
			
				<?php
				echo '<article class="post">
					<h2 class="entry-title">
						'.anchor('product/view_product/'.$prod['item_data']->pc_id,$prod['item_data']->product_name_en).'
						<span class="entry-cat">'.anchor('product/category/'.$prod['item_data']->pc_id,$prod['item_data']->cat_en).'</span>
					</h2>
					<div class="entry-meta row-fluid">
						<ul class="clearfix">
							<li><img src="'.base_url().'images/dummy/gravatar.png" height="15" width="15" /><a href="#" title="Posts by Admin" rel="author">'.$prod['item_data']->first_name.' '.$prod['item_data']->last_name.'</a></li>
							<li><img src="'.base_url().'images/time.png" alt="">'.$prod['item_data']->date.'</li>
							<li><img src="'.base_url().'images/view-bg.png" alt=""><span id="voted">'.$prod['item_data']->pc_rate.'</span></li>
							<li><img src="'.base_url().'images/price-icon.png" alt=""><a href="#" title="Product price">'.$prod['item_data']->max_price.' S.P</a></li>
							<li><div class="rate-result-cnt">';?>
		    			 	<div class="rate-bg" id="avg" style="width:<?php $rate_bg=(($prod['item_data']->pc_rate/5)*100);  echo $rate_bg; ?>%"></div>
		   				 <?php echo '<div class="rate-stars"></div>
						</div> </li>
						</ul>
					</div>
					<div class="entry-content">
						<a href="#" title="Permalink to Lectus non rutrum pulvinar urna leo dignissim lorem" rel="bookmark">
						';
						if($prod['item_data']->image != "NULL"){
							echo '<img style="width:774px; hieght:320px;" src="'.base_url().'uploads/images/770_320/'.$prod['item_data']->image.'" alt="shutterstock_58382248" />';
						}
						else {
							echo 'item has no media file';
						}
						echo '
						</a>
						
						<h2>Description</h2>
						<p>'.$prod['item_data']->details_en.'</p>
						
						
						<h2>Keywords</h2>';
						 foreach($prod['item_keywords'] as $keyword) : 	?>
							<h5> <?php echo $keyword->fk_keyword_ar;?>  , <?php echo $keyword->fk_keyword_en;?></h5>
						<?php  endforeach;?>
						
						<h2>specifications</h2>
						<table>
							<tr>
								<td>Color:</td><td><?php echo $prod['item_data']->color;?></td>
							</tr>
							<tr>
								<td>Weight:</td><td><?php echo $prod['item_data']->weight.' '.$prod['item_data']->weight_type;?></td>
							</tr>
							<tr>
								<td>Volum:</td><td><?php echo $prod['item_data']->volum ;?></td>
							</tr>
							<tr>
								<td>Height:</td><td><?php echo $prod['item_data']->height ;?></td>
							</tr>
							<tr>
								<td>Width:</td><td><?php echo $prod['item_data']->width ;?></td>
							</tr>
							
						</table>
						<br>
						
						<div class="rate-ex1-cnt">
					        <div id="1" class="rate-btn-1 rate-btn"></div>
					        <div id="2" class="rate-btn-2 rate-btn"></div>
					        <div id="3" class="rate-btn-3 rate-btn"></div>
					        <div id="4" class="rate-btn-4 rate-btn"></div>
					        <div id="5" class="rate-btn-5 rate-btn"></div>
			        	</div>
			        	<br />
			        	<?php $prod_id =  $prod['item_data']->pc_id; ?>
						
			        	
					</div>
					<?php 
						if(!empty($related_fol)){?>
							<div id="home-top">
					<h3 class="title"><span>Related Folclors</span></h3>
					<ul class="bxslider">
						
						<?php 
						$i = 0;
						foreach ($related_fol as $key => $fol){
				                  	
				               echo'<li><a class="image_thumb_zoom" href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">';
								if($fol->f_image != "NULL"){
									echo '<img width="225" height="136" src="'.base_url().'uploads/images/225_136/'.$fol->f_image.'"/>';
								}
								else if($fol->f_video != "NULL"){
									echo '<img width="225" height="136" src="'.base_url().'uploads/images/225_136/movies-icon.png" />';
								}
								else if($fol->f_sound != "NULL"){
									echo '<img width="225" height="136" src="'.base_url().'uploads/images/225_136/sound_thumb.png" />';
								}
								echo '</a>
								<h4 class="post-title clearfix">
									<h4>'.$fol->f_title_en.'</h4>
								</h4>
								<div class="meta clearfix">
									<span class="date">'.$fol->date.'</span>
									
								</div>
							</li>';
								$i++;
							}
                               ?>
					</ul>
				</div>
						<?php }
					?>
				</article>
				
				
				
			</div><!-- #main-left -->
<script>
        // rating script
        $(function(){ 
            $('.rate-btn').hover(function(){
                $('.rate-btn').removeClass('rate-btn-hover');
                var therate = $(this).attr('id');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-hover');
                };
            });
                            
            $('.rate-btn').click(function(){    
                var therate = $(this).attr('id');
                var dataRate = {product_id:<?php echo $prod_id; ?>,rate:therate}; //
                $('.rate-btn').removeClass('rate-btn-active');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-active');
                };
                $.ajax({
                    type : "POST",
                    url : "<?php echo base_url(); ?>index.php/product_voting/vote",
                    data: dataRate,
                    success:function(response){
                    	if(response == 'per_error'){
                    		alert("You don\'t have permisions to do this operation");
                    		window.location.replace("<?php echo base_url(); ?>index.php/auth/login/product/view_product/<?php echo $prod_id; ?>");
                    	}
                    	else if(response == 'rate_faild'){
                    		alert('Error in validation you are wasting your time :P ');
                    	}
                    	else
                    	{
                    		var parsed = JSON.parse(response);
						//	$("#times").html(parsed.rate_times);
							console.log(parsed);
							$("#voted").html(parsed.rate_times);
							//document.getElementById('avg').style.width=parsed.rate_bg+'%';
							//$("#avg").html(parsed.rate_bg);
                    	}
						
					}
                });
            });
        });
    </script>
    
		<div id="sidebar" class="span4">

			<div id="tabwidget" class="widget tab-container"> 
				<ul id="tabnav" class="clearfix"> 
					<li><h3><a href="#tab1" class="selected"><img src="images/view-white-bg.png" alt="Popular">Popular</a></h3></li>
					<li><h3><a href="#tab2"><img src="images/time-white.png" alt="Recent">Recent</a></h3></li>
				    <li><h3><a href="#tab3"><img src="images/komen-putih.png" alt="Comments">Comments</a></h3></li>
				</ul> 

			<div id="tab-content">
			
	 		<div id="tab1" style="display: block; ">
				<ul id="itemContainer" class="recent-tab">
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/shutterstock_134257640-225x136.jpg" class="thumb" alt="shutterstock_134257640" /></a>
						<h4 class="post-title"><a href="#">Lectus non rutrum pulvinar urna...</a></h4>
						<p>Nam nibh arcu, tristique eget pretium sed, porta id quam. Praesent dig...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/photodune-2043745-college-student-s-225x136.jpg" class="thumb" alt="photodune-2043745-college-student-s" /></a>
						<h4 class="post-title"><a href="#">Donec consectetuer ligula vulpu...</a></h4>
						<p>Nam nibh arcu, tristique eget pretium sed, porta id quam. Praesent dig...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/photodune-3517559-idea-s-225x136.jpg" class="thumb" alt="photodune-3517559-idea-s" /></a>
						<h4 class="post-title"><a href="#">Nam nibh arcu tristique eget pr...</a></h4>
						<p>Nam nibh arcu, tristique eget pretium sed, porta id quam. Praesent dig...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/shutterstock_123603871-225x136.jpg" class="thumb" alt="gallery-1" /></a>
						<h4 class="post-title"><a href="#">Quisque sodales viverra ornare ...</a></h4>
						<p>Quisque sodales viverra ornare. Aenean posuere lacus sed facilisis gra...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/photodune-3834701-laughing-girl-xs-225x136.jpg" class="thumb" alt="photodune-3834701-laughing-girl-xs" /></a>
						<h4 class="post-title"><a href="#">Morbi est est lectus non rutrum...</a></h4>
						<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/shutterstock_2114081-225x136.jpg" class="thumb" alt="shutterstock_2114081" /></a>
						<h4 class="post-title"><a href="#">Aliquam quam lectus pulvinar ur...</a></h4>
						<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/shutterstock_70184773-225x136.jpg" class="thumb" alt="shutterstock_70184773" /></a>
						<h4 class="post-title"><a href="#">Suspen disse auctor dapibus neq...</a></h4>
						<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/shutterstock_70787161-225x136.jpg" class="thumb" alt="shutterstock_70787161" /></a>
						<h4 class="post-title"><a href="#">Porta lorem ipsum dolor sit ame...</a></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer place...</p>
						<div class="clearfix"></div>				
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/shutterstock_58382248-225x136.jpg" class="thumb" alt="shutterstock_58382248" /></a>
						<h4 class="post-title"><a href="#">Integer vitae libero ac risus e...</a></h4>
						<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing...</p>
						<div class="clearfix"></div>				
					</li>
								
					<script type="text/javascript">
						jQuery(document).ready(function($){

							/* initiate the plugin */
							$("div.holder").jPages({
							containerID  : "itemContainer",
							perPage      : 3,
							startPage    : 1,
							startRange   : 1,
							links		   : "blank"
							});
						});		
					</script>

				</ul>
				
				<div class="holder clearfix"></div>
				<div class="clear"></div>

			<!-- End most viewed post -->		  

			</div><!-- /#tab1 -->
 
			<div id="tab2" style="display: none;">	
				<ul id="itemContainer2" class="recent-tab">
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/shutterstock_134257640-225x136.jpg" class="thumb" alt="shutterstock_134257640" /></a>
						<h4 class="post-title"><a href="http://magazine.themedesigner.in/lectus-non-rutrum-pulvinar-urna-leo-dignissim-lorem-8/">Lectus non rutrum pulvinar urna...</a></h4>
						<p>Nam nibh arcu, tristique eget pretium sed, porta id quam. Praesent dig...</p>
						<div class="clearfix"></div>	
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/shutterstock_70184773-225x136.jpg" class="thumb" alt="shutterstock_70184773" /></a>
						<h4 class="post-title"><a href="#">Suspen disse auctor dapibus neq...</a></h4>
						<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing...</p>
						<div class="clearfix"></div>	
					</li>
					<li>
						<a href="#"><img width="225" height="136" src="images/dummy/shutterstock_70787161-225x136.jpg" class="thumb" alt="shutterstock_70787161" /></a>
						<h4 class="post-title"><a href="#">Porta lorem ipsum dolor sit ame...</a></h4>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer place...</p>
						<div class="clearfix"></div>	
					</li>
				</ul> 	 
			</div><!-- /#tab2 --> 

			<div id="tab3" style="display: none; ">
				<ul>
					<li><span class="author">Magazine Author</span> on <a href="#" title=" View comment ">Lorem Ipsum is simply dummy te</a></li>
					<li><span class="author">Magazine Author</span> on <a href="#" title=" View comment ">Lorem Ipsum is simply dummy te</a></li>
					<li><span class="author">magazine</span> on <a href="#" title=" View comment ">This is threaded comment level</a></li>
					<li><span class="author">magazine</span> on <a href="#" title=" View comment ">This is threaded comment syste</a></li>
				</ul>
			</div><!-- /#tab2 --> 
	
			</div><!-- /#tab-content -->

			</div><!-- /#tab-widget --> 
		</div>
	</div>
</div>
