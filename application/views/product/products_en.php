<script>
	jQuery(document).ready(function() {
	    jQuery("a[rel^='prettyPhoto']").prettyPhoto({
			social_tools : false,
			show_title : false
	    });
	});
</script>


<div id="content" class="container">
	<?php
		foreach ($products as $key => $prod) {
			if(!empty($prod['products'])){
				echo'<div id="main" class="row-fluid">
						<header class="entry-header">
							<h1 class="entry-title"><span>'.anchor('product/category/'.$prod['category']['pcat_id'],$prod['category']['cat_en']).'</span></h1>
						</header><!-- .entry-header -->
						<div class="row-fluid">';
						if(sizeof($prod['products'])> 8){
							$max = 8;
						}
						else {
							$max = sizeof($prod['products']);
						}
						for ($i=0; $i < $max; $i++) { 
							
							echo '<div class="kontengal4 span3">
									<article class="galleries">'.anchor('product/view_product/'.$prod['products'][$i]->pc_id,$prod['products'][$i]->product_name_en);
										
											if($prod['products'][$i]->image != "NULL"){
												echo '<img style="width:570px; hieght:360px;" src="'.base_url().'uploads/images/270_171/'.$prod['products'][$i]->image.'" />';
											}
										echo '</a>
										<h3 class="gal-title">'.substr($prod['products'][$i]->details_en, 0,60) .'</h3>
									</article>
								</div>';
						}
				echo '</div>
					<div class="clearfix"></div>
				</div><!-- #main -->';
			}
		}
	?>

</div><!-- #content -->
