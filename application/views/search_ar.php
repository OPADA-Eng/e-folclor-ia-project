<style type="text/css">
	.tagcloud_f:before,
.tagcloud_f:after {
  content:"";
  display:table;
  line-height:0;
}

.tagcloud_f:after {
  clear:both;
}

.tagcloud_f a {
  float:right;
  background-color:#ccc;
  -webkit-transition:all 0.8s linear 0s;
  -moz-transition:all 0.8s linear 0s;
  -o-transition:all 0.8s linear 0s;
  transition:all 0.8s linear 0s;
  color:#555E6B;
  font-size:12px !important;
  margin:0 5px 5px 0;
  padding:3px 9px;
}

.tagcloud_f a:hover {
  background-color:#0088CC;
  color:#FFF;
  text-decoration:none;
}
.bg_username{
	background-color: #FFFFFF;
	padding: 10px;
	box-shadow: 0px 0px 8px #ccc;
}
.bg_type{
		background-color: #666666;
		padding: 5px;
		color:#FFFFFF;
	}
	.bg_type:hover{
		 -webkit-transition:all 0.8s linear 0s;
		  -moz-transition:all 0.8s linear 0s;
		  -o-transition:all 0.8s linear 0s;
		  transition:all 0.8s linear 0s;
		background-color: #222222;
		padding: 5px;
		color:#FFFFFF;
	}
</style>
<?php
function get_res($search_foloclor,$type)
{
	echo '<div class="row-fluid">
		<div class="bg_type">
			<p>نتائج الفولوكلور</p>
		</div>
	</div></br>';
	foreach ($search_foloclor as $key => $value_sf) { ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<a href="#" data-toggle="modal" data-target="#myModal<?php echo $value_sf['foloclor_contents']->f_id;?>"><p><?php echo $value_sf['foloclor_contents']->f_title_ar ;?></p></a>
			</div>
			<div class="panel-body">
				<div class="row">
					<div class="col-md-8 col-sm-8 col-xs-12">
						<div class="col-md-3 col-sm-3 col-xs-12">
							<img class="img-thumbnail" src="<?php echo base_url();?>uploads/images/thumbs/<?php echo $value_sf['foloclor_contents']->f_image;?>" />
						</div>
						<div class="col-md-9 col-sm-9 col-xs-12">
							<h4><?php echo  $value_sf['user_publisher']['username'] ;?></h4>
							<div class="post-time"><?php echo $value_sf['foloclor_contents']->date;?></div>
							<div class="ratings" style="float: none">
								<input class="star" type="radio" name="footer-latest-post-1" value="1" disabled="disabled"/>
								<input class="star" type="radio" name="footer-latest-post-1" value="2" disabled="disabled"/>
								<input class="star" type="radio" name="footer-latest-post-1" value="3" disabled="disabled"/>
								<input class="star" type="radio" name="footer-latest-post-1" value="4" disabled="disabled" checked="checked"/>
								<input class="star" type="radio" name="footer-latest-post-1" value="5" disabled="disabled"/>
							</div>
							<div class="modal fade" id="myModal<?php echo $value_sf['foloclor_contents']->f_id;?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
							  <div class="modal-dialog">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							        <h4 class="modal-title" id="myModalLabel"><p><?php echo $type ;?></p></h4>
							      </div>
							      <div class="modal-body">
							      	<div class="row-fluid">
								      	<div class="col-md-3 col-sm-3 col-xs-12">
											<img class="img-thumbnail" src="<?php echo base_url();?>uploads/images/thumbs/<?php echo $value_sf['foloclor_contents']->f_image;?>" />
										</div>
								        <div class="col-md-9 col-sm-9 col-xs-12">
											<h4><?php echo  $value_sf['user_publisher']['username'] ;?></h4>
											<div class="post-time"><?php echo $value_sf['foloclor_contents']->date;?></div>
											<div class="ratings" style="float: none">
												<input class="star" type="radio" name="footer-latest-post-1" value="1" disabled="disabled"/>
												<input class="star" type="radio" name="footer-latest-post-1" value="2" disabled="disabled"/>
												<input class="star" type="radio" name="footer-latest-post-1" value="3" disabled="disabled"/>
												<input class="star" type="radio" name="footer-latest-post-1" value="4" disabled="disabled" checked="checked"/>
												<input class="star" type="radio" name="footer-latest-post-1" value="5" disabled="disabled"/>
											</div>
										</div>
									</div>
									</br>
									<div class="row-fluid">
										<div class="well">
											<div class="bg_username"><b>المحتوى: </b></div></br>
											<?php echo $value_sf['foloclor_contents']->f_content_ar; ?>
										</div>
									</div>
									</br>
									<div class="row-fluid">
										<div class="well">
											<div class="bg_username"><b>Foloclor Historical Importance: </b></div></br>
											<?php echo $value_sf['foloclor_contents']->f_historical_importance_ar; ?>
										</div>
									</div>
									<hr /><p>الفيديو:</p><hr />
									<div class="row-fluid">
										<?php if($value_sf['foloclor_contents']->f_video != "NULL"){ ?>
										<video id="myVideo" controls>
											  <source id="mp4_src" src="<?php echo base_url();?>uploads/videos/<?php echo $value_sf['foloclor_contents']->f_video;?>" type="video/mp4">
											  Your browser does not support HTML5 video.
										</video>
										<br>
										<?php }?>
									</div>
									<hr /><p>ملف صوتي:</p><hr />
									<div class="row-fluid">
										<?php if($value_sf['foloclor_contents']->f_sound != "NULL"){ ?>
										<audio controls>
										   <source src="<?php echo base_url();?>uploads/sounds/<?php echo $value_sf['foloclor_contents']->f_sound;?>" type="audio/mpeg">
										   Your browser does not support the audio element.
										</audio>
										<?php } ?>
									</div>
									<hr /><p>التعليقات:</p><hr />
									<div class="row-fluid">
										<?php 
										if($value_sf['comments']){
											foreach ($value_sf['comments'] as $comment_value) { 
												if($comment_value['comment'] != FALSE){
													echo '<div class="bg_username"><b>BY:'.$comment_value['username'].' </b><hr />';
													echo '<h4>'.$comment_value['comment'].'</h4>';
													echo '<div class="text-muted">'.date('F j,Y, g:i a',$comment_value['date']).'</div></div></br>';
												}
											} 
										} else { echo '<p>Not found comment</p>'; }?>
										
									</div>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							      </div>
							    </div>
							  </div>
							</div>
						</div>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<?php foreach ($value_sf['tags'] as $key => $tag_value) { ?>
							<div class="tagcloud_f" style="float: right;">
								<?php echo'<a href="'.base_url().'/index.php/show_tag_content/'.$tag_value['ft_id'].'">'.$tag_value['f_tag_ar'].'</a>'; ?>
							</div>
						<?php } ?>
						
					</div>
				</div>
				<br />
				<div class="row-fluid">
					<div class="well">
						<div class="bg_username"><b>المحتوى: </b></div></br>
						<?php echo $value_sf['foloclor_contents']->f_content_ar; ?>
					</div>
				</div>
				
			</div>
		</div>
	<?php } 
}
function get_res_product($search_product,$type)
{
	echo '<div class="row-fluid">
		<div class="bg_type">
			<p>المنتجات</p>
		</div>
	</div></br>';
	foreach ($search_product as $key => $value_sf) { ?>
		<div class="panel panel-default">
			<div class="panel-heading">
				<a href="#" data-toggle="modal" data-target="#myModal<?php echo $value_sf['product_contents']->pc_id;?>"><p><?php echo $value_sf['product_contents']->product_name_ar ;?></p></a>
			</div>
			<div class="panel-body">
				<div class="latest-post clearfix">
					<div class="row">
						<div class="col-md-8 col-sm-8 col-xs-12">
							<div class="col-md-3 col-sm-3 col-xs-12">
								<img class="img-thumbnail" src="<?php echo base_url();?>uploads/images/thumbs/<?php echo $value_sf['product_contents']->image;?>" />
							</div>
							<div class="col-md-9 col-sm-9 col-xs-12">
								<h4><?php echo  $value_sf['user_publisher']['username'] ;?></h4>
								<div class="post-time"><?php echo $value_sf['product_contents']->date;?></div>
								<div class="ratings" style="float: none">
									<input class="star" type="radio" name="footer-latest-post-1" value="1" disabled="disabled"/>
									<input class="star" type="radio" name="footer-latest-post-1" value="2" disabled="disabled"/>
									<input class="star" type="radio" name="footer-latest-post-1" value="3" disabled="disabled"/>
									<input class="star" type="radio" name="footer-latest-post-1" value="4" disabled="disabled" checked="checked"/>
									<input class="star" type="radio" name="footer-latest-post-1" value="5" disabled="disabled"/>
								</div>
								
							</div>
						</div>
					</div>
					<br />
					<div class="row-fluid">
						<div class="well">
							<div class="bg_username"><b>Foloclor Contents: </b></div></br>
							<?php echo $value_sf['product_contents']->details_ar; ?>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	<?php } 
}
?>
<div id="content" class="container">
	<div id="main" class="row-fluid">
		<div class="row">
			<div class="col-md-8 col-sm-8 col-xs-12">
			<?php if(isset($mode_foloclor)&&($mode_foloclor == "search_foloclor")) { 
				get_res($search_foloclor,$type);
			 } 
			 if(isset($mode_product)&&($mode_product == "search_product")){
			 	get_res_product($search_product,$type);
			 }
			 if(isset($mode)&&($mode == 'error')){ ?>
			 	<?php if(!isset($mode_foloclor)) { 
					echo '<div class="row-fluid">
						<div class="bg_type">
							<p>نتائج الفولوكلور</p>
					</div></div></br>
					<div class="panel panel-default">
			 	 	<div class="panel-heading">
			 	 		
			 	 	</div>
			 	 	<div class="panel-body">
			 	 		<p>المحتوى المطلوب غير موجود في الفولوكلور..</p>
			 	 	</div>
			 	 </div>';
				} ?>  
			 	 <?php if(!isset($mode_product)) { 
					echo '<div class="row-fluid">
						<div class="bg_type">
							<p>المنتجات</p>
						</div>
					</div></br>
					<div class="panel panel-default">
			 	 	<div class="panel-heading">
			 	 		
			 	 	</div>
			 	 	<div class="panel-body">
			 	 		<p>المحتوى المطلوب غير موجود في المنتجات..</p>
			 	 	</div>
			 	 </div>';
				} ?>  
			 <?php } ?>
			</div>
		</div>
		
	</div><!-- #main -->
</div><!-- #content -->
