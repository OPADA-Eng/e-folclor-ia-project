


<style>
	.boreder_btn {
		position: relative;
		float:right;
	}
	.boreder_btn a{
		margin-top: 100px;
		text-decoration: none;
		color:#666666;
		border: 1px solid #ccc;
		padding: 10px;
		border-radius: 20px;
		
	}
	.boreder_btn a:hover{
		margin-top: 100px;
		text-decoration: none;
		color:#666666;
		border: 1px solid #666666;
		padding: 10px;
		border-radius: 20px;
		box-shadow: 0px 0px 8px #666666;
	
	}

</style>

<script type='text/javascript' src="<?php echo base_url(); ?>js/template/bootstrap-typeahead.js"></script>

<script>
	var users = <?php echo $available_users; ?> ;
	var confirm_window = new BootstrapDialog();;
	function delete_msg (id,box) {
	  $.ajax({
   // see the (*)
	   url: "<?php echo base_url(); ?>index.php/messages/delete_message_"+box,
	   type: "POST",
	   data: {'message_id':id},
	   success: function(response) {
	   	var dialogInstance2 = new BootstrapDialog();
	   	if(response == 'success'){
	    	//$('#mess').html('<p style="background:#7abd39; color:#000;">Message sent successfully ! press close</p>');
	    	dialogInstance2.setMessage('Message deleted Successfully !');
	    	dialogInstance2.setType(BootstrapDialog.TYPE_SUCCESS);
	    	$('#'+id+box).remove();
	    	
	    	
	    }
	    else{
	    	dialogInstance2.setMessage(response/*'Message has not deleted  :('*/);
	    	dialogInstance2.setType(BootstrapDialog.TYPE_WARNING);
	    }
	    dialogInstance2.open();
		},
		error: function (xhr, ajaxOptions, thrownError) {
			var dialogInstance2 = new BootstrapDialog();
	        dialogInstance2.setMessage(xhr.status+" "+thrownError );
	        dialogInstance2.setType(BootstrapDialog.TYPE_DANGER);
	        dialogInstance2.open();
	    }
	});
	}
	function confirm_delete (id,box) {
		confrim_window = new BootstrapDialog(
			{message: 'Are you sure ?',
		 	 title	: "Confirmation Message",
		 	 type	: BootstrapDialog.TYPE_INFO,
		 	 buttons:[{
		                label: 'Yes',
		                cssClass: 'btn-primary',
		                action: function(dialogItself){
		                    delete_msg(id,box);
		                    dialogItself.close();
		                }
	            	}, {
		                label: 'No',
		                action: function(dialogItself){
		                    dialogItself.close();
		                }
	            	}]
            }).open();
		
	  
	}
	
	
</script>

		<article class="page">
			<header class="entry-header">
				<h1 class="entry-title"><span>صندوق الرسائل</span></h1>
			</header><!-- .entry-header -->
			<div class="entry-content">
				
				<div class="boreder_btn"><a id="newmsg" href="javascript:void(0);" data-toggle="modal" data-target="#messagebox">رسالة جديدة</a><br></div>
					
				<div class="tabbable">
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#inbox" data-toggle="tab">صندوق الراسائل</a></li>
                            <li><a href="#sentbox" data-toggle="tab">الرسائل المرسلة</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="inbox">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>من</th>
                                            <th>الرسالة</th>
                                            <th>التاريخ</th>
                                            <th class="glyphicon glyphicon-flag">الفعل</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	<?php 
										if(empty($inbox_messages)){
											echo "لا يوجد رسائل في الصندوق";
										}
										else {
											//var_dump($messages); die();
											foreach ($inbox_messages as $key => $msg) {
												echo '<tr class="not-read" id="'.$msg->msg_id.'in">
														<td><input type="checkbox" /></td>
														<td>'.$msg->first_name." ".$msg->last_name.'</td>
														<td><a href="#">';
														if (strlen($msg->msg_text) > 70)echo substr($msg->msg_text, 0,70).'...';
														else echo $msg->msg_text;
													echo '</a></td><td>'.date("F j, Y, g:i a",$msg->msg_date).'</td>
														<td class="glyphicon glyphicon-star-empty"><a href="javascript:void(0);" id="drop" onclick="confirm_delete('.$msg->msg_id.',\'in\');"><img src="'.base_url().'images/Trash-Delete-icon.png" /></a></td>
													  </tr>';
											}
										}
										?>
                                    </tbody>
                                </table>
                            </div>
                            <div class="tab-pane" id="sentbox">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>إلى</th>
                                            <th>الرسالة</th>
                                            <th>في</th>
                                            <th class="glyphicon glyphicon-flag">الفعل</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php 
										if(empty($outbox_messages)){
											echo "no messages in this box";
										}
										else {
											//var_dump($outbox_messages); die();
											$messID = $outbox_messages[0]->msg_id;
											$from = $outbox_messages[0]->first_name." ".$outbox_messages[0]->last_name;;
											for ($i=1; $i < sizeof($outbox_messages); $i++) { 
												if($messID != $outbox_messages[$i]->msg_id){
													echo '<tr class="not-read" id="'.$outbox_messages[$i-1]->msg_id.'out">
														<td><input type="checkbox" /></td>
														<td>'.$from.'</td>
														<td><a href="#">';
														if (strlen($outbox_messages[$i-1]->msg_text) > 70)echo substr($outbox_messages[$i-1]->msg_text, 0,70).'...';
														else echo $outbox_messages[$i-1]->msg_text;
													echo '</a></td><td>'.date("F j, Y, g:i a",$outbox_messages[$i-1]->msg_date).'</td>
														<td class="glyphicon glyphicon-star-empty"><a href="javascript:void(0);" id="drop" onclick="confirm_delete('.$outbox_messages[$i-1]->msg_id.',\'out\');"><img src="'.base_url().'images/Trash-Delete-icon.png" /></a></td>
													  </tr>';
													$messID = $outbox_messages[$i]->msg_id;
													$from = $outbox_messages[$i]->first_name." ".$outbox_messages[$i]->last_name;
													if($i == 21)
													die();
												}
												else {
													$from .= ",".$outbox_messages[$i]->first_name." ".$outbox_messages[$i]->last_name;
												}
											}
											echo '<tr class="not-read" id="'.$outbox_messages[sizeof($outbox_messages)-1]->msg_id.'out">
													<td><input type="checkbox" /></td>
													<td>'.$from.'</td>
													<td><a href="#">';
													if (strlen($outbox_messages[sizeof($outbox_messages)-1]->msg_text) > 70)echo substr($outbox_messages[sizeof($outbox_messages)-1]->msg_text, 0,70).'...';
													else echo $outbox_messages[sizeof($outbox_messages)-1]->msg_text;
											echo '</a></td><td>'.date("F j, Y, g:i a",$outbox_messages[sizeof($outbox_messages)-1]->msg_date).'</td>
													<td class="glyphicon glyphicon-star-empty"><a href="javascript:void(0);" id="drop" onclick="confirm_delete('.$outbox_messages[sizeof($outbox_messages)-1]->msg_id.',\'out\');"><img src="'.base_url().'images/Trash-Delete-icon.png" /></a></td>
												  </tr>';
										}
										?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
				
				
				
						
					
			</div><!-- .entry-content -->
		</article><!-- #post-429 -->
		<div class="clearfix"></div>

<script type="text/javascript">
	$(document).ready(function () {
	  	$("[data-dismiss=modal]").trigger({ type: "click" });
	  	
	});
	$('#newmsg').click(function(){
		$('#user').val('');
		$('#msg').val('');
		$('#selected_users').html('');
		$('#res').html('');
	});
	function send_msg () {
	  
		var obj = $("#mess"), // (*) references the current object/form each time
	   	data = {};
	   userIds = [];
	  obj.find('[name]').each(function(index, value) {
	   var obj = $(this),
	    name = obj.attr('name'),
	    value = obj.val();
	    if(name == "to_user_id"){
	    	userIds.push(value);
	    }
	    else
	    	if(name != "user_id") data[name] = value;
	  });
	  data['to_user_id'] = userIds;
	  console.log(data);
	  $.ajax({
   // see the (*)
	   url: "<?php echo base_url(); ?>index.php/messages/send_message",
	   type: "POST",
	   data: data,
	   success: function(response) {
	   	if(response == 'success'){
	    	//$('#mess').html('<p style="background:#7abd39; color:#000;">Message sent successfully ! press close</p>');
	    	$("[data-dismiss=modal]").trigger({ type: "click" });
	    	var dialogInstance2 = new BootstrapDialog();
	    	dialogInstance2.setMessage('Message sent Successfully !');
	    	dialogInstance2.setType(BootstrapDialog.TYPE_SUCCESS);
	    	dialogInstance2.open();
	    }
	    else{
	    	$('#res').html('<center style="background:#DD3200;color:#000; border-radius: 25px;">Message has not been sent ! please try again<center>');
	    }
	   }
	  });
		
	}
	
	function displayRes(item, val, text) {
	    var id = val;
	    var new_user = '<div style="display:inline-block;"><a style="color:red;" onclick="removeparent(this);" href="javascript:void(0)">×</a>'+text+'<input type="hidden" name="to_user_id" value="'+id+'" /></div>';
	    $('#selected_users').append(new_user);
	    $('#user').val('');
	}
	function removeparent(e){
		e.parentNode.remove();
		console.log(e.parentNode);
	}
	
	$(function () {
		
		mysrc =[];
		for (var i = 0; i < users.length; i++) {
			mysrc.push({id: users[i].id , name: users[i].first_name+" "+users[i].last_name});
		}
		
	    $('#user').typeahead({
	         source:mysrc,
	        itemSelected: displayRes 
	    });
	});
	
	
	
	
	
	
</script>




<div class="modal fade" id="messagebox" tabindex="-1" role="dialog" aria-labelledby="messagebox" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
        <h4 class="modal-title">إرسال رسالة</h4>
      </div>
      <div class="modal-body" id="mess">
      				<div id="res"></div><br>
        			<input type="text" id="user" required="" name="user_id" placeholder="write a name"  /><br />
        			<div id="selected_users"></div>
        			<textarea cols="100" rows="10" name="message" id="msg"></textarea>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">إغلاق</button>
        <button type="button" class="btn btn-primary" id="sendmsg" onclick="send_msg();">إرسال</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->