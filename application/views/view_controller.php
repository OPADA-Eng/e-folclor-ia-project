<?php
$this->load->helper('language');


$this->load->view('template/header_'.get_current_lang_letter());
$this->load->view($view_page.'_'.get_current_lang_letter());
$this->load->view('template/footer_'.get_current_lang_letter());
