<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-9 col-sm-9 col-xs-12">
	
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>images/show_user.png"  />Show Users</p>
            </div>
            <div class="panel-body">
            	<p>Below is a list of the users.</p>
				
              <div class="table-responsive">
                    <table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
                            <tr>
								<th>First Name</th>
								<th>Last Name</th>
								<th>Email</th>
								<th>Groups</th>
								<th>Status</th>
							</tr>
                        </thead>
						
						<?php foreach ($users as $user):?>
						<tbody>
							<tr class="gradeX">
								<td><?php echo $user->first_name;?></td>
								<td><?php echo $user->last_name;?></td>
								<td><?php echo $user->email;?></td>
								<td><?php echo ($user->active) ? anchor("auth/deactivate/".$user->id, 'Active') : anchor("auth/activate/". $user->id, 'Inactive');?></td>
								<td><?php echo anchor("auth/edit_user/".$user->id, 'Edit') ;?></td>
							</tr>
						</tbody>
						<?php endforeach;?>
					</table>
				<p><?php echo anchor('auth/create_user', 'Create a new user')?>  <?php //echo anchor('account/create_group', lang('index_create_group_link'))?></p>
				<div id="infoMessage"><?php echo $message;?></div>
                <!-- On rows -->
                </div>
            </div>
        </div>
</div>
<div class="col-md-1 col-sm-1 col-xs-12"></div>
