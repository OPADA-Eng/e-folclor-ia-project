<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p> <img src="<?php echo base_url(); ?>images/delete_user.png"  />Deactivate User</p>
		</div>
		<div class="panel-body">
		  <p>Are you sure you want to deactivate the user '<?php echo $user->username; ?>'</p>
		  <?php echo form_open("auth/deactivate/".$user->id);?>
			<div class="radio">
                  <label>
                    <input type="radio" name="confirm" value="no" checked="checked">
                    <p>لا</p>
                  </label>
             </div>
             <div class="radio">
                  <label>
                    <input type="radio" name="confirm" value="yes" >
                    <p>نعم</p>
                  </label>
             </div>
		 
		
		  <?php echo form_hidden($csrf); ?>
		  <?php echo form_hidden(array('id'=>$user->id)); ?>
		
		  <p><?php echo form_submit('submit', 'Ok','class="btn btn-lg btn-primary btn-block" ');?></p>
		
		  <?php echo form_close();?>
		</div>
	</div>
</div>

<h1></h1>

	
