



<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p> <img src="<?php echo base_url(); ?>images/mypc_add.png"/>Create User</p>
		</div>
		<div class="panel-body">
			<p>Please enter the users information below.</p>
			<?php echo form_open("auth/create_user");?>
		      <p>
		            First Name: <br />
		            <?php echo form_input($first_name,set_value('first name'),'class="form-control font_input" placeholder="first name" required=""');?>
		      </p>
		
		      <p>
		            Last Name: <br />
		            <?php echo form_input($last_name,set_value('last name'),'class="form-control font_input" placeholder="last name" required=""');?>
		      </p>
		
		      <p>
		            Company Name: <br />
		            <?php echo form_input($company,set_value('company'),'class="form-control font_input" placeholder="company" required=""');?>
		            
		      </p>
		
		      <p>
		            Email: <br />
		            <?php echo form_input($email,set_value('email'),'class="form-control font_input" placeholder="email" required=""');?>
		          
		      </p>
			  <p>
		            Phone: <br />
            		<?php echo form_input($phone1,'','class="font_input" required=""');?>-<?php echo form_input($phone2,'','class="font_input" required=""');?>-<?php echo form_input($phone3,'','class="font_input" required=""');?>
			  </p>
		      <p>
		            Password: <br />
		            <?php echo form_input($password,set_value('password'),'class="form-control font_input" placeholder="password" required=""');?>
		      </p>
		
		      <p>
		            Confirm Password: <br />
		            <?php echo form_input($password_confirm,set_value('password confirm'),'class="form-control font_input" placeholder="password confirm" required=""');?>
		           
		      </p>
		
			  <p><?php echo form_submit('submit', 'Create User','class="btn btn-lg btn-primary btn-block"');?></p>
		      
			  <div id="infoMessage"><?php echo $message;?></div>
		<?php echo form_close();?>
		</div>
	</div>
</div>
<div class="col-md-2 col-sm-2 col-xs-12"></div>
