<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/edit_user.png" />Edit User</p>
		</div>
		<div class="panel-body">
			<p>Please enter the users information below.</p>
			<?php echo form_open(current_url());?>

			      <p>
			            First Name: <br />
			            <?php echo form_input($first_name,set_value('first name'),'class="form-control font_input" required=""');?>
			      </p>
			
			      <p>
			            Last Name: <br />
			            <?php echo form_input($last_name,set_value('last name'),'class="form-control font_input" required=""');?>
			      </p>
			
			      <p>
			            Company Name: <br />
			            <?php echo form_input($company,set_value('company'),'class="form-control font_input" required=""');?>
			      </p>
			
			      <p>
			            Phone: <br />
			            <?php echo form_input($phone1,'','class="font_input" required=""');?>-<?php echo form_input($phone2,'','class="font_input" required=""');?>-<?php echo form_input($phone3,'','class="font_input" required=""');?>
			      </p>
			
			      <p>
			            Password: (if changing password)<br />
			            <?php echo form_input($password,set_value('password'),'class="form-control font_input" required=""');?>
			      </p>
			
			      <p>
			            Confirm Password: (if changing password)<br />
			            <?php echo form_input($password_confirm,set_value('password confirm'),'class="form-control font_input" required=""');?>
			      </p>
			
			
			      <?php echo form_hidden('id', $user->id);?>
			      <?php echo form_hidden($csrf); ?>
				   <p><?php echo form_submit('submit', 'Save User','class="btn btn-lg btn-primary btn-block"');?></p>
			
			
			<?php echo form_close();?>
			<div id="infoMessage"><?php echo $message;?></div>
		</div>
	</div>
</div>




