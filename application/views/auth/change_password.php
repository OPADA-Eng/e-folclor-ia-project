<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-8">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/change_pass.png" />Change Password</p>
		</div>
		<div class="panel-body">
			<?php echo form_open("auth/change_password");?>

			      <p>Old Password:<br />
			      <?php echo form_input($old_password,set_value('Old Password'),'class="form-control font_input" placeholder="Old Password" required=""');?>
			      </p>
			      
			      <p>New Password (at least <?php echo $min_password_length;?> characters long):<br />
			      <?php echo form_input($new_password,set_value('New Password'),'class="form-control font_input" placeholder="New Password" required=""');?>
			      </p>
			      
			      <p>Confirm New Password:<br />
			      <?php echo form_input($new_password_confirm,set_value('New Password Confirm'),'class="form-control font_input" placeholder="New Password Confirm" required=""');?>
			      </p>
			      
			      <?php echo form_input($user_id);?>
			      <p><?php echo form_submit('submit', 'Change','class="btn btn-lg btn-primary btn-block font_input"');?></p>
			      
			<?php echo form_close();?>
			<div id="infoMessage"><?php echo $message;?></div>
		</div>
	</div>
</div>




