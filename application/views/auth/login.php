<html>
	<head>
		<title>CPanel Photoshop World</title>
		<meta charset="UTF-8" />
		<link rel="stylesheet" media="screen" href="<?php echo base_url();?>css/cp/bootstrap.css" />
		<link rel="stylesheet" media="screen" href="<?php echo base_url();?>css/cp/style-cpanel.css" />
	</head>
	<body style="background-color:#d0cece;">
		 <header class="navbar color_header navbar-inverse navbar-fixed-top navbar-gray ">
            
         </header>
         <div class="row">
         	<div class="col-md-4 col-sm-4 col-xs-12"></div>
         	<div class="col-md-4 col-sm-4 col-xs-12">
         		 <br/>
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <p>Login</p>
                    </div>
                    <div class="panel-body">
                    	<p>Please login with your email/username and password below.</p>
						
						<?php echo form_open("auth/login/".$action);?>
						  <p>
						    <label for="identity">Email/Username:</label>
						    <?php echo form_input('identity',set_value('identity'),'class="form-control font_input" placeholder="identity"   required=""');?>
						  </p>
						  <p>
						    <label for="identity">Password:</label>
						    <?php echo form_password('password','','class="form-control font_input" placeholder="password"   required=""');?>
						  </p>
						
						  <p>
						    <label for="remember">Remember Me:</label>
						    <?php echo form_checkbox('remember', '1', FALSE, 'id="remember"');?>
						  </p>
						
						
						  <p><?php echo form_submit('submit', 'Login','class="btn btn-lg btn-primary btn-block font_input"');?></p>
						
						<?php echo form_close();?>
						
						<p><a href="<?php echo base_url();?>index.php/auth/forgot_password">Forgot your password?</a></p>
						<div id="infoMessage" style="color: #CC0000"><?php echo $message;?></div>
                    </div>
                </div>
         	</div>
         	<div class="col-md-4 col-sm-4 col-xs-12"></div>
         </div>
         <script src="<?php echo base_url(); ?>js/cp/jquery-1.10.2.js"></script>
         <script src="<?php echo base_url(); ?>js/cp/bootstrap.js"></script>
        
	</body>
</html>


