<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8" />
        <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel.css">
        <link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel_style.css">
        <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
           
    </head>
 
    <body>
    <?php  
        $foloclor_id = '1'; 
    ?>
    <div class="tuto-cnt">
        <div class="rate-ex1-cnt">
            <div id="1" class="rate-btn-1 rate-btn"></div>
            <div id="2" class="rate-btn-2 rate-btn"></div>
            <div id="3" class="rate-btn-3 rate-btn"></div>
            <div id="4" class="rate-btn-4 rate-btn"></div>
            <div id="5" class="rate-btn-5 rate-btn"></div>
        </div>
        <div class="box-result-cnt">
            <?php if(isset($records)) : foreach ($records as $row):?>
			<hr>
            <h3>The rating is at <strong id="values"><?php echo $row->total_voting; ?></strong></h3>
            <hr>
            <?php $foloclor_id = $row->f_id;?>
      		<?php endforeach; endif; ?>      
        </div><!-- /rate-result-cnt -->
    </div><!-- /tuto-cnt -->
    <script>
        // rating script
        $(function(){ 
            $('.rate-btn').hover(function(){
                $('.rate-btn').removeClass('rate-btn-hover');
                var therate = $(this).attr('id');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-hover');
                };
            });
                            
            $('.rate-btn').click(function(){    
                var therate = $(this).attr('id');
                var dataRate = 'act=rate&foloclor_id=<?php echo $foloclor_id; ?>&rate='+therate; //
                $('.rate-btn').removeClass('rate-btn-active');
                for (var i = therate; i >= 0; i--) {
                    $('.rate-btn-'+i).addClass('rate-btn-active');
                };
                $.ajax({
                    type : "POST",
                    url : "<?php echo base_url(); ?>index.php/foloclor_voting/vote",
                    data: dataRate,
                    success:function(responce){
					//	alert(responce);
						var parsed = JSON.parse(responce);
					//	$("#times").html(parsed.rate_times);
						$("#values").html(parsed.rate_value);
						//$("#avg").html(parsed.rate_bg);
					}
                });
            });
        });
    </script>

</body>
</html>