<!DOCTYPE html>
<?php
$this->load->helper('functions');
$nessacery_header_data = get_nessacery_header_content();
$foloclor_articles = $nessacery_header_data['foloclor_articles'];
$p_categories = $nessacery_header_data['p_categories'];
$f_categories = $nessacery_header_data['f_categories']; 
$latest_newsbar = $nessacery_header_data['latest_newsbar']
?>
<!--[if IE 7]>
<html class="ie ie7" lang="en-US">
<![endif]-->

<!--[if IE 8]>
<html class="ie ie8" lang="en-US">
<![endif]-->

<!--[if IE 9]>
<html class="ie ie9" lang="en-US">
<![endif]-->

<!--[if !(IE 7) | !(IE 8) | !(IE 9)  ]><!-->
<html lang="en-US">
<!--<![endif]-->

<head>
<title>e-foloclor</title>
<meta charset="UTF-8" />
<link rel="shortcut icon" href="<?php echo base_url(); ?>images/favicon.png" title="Favicon"/>
<meta name="viewport" content="width=device-width" />

<link rel='stylesheet' id='magz-style-css'  href='<?php echo base_url(); ?>css/css_template_en/style.css' type='text/css' media='all' />
<link rel='stylesheet' id='swipemenu-css'  href='<?php echo base_url(); ?>css/css_template_en/swipemenu.css' type='text/css' media='all' />
<link rel='stylesheet' id='flexslider-css'  href='<?php echo base_url(); ?>css/css_template_en/flexslider.css' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-css'  href='<?php echo base_url(); ?>css/css_template_en/bootstrap.css' type='text/css' media='all' />
<link rel='stylesheet' id='bootstrap-responsive-css'  href='<?php echo base_url(); ?>css/css_template_en/bootstrap-responsive.css' type='text/css' media='all' />
<link rel='stylesheet' id='simplyscroll-css'  href='<?php echo base_url(); ?>css/css_template_en/jquery.simplyscroll.css' type='text/css' media='all' />
<link rel='stylesheet' id='jPages-css'  href='<?php echo base_url(); ?>css/css_template_en/jPages.css' type='text/css' media='all' />
<link rel='stylesheet' id='rating-css'  href='<?php echo base_url(); ?>css/css_template_en/jquery.rating.css' type='text/css' media='all' />
<link rel='stylesheet' id='ie-styles-css'  href='<?php echo base_url(); ?>css/css_template_en/ie.css' type='text/css' media='all' />
<link rel='stylesheet' id='Roboto-css'  href='http://fonts.googleapis.com/css?family=Roboto' type='text/css' media='all' />
<link rel='stylesheet' id='pretty-photo-css'  href='<?php echo base_url(); ?>css/css_template_en/prettyPhoto.css' type='text/css' media='screen' />
<!-- Slider -->
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/slider/style.css" />
<script type="text/javascript" src="<?php echo base_url(); ?>js/slider/jquery.js"></script>
<!-- / end Slider -->

<!-- bootstap modal -->
<link rel="stylesheet" href="<?php echo base_url();?>css/css_template_en/bootstrap-dialog.css"  />

<script type='text/javascript' src="<?php echo base_url(); ?>js/template/jquery-1.10.2.min.js"></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/html5.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/bootstrap.min.js'></script>
<script type='text/javascript' src="<?php echo base_url(); ?>js/template/bootstrap-typeahead.js"></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.flexslider.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.flexslider.init.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.bxslider.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.bxslider.init.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.rating.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.idTabs.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.simplyscroll.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/fluidvids.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jPages.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.sidr.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.touchSwipe.min.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.prettyPhoto.js'></script>
<script type='text/javascript' src='<?php echo base_url(); ?>js/template/custom.js'></script>
<!-- mhdprogr -->
<!--
	<link rel='stylesheet' href='<?php echo base_url(); ?>css/css_template_en/jquery-ui.css' type='text/css' media='all' />
<script type="text/javascript" src="<?php echo base_url(); ?>js/template/jquery-1.10.2.js"> </script>
<script type='text/javascript' src="<?php echo base_url(); ?>js/template/jquery-ui.js"></script>
-->
<script type="text/javascript" src="<?php echo base_url(); ?>js/template/bootstrap-dialog.js"> </script>
<script type='text/javascript' src="<?php echo base_url(); ?>js/template/jquery-1.10.2.min.js"></script>
<style>
.field:hover{
  background-color: #1C6093;
  -webkit-transition:all 0.8s linear 0s;
  -moz-transition:all 0.8s linear 0s;
  -o-transition:all 0.8s linear 0s;
  transition:all 0.8s linear 0s;
  color:#FFFFFF;
  padding: 10px;
  box-shadow: 0px 0px 8px #1C6093;
}

.field{
	  background-color: #428BCA;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}

</style>

<body>

<div id="page">

	<header id="header" class="container">
		<div id="mast-head">
			<div id="logo">
			<a href="index.html" title="Magazine" rel="home"><img src="<?php echo base_url(); ?>images/logo.png" alt="Magazine" /></a>
			</div>
		</div>

				
        <nav class="navbar navbar-inverse clearfix nobot">
						
			<a id="responsive-menu-button" href="#swipe-menu">
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>
			  <span class="icon-bar"></span>		
			</a>	

            <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
            <div class="nav-collapse" id="swipe-menu-responsive">

			<ul class="nav">
				
				<li>
				<span id="close-menu">
					<a href="#" class="close-this-menu">Close</a>
						<script type="text/javascript">
							jQuery('a.sidr-class-close-this-menu').click(function(){
								jQuery('div.sidr').css({
									'right': '-476px'
								});
								jQuery('body').css({
								'right': '0'
								});							
							});
						</script>
					
				</span>
				</li>
								
				<li><a href="<?php echo base_url();?>/index.php/main"><img src="<?php echo base_url(); ?>images/home.png" alt="Magazine"></a></li>
				<li ><?php echo anchor('main','Home') ?>
					
				<li><?php echo anchor('about','About') ?></li>
				<li class="dropdown"><?php echo anchor('foloclor','Folclors') ?>
					<ul class="sub-menu">

						<?php
						foreach ($f_categories as $key => $f_cat) {
							echo '<li>'.anchor('foloclor/category/'.$f_cat->fc_id,$f_cat->fc_name_en).'</li>';
						}
						?>

						
					</ul>
				</li>
				<li class="dropdown"><?php echo anchor('product','Products') ?>
					<ul class="sub-menu">

						<?php
						foreach ($p_categories as $key => $p_cat) {
							echo '<li>'.anchor('product/category/'.$p_cat->pcat_id,$p_cat->cat_en).'</a></li>';
						}
						?>
				
					</ul>
				</li>
				<li><?php echo anchor('contact_us','Contact'); ?></li>
				<?php  $this->load->helper('ion_auth');
				if (is_logged_in()){
					echo  '<li>'.anchor('auth/logout','logout').'</li>';
					echo  '<li> <a id ="cart" href="#" > My Cart</a>   </li>';
					echo  '<li>'.anchor('auth','my account').'</li>';
				}else{ 
					echo  '<li>'.anchor('auth/register','register').'</li>';
					echo  '<li>'.anchor('auth/login','login').'</li>';
				}
				?>
			</ul>

            </div><!--/.nav-collapse -->
			<div style="padding-top: 14px;display: inline-block;">
				<?php echo anchor('langSwitch/switchLanguage/english/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3),'<img src="'.base_url().'images/english_lang.png" id="style_lang" />'); ?>
				<?php echo anchor('langSwitch/switchLanguage/arabic/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3),'<img src="'.base_url().'images/arabic_lang.gif" id="style_lang" />'); ?>
			</div>
        </nav><!-- /.navbar -->
			
	</header><!-- #masthead -->

	<div id="headline" class="container">
	<div class="row-fluid">
		<?php $item1 = $foloclor_articles[rand(0, sizeof($foloclor_articles)-1)];//var_dump($item1); die(); ?>
		<div class="span3">
			<article class="post">
				<a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
				<?php if($item1['item_data']['f_image']!="NULL"){

					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/<?php echo $item1['item_data']['f_image']; ?>" class="thumb" alt="photodune-2043745-college-student-s" /><?php

				}
				else if($item1['item_data']['f_vedio']!="NULL"){
					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/movies-icon.png" class="thumb"/><?php
				}
				else if ($item1['item_data']['f_sound']!="NULL"){
					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/sound_thumb.png" class="thumb"/><?php
				}
				?>
				
				</a>
				<div class="entry">
					<h3><a href="#" rel="bookmark"><?php echo $item1['item_data']['f_title_en']?></a></h3>
					<p><?php echo $item1['item_data']['date'] ;?></p>
				</div>
				<div class="clearfix"></div>
			</article>
		</div>
		
		<?php $item1 = $foloclor_articles[rand(0, sizeof($foloclor_articles)-1)];//var_dump($item1); die(); ?>
		<div class="span3">
			<article class="post">
				<a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
				<?php if($item1['item_data']['f_image']!="NULL"){
					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/<?php echo $item1['item_data']['f_image']; ?>" class="thumb" alt="photodune-2043745-college-student-s" /><?php
				}
				else if($item1['item_data']['f_vedio']!="NULL"){
					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/movies-icon.png" class="thumb"/><?php
				}
				else if ($item1['item_data']['f_sound']!="NULL"){
					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/sound_thumb.png" class="thumb"/><?php
				}
				?>
				
				</a>
				<div class="entry">
					<h3><a href="#" rel="bookmark"><?php echo $item1['item_data']['f_title_en']?></a></h3>
					<p><?php echo $item1['item_data']['date'] ;?></p>
				</div>
				<div class="clearfix"></div>
			</article>
		</div>

		<?php $item1 = $foloclor_articles[rand(0, sizeof($foloclor_articles)-1)];//var_dump($item1); die(); ?>
		<div class="span3">
			<article class="post">
				<a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
				<?php if($item1['item_data']['f_image']!="NULL"){
					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/<?php echo $item1['item_data']['f_image']; ?>" class="thumb" alt="photodune-2043745-college-student-s" /><?php

				}
				else if($item1['item_data']['f_vedio']!="NULL"){
					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/movies-icon.png" class="thumb"/><?php
				}
				else if ($item1['item_data']['f_sound']!="NULL"){
					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/sound_thumb.png" class="thumb"/><?php
				}
				?>
				
				</a>
				<div class="entry">
					<h3><a href="#" rel="bookmark"><?php echo $item1['item_data']['f_title_en']?></a></h3>
					<p><?php echo $item1['item_data']['date'] ;?></p>
				</div>
				<div class="clearfix"></div>
			</article>
		</div>

		<?php $item1 = $foloclor_articles[rand(0, sizeof($foloclor_articles)-1)];//var_dump($item1); die(); ?>
		<div class="span3">
			<article class="post">
				<a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
				<?php if($item1['item_data']['f_image']!="NULL"){

					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/<?php echo $item1['item_data']['f_image']; ?>" class="thumb" alt="photodune-2043745-college-student-s" /><?php

				}
				else if($item1['item_data']['f_vedio']!="NULL"){
					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/movies-icon.png" class="thumb"/><?php
				}
				else if ($item1['item_data']['f_sound']!="NULL"){
					?><img width="225" height="136" src="<?php echo base_url(); ?>uploads/images/91_55/sound_thumb.png" class="thumb"/><?php
				}
				?>
				
				</a>
				<div class="entry">
					<h3><a href="#" rel="bookmark"><?php echo $item1['item_data']['f_title_en']?></a></h3>
					<p><?php echo $item1['item_data']['date'] ;?></p>
				</div>
				<div class="clearfix"></div>
			</article>
		</div>
		
	</div>
	</div>

	<div id="intr" class="container">
		<div class="row-fluid">
			<div class="brnews span9">
				<h3>Breaking News</h3>
				<ul id="scroller">
					<?php 
						foreach ($latest_newsbar as $key => $news) {
							echo '<li><p><a href="#" title="Permalink to Lectus non rutrum pulvinar urna leo dignissim lorem" rel="bookmark"><span class="title">'.$news->title_en.'</span>'.$news->details_en.'</a></p></li>';
						}
					?>
				</ul>
			</div>
		
		<div class="search span3"><div class="offset1">
			<div id="searchform">
				<?php echo form_open("search/index");?>
				<p><input type="text" value="Search here..." onfocus="if ( this.value == 'Search here...' ) { this.value = ''; }" onblur="if ( this.value == '' ) { this.value = 'Search here...'; }" name="keyword" id="s" />
				<input type="submit" name="submit" id="searchsubmit" value="Search" /></p>
				<?php echo form_close();?> 
			</div>
			
			
		</div></div>
		</div>
	</div>
<script>
	$('#cart').click(function(){ 
			show_cart();
	});
	
	function show_cart(){
		$.ajax({
			 url: "<?php echo base_url();?>index.php/accounting/cart",
			 type: 'POST',
			 data: '',
			 success : function(data){
			 	console.log(data);
			 	show_dialoge(data);
			 },
			 error : function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			 }
			});
	}
	
	function show_dialoge (html) {
	 BootstrapDialog.show({
            title: 'My Cart',
            message: $(html),
            buttons: [{
                label: 'update cart',
                cssClass: 'btn-primary',
                hotkey: 13, // Enter.
                action: function() {
                   /*$('#in_cart -> ').find('[name]').each(function(){
                    	//console.log($(this).attr('id'));
                    	var obj = $(this);
	   					 name = obj.attr('name');
	   					 alert(name);
                     });*/
                    data={};
                    var obj =$("#in_cart");
                    obj.find('[type]').each(function(index, value) {
   					// console.log(value);
				    var objj = $(this),
				    name = objj.attr('name'),
				    value = objj.val();
				    if ($(this).attr('type')=='text')
				    	data[index]=[name,value];
					
				  }); 
                    var formData={'in_cart' : data };
                    console.log(formData);
                     $.ajax({
                    	url:"<?php  echo base_url();?>index.php/accounting/update_cart",
                    	method:'GET',
                    	data:formData,
                    	success:function(data){
                    		console.log(data);
                    		showMessage(data);
                    	},
                    	error :function(xhr,textStatus,errorThrown){
                    		alert('request failed: '+ errorThrown);
                    	}	
                    });
                }
            },{ label :'delete cart',
            	 action :function(dialog){
            	 	 $.ajax({
			        	url:"<?php echo base_url();?>index.php/accounting/delete_cart",
			        	method:'GET',
			        	data:'',
			        	success:function(data){
			        		//console.log(data);
			        		showMessage(data);
			        	},
			        	error :function(xhr,textStatus,errorThrown){
			        		alert('request failed: '+ errorThrown);
			        	}	
        });
            	 } 
            	},{ label :'buy',
            	 action :function(dialog){	
            	 	
            	 	closeAllDialoge();
                        
            	 	 $.ajax({
			        	url:"<?php echo base_url();?>index.php/accounting/get_buy_form",
			        	method:'GET',
			        	data:'',
			        	success:function(data){
			        		//
			        		 	 var dialog = new BootstrapDialog({
					                title: 'buy' ,
					                message: data,
					                buttons: [{
					                    label: 'buy',
					                    action: function(dialogRef){
					                        formData={};
					                        var optionSelected = $("#bill_type  option:selected");
		   									formData['bill_type']= optionSelected.val();
		   									var optionSelected = $("#bill_kind  option:selected");
		   									formData['bill_kind']= optionSelected.val();
		   									var optionSelected = $("#bond option:selected");
		   									formData['bond']= optionSelected.val();
		   									console.log(formData);
		   									$.ajax({
						                    	url:"<?php  echo base_url();?>index.php/accounting/buy",
						                    	method:'POST',
						                    	data: formData ,
						                    	success:function(data){
						                    		showMessage(data);
						                    	},
						                    	error :function(xhr,textStatus,errorThrown){
						                    		alert('request failed: '+ errorThrown);
						                    	}	
						                    });
		   								
						                  //	
					                    }
					                }, {
					                    label: 'Close',
					                    cssClass: 'btn-warning',
					                    action: function(dialogRef){
					                        // You can also use BootstrapDialog.closeAll() to close all dialogs.
					                        dialogRef.close();
					                    }
					                }]
					            });
					            dialog.open();
			        		//
			        	},
			        	error :function(xhr,textStatus,errorThrown){
			        		alert('request failed: '+ errorThrown);
			        	}	
        });            	 	

			            	 } 
            	}]
        });
	}
	
	
	function showMessage(myMessage){
		BootstrapDialog.show({
            message: myMessage
        });
     }
     
     $('#add_to_cart').click(function(){
     	console.log('clicked');
     	id=$(this).attr('name');
     	//alert(id);
     	 $.ajax({
        	url:"<?php echo base_url();?>index.php/accounting/update_item/"+id,
        	method:'GET',
        	data:'',
        	success:function(data){
        		//show_cart();
        		showMessage(data);
        	},
        	error :function(xhr,textStatus,errorThrown){
        		alert('request failed: '+ errorThrown);
        	}	
        });
     });
     
     function closeAllDialoge () {
	      $.each(BootstrapDialog.dialogs, function(id, dialog){
	           dialog.close();
	       });
	      }
</script>