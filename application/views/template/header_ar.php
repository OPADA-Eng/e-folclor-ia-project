<html dir="rtl" >
	<?php
	$this->load->helper('functions');
	$nessacery_header_data = get_nessacery_header_content();
	$foloclor_articles = $nessacery_header_data['foloclor_articles'];
	$p_categories = $nessacery_header_data['p_categories'];
	$f_categories = $nessacery_header_data['f_categories']; 
	$latest_newsbar = $nessacery_header_data['latest_newsbar']
	?>
    <head>
        <meta charset="utf-8"> 
        
        <!-- bootstap modal -->
		<link rel="stylesheet" href="<?php echo base_url();?>css/css_template_en/bootstrap-dialog.css"  />

        <link rel='stylesheet' id='magz-style-css'  href='<?php echo base_url(); ?>css/css_template_ar/style.css' type='text/css' media='all' />
        <link rel='stylesheet' id='swipemenu-css'  href='<?php echo base_url(); ?>css/css_template_ar/swipemenu.css' type='text/css' media='all' />
        <link rel='stylesheet' id='bootstrap-css'  href='<?php echo base_url(); ?>css/css_template_ar/bootstrap.css' type='text/css' media='all' />
        <link rel='stylesheet' id='bootstrap-css'  href='<?php echo base_url(); ?>css/css_template_ar/bootstrap-ar.css' type='text/css' media='all' />
        <link rel='stylesheet' id='bootstrap-responsive-css'  href='<?php echo base_url(); ?>css/css_template_ar/bootstrap-responsive.css' type='text/css' media='all' />
        <link rel='stylesheet' id='simplyscroll-css'  href='<?php echo base_url(); ?>css/css_template_ar/jquery.simplyscroll.css' type='text/css' media='all' />
        <link rel='stylesheet' id='jPages-css'  href='<?php echo base_url(); ?>css/css_template_ar/jPages.css' type='text/css' media='all' />
        <link rel='stylesheet' id='rating-css'  href='<?php echo base_url(); ?>css/css_template_ar/jquery.rating.css' type='text/css' media='all' />
        <link rel='stylesheet' id='ie-styles-css'  href='<?php echo base_url(); ?>css/css_template_ar/ie.css' type='text/css' media='all' />
        <link rel='stylesheet' id='Roboto-css'  href='http://fonts.googleapis.com/css?family=Roboto' type='text/css' media='all' />
        <link rel='stylesheet' id='simplyscroll-css'  href='<?php echo base_url(); ?>css/css_template_ar/jquery.simplyscroll.css' type='text/css' media='all' />
        <link rel='stylesheet' id='pretty-photo-css'  href='<?php echo base_url(); ?>css/css_template_ar/prettyPhoto.css' type='text/css' media='screen' />
        <!-- Slider -->
        <link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>css/slider/style.css" />
        <script type="text/javascript" src="<?php echo base_url(); ?>js/slider/jquery.js"></script>
        <!-- / end Slider -->
        <script type='text/javascript' src="<?php echo base_url(); ?>js/template/jquery-1.10.2.min.js"></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/html5.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/bootstrap.js'></script>
        
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.bxslider.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.bxslider.init.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.rating.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.idTabs.min.js'></script>
        
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/jPages.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.sidr.min.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.touchSwipe.min.js'></script>
        
        <!--for dropdown menu in the header -->
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.simplyscroll.js'></script>
        
		<script type='text/javascript' src='<?php echo base_url(); ?>js/template/jquery.prettyPhoto.js'></script>
        <script type='text/javascript' src='<?php echo base_url(); ?>js/template/custom.js'></script>
        <!--for dropdown menu in the header -->
        <script type="text/javascript" src="<?php echo base_url(); ?>js/template/bootstrap-dialog.js"> </script>
		<script src="<?php echo base_url();?>js/bootstrap-typeahead.js"></script>
   	
<style>
.field:hover{
  background-color: #1C6093;
  -webkit-transition:all 0.8s linear 0s;
  -moz-transition:all 0.8s linear 0s;
  -o-transition:all 0.8s linear 0s;
  transition:all 0.8s linear 0s;
  color:#FFFFFF;
  padding: 10px;
  box-shadow: 0px 0px 8px #1C6093;
}

.field{
	  background-color: #428BCA;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}

</style>

    </head>
    <body>
        
        <div id="page">
            <header id="header" class="container">

                <div id="mast-head">
                    <div id="logo">
                        <a href="#" ><img src="<?php echo base_url(); ?>images/logo.png" alt="Magazine" /></a>
                    </div>
                </div>
                <nav class="navbar navbar-inverse clearfix nobot " style="width:65%; display: inline-block;">

                    <a id="responsive-menu-button" href="#swipe-menu">
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>		
                    </a>	

                    <!-- Responsive Navbar Part 2: Place all navbar contents you want collapsed withing .navbar-collapse.collapse. -->
                    <div class="nav-collapse" id="swipe-menu-responsive">

                    <ul class="nav">

                        <li>
                        <span id="close-menu">
                            <a href="#" class="close-this-menu">Close</a>
                                <script type="text/javascript">
                                    jQuery('a.sidr-class-close-this-menu').click(function(){
                                        jQuery('div.sidr').css({
                                            'right': '-476px'
                                        });
                                        jQuery('body').css({
                                        'right': '0'
                                        });							
                                    });
                                </script>

                        </span>
                        </li>

                        <?php  $this->load->helper('ion_auth');
							if (is_logged_in()){
								echo  '<li>'.anchor('auth/logout','تسجيل خروج').'</li>';
								echo '<li> <a id ="cart" href="#" >  الشراء</a>   </li>';
							}else{ 
								echo  '<li>'.anchor('auth/register','تسجيل').'</li>';
								echo  '<li>'.anchor('auth/login','تسجيل دخول').'</li>';
							}
							?>
                        
                        <li><a href="about.html">اتصل بنا</a></li>
                        <li class="dropdown"><a href="##">التراثيات</a>
                           <ul class="sub-menu">

								<?php
								foreach ($f_categories as $key => $f_cat) {
									echo '<li>'.anchor('foloclor/category/'.$f_cat->fc_id,$f_cat->fc_name_ar).'</li>';
								}
								?>
		
								
							</ul>
                        </li>
                        <li class="dropdown"><?php echo anchor('product','المنتجات') ?>
                            <ul class="sub-menu">

								<?php
								foreach ($p_categories as $key => $p_cat) {
									echo '<li>'.anchor('product/category/'.$p_cat->pcat_id,$p_cat->cat_ar).'</a></li>';
								}
								?>
						
							</ul>
                        </li>
                        <li><a href="<?php echo base_url();?>/index.php/about">حول الموقع</a></li>
                        <li class="dropdown"><a href="<?php echo base_url();?>/index.php/main">الرئيسية</a></li>
                       
                        
                    	 
							<li><a href="<?php echo base_url();?>/index.php/main"><img src="<?php echo base_url(); ?>images/home.png" alt="Magazine"></a></li>
                    </ul>

                    </div><!--/.nav-collapse -->
					
                </nav><!-- /.navbar -->

                <div style="display:inline-block;position: relative;float: left;padding-top: 14px;">
					<?php echo anchor('langSwitch/switchLanguage/english/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3),'<img src="'.base_url().'images/english_lang.png" id="style_lang" />'); ?>
					<?php echo anchor('langSwitch/switchLanguage/arabic/'.$this->uri->segment(1).'/'.$this->uri->segment(2).'/'.$this->uri->segment(3),'<img src="'.base_url().'images/arabic_lang.gif" id="style_lang" />'); ?>
				</div>
            </header>
            <!-- start 1st row -->
            <div id="headline" class="container">
                    <div class="row-fluid">
                        <div class="span3">
                            <article class="post">
                                <div class="row">
                                    <div class="col-md-7 col-sm-7 col-xs-7">
                                        <div class="entry">
                                            <h3><a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus"   rel="bookmark">تراث الممكلة السعودية</a></h3>
                                            <p>2014-12-10 21:27:40</p>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
                                        <img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/photodune-2043745-college-student-s-225x136.jpg" class="thumb"   alt="photodune-2043745-college-student-s" />
                                        </a>
                                    </div>
                                
                                
                                <div class="clearfix"></div>
                                </div>
                            </article>
                        </div>

                        <div class="span3">
                            <article class="post">
                                <div class="row">
                                    <div class="col-md-7 col-sm-7 col-xs-7">
                                        <div class="entry">
                                            <h3><a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus"  rel="bookmark"> الثروة التراثية </a></h3>
                                            <p>2014-12-10 12:54:56 </p>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
                                        <img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/photodune-2043745-college-student-s-225x136.jpg" class="thumb"                                        alt="photodune-2043745-college-student-s" />
                                        </a>
                                    </div>
                                
                                
                                <div class="clearfix"></div>
                                </div>
                            </article>
                        </div>

                        <div class="span3">
                            <article class="post">
                                <div class="row">
                                    <div class="col-md-7 col-sm-7 col-xs-7">
                                        <div class="entry">
                                            <h3><a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark"> متحف قمة المدينه للتراث الشعبى</a></h3>
                                            <p>2014-12-10 21:27:40</p>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
                                        <img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_2114081-225x136.jpg" class="thumb"   alt="photodune-2043745-college-student-s" />
                                        </a>
                                    </div>
                                
                                
                                <div class="clearfix"></div>
                                </div>
                                
                                
                            </article>
                        </div>

                        <div class="span3">
                            <article class="post">
                                <div class="row">
                                    <div class="col-md-7 col-sm-7 col-xs-7">
                                        <div class="entry">
                                            <h3><a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus"  rel="bookmark">متحف التراث الشعبي والفلكلور في راسون</a></h3>
                                            <p>2014-12-10 12:54:56 </p>
                                        </div>
                                    </div>
                                    <div class="col-md-5 col-sm-5 col-xs-5">
                                        <a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
                                        <img width="225" height="136" src="<?php echo base_url(); ?>images/dummy/shutterstock_109209743-225x136.jpg" class="thumb"   alt="photodune-2043745-college-student-s" />
                                        </a>
                                    </div>
                                
                                
                                <div class="clearfix"></div>
                                </div>
                            </article>
                        </div>	

                    </div>
	           </div>
                <!-- end 1st row -->
                <div id="intr" class="container">
                    <div class="row">
                        <div class="col-md-9 col-sm-9 col-xs-12">
                            <div class="brnews span9" style="margin-left: 8px;">
                                 <div class="table-responsive">
                                        <table>
                                            <thead>    
                                                <th> 
                                                    <h3>آخر الأخبار</h3>
                                                </th>
                                                <th>
                                                    <ul id="scroller">
                                                        <?php 
															foreach ($latest_newsbar as $key => $news) {
																echo '<li><p><a href="#" title="Permalink to Lectus non rutrum pulvinar urna leo dignissim lorem" rel="bookmark"><span class="title">'.$news->title_ar.'</span>'.$news->details_ar.'</a></p></li>';
															}
														?>
                                                    </ul>
                                                </th>
                                            </thead>
                                        </table>
                                 </div>
                                
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-12">
                            <div class="span3">
                            	<?php echo form_open("search/index");?>
                                <div id="searchform">
                                    <div class="table-responsive">
                                        <table>
                                            <thead>
                                            	
                                                <th> 
                                                   <p><input type="text" value="إبحث هنا..." onfocus="if ( this.value == 'إبحث هنا...' ) { this.value = ''; }" onblur="if ( this.value == '' ) { this.value = 'Search here...'; }" name="keyword" id="s" />
                                                </th>
                                                <th><input type="submit" name="submit" id="searchsubmit" value="Search" /></p></th>
                                                             
                                            </thead>
                                        </table>                     
                                    </div>
                                </div>
                                <?php echo form_close();?>
                            </div>
                        </div>
                    </div>
                </div>
                
<script>
	$('#cart').click(function(){ 
			show_cart();
	});
	
	function show_cart(){
		$.ajax({
			 url: "<?php echo base_url();?>index.php/accounting/cart",
			 type: 'POST',
			 data: '',
			 success : function(data){
			 	console.log(data);
			 	show_dialoge(data);
			 },
			 error : function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			 }
			});
	}
	
	function show_dialoge (html) {
	 BootstrapDialog.show({
            title: 'سلتي ',
            message: $(html),
            buttons: [{
                label: 'تعديل السلة',
                cssClass: 'btn-primary',
                hotkey: 13, // Enter.
                action: function() {
                   /*$('#in_cart -> ').find('[name]').each(function(){
                    	//console.log($(this).attr('id'));
                    	var obj = $(this);
	   					 name = obj.attr('name');
	   					 alert(name);
                     });*/
                    data={};
                    var obj =$("#in_cart");
                    obj.find('[type]').each(function(index, value) {
   					// console.log(value);
				    var objj = $(this),
				    name = objj.attr('name'),
				    value = objj.val();
				    if ($(this).attr('type')=='text')
				    	data[index]=[name,value];
					
				  }); 
                    var formData={'in_cart' : data };
                    console.log(formData);
                     $.ajax({
                    	url:"<?php  echo base_url();?>index.php/accounting/update_cart",
                    	method:'GET',
                    	data:formData,
                    	success:function(data){
                    		console.log(data);
                    		showMessage(data);
                    	},
                    	error :function(xhr,textStatus,errorThrown){
                    		alert('request failed: '+ errorThrown);
                    	}	
                    });
                }
            },{ label :'حذف السلة',
            	 action :function(dialog){
            	 	 $.ajax({
			        	url:"<?php echo base_url();?>index.php/accounting/delete_cart",
			        	method:'GET',
			        	data:'',
			        	success:function(data){
			        		//console.log(data);
			        		showMessage(data);
			        	},
			        	error :function(xhr,textStatus,errorThrown){
			        		alert('request failed: '+ errorThrown);
			        	}	
        });
            	 } 
            	},{ label :'شراء',
            	 action :function(dialog){	
            	 	
            	 	closeAllDialoge();
                        
            	 	 $.ajax({
			        	url:"<?php echo base_url();?>index.php/accounting/get_buy_form",
			        	method:'GET',
			        	data:'',
			        	success:function(data){
			        		//
			        		 	 var dialog = new BootstrapDialog({
					                title: 'شراء' ,
					                message: data,
					                buttons: [{
					                    label: 'شراء',
					                    action: function(dialogRef){
					                        formData={};
					                        var optionSelected = $("#bill_type  option:selected");
		   									formData['bill_type']= optionSelected.val();
		   									var optionSelected = $("#bill_kind  option:selected");
		   									formData['bill_kind']= optionSelected.val();
		   									var optionSelected = $("#bond option:selected");
		   									formData['bond']= optionSelected.val();
		   									console.log(formData);
		   									$.ajax({
						                    	url:"<?php  echo base_url();?>index.php/accounting/buy",
						                    	method:'POST',
						                    	data: formData ,
						                    	success:function(data){
						                    		showMessage(data);
						                    	},
						                    	error :function(xhr,textStatus,errorThrown){
						                    		alert('request failed: '+ errorThrown);
						                    	}	
						                    });
		   								
						                  //	
					                    }
					                }, {
					                    label: 'إغلاق',
					                    cssClass: 'btn-warning',
					                    action: function(dialogRef){
					                        // You can also use BootstrapDialog.closeAll() to close all dialogs.
					                        dialogRef.close();
					                    }
					                }]
					            });
					            dialog.open();
			        		//
			        	},
			        	error :function(xhr,textStatus,errorThrown){
			        		alert('request failed: '+ errorThrown);
			        	}	
        });            	 	

			            	 } 
            	}]
        });
	}
	
	
	function showMessage(myMessage){
		BootstrapDialog.show({
            message: myMessage
        });
     }
     
     $('#add_to_cart').click(function(){
     	id=$(this).attr('name');
     	//alert(id);
     	 $.ajax({
        	url:"<?php echo base_url();?>index.php/accounting/update_item/"+id,
        	method:'GET',
        	data:'',
        	success:function(data){
        		//show_cart();
        		showMessage(data);
        	},
        	error :function(xhr,textStatus,errorThrown){
        		alert('request failed: '+ errorThrown);
        	}	
        });
     });
     
     function closeAllDialoge () {
	      $.each(BootstrapDialog.dialogs, function(id, dialog){
	           dialog.close();
	       });
	      }
	      
	$('#close_bill').click(function(){ 
			$.ajax({
			 url: "<?php echo base_url();?>index.php/accounting/bill_not_paid",
			 type: 'POST',
			 data: '',
			 success : function(data){
			 	show_not_closed_bill(data);
			 	console.log(data);
			 },
			 error : function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			 }
			});
	});

	
</script>
