<div id="content" class="container">

		<div id="main" class="row-fluid">
			<article class="page">
				<header class="entry-header">
					<h1 class="entry-title"><span>حول الموقع</span></h1>
				</header><!-- .entry-header -->
				<div class="entry-content">
					<p>موقعنا مخصص لعرض أهم الـ foloclor وأهم المواضيع التراثية بالإضافة إلى انه يتيح أيضاً عرض منتجات تراثية مع إمكانية شرائها عبر الموقع.</p>
				</div><!-- .entry-content -->
			</article><!-- #post-429 -->
			<div class="clearfix"></div>
		</div><!-- #main -->

	</div><!-- #content -->