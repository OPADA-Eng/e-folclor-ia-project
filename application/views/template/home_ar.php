<?php
$this->load->helper('functions');
$nessacery_header_data = get_nessacery_header_content();
$foloclor_articles = $nessacery_header_data['foloclor_articles'];
$p_categories = $nessacery_header_data['p_categories'];
$f_categories = $nessacery_header_data['f_categories']; 
$latest_newsbar = $nessacery_header_data['latest_newsbar']
?>
<div id="content" class="container">
                    <div id="main" class="row-fluid">
                        <div class="row">
                            <div class="col-md-5 col-sm-5 col-xs-12" >
                                <!-- slider here -->
                                
                                <div id="wowslider-container1" style="height: 340px;">
									<div class="ws_images"><ul>
									<li><img src="<?php echo base_url();?>data1/images/1508450_439243759542383_1020371568_n.jpg" alt="1508450_439243759542383_1020371568_n" title="1508450_439243759542383_1020371568_n" id="wows1_0"/></li>
									<li><img src="<?php echo base_url();?>data1/images/20141010_140117.jpg" alt="20141010_140117" title="20141010_140117" id="wows1_1"/></li>
									<li><img src="<?php echo base_url();?>data1/images/20141011_175955.jpg" alt="20141011_175955" title="20141011_175955" id="wows1_2"/></li>
									<li><img src="<?php echo base_url();?>data1/images/_0.jpg" alt="ثقثق" title="ثقثق" id="wows1_3"/></li>
									<li><img src="<?php echo base_url();?>data1/images/_1.jpg" alt="يبيب" title="يبيب" id="wows1_4"/></li>
									<li><img src="<?php echo base_url();?>data1/images/_2.jpg" alt="ثث" title="ثث" id="wows1_5"/></li>
									</ul></div>
									<div class="ws_thumbs">
									<div>
									<a href="#" title="1508450_439243759542383_1020371568_n"><img src="<?php echo base_url();?>data1/tooltips/1508450_439243759542383_1020371568_n.jpg" alt="" /></a>
									<a href="#" title="20141010_140117"><img src="<?php echo base_url();?>data1/tooltips/20141010_140117.jpg" alt="" /></a>
									<a href="#" title="20141011_175955"><img src="<?php echo base_url();?>data1/tooltips/20141011_175955.jpg" alt="" /></a>
									<a href="#" title="ثقثق"><img src="<?php echo base_url();?>data1/tooltips/_0.jpg" alt="" /></a>
									<a href="#" title="يبيب"><img src="<?php echo base_url();?>data1/tooltips/_1.jpg" alt="" /></a>
									<a href="#" title="ثث"><img src="<?php echo base_url();?>data1/tooltips/_2.jpg" alt="" /></a>
									</div>
									</div>
								</div>
								<script type="text/javascript" src="<?php echo base_url(); ?>js/slider/wowslider.js"></script>
								<script type="text/javascript" src="<?php echo base_url(); ?>js/slider/script.js"></script>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12" >
                                <div id="slider" class="clearfix">
                                    <div class="row">
                                            <div id="slide-right" class="span4">
                                                <h3>الأخبار الأكثر شعبية</h3>
                                                    <ul>
                                                    	
                                                    	 <?php 
															$i = 0;
															foreach ($latest_newsbar as $key => $news) {
																	if($i < 5){
																		echo '<li><p><a href="#" title="Permalink to Lectus non rutrum pulvinar urna leo dignissim lorem" rel="bookmark"><span class="title">'.$news->title_ar.'</span></a></p></li>';
																		$i++;
																	}
																	
																}
															?>
                                                        
                                                    </ul><div class="clear"></div>
                                            </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 col-sm-4 col-xs-12">
                                    <div id="tabwidget" class="widget tab-container"> 
                                        <ul id="tabnav" class="clearfix"> 
                                            
                                            <li><h3><a href="#tab3">التعليقات<img src="<?php echo base_url(); ?>images/komen-putih.png" alt="Comments"></a></h3></li>
                                            <li><h3><a href="#tab2">الأخيرة<img src="<?php echo base_url(); ?>images/time-white.png" alt="Recent"></a></h3></li>
                                            <li><h3><a href="#tab1" class="selected">الأشهر<img src="<?php echo base_url(); ?>images/view-white-bg.png" alt="Popular"></a></h3></li>
                                        </ul> 

                                    <div id="tab-content">

                                    <div id="tab1" style="display: block; ">
                                        <ul id="itemContainer" class="recent-tab">
                                        	 <?php
											  for($i=0;$i<8;$i++)
									          {
									          	if(isset($popular_f[$i]['item_data']))
												{
									             	echo '<li>
												<a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/160_120/'.$popular_f[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
												<h4 class="post-title"><a href="#">'.$popular_f[$i]['item_data']['f_title_ar'].'</a></h4>
												<p>'.substr($popular_f[$i]['item_data']['f_historical_importance_ar'],0,60).'....</p>
												<div class="clearfix"></div>				
											    </li>';
												}
											  }?>

                                            <script type="text/javascript">
                                                jQuery(document).ready(function($){

                                                    /* initiate the plugin */
                                                    $("div.holder").jPages({
                                                    containerID  : "itemContainer",
                                                    perPage      : 3,
                                                    startPage    : 1,
                                                    startRange   : 1,
                                                    links		   : "blank"
                                                    });
                                                    
                                                     /* initiate the plugin */
                                                    $("div.holder1").jPages({
                                                    containerID  : "itemContainer2",
                                                    perPage      : 3,
                                                    startPage    : 1,
                                                    startRange   : 1,
                                                    links		   : "blank"
                                                    });
                                                });		
                                            </script>

                                        </ul>

                                        <div class="holder clearfix"></div>
                                        <div class="clear"></div>

                                    <!-- End most viewed post -->		  

                                    </div><!-- /#tab1 -->

                                    <div id="tab2" style="display: none;">	
                                        <ul id="itemContainer2" class="recent-tab">
                                        	<?php for($i=0;$i<3;$i++)
							                  {
							                  	if(isset($recently_f_added[$i]['item_data']))
									               echo'<li>
													<a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/160_120/'.$recently_f_added[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
													<h4 class="post-title"><a href="http://magazine.themedesigner.in/lectus-non-rutrum-pulvinar-urna-leo-dignissim-lorem-8/">'.$recently_f_added[$i]['item_data']['f_title_ar'].'</a></h4>
													<p>'.substr($recently_f_added[$i]['item_data']['f_historical_importance_ar'], 0,60).'....</p>
													<div class="clearfix"></div>	
												       </li>';
											  }
								  			?>
                                           

                                        </ul>

                                        <div class="holder1 clearfix"></div>
                                        <div class="clear"></div>
                                        
                                    </div><!-- /#tab2 --> 

                                    <div id="tab3" style="display: none; ">
                                        <ul>
                                            <li><span class="author">Magazine Author</span> on <a href="#" title=" View comment ">Lorem Ipsum is simply dummy te</a></li>
                                            <li><span class="author">Magazine Author</span> on <a href="#" title=" View comment ">Lorem Ipsum is simply dummy te</a></li>
                                            <li><span class="author">magazine</span> on <a href="#" title=" View comment ">This is threaded comment level</a></li>
                                            <li><span class="author">magazine</span> on <a href="#" title=" View comment ">This is threaded comment syste</a></li>
                                        </ul>
                                    </div><!-- /#tab3 --> 

                                    </div><!-- /#tab-content -->

                                    </div><!-- /#tab-widget --> 

                                   
                            </div>
                        
                        </div>
                        
                        <div class="row">
                            
                            <div class="row">
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div id="home-bottom" class="clearfix">
                                        <h3 class="title"><a href="#" title="Fashion News"><span>المنتجات المضافة حديثاً</span></a></h3>	
                                        <div class="row">	
                                        	<?php for($i=0;$i<4;$i++)
								                  {
								                  	if(isset($last_added_products[$i]['item_data']))
													{
										               echo'<div class="col-md-3 col-sm-3 col-xs-12">
		                                            	<a class="image_thumb_zoom" href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
		                                            	<img width="160px" height="120px" class="img-thumbnail" src="'.base_url().'uploads/images/225_136/'.$last_added_products[$i]['item_data']['image'].'" /></a>
		                                            	<div style="height:60px;"><p>'.$last_added_products[$i]['item_data']['product_name_ar'].'</p></div>
		                                            	<hr />
		                                            	<div class="text-muted">'.$last_added_products[$i]['item_data']['date'].'</div>
		                                            	<div class="ratings">
															<input class="star" type="radio" name="home-top-rating-1-'.$i.'" value="'.$i.'" disabled="disabled"/>
																<input class="star" type="radio" name="home-top-rating-1-'.$i.'" value="'.$i.'" disabled="disabled"/>
																<input class="star" type="radio" name="home-top-rating-1-'.$i.'" value="'.$i.'" disabled="disabled" checked="checked"/>
																<input class="star" type="radio" name="home-top-rating-1-'.$i.'" value="'.$i.'" disabled="disabled"/>
																<input class="star" type="radio" name="home-top-rating-1-'.$i.'" value="'.$i.'" disabled="disabled"/>
														</div>
		                                            	<hr />
		                                            </div>';
												}
												}
				                               ?>
                                            
                                           
                                           
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-4 col-sm-4 col-xs-12" style="height: 100px;">
                                    <iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fthe.finest.I.have.read&colorscheme=light&show_faces=true&border_color&stream=true&header=true&height=435" scrolling="yes" frameborder="0" style="border:none; overflow:hidden; width:370px;  height:365px;  background: white; float:left; " allowtransparency="true">
									</iframe>
                                </div>
                            </div>
                            
                            <div class="row" >
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <div id="home-bottom" class="clearfix">
                                        <h3 class="title"><a href="#" title="Fashion News"><span>المنتجات الأكثر تصويتاً</span></a></h3>	
                                        <div class="row">	
                                        	<?php for($i=0;$i<4;$i++)
								                  {
								                  	if(isset($top_products[$i]['item_data']))
													{
										               echo'<div class="col-md-3 col-sm-3 col-xs-12">
		                                            	<a class="image_thumb_zoom" href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
		                                            	<img width="160px" height="120px" class="img-thumbnail" src="'.base_url().'uploads/images/225_136/'.$top_products[$i]['item_data']['image'].'" /></a>
		                                            	<div style="height:60px;"><p>'.$top_products[$i]['item_data']['product_name_ar'].'</p></div>
		                                            	<hr />
		                                            	<div class="text-muted">'.$top_products[$i]['item_data']['date'].'</div>
		                                            	<div class="ratings">
															<input class="star" type="radio" name="home-top-rating-2-'.$i.'" value="'.$i.'" disabled="disabled"/>
															<input class="star" type="radio" name="home-top-rating-2-'.$i.'" value="'.$i.'" disabled="disabled"/>
															<input class="star" type="radio" name="home-top-rating-2-'.$i.'" value="'.$i.'" disabled="disabled" checked="checked"/>
															<input class="star" type="radio" name="home-top-rating-2-'.$i.'" value="'.$i.'" disabled="disabled"/>
															<input class="star" type="radio" name="home-top-rating-2-'.$i.'" value="'.$i.'" disabled="disabled"/>
														</div>
		                                            	<hr />
		                                            </div>';
												}
												}
				                               ?>
                                            
                                           
                                           
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            
                        </div>
                    </div><!-- #main -->
                </div><!-- #content -->