<?php
$this->load->helper('functions');
$nessacery_header_data = get_nessacery_header_content();
$foloclor_articles = $nessacery_header_data['foloclor_articles'];
$p_categories = $nessacery_header_data['p_categories'];
$f_categories = $nessacery_header_data['f_categories']; 
$latest_newsbar = $nessacery_header_data['latest_newsbar']
?>
<div id="content" class="container">

		<div id="main" class="row-fluid">
			<div id="main-left" class="span8">
				<div id="slider" class="clearfix">
					<div id="slide-left" class="flexslider span8">
					<div id="wowslider-container1" style="height: 335px;">
						<div class="ws_images"><ul>
						<li><img src="<?php echo base_url();?>data1/images/1508450_439243759542383_1020371568_n.jpg" alt="1508450_439243759542383_1020371568_n" title="1508450_439243759542383_1020371568_n" id="wows1_0"/></li>
						<li><img src="<?php echo base_url();?>data1/images/20141010_140117.jpg" alt="20141010_140117" title="20141010_140117" id="wows1_1"/></li>
						<li><img src="<?php echo base_url();?>data1/images/20141011_175955.jpg" alt="20141011_175955" title="20141011_175955" id="wows1_2"/></li>
						<li><img src="<?php echo base_url();?>data1/images/_0.jpg" alt="ثقثق" title="ثقثق" id="wows1_3"/></li>
						<li><img src="<?php echo base_url();?>data1/images/_1.jpg" alt="يبيب" title="يبيب" id="wows1_4"/></li>
						<li><img src="<?php echo base_url();?>data1/images/_2.jpg" alt="ثث" title="ثث" id="wows1_5"/></li>
						</ul></div>
						<div class="ws_thumbs">
						<div>
						<a href="#" title="1508450_439243759542383_1020371568_n"><img src="<?php echo base_url();?>data1/tooltips/1508450_439243759542383_1020371568_n.jpg" alt="" /></a>
						<a href="#" title="20141010_140117"><img src="<?php echo base_url();?>data1/tooltips/20141010_140117.jpg" alt="" /></a>
						<a href="#" title="20141011_175955"><img src="<?php echo base_url();?>data1/tooltips/20141011_175955.jpg" alt="" /></a>
						<a href="#" title="ثقثق"><img src="<?php echo base_url();?>data1/tooltips/_0.jpg" alt="" /></a>
						<a href="#" title="يبيب"><img src="<?php echo base_url();?>data1/tooltips/_1.jpg" alt="" /></a>
						<a href="#" title="ثث"><img src="<?php echo base_url();?>data1/tooltips/_2.jpg" alt="" /></a>
						</div>
						</div>
					</div>
				</div>
				<script type="text/javascript" src="<?php echo base_url(); ?>js/slider/wowslider.js"></script>
				<script type="text/javascript" src="<?php echo base_url(); ?>js/slider/script.js"></script>
				<!--
					<div id="slide-left" class="flexslider span8">
						<ul class="slides">
							<li data-thumb="<?php echo base_url(); ?>images/dummy/photodune-3834701-laughing-girl-xs-546x291.jpg">
								<a href="#" title="Permalink to Morbi est est lectus non rutrum commodo felis quis tortor" rel="bookmark">
								<img width="546" height="291" src="<?php echo base_url(); ?>images/dummy/photodune-3834701-laughing-girl-xs-546x291.jpg" alt="photodune-3834701-laughing-girl-xs" />
								</a>
								<div class="entry">
									<h4>Morbi est est lectus non rutrum commodo felis quis tortor...</h4>
									<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing erat, qui...</p>
								</div>
							</li>

							<li data-thumb="<?php echo base_url(); ?>images/dummy/photodune-2043745-college-student-s-546x291.jpg">
								<a href="#" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
								<img width="546" height="291" src="<?php echo base_url(); ?>images/dummy/photodune-2043745-college-student-s-546x291.jpg" alt="photodune-2043745-college-student-s" />
								</a>
								<div class="entry">
									<h4>Donec consectetuer ligula vulputate sem tristique cursus...</h4>
									<p>Nam nibh arcu, tristique eget pretium sed, porta id quam. Praesent dignissim ant...</p>
								</div>
								
							</li>
							
							<li data-thumb="<?php echo base_url(); ?>images/dummy/shutterstock_123603871-546x291.jpg">
								<a href="#" title="Permalink to Quisque sodales viverra ornare vitae libero ac risus" rel="bookmark">
								<img width="546" height="291" src="<?php echo base_url(); ?>images/dummy/shutterstock_123603871-546x291.jpg" alt="gallery-1" />
								</a>
								<div class="entry">
									<h4>Quisque sodales viverra ornare vitae libero ac risus...</h4>
									<p>Quisque sodales viverra ornare. Aenean posuere lacus sed facilisis gravida. Morb...</p>
								</div>
							</li>
							
							<li data-thumb="<?php echo base_url(); ?>images/dummy/shutterstock_109209743-546x291.jpg">
								<a href="#" title="Permalink to Vestibulum auctor dapibus neque pulvinar urna leo" rel="bookmark">
								<img width="546" height="291" src="<?php echo base_url(); ?>images/dummy/shutterstock_109209743-546x291.jpg" alt="shutterstock_109209743" />
								</a>
								<div class="entry">
									<h4>Vestibulum auctor dapibus neque pulvinar urna leo...</h4>
									<p>Fusce aliquet non ipsum vitae scelerisque. Nullam ultricies adipiscing erat, qui...</p>
								</div>
							</li>
						</ul>
					</div>
				-->
					<div id="slide-right" class="span4">
						<h3>Last News</h3>
							<ul>
								 <?php 
								 	$i = 0;
									foreach ($latest_newsbar as $key => $news) {
											if($i < 4){
												echo '<li><p><a href="#" title="Permalink to Lectus non rutrum pulvinar urna leo dignissim lorem" rel="bookmark"><span class="title">'.$news->title_en.'</span></a></p></li>';
												$i++;
											}
											
										}
									?>
							</ul><div class="clear"></div>
					</div>
				</div>
				
				<div id="home-top">
					<h3 class="title"><span>Product Recently Added</span></h3>
					<ul class="bxslider">
						
						<?php for($i=0;$i<4;$i++)
				                  {
				                  	
				                  	if(isset($last_added_products[$i]['item_data']))
									{
						               echo'<li><a class="image_thumb_zoom" href="'.base_url().'index.php/product/view_product/'.$last_added_products[$i]['item_data']['pc_id'].'" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
										<img width="225" height="136" src="'.base_url().'uploads/images/225_136/'.$last_added_products[$i]['item_data']['image'].'"  />
										</a>
										<div style="height:120px;"><p>'.$last_added_products[$i]['item_data']['product_name_en'].'</p></div>
										<div class="meta clearfix">
											<span class="date">'.$last_added_products[$i]['item_data']['date'].'</span>
											<div class="ratings">
												<input class="star" type="radio" name="home-top-rating-1-'.$i.'" value="'.$i.'" disabled="disabled"/>
												<input class="star" type="radio" name="home-top-rating-1-'.$i.'" value="'.$i.'" disabled="disabled"/>
												<input class="star" type="radio" name="home-top-rating-1-'.$i.'" value="'.$i.'" disabled="disabled" checked="checked"/>
												<input class="star" type="radio" name="home-top-rating-1-'.$i.'" value="'.$i.'" disabled="disabled"/>
												<input class="star" type="radio" name="home-top-rating-1-'.$i.'" value="'.$i.'" disabled="disabled"/>
											</div>
										</div>
									</li>';
								}
								}
                               ?>
					</ul>
				</div>
				
				<div id="home-top">
					<h3 class="title"><span>Most Voting Products</span></h3>
					<ul class="bxslider">
						
						<?php for($i=0;$i<4;$i++)
				                  {
				                  	if(isset($top_products[$i]['item_data']))
									{
						               echo'<li><a class="image_thumb_zoom" href="'.base_url().'index.php/product/view_product/'.$top_products[$i]['item_data']['pc_id'].'" title="Permalink to Donec consectetuer ligula vulputate sem tristique cursus" rel="bookmark">
										<img width="225" height="136" src="'.base_url().'uploads/images/225_136/'.$top_products[$i]['item_data']['image'].'" alt="photodune-2043745-college-student-s" />
										</a>
										<div style="height:60px;"><p>'.$top_products[$i]['item_data']['product_name_en'].'</p></div>
										<div class="meta clearfix">
											<span class="date">'.$top_products[$i]['item_data']['date'].'</span>
											<div class="ratings">
												<input class="star" type="radio" name="home-top-rating-2-'.$i.'" value="'.$i.'" disabled="disabled"/>
												<input class="star" type="radio" name="home-top-rating-2-'.$i.'" value="'.$i.'" disabled="disabled"/>
												<input class="star" type="radio" name="home-top-rating-2-'.$i.'" value="'.$i.'" disabled="disabled" checked="checked"/>
												<input class="star" type="radio" name="home-top-rating-2-'.$i.'" value="'.$i.'" disabled="disabled"/>
												<input class="star" type="radio" name="home-top-rating-2-'.$i.'" value="'.$i.'" disabled="disabled"/>
											</div>
										</div>
									</li>';
								}
								}
                               ?>
					</ul>
				</div>
				
				
				
				
				
			</div><!-- #main-left -->

		<div id="sidebar" class="span4">

			<div id="tabwidget" class="widget tab-container"> 
				<ul id="tabnav" class="clearfix"> 
					<li><h3><a href="#tab1" class="selected"><img src="<?php echo base_url(); ?>images/view-white-bg.png" alt="Popular">Popular</a></h3></li>
					<li><h3><a href="#tab2"><img src="<?php echo base_url(); ?>images/time-white.png" alt="Recent">Recent</a></h3></li>
				    <li><h3><a href="#tab3"><img src="<?php echo base_url(); ?>images/komen-putih.png" alt="Comments">Comments</a></h3></li>
				</ul> 

			<div id="tab-content">
			
	 		<div id="tab1" style="display: block; ">
				<ul id="itemContainer" class="recent-tab">
			           <?php
						  for($i=0;$i<8;$i++)
				          {
				          	echo '<li>';
				          	if(isset($popular_f[$i]['item_data']))
							{

								if($popular_f[$i]['item_data']['f_image'] != "NULL"){
								         echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/160_120/'.$popular_f[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
								         ';       	
													                                }
								else if($popular_f[$i]['item_data']['f_video'] != "NULL"){
									 echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/movies-icon.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';     
								                                                    }
								else if($popular_f[$i]['item_data']['f_sound'] != "NULL"){
									echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/sound_thumb.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';
								}
						
						             echo '
							         <h4 class="post-title"><a href="#">'.$popular_f[$i]['item_data']['f_title_en'].'</a></h4>
							         <p>'.substr($popular_f[$i]['item_data']['f_historical_importance_en'],0,60).'....</p>
							         <div class="clearfix"></div>				
						             </li>';

							}
						  }?>
													
					<script type="text/javascript">
						jQuery(document).ready(function($){

							/* initiate the plugin */
							$("div.holder").jPages({
							containerID  : "itemContainer",
							perPage      : 3,
							startPage    : 1,
							startRange   : 1,
							links		   : "blank"
							});
						});	
						
					</script>

				</ul>
				
				<div class="holder clearfix"></div>
				<div class="clear"></div>

			<!-- End most viewed post -->		  

			</div><!-- /#tab1 -->
 
			<div id="tab2" style="display: none;">	
				<ul id="itemContainer2" class="recent-tab">
					<?php for($i=0;$i<3;$i++)
				                  {
				                  	
				                  	if(isset($recently_f_added[$i]['item_data']))
									{
										echo '<li>';
										if($recently_f_added[$i]['item_data']['f_image'] != "NULL"){
								         echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/160_120/'.$recently_f_added[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
								         ';       	
													                                }
								         else if($recently_f_added[$i]['item_data']['f_video'] != "NULL"){
									      echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/movies-icon.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';     
								                                                    }
								        else if($recently_f_added[$i]['item_data']['f_sound'] != "NULL"){
									     echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/sound_thumb.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';
								                                                     }
						              echo'
										<h4 class="post-title"><a href="http://magazine.themedesigner.in/lectus-non-rutrum-pulvinar-urna-leo-dignissim-lorem-8/">'.$recently_f_added[$i]['item_data']['f_title_en'].'</a></h4>
										<p>'.substr($recently_f_added[$i]['item_data']['f_historical_importance_en'], 0,60).'....</p>
										<div class="clearfix"></div>	
									       </li>';
									       }
								  }
					  ?>
				</ul> 	 
			</div><!-- /#tab2 --> 

			<div id="tab3" style="display: none; ">
				<ul id="itemContainer2" class="recent-tab">
					<?php for($i=0;$i<3;$i++)
				                  {
				                  	
				                  	if(isset($top_commented_f[$i]['item_data']))
									{
										echo '<li>';
										if($top_commented_f[$i]['item_data']['f_image'] != "NULL"){
								         echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/160_120/'.$top_commented_f[$i]['item_data']['f_image'].'" class="thumb" alt="shutterstock_134257640" /></a>
								         ';       	
													                                }
								         else if($top_commented_f[$i]['item_data']['f_video'] != "NULL"){
									      echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/movies-icon.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';     
								                                                    }
								        else if($top_commented_f[$i]['item_data']['f_sound'] != "NULL"){
									     echo ' <a href="#"><img width="225" height="136" src="'.base_url().'uploads/images/sound_thumb.png" class="thumb" alt="shutterstock_134257640" /></a>
								         ';
								                                                     }
						
									
						               echo'
										<h4 class="post-title"><a href="http://magazine.themedesigner.in/lectus-non-rutrum-pulvinar-urna-leo-dignissim-lorem-8/">'.$top_commented_f[$i]['item_data']['f_title_en'].'</a></h4>
										<p>'.substr($top_commented_f[$i]['item_data']['f_historical_importance_en'], 0,50).'...</p>
										<div class="clearfix"></div>	
									       </li>';
									}
								  }
					  ?>
                  					
				</ul>
			</div><!-- /#tab2 --> 
	
			</div><!-- /#tab-content -->

			</div><!-- /#tab-widget --> 
			<!--  facebook newsfeed-->

				

			<div class="widget widget_latestpost">
				
				<iframe src="http://www.facebook.com/plugins/likebox.php?href=http%3A%2F%2Fwww.facebook.com%2Fthe.finest.I.have.read&colorscheme=light&show_faces=true&border_color&stream=true&header=true&height=435" scrolling="yes" frameborder="0" style="border:none; overflow:hidden; width:370px;  height:365px;  background: white; float:left; " allowtransparency="true">
				</iframe>
				</div>
			
				
			
		</div><!-- sidebar -->
		
		<div class="clearfix"></div>

		
		</div><!-- #main -->

		</div><!-- #content -->