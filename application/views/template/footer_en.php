
<?php
$nessacery_footer_data = get_nessacery_footer_content();
$foloclors_tags = $nessacery_footer_data['foloclors_tags'];
$top_products = $nessacery_footer_data['top_products'];
?>
<footer id="footer" class="row-fluid">
		<div id="footer-widgets" class="container">
			<div class="footer-widget span3 block1">
				<div class="widget widget_latestpost">
				</div>
			</div>
			
				
			<div class="footer-widget span3 block3">
				<div class="widget">
					<h3 class="title"><span>Some Foloclor Tags </span></h3>
					<div class="tagcloud">
						<?php
						// var_dump($foloclor_articles);
						// die();
						 for($i=0;$i<5;$i++)
				          {
				             	echo ' <a href="">'.$foloclors_tags[$i]->f_tag_en.'</a>';
				    	  }?>
					</div>
				</div>
			</div>
			
			<div class="footer-widget span3 block4">
				<div class="widget">
					<h3 class="title"><span>Social Media</span></h3>
						<div class="socmed clearfix">		
							<ul>
								
                                <li>
                                    <a href="#"><img src="<?php echo base_url(); ?>images/rss-icon.png" alt=""></a>
                                    <h4>RSS</h4>
                                    <p>Subscribe</p>
                                </li>
								<li>
									<a href="#"><img src="<?php echo base_url(); ?>images/twitter-icon.png" alt=""></a>
									<h4>37005</h4>
									<p>Followers</p>
								</li>
								<li>
									<a href="#"><img src="<?php echo base_url(); ?>images/fb-icon.png" alt=""></a>
									<h4>109</h4>
									<p>Fans</p>
								</li>
							</ul>
						</div>
				</div>
			</div>
			
			<div class="footer-widget span6 block5">
				<img class="footer-logo" src="<?php echo base_url(); ?>images/footer-logo.png" alt="Magazine">
					</div>

		</div><!-- footer-widgets -->

	
		<div id="site-info" class="container">
		
			<div id="footer-nav" class="fr">
				<ul class="menu">
					<li><a href="index.html">Home</a></li>
					<li><a href="about.html">About</a></li>
					<li><a href="blog.html">Blog</a></li>
					<li><a href="contact.html">Contact</a></li>
				</ul>
			</div>

			<div id="credit" class="fl">
				<p>All Right Reserved by Internet Application Project</p>
			</div>

		</div><!-- .site-info -->
		
	</footer>

</div><!-- #wrapper -->

</body>
</html>
