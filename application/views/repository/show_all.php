<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-9 col-sm-9 col-xs-12">
	
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>images/other.png"  />Show repositories</p>
            </div>
            <div class="panel-body">
				
              <div class="table-responsive">
                   <?php if(!$repositories){?>
            		<?php echo '<p>لا يوجد مستودعات</p>';?>
            <?php }else{?>	
				<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
                            <tr>
								<th><?php echo '<p>اسم المستودع</p>';?></th>
								<th><?php echo '<p>تعديل</p>';?></th>
								<th><?php echo '<p>حذف</p>';?></th>
							</tr>
                        </thead>
                        <tbody>
				    	    <?php  foreach($repositories as $rep){?>
				    	  	<tr class="gradeX">
				    	  		
				    	    <td> <?php 	echo $rep->r_name_en; ?></td>
							<td><?php echo anchor("accounting/edit_repository/$rep->r_id",'<img src="'.base_url().'images/c_edit.png"  />','');?> </td>
							<td><?php echo anchor("accounting/delete_repository/$rep->r_id",'<img src="'.base_url().'images/c_drop.png"  />','confirm("are you sure to delete")');?></td>
					    	<?php  }}?>
				 			  
			 			</tbody>
 				</table>
				<p><?php echo anchor('accounting/add_repository/', 'Create a new repository')?>  <?php //echo anchor('account/create_group', lang('index_create_group_link'))?></p>
				<div id="infoMessage"><?php //echo $message;?></div>
                <!-- On rows -->
                </div>
            </div>
        </div>
</div>
<div class="col-md-1 col-sm-1 col-xs-12"></div>
