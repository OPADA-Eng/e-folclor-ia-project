
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-8">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/other.png" />Add repository</p>
		</div>
		<div class="panel-body">
			<div class="row">
				<?php if ($mode != 'sucessful'){ ?>
					<?php echo form_open('accounting/add_repository') ?>
					<p>Enter english name</p>
					<?php echo form_input('name_en',set_value('name_en'),'class="font-input form-control" placeholder="Enter english name" required=""'); ?>
					<br />
					<p>Enter arabic name</p>
					<?php echo form_input('name_ar',set_value('name_ar'),'class="font-input form-control" placeholder="Enter arabic name" required=""'); ?>
					<br />
					<p>Enter capacity</p>
					<?php echo form_input('capacity',set_value('capacity'),'class="font-input form-control" placeholder="Enter capacity" required=""'); ?>
					<br/>
					<?php echo form_submit('insert','Add','class="btn btn-log btn-block btn-primary"');?>
					<?php echo form_close();?>
				<?php } 
				echo $message;
				?>
				
			</div>
			
		</div>
	</div>
</div>




