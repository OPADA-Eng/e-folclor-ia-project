<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-9 col-sm-9 col-xs-12">
	
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>images/repository_report.png"  />report repository</p>
            </div>
            <div class="panel-body">
				
              <div class="table-responsive">
                   <?php  if ($message != 'ok')
				   			{ 
                   			if(!$repositories){
            					 echo '<p>repositories not found </p>';
          				  	}else {?>	
								<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
										<thead>
				                            <tr>
												<th><?php echo '<p>repository</p>';?></th>
												<th><?php echo '<p>report</p>';?></th>		
											</tr>
				                        </thead>
				                        <tbody>
								    	    <?php  foreach($repositories as $rep){?>
								    	  	<tr class="gradeX">
								    	  		
								    	    <td> <?php 	echo $rep->r_name_en; ?></td>
											<td><?php echo anchor("accounting/repository_report/$rep->r_id",'<img src="'.base_url().'images/repository_report.png"  />','');?> </td>
									    	<?php  }?>
								 			  
							 			</tbody>
				 				</table>
				 			<?php	}}else{ ?>
				 					<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
										<thead>
				                            <tr>
												<th><?php echo '<p>product name</p>';?></th>
												<th><?php echo '<p>quntity</p>';?></th>		
											</tr>
				                        </thead>
				                        <tbody>
								    	    <?php   foreach($repository as $rep){?>
								    	  	<tr class="gradeX">
								    	  		
								    	    <td> <?php 	echo $rep->pr_count; ?></td>
											<td> <?php echo $rep->product_name_en;?> </td>
									    	<?php  }?>
								 			  
							 			</tbody>
				 				</table>
				 	<?php echo anchor('accounting/repository_report_csv/'.$this->uri->segment(3),'save as csv file');
				 	      echo '<br>';
				 	      echo anchor('accounting/repository_report_pdf/'.$this->uri->segment(3),'save as pdf file');
				 	 } ?>
                <!-- On rows -->
                </div>
            </div>
        </div>
</div>
<div class="col-md-1 col-sm-1 col-xs-12"></div>
