<script>
	var mytags = <?php echo $tags; ?>;
	var mykeys = <?php echo $keys; ?>;
</script>
	
<style type="text/css">
	h3 {
		margin: 0px;
		padding: 0px;	
	}
	.suggestionsBox {
		position: relative;
		left: 30px;
		margin: 10px 0px 0px 0px;
		width: 200px;
		background-color: #212427;
		-moz-border-radius: 7px;
		-webkit-border-radius: 7px;
		border: 2px solid #000;	
		color: #fff;
	}
	.suggestionList {
		margin: 0px;
		padding: 0px;
	}
	
	.suggestionList li {
		
		margin: 0px 0px 3px 0px;
		padding: 3px;
		cursor: pointer;
	}
	
	.suggestionList li:hover {
		background-color: #659CD8;
	}
</style>
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">	
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/add.png" />Add Foloclor</p>
		</div>
		<div class="panel-body">
			<form class="adder" id="adder" method="post" action="">
				<label>Foloclor category</label> 
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<!-- drop down list-->
						<select id="fc_name" name="fc_name" class="form-control">
							 <option  value="" disabled="disabled" selected="selected">Please select a category</option>
							<?php 
					            foreach($foloclor_categories as $row)
					            { 
					              echo '<option value="'.$row->fc_id.'">'.$row->fc_name_en.'</option>';
					            }
			            	?>
						</select>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php if($user_type){ ?>
						<button type="button" class="btn btn-success btn-block" data-toggle="modal" data-target="#category_adder" >Add New Category</button>
						<?php } ?>
					</div>
				</div>
				<br />	
				
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>Arabic title</label> <br>
						<input type="text" id="f_title_ar" name="f_title_ar" class="form-control">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>English title</label> <br> 
						<input type="text" id="f_title_en" name="f_title_en" class="form-control">
					</div>
				</div>	
				<br />	
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>Arabic content</label> <br>
						<textarea rows="5" name="f_content_ar" id="f_content_ar" class="form-control"></textarea>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>English content</label> <br> 
						<textarea rows="5" name="f_content_en" id="f_content_en" class="form-control"></textarea>
					</div>
				</div>	
				<br />	
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>Arabic historical importance</label> <br>
						<textarea rows="5" name="f_hi_ar" id="f_hi_ar" class="form-control"></textarea>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>English historicl importance</label>  <br> 
						<textarea rows="5" name="f_hi_en" id="f_hi_en" class="form-control"></textarea>
					</div>
				</div>
				<br />	
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>Arabic location</label> <br>
						<input type="text" name="f_location_ar" id="f_location_ar" class="form-control" />
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>English location</label><br> 
						<input type="text" name="f_location_en" id="f_location_en" class="form-control"/>
					</div>
				</div>
				<br />	
				<label>keywords</label> <br>
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						
						<input type="text" id="keyword" class="form-control" />
						<table cellpadding="4" border="0">
							<tbody id="selectedkeywords">
							</tbody>
						</table>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php if($user_type){ ?>
						<!-- Button trigger modal -->
						<button type="button" class="btn btn-success btn-block btn-leg" data-toggle="modal" data-target="#keyword_adder" >Add New Keyword</button>
						<?php } ?>
					</div>
				</div>
				<br />	
				<label>tags</label> <br>
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input type="text" id="tag" class="form-control" />
						<table cellpadding="4"   border="0">
							<tbody id="selectedtags">
							</tbody>
						</table>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php if($user_type){ ?>
						<!-- Button trigger modal -->
						<button type="button" class="btn btn-success btn-block btn-leg" data-toggle="modal" data-target="#tag_adder" >Add New Tag</button>
						<?php } ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12"></div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input id="submit" type="submit" value="Submit" class="btn btn-success btn-block btn-leg">
					</div>
				</div>
				</form>
				<div id="nnn">
					<div class="modal fade" id="tag_adder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								    <h4 class="modal-title" id="myModalLabel">Add New Tag</h4>
								</div>
								<div class="modal-body" id="form-tagadd">
									<form class="tagadd" name="tagadd">	
									<label>Arabic tag</label>
									<input type="text" name="ft_ar" id="ft_ar" class="form-control" />
									<br><br>
									<label>English tag</label> 
									<input type="text" id="ft_en" name="ft_en" class="form-control" />
									</form>
								</div>
								<div class="modal-footer">
									<input class="btn btn-success btn-block btn-leg" type="submit" value="Add" id="tagadd"/>
									<a href="#" class="btn" data-dismiss="modal">close</a>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div id="kkk">
					<div class="modal fade" id="keyword_adder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								    <h4 class="modal-title" id="myModalLabel">Add New Keyword</h4>
	    						</div>
								<div class="modal-body" id="form-keyadd">
									<form class="keyadd" name="keyadd">	
									<label>Arabic keyword</label>
									<input type="text" name="fk_ar" id="fk_ar" class="form-control" />
									<br><br>
									<label>English keyword</label> 
									<input type="text" id="fk_en" name="fk_en" class="form-control" />
									</form>
								</div>
								<div class="modal-footer">
									<input class="btn btn-success btn-block btn-leg" type="submit" value="Add" id="keyadd"/>
									<a href="#" class="btn" data-dismiss="modal">close</a>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div id="ccc">
					<div class="modal fade" id="category_adder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
								    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
								    <h4 class="modal-title" id="myModalLabel">Add Category</h4>
								</div>
								<div class="modal-body" id="form-catadd">
									<form class="catadd" name="catadd">	
									<label>Arabic content category</label>
									<input type="text" name="fc_name_ar" id="fc_name_ar" class="form-control" />
									<br>
									<br>
									<label>English content category</label> 
									<input type="text" id="fc_name_en" name="fc_name_en" class="form-control" />
									<br>
									</form>
								</div>
								<div class="modal-footer">
									<input class="btn btn-success btn-block btn-leg" type="submit" value="Add" id="catadd"/>
									<a href="#" class="btn" data-dismiss="modal">close</a>
								</div>
							</div>
						</div>
					</div>	
				</div>
				<div id="multimedia" style="display: none">
					<!-- to upload image-->
					<form method="post" action="" id="upload_image_form">
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-4">
								 <label for="userfile">Upload Image</label>
				        		 <input type="hidden" name="title" id="title" value="" class="form-control" />
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
			       			    <input type="submit" name="upload_image" value="upload image" id="uploadImage" class="form-control" />
							</div>
						</div>
    				</form>
    				<br>
					<!-- to upload video-->
					<form method="post" action="upload" id="upload_video_form">
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-4">
								  <label for="userfile">Upload Video</label>
								  <input type="hidden" name="title" id="video_title" class="form-control" />
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<input type="file" name="videoUserfile" id="videoUserfile" size="20" class="form-control" />
								
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<input type="submit" name="upload_video" value="upload video" id="uploadVideo" class="form-control" />
							</div>
						</div>
    				</form>
    				<br>
					<!-- to upload sound file-->
					<form method="post" action="" id="upload_sound_form">
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-4">
								   <label for="userfile">Upload Sound</label>
								   <input type="hidden" name="title" id="sound_title" value="" class="form-control"/>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								 <input type="file" name="soundUserfile" id="soundUserfile" size="20" class="form-control"/>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<input type="submit" name="upload_sound" value="upload sound file" id="uploadSound"  class="form-control"/>
							</div>
						</div>
    				</form>
    				<br>
    				<div class="row">
    					<div class="col-md-6 col-sm-6 col-xs-12">
    						<input type="button" class="btn btn-success btn-block btn-leg" value="Post" id="add_mm" name="add_mm" onclick="update_mm();"/>
    					</div>
    					<div class="col-md-6 col-sm-6 col-xs-12">
    						<input type="button" class="btn btn-success btn-block btn-leg" value="Skip" id="skip_mm" name="skip_mm" onclick="skip();" />
    					</div>
    				</div>
				</div>
			<div id="divt">	</div>
			
		</div>
	</div>
</div>
<script type="text/javascript" src="<?php echo base_url();?>js/ajaxfileupload.js"></script>
	
<!--upload image using ajax then submit new content with ajax also-->
<script>
var image_name = "NULL";
var video_name= "NULL";
var sound_name = "NULL";
var f_id;
// upload image event
$(document).ready(function () {
 $('form.adder').on('submit', function() {
   var obj = $(this), // (*) references the current object/form each time
   method = obj.attr('method'),
   data = {};
   tags=[];
   keys=[];
   obj.find('[name]').each(function(index, value) {
   // console.log(value);
   var obj = $(this),
    name = obj.attr('name'),
    value = obj.val();
    if($(obj).is(':checked')) { 
			if(name=='tags[]')
			{
				tags.push(value);
			}
			else if(name =='keywords[]')
			{
				keys.push(value);
			}
    	}
    if($(obj).val()!="" & obj.attr('checked')!="checked"){data[name] = value;}
  });
  console.log(tags);
  data['keys']=keys;
  data['tags']=tags; 
  console.log(data);
  //AJAX Code To Submit Form.
	$.ajax({
	type: method,
	url: "<?php echo base_url();?>index.php/cpanel/add_foloclor_record",
	data: data,
	cache: false,
	success: function(result){
	  f_id = result;
	  $('#adder').hide(1000);
	  $('#multimedia').show();
	  
	}
	});
		return false;
	});
});

$(function() {
	$('#upload_image_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_image",
				secureuri:false,
				fileElementId:'userfile',
				dataType: 'json',
				data    : {
                'title' : $('#title').val()
           		 },
				success: function (data, status)
				{
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							alert(data.error);
						}
						else
						{
							image_name = data.msg;
							document.getElementById("title").type='text';
							document.getElementById("title").value = image_name;
						}
					}
				},
				error: function (data, status, e)
				{
					alert(e);
				}
			}
		)
		return false;
	});
});
// upload video event
	$(function() {
	$('#upload_video_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_video",
				secureuri:false,
				fileElementId:'videoUserfile',
				dataType: 'json',
				data    : {
                'title' : $('#video_title').val()
           		 },
				success: function (data, status)
				{
					var dummy = document.getElementById("video_title");
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							dummy.type='text';
							dummy.value = data.error;
						}
						else
						{
							video_name = data.msg;
							dummy.type='text';
							dummy.value = video_name;
						}
					}
				},
				error: function (data, status, e)
				{
					alert(data);
					alert(status);
					
				}
			}
		)
		return false;
	});
});
// upload sound event
	$(function() {
	$('#upload_sound_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_soundFile",
				secureuri:false,
				fileElementId:'soundUserfile',
				dataType: 'json',
				data    : {
                'title' : $('#sound_title').val()
           		 },
				success: function (data, status)
				{
					var dummy = document.getElementById("sound_title");
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							dummy.type='text';
							dummy.value = data.error;
						}
						else
						{
							sound_name = data.msg;
							dummy.type='text';
							dummy.value = sound_name;
						}
					}
				},
				error: function (data, status, e)
				{
					dummy.type='text';
					dummy.value = e;
				}
			}
		)
		return false;
	});
});
function update_mm() {
  data=	 'f_image=' + image_name +
  		 '&f_video='+video_name+
  		 '&f_sound='+sound_name+
         '&f_id='+f_id;
  	$.ajax({
	type: "POST",
	url: "<?php echo base_url();?>index.php/cpanel/update_mm_foloclor_record",
	data: data,
	cache: false,
	success: function(result){
	  $('#multimedia').hide(1000);
	  $('#adder').show();
	  clear_form();
	}
});
}
function skip()
{
	clear_form();
}

function clear_form()
{
	$("#f_title_ar").val("");
	$("#f_title_en").val("");
	$("#f_location_ar").val("");
	$("#f_location_en").val("");
	$("#f_hi_ar").val("");
	$("#f_hi_en").val("");
	$("#f_content_ar").val("");
	$("#f_content_en").val("");
	$("#fc_name").val("");
	delete_tablerows(document.getElementById("selectedkeywords"));
	delete_tablerows(document.getElementById("selectedtags"));
}
	
function delete_tablerows(table){
	var rowCount = table.rows.length;
	while(table.rows.length > 0) {
		table.deleteRow(0);
}
}
</script>

<script>
		$(document).ready(function () {
			$("input#catadd").click(function(){
			$.ajax({			
					type: "POST",
					url:"<?php echo base_url();?>index.php/cpanel/add_category",
					data: $('form.catadd').serialize(),
					success: function(responce){
						var parsed = JSON.parse(responce);
    					var newOption = $('<option value='+parsed.fc_id+'>'+ parsed.fc_name_en +'</option>');
   						$('#fc_name').append(newOption);
   						$('#fc_name_ar').val("");
   						$('#fc_name_en').val("");	
   					},
					error: function(){
						alert("failure");
					}
				});
			});
		});
</script>
<script>
	function removeparent(e){
			e.parentNode.remove();
			console.log(e.parentNode);
		}
		$(document).ready(function () {
			$("input#keyadd").click(function(){
				$.ajax({
					type: "POST",
					url:"<?php echo base_url();?>index.php/cpanel/add_keyword",
					data: $('form.keyadd').serialize(),
					success: function(responce){
							var parsed = JSON.parse(responce);
							var name = parsed.fk_keyword_en;
							var val = parsed.fk_id;
    					  $('#selectedkeywords').append('<tr><td><input checked ="checked" type="checkbox" name="keywords[]"  value="'+val+'"/>'+name+'</td></tr>');
							$("#fk_ar").val("");
						    $("#fk_en").val("");
   					},
					error: function(){
						alert("failure");
					}
				});
			});
		});
 </script>
    
<script>
	function removeparent(e){
			e.parentNode.remove();
			console.log(e.parentNode);
		}
		$(document).ready(function () {
			$("input#tagadd").click(function(){
				$.ajax({
					type: "POST",
					url:"<?php echo base_url();?>index.php/cpanel/add_tag",
					data: $('form.tagadd').serialize(),
					success: function(responce){
						  var parsed = JSON.parse(responce);
						  var name = parsed.f_tag_en;
						  var val = parsed.ft_id;
    					  $('#selectedtags').append('<tr><td><input checked ="checked" type="checkbox" name="tags[]"  value="'+val+'"/>'+name+'</td></tr>');
   							$("#ft_ar").val("");
						    $("#ft_en").val("");
   					},
					error: function(){
						alert("failure");
					}
				});
			});
		});
    </script>
<script>
	
function displayRes(item, val, text) {
    var name = val; 
    console.log(name);
    var newTag = $('<tr><td><input checked ="checked" type="checkbox" name="tags[]"  value="'+val+'"/>'+text+'</td></tr>');
    $('#selectedtags').append(newTag);
    $('#tag').val('');
}
function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

$(function () {
	
	mysrc =[];
	for (var i = 0; i < mytags.length; i++) {
		mysrc.push({id: mytags[i].ft_id , name: mytags[i].f_tag_en});
	}
	
    $('#tag').typeahead({
         source:mysrc,
        itemSelected: displayRes 
    });
});


function displayKeys(item, val, text) {
    var name = val; 
    console.log(name);
    var newKey = $('<tr><td><input checked ="checked" type="checkbox" name="keywords[]"  value="'+val+'"/>'+text+'</td></tr>');
    $('#selectedkeywords').append(newKey);
    $('#keyword').val('');
}
function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

$(function () {
	
	mysrc =[];
	for (var k = 0; k < mykeys.length; k++) {
		mysrc.push({id: mykeys[k].fk_id , name: mykeys[k].fk_keyword_en});
	}
	
    $('#keyword').typeahead({
        source:mysrc,
        itemSelected: displayKeys 
    });
});
</script>