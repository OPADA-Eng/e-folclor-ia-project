<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-10 col-sm-10 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/check_comment.png" />Accept Comments Foloclor</p>
		</div>
		<div class="panel-body">
			<?php 
			if($result == "sucess"){ 
			 	if($active == "success"){
					echo "Comment became active";
				} 
				if($inactive == "success"){
					echo "Comment became inactive";
				} 
			?>
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="table-responsive">
						<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
			                    <tr>
									<th><p>Pending Comments</p></th>
								</tr>
			                </thead>
			                <tbody>
			                	<tr>
			                		<td>
			                			<?php if($this->cpanel_model->is_there_pending_comments()){ ?>
									    	<div class="DemoBS2">
												<div class="panel-group" id="accordion">
									    	<?php 
									    	$flag = TRUE;
								    		foreach ($foloclor_content as $f_c_value) { 
								    			$f_c_c = $this->cpanel_model->get_pending_foloclor_comments($f_c_value->f_id);
												if(!$flag){
												echo '<!-- div class="panel-body" -->
											            </div>
											            <!-- div id=accordion -->
											        </div>
											      </div>';
									 					
												
												} 
												$flag = TRUE;
												if($f_c_c){ 
													foreach ($f_c_c as $f_c_c_value) {
														if($flag){ $flag = FALSE; 
															?>
														      <div class="panel panel-primary">
														        <div class="panel-heading">
														          <h4 class="panel-title">
														            <a data-toggle="collapse" data-parent="#accordion"  href="#accordion<?php echo $f_c_c_value['fcc_id'];?>">
														              <?php echo $f_c_value->f_title_en;?>
														            </a>
														          </h4>
														        </div>
														         <div id="accordion<?php echo $f_c_c_value['fcc_id'];?>" class="panel-collapse collapse">
														         	 <div class="panel-body">
															            <div class="titles">
																	    	<h4>Historical Importance </h4>
																	        <span></span>
																        </div>
														         	 	<div class="well">
														         	 		<?php echo $f_c_value->f_historical_importance_en;?>
														         	 	</div>
														         	 	<div class="titles">
																	    	<h4>Comments </h4>
																	        <span></span>
																        </div>
																   
													 <?php } ?>
												<div class="well">
													<div>
														<?php echo $f_c_c_value['comment']; ?></br>
									          			<small class="text-muted"><?php echo date('F j, Y, g:i a',$f_c_c_value['date_time']); ?></small>
													</div>
													<br />
													<div  class="boreder_btn">
														<a href="<?php echo base_url();?>index.php/cpanel/active_comment/<?php echo $f_c_c_value['fcc_id'];?>" > Active Comment</a>
														<a href="<?php echo base_url();?>index.php/cpanel/inactive_comment/<?php echo $f_c_c_value['fcc_id'];?>" > Inactive Comment</a>
													</div>
									          		
												</div>
												<?php 
												// End foreach ($f_c_c as $f_c_c_value) 
												} 
											// End if($f_c_c)
											}
										// End foreach ($foloclor_content as $f_c_value) 
										}?>
									    <!-- End tap pending -->
									    </div></div>
									    <?php } else {
									    	echo "Not found a pending comments";
									    } ?>
			                		</td>
			                	</tr>
			                </tbody>
						</table>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-6">
					<div class="table-responsive">
						<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
							<thead>
			                    <tr>
									<th><p>Inactive Comments</p></th>
								</tr>
			                </thead>
			                <tbody>
			                	<tr>
			                		<td>
			                			<?php if($this->cpanel_model->is_there_inactive_comments()){ ?>
									    	<div class="DemoBS2">
												<div class="panel-group" id="accordion">
									    	<?php 
									    	$flag = TRUE;
								    		foreach ($foloclor_content as $f_c_value) { 
								    			$f_c_c = $this->cpanel_model->get_inactive_foloclor_comments($f_c_value->f_id);
												if(!$flag){
												echo '<!-- div class="panel-body" -->
											            </div>
											            <!-- div id=accordion -->
											        </div>
											      </div>';
												} 
												$flag = TRUE;
												if($f_c_c){ 
													foreach ($f_c_c as $f_c_c_value) {
														if($flag){ $flag = FALSE; 
															?>
														      <div class="panel panel-primary">
														        <div class="panel-heading">
														          <h4 class="panel-title">
														            <a data-toggle="collapse" data-parent="#accordion"  href="#accordion<?php echo $f_c_c_value['fcc_id'];?>">
														              <?php echo $f_c_value->f_title_en;?>
														            </a>
														          </h4>
														        </div>
														         <div id="accordion<?php echo $f_c_c_value['fcc_id'];?>" class="panel-collapse collapse">
														         	 <div class="panel-body">
															            <div class="titles">
																	    	<h4>Historical Importance </h4>
																	        <span></span>
																        </div>
														         	 	<div class="well">
														         	 		<?php echo $f_c_value->f_historical_importance_en;?>
														         	 	</div>
														         	 	<div class="titles">
																	    	<h4>Comments </h4>
																	        <span></span>
																        </div>
																   
													 <?php } ?>
												<div class="well">
													<div>
														<?php echo $f_c_c_value['comment']; ?></br>
									          			<small class="text-muted"><?php echo date('F j, Y, g:i a',$f_c_c_value['date_time']); ?></small>
													</div>
													<br />
													<div  class="boreder_btn">
														<a href="<?php echo base_url();?>index.php/cpanel/active_comment/<?php echo $f_c_c_value['fcc_id'];?>" > Active Comment</a>
														
													</div>
									          		
												</div>
												<?php 
												// End foreach ($f_c_c as $f_c_c_value) 
												} 
											// End if($f_c_c)
											}
										// End foreach ($foloclor_content as $f_c_value) 
										}?>
									    <!-- End tap pending -->
									    </div></div>
									    <?php } else {
									    	echo "Not found an inactive comments";
									    } ?>
									    </div>
			                		</td>
			                	</tr>
			                </tbody>
						</table>
					</div>
				</div>
			</div>
			<?php	
			}
			?>
		</div>
	</div>
</div>




