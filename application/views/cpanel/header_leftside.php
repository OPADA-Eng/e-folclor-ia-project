<html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>C_Panel E-Foloclor</title>
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>css/cp/bootstrap.css" rel="stylesheet">
	<link href="<?php echo base_url();?>css/cp/style-cpanel.css" rel="stylesheet" />
	<link href="<?php echo base_url();?>css/cp/bootstrap-dialog.css" rel="stylesheet">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel.css">
	<link type="text/css" rel="stylesheet" href="<?php echo base_url();?>css/vs/vote_panel_style.css">
    <!-- Add custom CSS here -->
    <link href="<?php echo base_url();?>css/cp/sb-admin.css" rel="stylesheet">
    <!-- Page Specific CSS -->
    <script src="<?php echo base_url();?>js/template/jquery-1.10.2.min.js" type="text/javascript" ></script>
    <script src="<?php echo base_url();?>js/jquery.js" type="text/javascript" ></script>
    <script src="<?php echo base_url();?>js/bootstrap-typeahead.js"></script>
    <script src="<?php echo base_url();?>js/bootstrap.js"></script>
    <script src="<?php echo base_url();?>js/cp/bootstrap-dialog.js"></script>
  </head>
  <body>
    <div id="wrapper">
      <!-- Sidebar -->
      <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo base_url();?>index.php/auth/index">C_Panel E-Foloclor</a>
        </div>
        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
        	 <ul class="nav navbar-nav navbar-right navbar-user">
	        	<?php $user_id=$this->session->userdata('user_id');
	                  $username=$this->session->userdata('username');?>
	            <li><?php echo  anchor("auth/index/$user_id",$username); ?></li>
	        	<li><?php echo anchor("auth/logout",'Logout','id="button_all"');?></li>
	         </ul>
          	<ul class="nav navbar-nav side-nav">
          		<?php 
          		if($this->ion_auth->has_priv($this->session->userdata('user_id'))){
          			$flag = TRUE;
          			$cat = $this->ion_auth->get_priv_cat();
					foreach ($cat as $cat_value) {
						if(!$flag){echo '</ul></li>';}
			          	$priv = $this->ion_auth->has_priv($this->session->userdata('user_id'));
						$flag = TRUE;
			          	foreach ($priv as $priv_value) {
			          		if($cat_value->id == $priv_value['id_cat']){
			          			if($flag){ $flag = FALSE; ?>
			          				<li class="dropdown">
						              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
						              	<i class="fa fa-caret-square-o-down"></i> <img src="<?php echo base_url();?>images/<?php echo $cat_value->img;?>" /><?php echo $cat_value->cat_en;?> <b class="caret"></b>
						              </a>
						              <ul class="dropdown-menu">
						       		  <?php } ?>
				              		 <li <?php if(uri_string()== $priv_value['controller_name'].'/'.$priv_value['func']){?>class="active"><?php } else {?> > <?php }?><a href="<?php echo base_url();?>index.php/<?php echo $priv_value['controller_name'];?>/<?php echo $priv_value['func'];?>"><img src="<?php echo base_url();?>images/<?php echo $priv_value['img'];?>" /><?php echo $priv_value['privilage_en'];?></a></li>              
						       <?php   
		          			}
						}
					}
				}
			    ?>
            <!-- <li><a href="blank-page.html"><i class="fa fa-file"></i> Blank Page</a></li>
            <li class="dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="fa fa-caret-square-o-down"></i> Dropdown <b class="caret"></b></a>
              <ul class="dropdown-menu">
                <li><a href="#">Dropdown Item</a></li>
                <li><a href="#">Another Item</a></li>
                <li><a href="#">Third Item</a></li>
                <li><a href="#">Last Item</a></li>
              </ul>
            </li> 
	          -->
          </ul>
        </div><!-- /.navbar-collapse -->
      </nav>
    </div><!-- /#wrapper -->

    
