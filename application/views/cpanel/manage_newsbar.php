<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/newsbar.png" />Manage News Bar</p>
		</div>
		<div class="panel-body">
			<?php if($result == "sucess"){ ?>
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="table-responsive">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
				                    <tr>
										<th><div class="well"><p>Current Product</p></div></th>
									</tr>
				                </thead>
				                <tbody>
				                	<tr>
				                		<td>
				                			<?php
											echo form_open('cpanel/hide_product_from_newsbar');
											echo '<select multiple="10" name="id" class="form-control"> ';
											foreach ($product_current_news as $p_c_n_value) {
												echo '<option value="'.$p_c_n_value['ln_id'].'"><p>'.$p_c_n_value['title_en'].'</p></option>';
											}
											echo '</select><br>';
											echo form_hidden('type',$p_c_n_value['type']); 
											echo form_submit('hide_product','Hide','class="btn btn-leg btn-primary btn-block"');
											echo '<br>';
											echo form_close();
				                			?>
				                		</td>
				                		
				                	</tr>
				                </tbody>
				            </table>
						</div>
						<div class="table-responsive">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
				                    <tr>
										<th><div class="well"><p>All Products with classifications</p></div></th>
									</tr>
				                </thead>
				                <tbody>
				                	<tr>
				                		<td>
				                			<?php
				                			foreach ($product_categories as $key => $p_c_c) {
												if($p_c_c['products'] != NULL){
													echo '<p>'.$p_c_c['category']->cat_en.':</p>';
													echo form_open('cpanel/add_product_to_newsbar');
													echo '<select multiple="10" name="id" class="form-control"> ';
													foreach ($p_c_c['products'] as $key => $p_c) {
														echo '<option value="'.$p_c->pc_id.'"><p>'.$p_c->title_en.'</p></option>';
													}
													echo '</select><br>';
													echo form_submit('add_product','Add','class="btn btn-leg btn-primary btn-block"');
													echo '<br>';
													echo form_close();
												}
											}
				                			?>
				                		</td>
				                		
				                	</tr>
				                </tbody>
				            </table>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="table-responsive">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
				                    <tr>
										<th><div class="well"><p>Current Foloclor</p></div></th>
									</tr>
				                </thead>
				                <tbody>
				                	<tr>
				                		<td>
				                			<?php
											echo form_open('cpanel/hide_foloclor_from_newsbar');
											echo '<select multiple="10" name="id" class="form-control"> ';
											foreach ($foloclor_current_news as $f_c_n_value) {
												echo '<option value="'.$f_c_n_value['ln_id'].'"><p>'.$f_c_n_value['f_title_en'].'</p></option>';
											}
											echo '</select><br>';
											echo form_hidden('type',$f_c_n_value['type']); 
											echo form_submit('hide_foloclor','Hide','class="btn btn-leg btn-primary btn-block"');
											echo '<br>';
											echo form_close();
				                			?>
				                		</td>
				                		
				                	</tr>
				                </tbody>
				            </table>
						</div>
						<div class="table-responsive">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
				                    <tr>
										<th><div class="well"><p>All Foloclors with classifications</p></div></th>
									</tr>
				                </thead>
				                <tbody>
				                	<tr>
				                		
				                		<td>
				                			<?php
				                			foreach ($foloclor_categories as $key => $f_c_c) {
												if($f_c_c['foloclors'] != NULL){
													echo '<p>'.$f_c_c['category']->fc_name_en.':</p>';
													echo form_open('cpanel/add_foloclor_to_newsbar');
													echo '<select multiple="10" name="id" class="form-control"> ';
													foreach ($f_c_c['foloclors'] as $key => $f_c) {
														echo '<option value="'.$f_c->f_id.'"><p>'.$f_c->f_title_en.'</p></option>';
													}
													echo '</select><br>';
													echo form_submit('add_foloclor','Add','class="btn btn-leg btn-primary btn-block"');
													echo '<br>';
													echo form_close();
												}
											}
				                			?>
				                		</td>
				                	</tr>
				                </tbody>
				            </table>
						</div>
					</div>
				</div>
				
				
		<?php } ?>
		</div>
	</div>
</div>




