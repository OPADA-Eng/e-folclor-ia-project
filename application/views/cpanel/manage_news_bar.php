<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/manage_newsbar.png" />Manage NewsBar</p>
		</div>
		<div class="panel-body">
			<?php if($result == "success"){ ?>
				<div class="row">
					<?php
					if($mode === 'error'){
						echo $error;
					}
					?>
				</div>
				<div class="row">
					
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="table-responsive">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
				                    <tr>
										<th><div class="well"><p>All News</p></div></th>
									</tr>
				                </thead>
				                <tbody>
				                	<tr>
				                		<td>
				                			<?php
											echo form_open('cpanel/add_news_to_newsbar');
											echo '<select multiple="10" name="n_id" class="form-control"> ';
											foreach ($news as $key => $news_value) {
												echo '<option value="'.$news_value->n_id.'"><p>'.$news_value->title_en.'</p></option>';
											}
											echo '</select><br>';
											echo form_submit('add','Add To Newsbar','class="btn btn-leg btn-primary btn-block"');
											echo '<br>';
											echo form_close();
				                			?>
				                		</td>
				                		
				                	</tr>
				                </tbody>
				            </table>
						</div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<div class="table-responsive">
							<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
								<thead>
				                    <tr>
										<th><div class="well"><p>Current news in newsbar</p></div></th>
									</tr>
				                </thead>
				                <tbody>
				                	<tr>
				                		<td>
				                			<?php
											echo form_open('cpanel/hide_news_from_newsbar');
											echo '<select multiple="10" name="n_id" class="form-control"> ';
											foreach ($current_news as $current_news_value) {
												echo '<option value="'.$current_news_value['n_id'].'"><p>'.$current_news_value['title_en'].'</p></option>';
											}
											echo '</select><br>';
											echo form_submit('hide','Hide From Newsbar','class="btn btn-leg btn-primary btn-block"');
											echo '<br>';
											echo form_close();
				                			?>
				                		</td>
				                		
				                	</tr>
				                </tbody>
				            </table>
						</div>
					</div>
				</div>
				
				
		<?php } ?>
		</div>
	</div>
</div>




