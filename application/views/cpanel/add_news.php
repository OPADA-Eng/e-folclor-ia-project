<?php
function print_form()
{
	echo form_open_multipart('cpanel/insert_news');
	
	echo '<p>Arabic Adress:</p>';
	echo form_input('title_ar',set_value('title_ar'),'class="form-control" placeholder="Adress" required=""');
	echo '<p>English Adress:</p>';
	echo form_input('title_en',set_value('title_en'),'class="form-control" placeholder="Adress" required=""');
	echo '<p>Arabic Details:</p>';
	echo form_textarea('text_ar',set_value('text_ar'),'class="form-control" placeholder="نص الخبر" rows="5"  ');
	echo '<p>English Details:</p>';
	echo form_textarea('text_en',set_value('text_en'),'class="form-control" placeholder="نص الخبر" rows="5"  ');
	?>
	<p>display on news bar:</p>
	<div class="radio">
      <label>
        <input required="" type="radio" name="news_bar" id="optionsRadios1" value="1" checked>
        Yes
      </label>
    </div>
    <div class="radio">
      <label>
        <input required="" type="radio" name="news_bar" id="optionsRadios2" value="0">
        No
      </label>
    </div>
    <hr/> 
	<?php echo form_submit('send','Send',' class="btn btn-leg btn-primary btn-block"'); 
	echo form_close();
}
?>
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/add_news.png" />Add News</p>
		</div>
		<div class="panel-body">
			<?php if($result == "success"){
                  	if($mode == 'success'){
						echo 'news has been added successfuly';
					}
					else {
						print_form();
						if($mode == 'error'){
							echo $error;
						}
						echo validation_errors();
					}
			}else{
				echo "You don't have a permission";
			}  ?>
		</div>
	</div>
</div>

