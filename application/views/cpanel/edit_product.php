<style>
.tagcloud_f:before,
.tagcloud_f:after {
  content:"";
  display:table;
  line-height:0;
}

.tagcloud_f:after {
  clear:both;
}

.tagcloud_f a {
  float:left;
  background-color:#ccc;
  -webkit-transition:all 0.8s linear 0s;
  -moz-transition:all 0.8s linear 0s;
  -o-transition:all 0.8s linear 0s;
  transition:all 0.8s linear 0s;
  color:#555E6B;
  font-size:12px !important;
  margin:0 5px 5px 0;
  padding:3px 9px;
}

.tagcloud_f a:hover {
  background-color:#0088CC;
  color:#FFF;
  text-decoration:none;
}
.border_p p{
	border: 1px solid #ccc;
	padding: 10px;
	border-radius: 20px;
}
.general_info:hover{
	  background-color: #1C6093;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #1C6093;
}

.general_info{
	  background-color: #428BCA;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.content:hover{
	  background-color: #560B46;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #560B46;
}

.content{
	  background-color: #7E1C65;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.multimedia:hover{
	  background-color: #B2794F;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #B2794F;
}

.multimedia{
	  background-color: #D39D6E;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.extra:hover{
	  background-color: #338233;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #338233;
}

.extra{
	  background-color: #5AB75B;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.added:hover{
	  background-color: #662258;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #662258;
}

.added{
	  background-color: #9E438B;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
	.pac-container {
    background-color: #FFF;
    z-index: 20;
    position: fixed;
    display: inline-block;
    float: left;
}

	.pac-container {
    background-color: #FFF;
    z-index: 20;
    position: fixed;
    display: inline-block;
    float: left;
}
.modal{
    z-index: 20;   
}
.modal-backdrop{
    z-index: 10;        
}?
</style>
<script>
	var mytypes = <?php echo $product_types; ?>;
	var mykeys = <?php echo $keys; ?>;
	var mycats = <?php echo $product_categories;
	?>;
</script>
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/c_edit.png" />Edit Product</p>
		</div>
		<div class="panel-body">
			<?php if($result == "sucess"){
				foreach($products as $row) : ?>
				<article >
					<details>
						<summary class="form-control">General information</summary>
						<h4 class="general_info">- English Name</h4>
						<p> <?php echo $row['item_data']['product_name_en']; ?></p>
						<h4 class="general_info">- Arabic Name</h4>
						<p> <?php echo $row['item_data']['product_name_ar']; ?></p>
						<h4 class="general_info">- Category Name</h4>
						<p> <?php echo $row['item_data']['cat_en']; ?></p>
						<h4 class="general_info">- Product type</h4>
						<p> <?php echo $row['item_data']['pt_name_en']; ?></p>
					</details>
					<details>
						<summary class="form-control">Product proberities</summary><br />
						<div class="row">
							<div class="col-md-4 col-sx-4 col-xs-4">
								<p class="content">- Min price = <?php echo $row['item_data']['min_price']; ?> </p>
							</div>
							<div class="col-md-4 col-sx-4 col-xs-4">
								<p class="content">- Max price = <?php echo $row['item_data']['max_price'];  ?> </p>
							</div>
							<div class="col-md-4 col-sx-4 col-xs-4">
								<p class="content">- unit weight = <?php echo $row['item_data']['weight']; ?> <?php echo $row['item_data']['weight_type']; ?> </p>
							</div>
						</div>
						<div class="row">
							<div class="col-md-4 col-sx-4 col-xs-4">
								<p class="content">- volume = <?php echo $row['item_data']['volum']; ?> </p>
							</div>
							<div class="col-md-4 col-sx-4 col-xs-4">
								<p class="content">- Height = <?php echo $row['item_data']['height']; ?> </p>
							</div>
							<div class="col-md-4 col-sx-4 col-xs-4">
								<p class="content">- Width = <?php echo $row['item_data']['width']; ?></p>
							</div>
						</div>
						
					</details>
					<details>
						<summary class="form-control">Extra details</summary><br />
						<h4 class="multimedia">Arabic culture detail</h4>
						<p> <?php echo $row['item_data']['details_ar']; ?> </p>
						<h4 class="multimedia">English culture detail</h4>
						<p> <?php echo $row['item_data']['details_en']; ?> </p>
							
					</details>
					<br />
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-12">
							<?php if($row['item_data']['image']!="NULL"){ ?>
							<img class="img-thumbnail" src="<?php echo base_url();?>uploads/images/371_177/<?php echo $row['item_data']['image'];?>"/>
						</div>
						<div class="col-md-7 col-sm-7 col-xs-12 tagcloud_f"  >
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<a>Min Repo Quantity =<?php echo $row['item_data']['min_quantity']; ?></a>
									<a>Max Repo Quntity =<?php echo $row['item_data']['max_quantity']; ?></a>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12 col-sm-12 col-xs-12">
									<h4 class="general_info">- KeyWords</h4>
									<div tagcloud_f>
										<?php foreach($row['item_keywords'] as $keyword) : 	?>
											<a> <?php echo $keyword->fk_keyword_ar;?>  </a><a> <?php echo $keyword->fk_keyword_en;?></a>
										<?php  endforeach;?>
									</div>
								</div>
							</div>
						</div>
					</div>
					
					<br>
					<?php if($row['item_data']['publish_state'] == '3'){ ?>
						<p>this content rejected by marketing responce for :</p>
						<p><?php echo $row['item_data']['marketing_respons_opinion']; ?> </p>
					<?php } ?>

					<?php } ?>
					
					<div>
						<a class="btn btn-primary" id="<?php echo $row['ID'];?>approve" onclick="approve(<?php echo $row['ID'];?>,<?php echo $user_id;?>);" href="javascript:void(0);">approve</a>
						<?php if(!$user_type){ ?>
						<a class="btn btn-primary" id="<?php echo $row['ID'];?>reject" onclick="reject(<?php echo $row['ID'];?>);" href="javascript:void(0);">reject</a>
						<div id="<?php echo $row['ID'];?>divOpinion"; style="display: none;">
							<label>write your opinion</label>
							<br>
							<input  type="hidden" id="<?php echo $row['ID'];?>content_id" value="<?php echo $row['ID'];?>"></input>
							<textarea name="checker_opinion" rows="3" cols="40" id ="<?php echo $row['ID'];?>checker_opinion" ></textarea>
							<br>
							<br>
							<button  type="button" id="<?php echo $row['ID'];?>submit" value="Submit" onclick="submitreject(<?php echo $row['ID'];?>,<?php echo $user_id;?>)" >
								add opinion
							</button>
							<br>
						</div>
						<?php } 
						else
							{ ?>
								<a class="btn btn-primary" id="<?php echo $row['ID'];?>reject" onclick="adminreject(<?php echo $row['ID'];?>,<?php echo $user_id;?>);" href="javascript:void(0);">reject</a>
						<?php }
							 ?>
					<!-- Button trigger modal -->
						<button type="button" class="btn btn-primary" 
						onclick='done(<?php echo json_encode($row["item_data"]);?>,<?php echo json_encode($row["item_keywords"]);?>);'
						data-toggle="modal" data-target="#myModal" >update product</button>
		   		 </div>  
		   		 </article>
				<hr>		
				<article>
					<details>
					<summary class="form-control">Update multi media</summary><br />
						<div>
						<form method="post" action="" id="upload_image_form">
							<div class="row">
								<div class="col-md-4 col-sm-4 col-xs-4">
									 <label for="userfile">Upload Image</label>
					        		 <input type="hidden" name="title" id="title" value="" class="form-control" />
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
									<input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
								</div>
								<div class="col-md-4 col-sm-4 col-xs-4">
				       			    <input type="submit" name="upload_image" value="upload image" id="uploadImage" class="form-control" />
								</div>
							</div>
	    				</form>
	    				<br>
	    				<br>
    				<div class="row">
    					<div class="col-md-6 col-sm-6 col-xs-12">
    						<input type="button" class="btn btn-success btn-block btn-leg" value="Post" id="add_mm" name="add_mm" onclick="update_mm(<?php echo $row['ID'];?>);"/>
    					</div>
    					<div class="col-md-6 col-sm-6 col-xs-12">
    						<input type="button" class="btn btn-success btn-block btn-leg" value="Skip" id="skip_mm" name="skip_mm" onclick="skip();" />
    					</div>
    				</div>
    				</details>
    			</article>
		<?php endforeach; } 
			else{
				echo "You don't have a permission";
			}?>
		</div>
	</div>
		<div id="divy"> </div>
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					 <div class="modal-header">
						 <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						    <h4 class="modal-title" id="myModalLabel">update content</h4>
							  </div>
					  <div class="modal-body">
						  <form class="contact" name="contact" id="contact">	
    						  	<input type="hidden" id="pc_id" name="pc_id" value="" />	 
    						  	Product category
    						  	<select id="cat_en" name="cat_en" class="form-control">
								</select>
								<br>				   
    						  	Product Arabic Name
								<input type="text" id="product_name_ar" name="product_name_ar" class="form-control">
								<br>
								Product English Name
								<input type="text" id="product_name_en" name="product_name_en" class="form-control">
								<br>
								Product Type 
								<select id="type_en" name="type_en" class="form-control"></select>
								<br>
								Minimum Price 
								<input type="number" id="min_price" name="min_price" class="form-control">
								<br>
								Maximum Price
								<input type="number" name="max_price" id="max_price" class="form-control" />
								<br>
								<details><summary class="form-control">reset product proberities</summary>
								Color
								<input type="text" name="color" id="color" class="form-control"/>	<br>
								Weight
								<input type="number" name="weight" id="weight" class="form-control"/>	<br>						
								Weight Type
								<input type="text" name="weight_type" id="weight_type" class="form-control"/>
								<br>
								Volume
								<input type="number" name="volum" id="volum" class="form-control"/>	<br>
								Height
								<input type="number" name="height" id="height" class="form-control"/>	<br>
								<br>
								Width
								<input type="number" name="width" id="width" class="form-control"/>	<br>
								Max Quantity
								<input type="number" name="max_quantity" id="max_quantity" class="form-control"/>	<br>
								Min Quantity
								<input type="number" name="min_quantity" id="min_quantity" class="form-control"/>	<br>
								</details>
								Arabic Details
								<textarea name="details_ar" id="details_ar" class="form-control"></textarea>
								<br>				
								English Details
								<textarea name="details_en" id="details_en" class="form-control"></textarea>
								<br>
								<div>
									<label>keywords</label>
									<input type="text" id="keyword" class="form-control"/>
									<table cellpadding="4"   border="0">
										<tbody id="selectedkeywords">
										</tbody>
									</table>
								</div>	
   							</form>
						 </div>
					  <div class="modal-footer">
					  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					    	<button type="button" class="btn btn-primary" id="save" onclick="save();">Save changes</button>
    			</div>
    		</div>
    	</div>
<script type="text/javascript" src="<?php echo base_url();?>js/ajaxfileupload.js"></script>

<script>

function done(row,keys) { 
	
	$("#pc_id").val(row.pc_id);
	$("#product_name_ar").val(row.product_name_ar);
	$("#product_name_en").val(row.product_name_en);
	
	$("#min_price").val(row.min_price);
	$("#max_price").val(row.max_price);
	
	$("#color").val(row.color);
	$("#weight").val(row.weight);
	$("#weight_type").val(row.weight_type);
	
	
	$("#volum").val(row.volum);
	$("#height").val(row.height);
	$("#width").val(row.width);

	$("#min_quantity").val(row.min_quantity);
	$("#max_quantity").val(row.max_quantity);
	
	$("#details_ar").val(row.details_ar);
	$("#details_en").val(row.details_en);

	delete_tablerows(document.getElementById("selectedkeywords"));
	
	if(keys.length !=0)
	{
		for (var k= 0; k < keys.length; k++) {
			var newKey = $('<tr><td><input checked ="checked" type="checkbox" name="keywords[]"  value="'+keys[k].fk_id+'"/>'+keys[k].fk_keyword_en+'</td></tr>');
			$('#selectedkeywords').append(newKey);
		}
	}
	
	for (var t=0; t < mycats.length; t++) {
			var newOption = $('<option value="'+mycats[t].pcat_id+'">'+mycats[t].cat_en+'</option>');
   			$('#cat_en').append(newOption);
	}
	$("#cat_en").val(row.pcat_id);
	
	for (var p=0; p < mytypes.length; p++) {
			var newOption = $('<option value="'+mytypes[p].pt_id+'">'+mytypes[p].pt_name_en+'</option>');
   			$('#type_en').append(newOption);
	}
	$("#type_en").val(row.pt_id);	
}

function save()
{
	var obj = $("#contact"), // (*) references the current object/form each time
  	data = {};
  	keys=[];
    obj.find('[name]').each(function(index, value) {
    // console.log(value);
  	var obj = $(this),
    name = obj.attr('name'),
    value = obj.val();

    if($(obj).is(':checked')) { 
		
		if(name =='keywords[]')
   		{
    		keys.push(value);
    	}
	}
   	if($(obj).val()!="" & obj.attr('checked')!="checked"){data[name] = value;}
  });
    
  data['keys'] = keys;
  console.log(data);
  $.ajax({
		type: "POST",
		url: "<?php echo base_url();?>index.php/cpanel/update_product",
		data: data,
		cache: false,
		success: function(result){
		}
		});
		return false;
}


function displayKeys(item, val, text) {
    var name = val; 
    console.log(name);
    var newKey = $('<tr><td><input checked ="checked" type="checkbox" name="keywords[]"  value="'+val+'"/>'+text+'</td></tr>');
    $('#selectedkeywords').append(newKey);
    $('#keyword').val('');
}

function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

function delete_tablerows(table){
	var rowCount = table.rows.length;
	while(table.rows.length > 0) {
  		table.deleteRow(0);
	}
}

$(function () {
	
	mysrc =[];
	for (var k = 0; k < mykeys.length; k++) {
		mysrc.push({id: mykeys[k].fk_id , name: mykeys[k].fk_keyword_en});
	}

	$('input').change(function(){  
          var str = this.id;
		  var id =str.substr(0, 7);
		
	});
	
    $('#keyword').typeahead({
        source:mysrc,
        itemSelected: displayKeys 
    });
});

	function reject(content_id){
	var str= "#"+content_id+"divOpinion";
	$(str).show();
}

function approve(content_id,user_id){
	var dataString = 'content_id='+ content_id+
					 '&user_id='+ user_id;	
			
	$.ajax({
	type: 'POST',
	url: "<?php echo base_url();?>index.php/cpanel/approve_product",
	data: dataString,
	cache: false,
	success: function(result){
	var atext = "#"+content_id+"reject";
	var btext = "#"+content_id+"approve";
	$(atext).text("reject");
	$(btext).text("approved");
	}
	});
}
function submitreject (content_id,user_id) {
  var co = "#"+content_id+"checker_opinion";
  var checker_opinion =  $(co).val();
  var ci = "#"+content_id+"content_id";
  var content_id = $(ci).val();
 
 var dataString = 'content_id='+ content_id+
				 '&checker_opinion='+ checker_opinion+
 				 '&user_id='+ user_id;			;		
$.ajax({
	type: 'POST',
	url: "<?php echo base_url();?>index.php/cpanel/reject_product",
	data: dataString,
	cache: false,
	success: function(result){
	var str= "#"+content_id+"divOpinion";
	var div= "#"+content_id+"content_body";
	var atext = "#"+content_id+"reject";
	var btext = "#"+content_id+"approve";
	$(str).hide();
	$(div).hide();
	$(atext).text("rejected");
	$(btext).text("approve");
	}
});
}			

function adminreject(content_id,user_id) {
  var checker_opinion =  "NULL";
  var dataString = 'content_id='+ content_id+
				   '&checker_opinion='+ checker_opinion+
 				   '&user_id='+ user_id;	
$.ajax({
	type: 'POST',
	url: "<?php echo base_url();?>index.php/cpanel/reject_product",
	data: dataString,
	cache: false,
	success: function(result){
	var atext = "#"+content_id+"reject";
	var btext = "#"+content_id+"approve";
	$(atext).text("rejected");
	$(btext).text("approve");
	}
});
}

var image_name = "NULL";
var video_name= "NULL";
var sound_name = "NULL";
$(function() {
	$('#upload_image_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_image",
				secureuri:false,
				fileElementId:'userfile',
				dataType: 'json',
				data    : {
                'title' : $('#title').val()
           		 },
				success: function (data, status)
				{
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							alert(data.error);
						}
						else
						{
							image_name = data.msg;
							document.getElementById("title").type='text';
							document.getElementById("title").value = image_name;
						}
					}
				},
				error: function (data, status, e)
				{
					alert(e);
				}
			}
		)
		return false;
	});
});

function update_mm(pc_id) {
  data=	 'f_image=' + image_name +
         '&pc_id='+pc_id;
  	$.ajax({
	type: "POST",
	url: "<?php echo base_url();?>index.php/cpanel/update_im_product_record",
	data: data,
	cache: false,
	success: function(result){	 
	}
});
}
function skip()
{
	$("#multimedia").hide();
}
</script>