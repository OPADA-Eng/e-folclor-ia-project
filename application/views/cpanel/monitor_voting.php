<style type="text/css">
.field:hover{
  background-color: #1C6093;
  -webkit-transition:all 0.8s linear 0s;
  -moz-transition:all 0.8s linear 0s;
  -o-transition:all 0.8s linear 0s;
  transition:all 0.8s linear 0s;
  color:#FFFFFF;
  padding: 10px;
  box-shadow: 0px 0px 8px #1C6093;
}

.field{
	  background-color: #428BCA;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.added:hover{
	  background-color: #560B46;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #560B46;
}

.added{
	  background-color: #7E1C65;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.rating_field:hover{
	  background-color: #C48D12;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #C48D12;
}

.rating_field{
	  background-color: #FDBD15;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.border_added{
	border: 1px solid #CCCCCC;
	border-radius: 5px;
	padding: 10px;
}
strong{
	color: red;
}
</style>
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<div class="panel panel-default">
		
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/check_comment.png" />Monitor voting panel</p>
		</div>
		
		<div class="panel-body">
			<?php if($result == "sucess"){ ?>
				
				<?php foreach($foloclor_articles as $row) :
				 ?>
				<div style="border: 1px solid #ccc;border-radius: 10px;padding: 15px;">
				<div class="panel-body" id="<?php echo $row['ID']?>content_body">
					<div class="row">
						<div class="col-md-5 col-sm-5 col-xs-12">
							<?php if($row['item_data']['f_image']!="NULL"){ ?>
								<img class="img-thumbnail"  src="<?php echo base_url();?>uploads/images/371_177/<?php echo $row['item_data']['f_image'];?>"/>
							<?php }?>
						</div>
						<div class="col-md-7 col-sm-7 col-xs-12">
							<h4 class="field">Title</h4>
							<p> <?php echo $row['item_data']['f_title_en']; ?></p>
							<h4 class="field">Category name</h4>
							<p> <?php echo $row['item_data']['fc_name_en']; ?></p>
						</div>
					</div>
					
					<h4 class="field">Historical importance</h4>
					<p> <?php  echo $row['item_data']['f_historical_importance_en']; ?></p>
					<h4 class="border_added">Added by <?php echo $row['item_data']['username']; ?></h4>
					<div class="row">
						 <div class="col-md-6 col-sm-6 col-xs-12">
			            <h4 class="rating_field">The average rating is at <strong id="values"><?php echo $row['item_data']['total_voting']; ?></strong></h4>
				        </div>
				        <div class="col-md-6 col-sm-6 col-xs-12">
				        	<h4 class="rating_field">This content was rated <strong id="times"><?php echo $row['rate_times']; ?></strong> times</h4>
				        </div>
					</div>
		             <div class="rate-result-cnt">
	        			 <div class="rate-bg" id="avg" style="width:<?php $rate_bg=(($row['item_data']['total_voting']/5)*100);  echo $rate_bg; ?>%"></div>
	       				 <div class="rate-stars"></div>
    				</div>    
	    			<br />
					<a class="btn btn-primary" id="<?php echo $row['ID'];?>deactive" onclick="deactive_content(<?php echo $row['ID'];?>);" href="javascript:void(0);">deactive</a>
					<a class="btn btn-primary" id="history" onclick="rating_history(<?php echo $row['ID'];?>);" href="javascript:void(0);"> view rating history</a>
					<a class="btn btn-primary" id="details" onclick='content_details(<?php echo json_encode($row["item_data"]);?>);' href="javascript:void(0);"> view content details</a>
				</div></div><br />
				<?php endforeach; } ?>
			</div>
		</div>
	</div>
<script type="text/javascript">
function rating_history(f_id)
{
	var dataString = 'f_id='+f_id;	
	$.ajax({
	type: 'POST',
	url: "<?php echo base_url();?>index.php/cpanel/get_rating_history",
	data: dataString,
	cache: false,
	success: function(result){
		var parsed =JSON.parse(result);
		console.log(parsed);
		displayRates(parsed);
	}
	});
}
function displayRates(rates)
{
	
	$table_body = "";
	$modal_body="";
	for (var i=0; i < rates.length; i++) {
		$table_body +='<tr><td>'+rates[i].username+'</td><td>'+rates[i].fv_voting+'</td>'+'<td>'+rates[i].fv_date+'</td></tr>';
	};
	$modal_body = '<div><table> <tr><th>User name</th> <th>User rates</th> <th>Rate date</th> </tr>'+$table_body+'</table></div>';
	BootstrapDialog.show({
            title: 'Foloclor Rate History',
            message: 
            		$modal_body,
            buttons: [{
                label: 'Close',
                action: function(dialogItself) {
                    dialogItself.close();
                }
            }
            ]
   });
}
function deactive_content(f_id)
 {
  var atext = "#"+f_id+"deactive";
   var dataString = 'f_id='+ f_id;	
  
  if($(atext).text() == "deactive")
  {  
		$.ajax({
		type: 'POST',
		url: "<?php echo base_url();?>index.php/cpanel/deactivate_content",
		data: dataString,
		cache: false,
		success: function(result){
		$(atext).text("active");
		}
		});
	}
	else
	{
		$.ajax({
		type: 'POST',
		url: "<?php echo base_url();?>index.php/cpanel/activate_content",
		data: dataString,
		cache: false,
		success: function(result){
		$(atext).text("deactive");
		}
		});
	}
}
function content_details(parsed){
  
 		$src = "<?php echo base_url();?>";	
		$image="";
  		$modal_body="";
  			if(parsed.f_image != 'null' && parsed.f_image != 'NULL' &&  parsed.f_image != null)
			{
				$image = 'Image <br><img width="150" height="100" src="'+$src+'uploads/images/thumbs/'+parsed.f_image+'" />';
				console.log($image);
			}	
			$modal_body += '<article>'+
						   '<h4 class="field">arabic title <h4><p>'+parsed.f_title_ar+'</p>'+
						   '<h4 class="field">English title<h4><p>'+parsed.f_title_en+'</p>'+
						   '<h4 class="field">Arabic category namet<h4><p>'+parsed.fc_name_ar+'</p>'+
						   '<h4 class="field">English category namet<h4><p>'+parsed.fc_name_en+'</p>'+
						   '<h4 class="field">Arabic content<h4><p>'+parsed.f_content_ar+'</p>'+
						   '<h4 class="field">English content<h4><p>'+parsed.f_content_en+'</p>'+
						   '<h4 class="field">Arabic historical importance<h4><p>'+parsed.f_historical_importance_ar+'</p>'+
						   '<h4 class="field">English historical importance<h4><p>'+parsed.f_historical_importance_en+'</p>'+
						   '<h4 class="field">Arabic location<h4><p>'+ parsed.f_location_ar+'</p>'+
						   '<h4 class="field">English location<h4><p>'+ parsed.f_location_en+'</p>'+
						   '</article><br>'+
						   $image+'<hr>';				   			
						   
		BootstrapDialog.show({
            title: 'Content Details ',
            message: 
            		$modal_body,
            buttons: [{
                label: 'Close',
                action: function(dialogItself) {
                   dialogItself.close();
                }
            }
            ]
        });
}
</script>