<style>
.general_info:hover{
	  background-color: #1C6093;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #1C6093;
}

.general_info{
	  background-color: #428BCA;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.content:hover{
	  background-color: #560B46;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #560B46;
}

.content{
	  background-color: #7E1C65;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.multimedia:hover{
	  background-color: #B2794F;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #B2794F;
}

.multimedia{
	  background-color: #D39D6E;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.extra:hover{
	  background-color: #338233;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #338233;
}

.extra{
	  background-color: #5AB75B;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
.added:hover{
	  background-color: #662258;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #662258;
}

.added{
	  background-color: #9E438B;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
	.pac-container {
    background-color: #FFF;
    z-index: 20;
    position: fixed;
    display: inline-block;
    float: left;
}

.border_foloclor{
	border: 1px solid #CCCCCC;
	border-radius: 5px;
	
}
.modal{
    z-index: 20;   
}
.modal-backdrop{
    z-index: 10;        
}?
</style>
<script>
	var mytags = <?php echo $tags; ?>;
	var mykeys = <?php echo $keys; ?>;
	var mycats = <?php echo $foloclor_categories;
	?>;
</script>
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/check_comment.png" />Edit Foloclor</p>
		</div>
		
		<div class="panel-body">
			<?php if($result == "sucess"){ ?>
				
				<?php foreach($foloclor_articles as $row) :
				 ?>
				<div class="border_foloclor">
					
					
				<div class="panel-body" id="<?php echo $row['ID']?>content_body">
				<article >

					<details >
						<summary class="form-control">general information</summary>
						<h4 class="general_info">- English Title</h4>
						<p> <?php echo $row['item_data']['f_title_en']; ?></p>
						<h4 class="general_info">- Arabic Title </h4>
						<p> <?php echo $row['item_data']['f_title_ar']; ?></p>
						<h4 class="general_info">- Arabic Category Name </h4>
						<p> <?php echo $row['item_data']['fc_name_ar']; ?></p>
						<h4 class="general_info">- English Category Name </h4>
						<p> <?php echo $row['item_data']['fc_name_en']; ?></p>
					</details>
					<details>
						<summary class="form-control"> Content </summary>
						<h4 class="content">- Arabic Content </h4>
						<p> <?php  echo $row['item_data']['f_content_ar']; ?></p>
						<h4 class="content">- English Content </h4>
						<p> <?php  echo $row['item_data']['f_content_en']; ?></p>
						<h4 class="content">- Arabic Historical Importance </h4>
						<p> <?php  echo $row['item_data']['f_historical_importance_ar']; ?></p>
						<h4 class="content">- English Historical Importance </h4>
						<p> <?php  echo $row['item_data']['f_historical_importance_en']; ?></p>
						<h4 class="content">- Arabic Location </h4>
						<p> <?php echo $row['item_data']['f_location_ar']; ?></p>
						<h4 class="content">- English Location </h4>
						<p> <?php echo $row['item_data']['f_location_en']; ?></p>
					</details>
					<details>
						<summary class="form-control">Multimedia</summary>
						<?php if($row['item_data']['f_image']!="NULL"){ ?>
							<h4 class="multimedia">- Image </h4>

							<img style="border: 5px solid #99643C;" width="150" height="100" src="<?php echo base_url();?>uploads/images/160_120/<?php echo $row['item_data']['f_image'];?>"/>

							<br>
						<?php }?>
						<?php if($row['item_data']['f_video']!="NULL"){ ?>
						Video <br>
						<video id="myVideo" controls>
							  <source id="mp4_src" src="<?php echo base_url();?>uploads/videos/<?php echo $row['item_data']['f_video'];?>" type="video/mp4">
							  Your browser does not support HTML5 video.
						</video>
						<br>
						<?php }?>
						<?php if($row['item_data']['f_sound']!="NULL"){ ?>
						Audio <br>
						<audio controls>
						   <source src="<?php echo base_url();?>uploads/sounds/<?php echo $row['item_data']['f_sound'];?>" type="audio/mpeg">
						   Your browser does not support the audio element.
						</audio>
						<br>
						<?php }?>
					</details>
					<details>
						<summary class="form-control">extra</summary>
						<h4 class="extra">- Keywords </h4>
						<?php foreach($row['item_keywords'] as $keyword) : 	?>
							<p> <?php echo $keyword->fk_keyword_ar;?>  , <?php echo $keyword->fk_keyword_en;?></p>
						<?php  endforeach;?>
						<h4 class="extra">- Tags </h4>
						<?php foreach($row['item_tags'] as $tag) : 	?>
							<input class="btn btn-success" id="tag_ar" type="button" onclick=getTags(<?php echo $tag->ft_id;?>); value="<?php echo $tag->f_tag_ar;?>" />
							<input class="btn btn-success" id="tag_en" type="button" onclick=getTags(<?php echo $tag->ft_id;?>); value="<?php echo $tag->f_tag_en;?>" /> 
							<br>
							<br>
						<?php  endforeach;?>
						<br>
					</details>
					<h4 class="added">- Added By : <?php echo $row['item_data']['username']; ?></h4> 
					<?php if($row['item_data']['publish_state'] == '2'){ ?>
						<div style="border: 1px solid #ccc;border-radius: 5px;padding: 5px;"> 
						<h4>this content rejected by checker for: </h4>
						<p><?php echo $row['item_data']['chacker_opinion']; ?> </p>
						</div><br />
					<?php } ?>
				</article>
				<div>
					<a class="btn btn-primary" id="<?php echo $row['ID'];?>approve" onclick="approve(<?php echo $row['ID'];?>,<?php echo $user_id;?>);" href="javascript:void(0);">approve</a>
					<?php if(!$user_type){ ?>
					<a class="btn btn-primary" id="<?php echo $row['ID'];?>reject" onclick="reject(<?php echo $row['ID'];?>);" href="javascript:void(0);">reject</a>
					<div id="<?php echo $row['ID'];?>divOpinion"; style="display: none;">
						<label>write your opinion</label>
						<br>
						<input  type="hidden" id="<?php echo $row['ID'];?>content_id" value="<?php echo $row['ID'];?>"></input>
						<textarea name="checker_opinion" rows="3" cols="40" id ="<?php echo $row['ID'];?>checker_opinion" ></textarea>
						<br>
						<br>
						<button  type="button" id="<?php echo $row['ID'];?>submit" value="Submit" onclick="submitreject(<?php echo $row['ID'];?>,<?php echo $user_id;?>)" >
							add opinion
						</button>
						<br>
					</div>
					<?php } 
					else
						{ ?>
							<a class="btn btn-primary" id="<?php echo $row['ID'];?>reject" onclick="adminreject(<?php echo $row['ID'];?>,<?php echo $user_id;?>);" href="javascript:void(0);">reject</a>
					<?php }
						 ?>
				<!-- Button trigger modal -->
					<button type="button" class="btn btn-primary" 
					onclick='done(<?php echo json_encode($row["item_data"]);?>,<?php echo json_encode($row["item_tags"]);?>,<?php echo json_encode($row["item_keywords"]);?>);'
					data-toggle="modal" data-target="#myModal" >update</button>
		   	 </div>  

			<hr>
			<article>
				<details>
				<summary class="form-control">Update multi media</summary><br />
					<div>
					<form method="post" action="" id="upload_image_form">
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-4">
								 <label for="userfile">Upload Image</label>
				        		 <input type="hidden" name="title" id="title" value="" class="form-control" />
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<input type="file" name="userfile" id="userfile" size="20" class="form-control"/>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
			       			    <input type="submit" name="upload_image" value="upload image" id="uploadImage" class="form-control" />
							</div>
						</div>
    				</form>
    				<br>
					<!-- to upload video-->
					<form method="post" action="upload" id="upload_video_form">
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-4">
								  <label for="userfile">Upload Video</label>
								  <input type="hidden" name="title" id="video_title" class="form-control" />
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<input type="file" name="videoUserfile" id="videoUserfile" size="20" class="form-control" />
								
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<input type="submit" name="upload_video" value="upload video" id="uploadVideo" class="form-control" />
							</div>
						</div>
    				</form>
    				<br>
					<!-- to upload sound file-->
					<form method="post" action="" id="upload_sound_form">
						<div class="row">
							<div class="col-md-4 col-sm-4 col-xs-4">
								   <label for="userfile">Upload Sound</label>
								   <input type="hidden" name="title" id="sound_title" value="" class="form-control"/>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								 <input type="file" name="soundUserfile" id="soundUserfile" size="20" class="form-control"/>
							</div>
							<div class="col-md-4 col-sm-4 col-xs-4">
								<input type="submit" name="upload_sound" value="upload sound file" id="uploadSound"  class="form-control"/>
							</div>
						</div>
    				</form>
    				<br>
    				<div class="row">
    					<div class="col-md-6 col-sm-6 col-xs-12">
    						<input type="button" class="btn btn-success btn-block btn-leg" value="Post" id="add_mm" name="add_mm" onclick="update_mm(<?php echo $row['ID'];?>);"/>
    					</div>
    					<div class="col-md-6 col-sm-6 col-xs-12">
    						<input type="button" class="btn btn-success btn-block btn-leg" value="Skip" id="skip_mm" name="skip_mm" onclick="skip();" />
    					</div>
    				</div>
    				</details></article>			
			</div>
		</div> <br />
		<?php endforeach;
			 }
			 else{
				echo "You don't have a permission";
				}?>
		</div>
		
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
			<div class="modal-dialog">
				<div class="modal-content">
					 <div class="modal-header">
						 <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
						    <h4 class="modal-title" id="myModalLabel">update content</h4>
							  </div>
							  <div class="modal-body">
    						  <form class="contact" name="contact" id="contact">	
    						  	<input type="hidden" id="f_id" name="f_id" value="" />	    
								<label>Foloclor category</label> 
								<br>
								
								<select id="fc_name" name="fc_name" class="form-control">
								
								</select>
								<label>Arabic title</label> <br>
								<input type="text" id="f_title_ar" name="f_title_ar" value="" class="form-control" />
								<label>English title</label> <br> 
								<input type="text" id="f_title_en" name="f_title_en" value="" class="form-control" >
								<label>Arabic content</label>
								<textarea name="f_content_ar" id="f_content_ar" class="form-control"></textarea>
								<label>English content</label>
								<textarea name="f_content_en" id="f_content_en" class="form-control" ></textarea>
								<label>Arabic historical importance</label>
								<textarea name="f_hi_ar" id="f_hi_ar" class="form-control"> </textarea>
								<label>English historicl importance</label> 
								<textarea name="f_hi_en" id="f_hi_en" class="form-control"></textarea>
								<label>Arabic location</label>
								<input type="text" name="f_location_ar" id="f_location_ar" value="" class="form-control" />
								<label>English location</label> 
								<input type="text" name="f_location_en" id="f_location_en" value="" class="form-control"  />
								<div>
									<label>keywords</label>
									<br>
									<input type="text" id="keyword" class="form-control" />
									<table cellpadding="4"   border="0">
										<tbody id="selectedkeywords">
										</tbody>
									</table>
								</div>
								<div>
									<label>tags</label>
									<input type="text" id="tag" class="form-control" />
									<table cellpadding="4"   border="0">
										<tbody id="selectedtags">
										</tbody>
									</table>
								</div>
							</div>
						</form>
					  	<div class="modal-footer">
					    	<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					    	<button type="button" class="btn btn-primary" id="save" onclick="save();">Save changes</button>
					  	</div>
					</div>
				  </div>
				</div>
			<br>	
	</div>
</div>


<script type="text/javascript">


function done(row,tags,keys) { 
	
	$("#f_id").val(row.f_id);
	$("#f_title_ar").val(row.f_title_ar);
	$("#f_title_en").val(row.f_title_en);
	
	$("#f_content_ar").val(row.f_content_ar);
	$("#f_content_en").val(row.f_content_en);
	
	$("#f_hi_ar").val(row.f_historical_importance_ar);
	$("#f_hi_en").val(row.f_historical_importance_en);
	
	$("#f_location_ar").val(row.f_location_ar);
	$("#f_location_en").val(row.f_location_en);
	
	$("#fc_name").val(row.fc_id);
	
	delete_tablerows(document.getElementById("selectedtags"));
	delete_tablerows(document.getElementById("selectedkeywords"));
	
	if(tags.length !=0)
	{
		for (var i = 0; i < tags.length; i++) {
				var newTag = $('<tr><td><td><input checked ="checked" type="checkbox" name="tags[]"  value="'+tags[i].ft_id+'"/>'+tags[i].f_tag_en+'</td></tr>');
				$('#selectedtags').append(newTag);
		};
	}
	
	if(keys.length !=0)
	{
		for (var k= 0; k < keys.length; k++) {
			var newKey = $('<tr><td><input checked ="checked" type="checkbox" name="keywords[]"  value="'+keys[k].fk_id+'"/>'+keys[k].fk_keyword_en+'</td></tr>');
			$('#selectedkeywords').append(newKey);
		}
	}
	
	for (var t=0; t < mycats.length; t++) {
			var newOption = $('<option value="'+mycats[t].fc_id+'">'+mycats[t].fc_name_en+'</option>');
   			$('#fc_name').append(newOption);
	}
	console.log(row.fc_id);
	$("#fc_name").val(row.fc_id);
}
function save()
{

	var obj = $("#contact"), // (*) references the current object/form each time
  	data = {};
  	tags=[];
  	keys=[];
    obj.find('[name]').each(function(index, value) {
    // console.log(value);
  	var obj = $(this),
    name = obj.attr('name'),
    value = obj.val();

    if($(obj).is(':checked')) { 
		
		if(name=='tags[]')
		{
			tags.push(value);
		}
		else if(name =='keywords[]')
   		{
    		keys.push(value);
    	}
	}
	
   	if($(obj).val()!="" & obj.attr('checked')!="checked"){data[name] = value;}
  });
    
  data['keys'] = keys;
  data['tags'] = tags;
  console.log(data);
  $.ajax({
		type: "POST",
		url: "<?php echo base_url();?>index.php/cpanel/update_content",
		data: data,
		cache: false,
		success: function(result){
		}
		});
			return false;
		}
		
function displayRes(item, val, text) {
    var name = val; 
    console.log(name);
    var newTag = $('<tr><td><input checked ="checked" type="checkbox" name="tags[]"  value="'+val+'"/>'+text+'</td></tr>');
    $('#selectedtags').append(newTag);
    $('#tag').val('');
}

function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

function delete_tablerows(table){
	var rowCount = table.rows.length;
	while(table.rows.length > 0) {
  		table.deleteRow(0);
	}
}

$(function () {
	
	mysrc =[];
	for (var i = 0; i < mytags.length; i++) {
		mysrc.push({id: mytags[i].ft_id , name: mytags[i].f_tag_en});
	}
	
    $('#tag').typeahead({
         source:mysrc,
        itemSelected: displayRes 
    });
});


function displayKeys(item, val, text) {
    var name = val; 
    console.log(name);
    var newKey = $('<tr><td><input checked ="checked" type="checkbox" name="keywords[]"  value="'+val+'"/>'+text+'</td></tr>');
    $('#selectedkeywords').append(newKey);
    $('#keyword').val('');
}

function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

$(function () {
	
	mysrc =[];
	for (var k = 0; k < mykeys.length; k++) {
		mysrc.push({id: mykeys[k].fk_id , name: mykeys[k].fk_keyword_en});
	}

	$('input').change(function(){  
          var str = this.id;
		  var id =str.substr(0, 7);
		
	});
	
    $('#keyword').typeahead({
        source:mysrc,
        itemSelected: displayKeys 
    });
});


function reject(content_id){
	var str= "#"+content_id+"divOpinion";
	$(str).show();
}

function approve(content_id,user_id){
	var dataString = 'content_id='+ content_id+
					 '&user_id='+ user_id;	
			
	$.ajax({
	type: 'POST',
	url: "<?php echo base_url();?>index.php/cpanel/approve_content",
	data: dataString,
	cache: false,
	success: function(result){
	var atext = "#"+content_id+"reject";
	var btext = "#"+content_id+"approve";
	$(atext).text("reject");
	$(btext).text("approved");
	}
	});
}
function submitreject (content_id,user_id) {
  var co = "#"+content_id+"checker_opinion";
  var checker_opinion =  $(co).val();
  var ci = "#"+content_id+"content_id";
  var content_id = $(ci).val();
 
 var dataString = 'content_id='+ content_id+
				 '&checker_opinion='+ checker_opinion+
 				 '&user_id='+ user_id;			;		
$.ajax({
	type: 'POST',
	url: "<?php echo base_url();?>index.php/cpanel/reject_content",
	data: dataString,
	cache: false,
	success: function(result){
	var str= "#"+content_id+"divOpinion";
	var div= "#"+content_id+"content_body";
	var atext = "#"+content_id+"reject";
	var btext = "#"+content_id+"approve";
	$(str).text('');
	$(str).hide();
	$(atext).text("rejected");
	$(btext).text("approve");
	}
});
}			

function adminreject(content_id,user_id) {
  var checker_opinion =  "NULL";
  var dataString = 'content_id='+ content_id+
				   '&checker_opinion='+ checker_opinion+
 				   '&user_id='+ user_id;	
$.ajax({
	type: 'POST',
	url: "<?php echo base_url();?>index.php/cpanel/reject_content",
	data: dataString,
	cache: false,
	success: function(result){
	var atext = "#"+content_id+"reject";
	var btext = "#"+content_id+"approve";
	$(atext).text("rejected");
	$(btext).text("approve");
	}
});
}

function getTags(tagId)
{
	var dataString = 'tag_id='+tagId;
	$.ajax({
	type: 'POST',
	url: "<?php echo base_url();?>index.php/cpanel/getContentTotag",
	data: dataString,
	cache: false,
	success: function(result){
		$("#divy").html(result);
		var parsed = JSON.parse(result);
		displayTags(parsed);
	}
});		
}
	function displayTags(parsed)
	{
		$modal_body = "";
		for (var i=0; i < parsed.length; i++) {
			
			$src = "<?php echo base_url();?>";	
			$image="";
			if(parsed[i].item_data.f_image != 'null' && parsed[i].item_data.f_image != 'NULL' &&  parsed[i].item_data.f_image != null)
			{
				$image = 'Image <br><img width="150" height="100" src="'+$src+'uploads/images/160_120/'+parsed[i].item_data.f_image+'" />';
				console.log($image);
			}
			$keys = "";
			for (var j=0; j < parsed[i].item_keywords.length; j++) {
			  $keys+= parsed[i].item_keywords[j].fk_keyword_ar+','+parsed[i].item_keywords[j].fk_keyword_ar+'<br>';
			};
			$modal_body += 
						   '<article>'+
						   '<details>'+
						   '<summary class="form-control">'+
						   'General Info'+
						   '</summary>'+
						   '<h4 class="general_info">'+
						   'English title </h4><p>'+parsed[i].item_data.f_title_en+'</p>'+
						   '<h4 class="general_info">arabic title</h4><p>'+parsed[i].item_data.f_title_ar+'</p>'+
						   '<h4 class="general_info">Arabic category name </h4><p>'+parsed[i].item_data.fc_name_ar+'</p>'+
						   '<h4 class="general_info">English category name </h4><p>'+parsed[i].item_data.fc_name_en+'</p>'+
						   '<h4 class="general_info">Arabic location </h4><p>'+ parsed[i].item_data.f_location_ar+'</p>'+
						   '<h4 class="general_info">English location </h4><p>'+ parsed[i].item_data.f_location_en+'</p>'+
						   '<details>'+
						   '<summary class="form-control">'+
						   'Content Details'+
						   '</summary>'+
						   '<h4 class="content">Arabic content</h4><p>'+parsed[i].item_data.f_content_ar+'</p>'+
						   '<h4 class="content">English content</h4><p>'+parsed[i].item_data.f_content_en+'</p>'+
						   '<h4 class="content">Arabic historical importance</h4><p>'+parsed[i].item_data.f_historical_importance_ar+'</p>'+
						   '<h4 class="content">English historical importance</h4><p>'+parsed[i].item_data.f_historical_importance_en+'</p>'+
						   '<h4 class="content">Keywords</h4><p>'+$keys+'</p>'+
						   '</details>'+
						   '</details>'+
						   '</article>'+
						   $image+'<hr>';				   			
		};
		BootstrapDialog.show({
            title: 'all content related',
            message: 
            		$modal_body,
            buttons: [{
                label: 'Close',
                action: function(dialogItself) {
                    dialogItself.close();
                }
            }
            ]
        });
	}
				
					
					
					
</script>
    
<script type="text/javascript" src="<?php echo base_url();?>js/ajaxfileupload.js"></script>
<script>
var image_name = "NULL";
var video_name= "NULL";
var sound_name = "NULL";
$(function() {
	$('#upload_image_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_image",
				secureuri:false,
				fileElementId:'userfile',
				dataType: 'json',
				data    : {
                'title' : $('#title').val()
           		 },
				success: function (data, status)
				{
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							alert(data.error);
						}
						else
						{
							image_name = data.msg;
							document.getElementById("title").type='text';
							document.getElementById("title").value = image_name;
						}
					}
				},
				error: function (data, status, e)
				{
					alert(e);
				}
			}
		)
		return false;
	});
});
// upload video event
	$(function() {
	$('#upload_video_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_video",
				secureuri:false,
				fileElementId:'videoUserfile',
				dataType: 'json',
				data    : {
                'title' : $('#video_title').val()
           		 },
				success: function (data, status)
				{
					var dummy = document.getElementById("video_title");
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							dummy.type='text';
							dummy.value = data.error;
						}
						else
						{
							video_name = data.msg;
							dummy.type='text';
							dummy.value = video_name;
						}
					}
				},
				error: function (data, status, e)
				{
					alert(data);
					alert(status);
					
				}
			}
		)
		return false;
	});
});
// upload sound event
	$(function() {
	$('#upload_sound_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_soundFile",
				secureuri:false,
				fileElementId:'soundUserfile',
				dataType: 'json',
				data    : {
                'title' : $('#sound_title').val()
           		 },
				success: function (data, status)
				{
					var dummy = document.getElementById("sound_title");
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							dummy.type='text';
							dummy.value = data.error;
						}
						else
						{
							sound_name = data.msg;
							dummy.type='text';
							dummy.value = sound_name;
						}
					}
				},
				error: function (data, status, e)
				{
					dummy.type='text';
					dummy.value = e;
				}
			}
		)
		return false;
	});
});
function update_mm(f_id) {
  data=	 'f_image=' + image_name +
  		 '&f_video='+video_name+
  		 '&f_sound='+sound_name+
         '&f_id='+f_id;
  	$.ajax({
	type: "POST",
	url: "<?php echo base_url();?>index.php/cpanel/update_mm_foloclor_record",
	data: data,
	cache: false,
	success: function(result){
	}
});
}
function skip()
{
	$("#multimedia").hide();
}
</script>