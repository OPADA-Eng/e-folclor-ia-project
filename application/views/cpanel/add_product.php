
<script>
	var mykeys = <?php echo $keys; ?>;
</script>
	
<style type="text/css">
	h3 {
		margin: 0px;
		padding: 0px;	
	}
	.suggestionsBox {
		position: relative;
		left: 30px;
		margin: 10px 0px 0px 0px;
		width: 200px;
		background-color: #212427;
		-moz-border-radius: 7px;
		-webkit-border-radius: 7px;
		border: 2px solid #000;	
		color: #fff;
	}
	.suggestionList {
		margin: 0px;
		padding: 0px;
	}
	
	.suggestionList li {
		
		margin: 0px 0px 3px 0px;
		padding: 3px;
		cursor: pointer;
	}
	
	.suggestionList li:hover {
		background-color: #659CD8;
	}
</style>

<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-12">	
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/add.png" />Add Product</p>
		</div>
		<div class="panel-body">
			<form class="adder" id="adder" name="adder" method="post" action="">
				<label>Product category</label> 
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<select id="cat_en" name="cat_en" class="form-control">
							 <option value="" disabled="disabled" selected="selected">Please select a category</option>
							<?php 
					            foreach($product_category as $row)
					            { 
					              echo '<option value="'.$row->pcat_id .'">'.$row->cat_en.'</option>';
					            }
			            	?>
						</select>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php if($user_type){ ?>
						<!-- Button trigger modal -->
						<button type="button" class="btn btn-success btn-block"  data-toggle="modal" data-target="#category_adder" class="form-control" >add new category</button>
						<?php } ?>
					</div>
				</div>
				<br />	
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>Product Arabic Name</label> <br>
						<input type="text" id="product_name_ar" name="product_name_ar" class="form-control">
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>Product English Name</label><br> 
						<input type="text" id="product_name_en" name="product_name_en" class="form-control">
					</div>
				</div>
				<br />	
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label>Product Type</label> <br>
						<select id="type_en" name="type_en" class="form-control">
							 <option value="" disabled="disabled" selected="selected">Please select The Type</option>
							<?php 
					            foreach($product_type as $row)
					            { 
					              echo '<option value="'.$row->pt_id .'">'.$row->pt_name_en.'</option>';
					            }
			            	?>
						</select>
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label>Minimum Price</label> <br> 
						<input type="number" id="min_price" name="min_price" class="form-control">
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label>Maximum Price</label><br>
						<input type="number" name="max_price" id="max_price" class="form-control" />
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label>Color</label><br>
						<input type="text" name="color" id="color" class="form-control"  />
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label>Weight</label><br>
						<input type="number" name="weight" id="weight" class="form-control"  />
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label>Weight Type</label><br>
						<input type="text" name="weight_type" id="weight_type" class="form-control" />
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label>Volum</label><br>
						<input type="number" name="volum" id="volum" class="form-control" />	
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label>Height</label><br>
						<input type="number" name="height" id="height" class="form-control" />
					</div>
					<div class="col-md-4 col-sm-4 col-xs-12">
						<label>Width</label><br>
						<input type="number" name="width" id="width" class="form-control" />
					</div>
				</div>
				<br>
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>Max Quantity</label><br>
						<input type="number" name="max_quantity" id="max_quantity" class="form-control" />
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>Min Quantity</label><br>
						<input type="number" name="min_quantity" id="min_quantity" class="form-control" />
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>Arabic Details</label><br>
						<textarea name="details_ar" id="details_ar" class="form-control" rows="5"></textarea>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<label>English Details</label> <br>
						<textarea name="details_en" id="details_en" class="form-control" rows="5"></textarea>
					</div>
				</div>
				<br />
				<label>keywords</label><br>
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
						
						<input type="text" id="keyword" class="form-control" />
						<table cellpadding="4"   border="0">
							<tbody id="selectedkeywords">
							</tbody>
						</table>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<?php if($user_type){ ?>
						<!-- Button trigger modal -->
						<button type="button" class="btn btn-success btn-block btn-leg" data-toggle="modal" data-target="#keyword_adder" >add new keyword</button>
						<?php } ?>
					</div>
				</div>
				<br />
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12"></div>
					<div class="col-md-6 col-sm-6 col-xs-12">
						<input id="submit" type="submit" value="Submit" class="btn btn-success btn-block btn-leg" >
					</div>
				</div>
			</form>
			<div id="kkk">
				<div class="modal fade" id="keyword_adder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
							    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							    <h4 class="modal-title" id="myModalLabel">add new keyword</h4>
    						</div>
							<div class="modal-body" id="form-keyadd">
								<form class="keyadd" name="keyadd">	
									<label>Arabic keyword</label>
									<input type="text" name="fk_ar" id="fk_ar" class="form-control" />
									
									<label>English keyword</label> 
									<input type="text" id="fk_en" name="fk_en" class="form-control" />
								</form>
							</div>
							<div class="modal-footer">
								<input class="btn btn-success btn-block btn-leg" type="submit" value="Add" id="keyadd"/>
								<a href="#" class="btn" data-dismiss="modal">close</a>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div id="ccc">
				<div class="modal fade" id="category_adder" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
							    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
							    <h4 class="modal-title" id="myModalLabel">add category</h4>
    						</div>
							<div class="modal-body" id="form-catadd">
								<form class="catadd" name="catadd">	
								<label>Arabic content category</label>
								<input type="text" name="p_cat_ar" id="p_cat_ar" class="form-control" />
								
								<label>English content category</label> 
								<input type="text" id="p_cat_en" name="p_cat_en" class="form-control" />
								<br>
								</form>
							</div>
							<div class="modal-footer">
								<input class="btn btn-success" type="submit" value="Add" id="catadd"/>
								<a href="#" class="btn" data-dismiss="modal">close</a>
							</div>
						</div>
					</div>
				</div>	
			</div>
			<div id="multimedia" style="display: none">
				<form method="post" action="" id="upload_image_form">
					<div class="row">
						<div class="col-md-4 col-sm-4 col-xs-4">
							    <label for="userfile">Upload Image</label>
							   <input type="hidden" name="title" id="title" value="" class="form-control" />
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4">
							 <input type="file" name="userfile" id="userfile" size="20" class="form-control" />
						</div>
						<div class="col-md-4 col-sm-4 col-xs-4">
							<input type="submit" name="upload_image" value="upload image" id="uploadImage" class="form-control" />
						</div>
					</div>
				</form>
				<br />
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-6">
						<input type="button" value="Post" id="add_mm" name="add_mm" class="btn btn-success btn-block btn-leg" onclick="update_mm();"/>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-6">
						<input type="button" value="Skip" id="skip_mm" name="skip_mm" onclick="skip();" class="btn btn-success btn-block btn-leg"/>
					</div>
				</div>	
			</div>
			<div id="divt">	</div>
			
		</div>
	</div>
</div>
	

<script type="text/javascript" src="<?php echo base_url();?>js/ajaxfileupload.js"></script>
	
<!--upload image using ajax then submit new content with ajax also-->
<script>
var image_name = "NULL";
var pc_id;
// upload image event
$(document).ready(function () {
 $('form.adder').on('submit', function() {
   var obj = $(this), // (*) references the current object/form each time
   method = obj.attr('method'),
   data = {};
   keys=[];
   obj.find('[name]').each(function(index, value) {
   // console.log(value);
   var obj = $(this),
    name = obj.attr('name'),
    value = obj.val();
    if($(obj).is(':checked')) { 
    							
    							if(name =='keywords[]')
    							{
    								keys.push(value);
    							}
    	}
    if($(obj).val()!="" & obj.attr('checked')!="checked"){data[name] = value;}
  });
  data['keys']=keys;
  console.log(data);
  //AJAX Code To Submit Form.
	$.ajax({
	type: method,
	url: "<?php echo base_url();?>index.php/cpanel/add_product_record",
	data: data,
	cache: false,
	success: function(result){
		if(result != "failed")
		{
		  pc_id = result;
		  $('#adder').hide(1000);
		  $('#multimedia').show();
		}	
	}
	});
		return false;
	});
});

$(function() {
	$('#upload_image_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_image",
				secureuri:false,
				fileElementId:'userfile',
				dataType: 'json',
				data    : {
                'title' : $('#title').val()
           		 },
				success: function (data, status)
				{
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							alert(data.error);
						}
						else
						{
							image_name = data.msg;
							document.getElementById("title").type='text';
							document.getElementById("title").value = image_name;
						}
					}
				},
				error: function (data, status, e)
				{
					alert(e);
				}
			}
		)
		return false;
	});
});


function update_mm() {
  data=	 'f_image=' + image_name +
         '&pc_id='+pc_id;
  	$.ajax({
	type: "POST",
	url: "<?php echo base_url();?>index.php/cpanel/update_im_product_record",
	data: data,
	cache: false,
	success: function(result){
	 $("#multimedia").hide(1000);
	 $("#adder").show();
	 clearform();
	}
});
}

function skip()
{
	$("#multimedia").hide(1000);
	 $("#adder").show();
	 clearform();
}

function clearform()
{
	$("#product_name_ar").val("");
	$("#product_name_en").val("");
	$("#min_price").val("");
	$("#max_price").val("");
	$("#details_ar").val("");
	$("#details_en").val("");
	$("#max_quantity").val("");
	$("#min_quantity").val("");
	$("#weight").val("");
	$("#weight_type").val("");
	$("#width").val("");
	$("#height").val("");
	$("#color").val("");
	$("#volum").val("");
	delete_tablerows(document.getElementById("selectedkeywords"));
}

function delete_tablerows(table){
	var rowCount = table.rows.length;
	while(table.rows.length > 0) {
		table.deleteRow(0);
}
}
</script>

<script>
		$(document).ready(function () {
			$("input#catadd").click(function(){
			
		$.ajax({			
					type: "POST",
					url:"<?php echo base_url();?>index.php/cpanel/add_p_category",
					data: $('form.catadd').serialize(),
					success: function(responce){
						var parsed = JSON.parse(responce);
    					var newOption = $('<option value='+parsed.pcat_id +'>'+ parsed.cat_en +'</option>');
   						$('#cat_en').append(newOption);
   						$('#p_cat_ar').val("");
   						$('#p_cat_en').val("");			
   					},
					error: function(){
						alert("failure");
					}
				});
			});
		});
</script>
<script>
	function removeparent(e){
			e.parentNode.remove();
			console.log(e.parentNode);
		}
		$(document).ready(function () {
			$("input#keyadd").click(function(){
				$.ajax({
					type: "POST",
					url:"<?php echo base_url();?>index.php/cpanel/add_keyword",
					data: $('form.keyadd').serialize(),
					success: function(responce){
							var parsed = JSON.parse(responce);
							var name = parsed.fk_keyword_en;
							var val = parsed.fk_id;
    					    $('#selectedkeywords').append('<tr><td><input checked ="checked" type="checkbox" name="keywords[]"  value="'+val+'"/>'+name+'</td></tr>');
						    $("#fk_ar").val("");
						    $("#fk_en").val("");
   					},
					error: function(){
						alert("failure");
					}
				});
			});
		});
 </script>
    
<script>
function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

function displayKeys(item, val, text) {
    var name = val; 
    console.log(name);
    var newKey = $('<tr><td><input checked ="checked" type="checkbox" name="keywords[]"  value="'+val+'"/>'+text+'</td></tr>');
    $('#selectedkeywords').append(newKey);
    $('#keyword').val('');
}
function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

$(function () {
	
	mysrc =[];
	for (var k = 0; k < mykeys.length; k++) {
		mysrc.push({id: mykeys[k].fk_id , name: mykeys[k].fk_keyword_en});
	}
	
    $('#keyword').typeahead({
        source:mysrc,
        itemSelected: displayKeys 
    });
    function getType(item, val, text)
    {
    	$('#weight_type').val(text);
    }
    $('#weight_type').typeahead({
    	source:[
		    { id: 1, name: 'kg' ,p: 'kilo ghram'},
		    { id: 2, name: 'ton',contry: 'ton' },
		    { id: 3, name: 'unit',contry: 'unit' }
	    ],
	    itemSelected: getType
    });
});
</script>