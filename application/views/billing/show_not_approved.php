<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-9 col-sm-9 col-xs-12">
	
        <br/>
        <div class="panel panel-default">
            <div class="panel-heading">
                <p> <img src="<?php echo base_url(); ?>images/accept_bill.png"  />accept bill</p>
            </div>
            <div class="panel-body">
				
              <div class="table-responsive">
                   		<?php	if(!$bills){
            					  echo '<p>bills not found</p>';
          				  	}else {?>	
								<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
										<thead>
				                            <tr>
												<th><?php echo '<p>buyer </p>';?></th>
												<th><?php echo '<p>bill type</p>';?></th>		
												<th><?php echo '<p>bill kind </p>';?></th>	
												<th><?php echo '<p>total</p>';?></th>
												<th><?php echo '<p>accept</p>';?></th>	
											</tr>
				                        </thead>
				                        <tbody>
								    	    <?php  foreach($bills as $b){?>
								    	  	<tr class="gradeX">
								    	  		
								    	    <td> <?php 	echo $b->username; ?></td>
											<td> <?php 	echo $b->bt_name_en; ?></td>
											<td> <?php 	echo $b->bill_kind; ?></td>
											<td> <?php 	echo $b->b_total; ?></td>
											<td><a href="#" id ="approve_bill" name="<?php echo $b->b_id?>"> approve</a> </td>
									    	<?php  }?>
								 			  
							 			</tbody>
				 				</table>
				 				<?php } ?>
               	 <!-- On rows -->
                </div>
            </div>
        </div>
</div>
<div class="col-md-1 col-sm-1 col-xs-12"></div>

<script>
	$("#approve_bill").click(function(){
		bill_id=$(this).attr('name');
		$.ajax({
        	url:"<?php echo base_url();?>index.php/accounting/approve_bill/"+bill_id,
        	method:'GET',
        	data:'',
        	success:function(data){
        		//show_cart();
        		showMessage(data);
        	},
        	error :function(xhr,textStatus,errorThrown){
        		alert('request failed: '+ errorThrown);
        	}	
        });
	});
	
	function showMessage(myMessage){
		BootstrapDialog.show({
            message: myMessage
        });
     }
     
</script>