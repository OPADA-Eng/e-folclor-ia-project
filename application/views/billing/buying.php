<style>
.field:hover{
  background-color: #1C6093;
  -webkit-transition:all 0.8s linear 0s;
  -moz-transition:all 0.8s linear 0s;
  -o-transition:all 0.8s linear 0s;
  transition:all 0.8s linear 0s;
  color:#FFFFFF;
  padding: 10px;
  box-shadow: 0px 0px 8px #1C6093;
}

.field{
	  background-color: #428BCA;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}
</style>
<div class="col-md-2 col-sm-2 col-xs-12"></div>
<div class="col-md-8 col-sm-8 col-xs-8">
	<br/>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/buying_bill.png" />Add buying bill</p>
		</div>
		<div class="panel-body">
			<div class="row">
				<?php if ($mode != 'sucessful'){ ?>
					<?php echo form_open('accounting/buying_bill') ?>
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6"> 
							<p class="field"> chosse product</p>
							<select name="prod_id" class=" font-input form-control">
								<?php  foreach ($products as $key => $value) {
									echo '<option value="'.$value->pc_id.'"   >'.$value->product_name_en.'</option>';
								}?>
							</select>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6">
							<p class="field"> chosse respository</p>
							<select name="rep_id" class="font-input form-control">
								<?php  foreach ($repositories as $key => $value) {
									echo '<option value="'.$value->r_id.'">'.$value->r_name_en.' </option>';
								}?>
							</select>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6">
							<p class="field">Enter product qountity</p>
							<?php echo form_input('qty',set_value('qty'),'class="font-input form-control" placeholder="Enter product qountity" required=""'); ?>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6"> 
							<p class="field">Enter product price</p>
							<?php echo form_input('price',set_value('price'),'class="font-input form-control" placeholder="Enter price" required=""'); ?>
						</div>
					</div>
					<br />
					<div class="row">
						<div class="col-md-6 col-sm-6 col-xs-6"> 
							<p class="field"> enter bill kind </p>
							<select name="bill_kind" class="font-input form-control">
								<option  value="wholesale"> wholesale</option>
								<option  value="retail">retail</option>
							</select>
						</div>
						<div class="col-md-6 col-sm-6 col-xs-6"> 
							<p class="field"> enter bond type </p>
							<select name="bond_type" class="font-input form-control">
								<option  value="cash"> cash</option>
								<option  value="later">later</option>
							</select>
						</div>
					</div>
					<br />
					<?php echo form_submit('insert','Add','class="btn btn-log btn-block btn-primary"');?>
					<?php echo form_close();?>
				<?php } 
				echo $message;
				?>
				
			</div>
			
		</div>
	</div>
</div>




