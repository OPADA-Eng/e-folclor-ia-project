
<div id="in_cart">
<table class="tabel-responsive" cellpadding="6" cellspacing="1" style="width:100%" border="0">

<tr>
  <th>الكمية</th>
  <th>الوصف</th>
  <th style="text-align:right">السعر</th>
  <th style="text-align:right">الأجمالي الجزئي</th>
</tr>

<?php $i = 1; ?>

<?php foreach ($this->cart->contents() as $items): ?>

	<?php  // echo form_hidden($i.'[rowid]', $items['rowid']); ?>

	<tr>
	  <td><?php  echo form_input(array('name' => $items['id'], 'value' => $items['qty'], 'maxlength' => '3', 'size' => '5')); ?> </td>
	  <td>
		<?php echo $items['name']; ?>

			<?php if ($this->cart->has_options($items['rowid']) == TRUE): ?>

				<p>
					<?php foreach ($this->cart->product_options($items['rowid']) as $option_name => $option_value): ?>

						<strong><?php echo $option_name; ?>:</strong> <?php echo $option_value; ?><br />

					<?php endforeach; ?>
				</p>

			<?php endif; ?>

	  </td>
	  <td style="text-align:right"><?php echo $this->cart->format_number($items['price']); ?></td>
	  <td style="text-align:right">$<?php echo $this->cart->format_number($items['subtotal']); ?></td>
	</tr>

<?php $i++; ?>

<?php endforeach; ?>

<tr>
  <td colspan="2"> </td>
  <td class="right"><strong>الإجمالي</strong></td>
  <td class="right">$<?php echo $this->cart->format_number($this->cart->total()); ?></td>
</tr>

</table>
</div>