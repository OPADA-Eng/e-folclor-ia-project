<head>
	<script src="<?php echo base_url();?>js/jquery.js" type="text/javascript" ></script>
<script src="<?php echo base_url();?>js/bootstrap-typeahead.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.js"></script>
 <link href="<?php echo base_url();?>css/cp/bootstrap.css" rel="stylesheet">
<script>
	var mykeys = <?php echo $keys; ?>;
</script>
	
<style type="text/css">
	h3 {
		margin: 0px;
		padding: 0px;	
	}
	.suggestionsBox {
		position: relative;
		left: 30px;
		margin: 10px 0px 0px 0px;
		width: 200px;
		background-color: #212427;
		-moz-border-radius: 7px;
		-webkit-border-radius: 7px;
		border: 2px solid #000;	
		color: #fff;
	}
	.suggestionList {
		margin: 0px;
		padding: 0px;
	}
	
	.suggestionList li {
		
		margin: 0px 0px 3px 0px;
		padding: 3px;
		cursor: pointer;
	}
	
	.suggestionList li:hover {
		background-color: #659CD8;
	}
</style>
</head>
<body>
		<div class="panel panel-default">
			<div class="panel-heading">
				<p> <img src="<?php echo base_url();?>images/manage_product.png" /> إضافة منتج</p>
			</div>
			<div class="panel-body">
				<form class="adder" id="adder" name="adder" method="post" action="">
				<p class="field">تصنيف المنتج</p> 
				<br>	
				<!-- drop down list-->
				<select id="cat_en" name="cat_en">
					 <option value="" disabled="disabled" selected="selected">اختر تصنيف المنتج</option>
					<?php 
			            foreach($product_category as $row)
			            { 
			              echo '<option value="'.$row->pcat_id .'">'.$row->cat_en.'</option>';
			            }
	            	?>
				</select>				
				<br>			
				<p class="field">الاسم العربي</p> <br>
				<input type="text" id="product_name_ar" name="product_name_ar" class="form-control font-input">
				<br>
				<p class="field">الاسم الإنكليزي</p> <br>
				<input type="text" id="product_name_en" name="product_name_en" class="form-control font-input">
				<br>
				<p class="field">نوع المنتج</p> 
				<br>	
				<!-- drop down list-->
				<select id="type_en" name="type_en">
					 <option value="" disabled="disabled" selected="selected">اختر نوع</option>
					<?php 
			            foreach($product_type as $row)
			            { 
			              echo '<option value="'.$row->pt_id .'">'.$row->pt_name_en.'</option>';
			            }
	            	?>
				</select>
				<br>
				<p class="field">السعر الأدنى</p> <br> 
				<input type="number" id="min_price" name="min_price" class="form-control font-input">
				<br>
				<p class="field">السعر الأعلى</p>
				<br>
				<input type="number" name="max_price" id="max_price"  class="form-control "/>
				<br>
				<p class="field">اللون</p>
				<br>
				<input type="text" name="color" id="color"  class="form-control font-input"/>	<br>
				<p class="field">الوزن</p>
				<br>
				<input type="number" name="weight" id="weight"  class="form-control font-input"/>	<br>
				<p class="field">نزع الوزن</p>
				<br>
				<input type="text" name="weight_type" id="weight_type" class="form-control font-input"/>	<br>
				<p class="field">Volum</p>
				<br>
				<input type="number" name="volum" id="volum" class="form-control font-input" />	<br>
				<p class="field">الارتفاع</p>
				<br>
				<input type="number" name="height" id="height" class="form-control font-input"/>	<br>
				<p class="field">العرض</p>
				<br>
				<input type="number" name="width" id="width" class="form-control font-input"/>	<br>
				<p class="field">الكمية القصوى</p>
				<br>
				<input type="number" name="max_quantity" id="max_quantity" class="form-control font-input" />	<br>
				<p class="field">الكمية الأدنى</p>
				<br>
				<input type="number" name="min_quantity" id="min_quantity" class="form-control font-input"/>	<br>
				<p class="field">التفاصيل بالعربي</p>
				<br>
				<textarea name="details_ar" id="details_ar" class="form-control font-input" ></textarea>
				<br>
				
				<p class="field">التفاصيل بالإنكليزي</p> 
				<br>
				<textarea name="details_en" id="details_en" class="form-control font-input"></textarea>
				<br>
				<div>
					<p class="field">الكلمات المفتاحية</p>
					<br>
					<input type="text" id="keyword" class="form-control font-input"/>
					<table cellpadding="4"   border="0">
						<tbody id="selectedkeywords">
						</tbody>
					</table>
				</div>
				<input id="submit" type="submit" value="Add Product" class="btn btn-primary btn-block">
				</form>
				<div id="multimedia" style="display: none">
					تمت إضافة المنتج بنجاح, يمكنك أختيار صورة للمنتج.
					
					<!-- to upload image-->
					<p class="field" for="userfile">رفع صورة</p>
					<div class="row">
						<div class="col-md-4 col-sm-12 col-xs-12"></div>
						<form method="post" action="" id="upload_image_form">
							<div class="col-md-4 col-sm-12 col-xs-12">
								 <input type="hidden" name="title" id="title" value="" />
					       		 <input type="file" name="userfile" id="userfile" size="20" class="form-control" />
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
								 <input type="submit" name="upload_image" value="رفع صورة" id="uploadImage"  class="form-control"/>
							</div>
						</form>
					</div>
					
				        
				       
				       
				
				<input type="button" value="Post" id="add_mm" name="add_mm" onclick="update_mm();" class="btn btn-primary"/>
				<input type="button" value="Skip" id="skip_mm" name="skip_mm" onclick="thankyou();" class="btn btn-primary" />
				</div>
				<div id="divt"  style="display: none">	
					شكراً
				</div>
			</div>
		</div>
</body>
<script type="text/javascript" src="<?php echo base_url();?>js/ajaxfileupload.js"></script>
	
<!--upload image using ajax then submit new content with ajax also-->
<script>
var image_name = "NULL";
var pc_id;
// upload image event
$(document).ready(function () {
 $('form.adder').on('submit', function() {
   var obj = $(this), // (*) references the current object/form each time
   method = obj.attr('method'),
   data = {};
   keys=[];
   obj.find('[name]').each(function(index, value) {
   // console.log(value);
   var obj = $(this),
    name = obj.attr('name'),
    value = obj.val();
    if($(obj).is(':checked')) { 
    							
    							if(name =='keywords[]')
    							{
    								keys.push(value);
    							}
    	}
    if($(obj).val()!="" & obj.attr('checked')!="checked"){data[name] = value;}
  });
  data['keys']=keys;
  console.log(data);
  //AJAX Code To Submit Form.
	$.ajax({
	type: method,
	url: "<?php echo base_url();?>index.php/pmg/add_product_record",
	data: data,
	cache: false,
	success: function(result){
	  pc_id = result;
	  $('#adder').hide(1000);
	  $('#multimedia').show();
	}
	});
		return false;
	});
});

$(function() {
	$('#upload_image_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_image",
				secureuri:false,
				fileElementId:'userfile',
				dataType: 'json',
				data    : {
                'title' : $('#title').val()
           		 },
				success: function (data, status)
				{
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							alert(data.error);
						}
						else
						{
							image_name = data.msg;
							document.getElementById("title").type='text';
							document.getElementById("title").value = image_name;
							// alert(data.msg);	
							// alert(image_name);	
						}
					}
				},
				error: function (data, status, e)
				{
					alert(e);
				}
			}
		)
		return false;
	});
});


function update_mm() {
  data=	 'f_image=' + image_name +
         '&pc_id='+pc_id;
  	$.ajax({
	type: "POST",
	url: "<?php echo base_url();?>index.php/pmg/update_im_product_record",
	data: data,
	cache: false,
	success: function(result){
	  $('#multimedia').hide(1000);
	  $('#divt').show(); 
	}
});
}
function thankyou()
{
	  $('#multimedia').hide(1000);
	  $('#divt').show(); 
}
</script>
   
<script>
function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

function displayKeys(item, val, text) {
    var name = val; 
    console.log(name);
    var newKey = $('<tr><td><input checked ="checked" type="checkbox" name="keywords[]"  value="'+val+'"/>'+text+'</td></tr>');
    $('#selectedkeywords').append(newKey);
    $('#keyword').val('');
}
function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

$(function () {
	
	mysrc =[];
	for (var k = 0; k < mykeys.length; k++) {
		mysrc.push({id: mykeys[k].fk_id , name: mykeys[k].fk_keyword_en});
	}
	
    $('#keyword').typeahead({
        source:mysrc,
        itemSelected: displayKeys 
    });
    function getType(item, val, text)
    {
    	$('#weight_type').val(text);
    }
    $('#weight_type').typeahead({
    	source:[
		    { id: 1, name: 'kg' ,p: 'kilo ghram'},
		    { id: 2, name: 'ton',contry: 'ton' },
		    { id: 3, name: 'unit',contry: 'unit' }
	    ],
	    itemSelected: getType
    });
});
</script>