
<div class="thumbnail sidebar">
    <ul class="nav nav-pills nav-stacked">
    	<li><?php echo anchor('auth/change_password_user', 'تغيير كلمة السر'); ?></li>
        <li><?php echo anchor('auth/edit_profile/'.$this->session->userdata('user_id'), 'تعديل حسابي'); ?></li>
        <li><?php echo anchor('messages', 'صندوق الرسائل'); ?></li>
        <li><?php echo anchor('accounting/add_balance', 'إضافة رصيد'); ?></li>
        <li><?php echo anchor('accounting/bill_not_paid', 'إدارة فواتيري'); ?></li>
    	<li><?php echo anchor('fmg/add_foloclor_content_by_user', 'إضافة فولوكلور'); ?></li>
    	<li><?php echo anchor('pmg/add_product', 'إضافة منتج'); ?></li>
    </ul>
</div>