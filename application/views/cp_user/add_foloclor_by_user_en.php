<head>
<script src="<?php echo base_url();?>js/jquery.js" type="text/javascript" ></script>
<script src="<?php echo base_url();?>js/bootstrap-typeahead.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.js"></script>
<link href="<?php echo base_url();?>css/cp/bootstrap.css" rel="stylesheet">
<script>
	var mytags = <?php echo $tags; ?>;
	var mykeys = <?php echo $keys; ?>;
</script>
	
<style type="text/css">
	h3 {
		margin: 0px;
		padding: 0px;	
	}
	.suggestionsBox {
		position: relative;
		left: 30px;
		margin: 10px 0px 0px 0px;
		width: 200px;
		background-color: #212427;
		-moz-border-radius: 7px;
		-webkit-border-radius: 7px;
		border: 2px solid #000;	
		color: #fff;
	}
	.suggestionList {
		margin: 0px;
		padding: 0px;
	}
	
	.suggestionList li {
		
		margin: 0px 0px 3px 0px;
		padding: 3px;
		cursor: pointer;
	}
	
	.suggestionList li:hover {
		background-color: #659CD8;
	}
</style>
</head>
<body>
	<div class="panel panel-default">
		<div class="panel-heading">
			<p > <img src="<?php echo base_url();?>images/manage_foloclor.png" />  add folclor</p>
		</div>
		<div class="panel-body">
			<form class="adder" id="adder" method="post" action="">
				<p class="field ">Foloclor category</p> 
					<br>	
					<!-- drop down list-->
					<select id="fc_name" name="fc_name">
						 <option value="" disabled="disabled" selected="selected">Please select a category</option>
						<?php 
				            foreach($foloclor_categories as $row)
				            { 
				              echo '<option value="'.$row->fc_id.'">'.$row->fc_name_en.'</option>';
				            }
		            	?>
					</select>
					<br>	
					<br>		
					<p class="field">Arabic title</p> <br>
					<input type="text" id="f_title_ar" name="f_title_ar" class="form-control font_input">
					<br>		
					<p class="field">English title</p> <br> 
					<input type="text" id="f_title_en" name="f_title_en" class="form-control font_input">
					<br>
					<p class="field">Arabic content</p>
					<br>
					<textarea name="f_content_ar" id="f_content_ar" class="form-control font_input"></textarea>
					<br>
					<p class="field">English content</p>
					<br>
					<textarea name="f_content_en" id="f_content_en" class="form-control font_input"></textarea>
					<br>
					<p class="field">Arabic historical importance</p>
					<br>
					<textarea name="f_hi_ar" id="f_hi_ar" class="form-control font_input"></textarea>
					<br>
					<p class="field">English historicl importance</p> 
					<br>
					<textarea name="f_hi_en" id="f_hi_en" class="form-control font_input"></textarea>
					<br>
					<p class="field">Arabic location</p>
					<input type="text" name="f_location_ar" id="f_location_ar" class="form-control font_input"/>
					<p class="field">English location</p> 
					<input type="text" name="f_location_en" id="f_location_en" class="form-control font_input" />
					<br>
					<div>
						<p class="field">keywords</p>
						<br>
						<input type="text" id="keyword" class="form-control font_input"/>
						<table cellpadding="4"   border="0">
							<tbody id="selectedkeywords">
							</tbody>
						</table>
					</div>
					<div>
						<p class="field">tags</p>
						<br>
						<input type="text" id="tag" class="form-control font_input" />
						<table cellpadding="4"   border="0">
							<tbody id="selectedtags">
							</tbody>
						</table>
					</div>
						
					<input id="submit" type="submit" value="Add Foloclor" class="btn btn-primary btn-block">
					</div>
					</form>
					<div id="multimedia" style="display: none">
						Content was added, please load multimedia data if you want or skip :-)
						
						
					<!-- to upload image-->
					<p  class="field" for="userfile">Upload Image</p>
						<div class="row">
							<form method="post" action="" id="upload_image_form">
	    					
							<div class="col-md-4 col-sm-12 col-xs-12">
								 <input type="hidden" name="title" id="title" value="" />
					      		 <input type="file" name="userfile" id="userfile" size="20"  class="form-control"/>
					      		 
							</div>
							<div class="col-md-4 ">
								<input type="submit" name="upload_image" value="upload image" id="uploadImage" class="form-control" />
							</div>	
							</form>	
						</div>	
						
						<!-- to upload video-->
						 <p class="field" for="userfile">Upload Video</p>	 
						 <div class="row">
						 	<form method="post" action="" id="upload_video_form">
						 	<div class="col-md-4 col-sm-12 col-xs-12">
						 		<input type="hidden" name="title" id="video_title" />
					        	<input type="file" name="videoUserfile" id="videoUserfile" size="20" class="form-control" />
						 	</div>
						 	<div class="col-md-4 col-sm-12 col-xs-12">
						 		<input type="submit" name="upload_video" value="upload video" id="uploadVideo"  class="form-control"/>
						 	</div>
						 	</form>
						 </div>
						 
						 <!-- to upload sound file-->
						<p class="field" for="userfile">Upload Sound</p>
						<div class="row">
							<form method="post" action="" id="upload_sound_form">
							<div class="col-md-4 col-sm-12 col-xs-12">
								<input type="hidden" name="title" id="sound_title" value="" />
					        	<input type="file" name="soundUserfile" id="soundUserfile" size="20" class="form-control" />
							</div>
							<div class="col-md-4 col-sm-12 col-xs-12">
							 	<input type="submit" name="upload_sound" value="upload sound file" id="uploadSound" class="form-control" />
							</div>
							</form>
						</div>
					      
					        
	    				
						
					
					        

					        
	    				
					<input type="button" value="Post" id="add_mm" name="add_mm"  class="btn btn-primary btn-block" onclick="update_mm();"/>
					<input type="button" value="Skip" id="skip_mm" name="skip_mm"  class="btn btn-primary btn-block" onclick="thankyou();" />
					</div>
					<div id="divt" style="display: none">
						Thankyou for adding new foloclor, we will check it.	
					</div>
			</div>
			</div>
		</div>
	</div>	
</body>
<script type="text/javascript" src="<?php echo base_url();?>js/ajaxfileupload.js"></script>
	
<!--upload image using ajax then submit new content with ajax also-->
<script>
var image_name = "NULL";
var video_name= "NULL";
var sound_name = "NULL";
var f_id;
// upload image event
$(document).ready(function () {
 $('form.adder').on('submit', function() {
   var obj = $(this), // (*) references the current object/form each time
   method = obj.attr('method'),
   data = {};
   tags=[];
   keys=[];
   obj.find('[name]').each(function(index, value) {
   // console.log(value);
   var obj = $(this),
    name = obj.attr('name'),
    value = obj.val();
    if($(obj).is(':checked')) { 
    							if(name=='tags[]')
    							{
    								tags.push(value);
    							}
    							else if(name =='keywords[]')
    							{
    								keys.push(value);
    							}
    	}
    if($(obj).val()!="" & obj.attr('checked')!="checked"){data[name] = value;}
  });
  console.log(tags);
  data['keys']=keys;
  data['tags']=tags; 
  console.log(data);
  //AJAX Code To Submit Form.
	$.ajax({
	type: method,
	url: "<?php echo base_url();?>index.php/fmg/add_foloclor_record",
	data: data,
	cache: false,
	success: function(result){
	  f_id = result;
	  $('#adder').hide(1000);
	  $('#multimedia').show();
	}
	});
		return false;
	});
});

function thankyou()
{
	  $('#multimedia').hide(1000);
	  $('#divt').show();
}

$(function() {
	$('#upload_image_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_image",
				secureuri:false,
				fileElementId:'userfile',
				dataType: 'json',
				data    : {
                'title' : $('#title').val()
           		 },
				success: function (data, status)
				{
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							alert(data.error);
						}
						else
						{
							image_name = data.msg;
							document.getElementById("title").type='text';
							document.getElementById("title").value = image_name;
							// alert(data.msg);	
							// alert(image_name);	
						}
					}
				},
				error: function (data, status, e)
				{
					alert(e);
				}
			}
		)
		return false;
	});
});
// upload video event
	$(function() {
	$('#upload_video_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_video",
				secureuri:false,
				fileElementId:'videoUserfile',
				dataType: 'json',
				data    : {
                'title' : $('#video_title').val()
           		 },
				success: function (data, status)
				{
					var dummy = document.getElementById("video_title");
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							dummy.type='text';
							dummy.value = data.error;
						}
						else
						{
							video_name = data.msg;
							dummy.type='text';
							dummy.value = video_name;
						}
					}
				},
				error: function (data, status, e)
				{
					alert(data);
					alert(status);
					
				}
			}
		)
		return false;
	});
});
// upload sound event
	$(function() {
	$('#upload_sound_form').submit(function(e) {
		e.preventDefault();
		$.ajaxFileUpload
		(
			{
				url:"<?php echo base_url();?>index.php/cpanel/upload_soundFile",
				secureuri:false,
				fileElementId:'soundUserfile',
				dataType: 'json',
				data    : {
                'title' : $('#sound_title').val()
           		 },
				success: function (data, status)
				{
					var dummy = document.getElementById("sound_title");
					if(typeof(data.error) != 'undefined')
					{
						if(data.error != '')
						{	
							dummy.type='text';
							dummy.value = data.error;
						}
						else
						{
							sound_name = data.msg;
							dummy.type='text';
							dummy.value = sound_name;
						}
					}
				},
				error: function (data, status, e)
				{
					dummy.type='text';
					dummy.value = e;
				}
			}
		)
		return false;
	});
});
function update_mm() {
  data=	 'f_image=' + image_name +
  		 '&f_video='+video_name+
  		 '&f_sound='+sound_name+
         '&f_id='+f_id;
  	$.ajax({
	type: "POST",
	url: "<?php echo base_url();?>index.php/fmg/update_mm_foloclor_record",
	data: data,
	cache: false,
	success: function(result){
	  $('#multimedia').hide(1000);
	  $('#divt').show();
	}
});
}
</script>
<script>	
function displayRes(item, val, text) {
    var name = val; 
    console.log(name);
    var newTag = $('<tr><td><input checked ="checked" type="checkbox" name="tags[]"  value="'+val+'"/>'+text+'</td></tr>');
    $('#selectedtags').append(newTag);
    $('#tag').val('');
}
function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

$(function () {
	
	mysrc =[];
	for (var i = 0; i < mytags.length; i++) {
		mysrc.push({id: mytags[i].ft_id , name: mytags[i].f_tag_en});
	}
	
    $('#tag').typeahead({
         source:mysrc,
        itemSelected: displayRes 
    });
});


function displayKeys(item, val, text) {
    var name = val; 
    console.log(name);
    var newKey = $('<tr><td><input checked ="checked" type="checkbox" name="keywords[]"  value="'+val+'"/>'+text+'</td></tr>');
    $('#selectedkeywords').append(newKey);
    $('#keyword').val('');
}
function removeparent(e){
	e.parentNode.remove();
	console.log(e.parentNode);
}

$(function () {
	
	mysrc =[];
	for (var k = 0; k < mykeys.length; k++) {
		mysrc.push({id: mykeys[k].fk_id , name: mykeys[k].fk_keyword_en});
	}
	
    $('#keyword').typeahead({
        source:mysrc,
        itemSelected: displayKeys 
    });
});
</script>