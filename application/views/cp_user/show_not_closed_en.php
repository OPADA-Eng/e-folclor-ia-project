<div id="show_not_closed">
			<div class="panel panel-default">
				<div class="panel-heading">
					<p> <img src=" <?php echo base_url(); ?>images/billing.png" /></p>
				</div>
				<div class="panel-body">
						<?php	if(!$bills){
				  echo '<p>bills not found</p>';
		  	}else {?>	
				<table cellpadding=0 cellspacing=10 class="table table-striped table-bordered table-hover" id="dataTables-example">
						<thead>
                            <tr>
								<th><?php echo '<p>bill type</p>';?></th>		
								<th><?php echo '<p>total </p>';?></th>	
								<th><?php echo '<p>close</p>';?></th>
								<th><?php echo '<p>retreat</p>';?></th>	
							</tr>
                        </thead>
                        <tbody>
				    	    <?php  foreach($bills as $b){?>
				    	  	<tr class="gradeX">
				    	  		
				    	    <td> <?php 	echo $b->bt_name_en; ?></td>	
							<td> <?php 	echo $b->b_total; ?></td>
							<td><a href="#" id ="close_bill_bt" name="<?php echo $b->b_id?>"> close</a> </td>
							<td><a href="#" id ="back_bill_bt" name="<?php echo $b->b_id?>"> retreat</a> </td>
					    	<?php  }?>
				 			  
			 			</tbody>
 				</table>
 				<?php } ?>
				</div>
			</div>

 </div>
<script>
	$('#close_bill_bt').click(function(){ 
			id=$(this).attr('name');
			$.ajax({
			 url: "<?php echo base_url();?>index.php/accounting/close_bill/"+id,
			 type: 'POST',
			 data: '',
			 success : function(data){
			 	showMessage(data);
			 },
			 error : function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			 }
			});
			
	});
	
	$('#back_bill_bt').click(function(){ 
			id=$(this).attr('name');
			$.ajax({
			 url: "<?php echo base_url();?>index.php/accounting/back_bill/"+id,
			 type: 'POST',
			 data: '',
			 success : function(data){
			 	showMessage(data);
			 },
			 error : function(xhr, textStatus, errorThrown){
			       alert('request failed: '+ errorThrown);
			 }
			});
			
	});
</script>