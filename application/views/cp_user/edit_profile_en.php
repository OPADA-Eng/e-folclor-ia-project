
	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/edit_user.png" />Edit Profile</p>
		</div>
		<div class="panel-body">
			<?php echo form_open('auth/edit_profile/'.$this->session->userdata('user_id'));?>

			      <p class="field">First Name: </p>  <br />
			            <?php echo form_input($first_name,set_value('first name'),'class="form-control font_input" required=""');?>
			      
			
			      <p class="field">Last Name: </p><br />
			            <?php echo form_input($last_name,set_value('last name'),'class="form-control font_input" required=""');?>
			      

			      <p class="field"> Phone part 1:</p> <br />
			            <?php echo form_input($phone1,'','class="font_input" required=""');?><br />
			             <p class="field">Phone part 2:</p>
			            <?php echo form_input($phone2,'','class="font_input" required=""');?><br />
			              <p class="field">phone part 3:</p>
			            <?php echo form_input($phone3,'','class="font_input" required=""');?>
			      
			
			      <p class="field">Password: (if changing password): </p> <br />
			            <?php echo form_input($password,set_value('password'),'class="form-control font_input" ');?>
			     
			
			      <p class="field">Confirm Password: (if changing password) </p> <br />
			            <?php echo form_input($password_confirm,set_value('password confirm'),'class="form-control font_input" ');?>
			     
			
			
			      <?php echo form_hidden('id', $user->id);?>
			      <?php echo form_hidden($csrf); ?>
				   <p><?php echo form_submit('submit', 'Save User','class="btn btn-lg btn-primary btn-block"');?></p>
			
			
			<?php echo form_close();?>
			<div id="infoMessage"><?php echo $message;?></div>
		</div>
	</div>




