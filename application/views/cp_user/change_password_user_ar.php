<style>
.field:hover{
  background-color: #1C6093;
  -webkit-transition:all 0.8s linear 0s;
  -moz-transition:all 0.8s linear 0s;
  -o-transition:all 0.8s linear 0s;
  transition:all 0.8s linear 0s;
  color:#FFFFFF;
  padding: 10px;
  box-shadow: 0px 0px 8px #1C6093;
}

.field{
	  background-color: #428BCA;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}

</style>

	<div class="panel panel-default">
		<div class="panel-heading">
			<p><img src="<?php echo base_url();?>images/change_pass.png" />تغيير كلمة السر </p>
		</div>
		<div class="panel-body">
			<?php echo form_open("auth/change_password_user");?>

			      <p class="field">كلمة السر القديمة:   </p> <br />
			      <?php echo form_input($old_password,set_value('Old Password'),'class="form-control font_input" placeholder="Old Password" required=""');?>
			    
			      
			      <p class="field">كلمة السر الجديدة (على الأقل <?php echo $min_password_length;?> طول المحارف): </p> <br />
			      <?php echo form_input($new_password,set_value('New Password'),'class="form-control font_input" placeholder="New Password" required=""');?>
			      
			      
			      <p class="field">تأكيد كلمة السر الجديدة: </p> <br />
			      <?php echo form_input($new_password_confirm,set_value('New Password Confirm'),'class="form-control font_input" placeholder="New Password Confirm" required=""');?>
			      
			      
			      <?php echo form_input($user_id);?>
			      <p><?php echo form_submit('submit', 'تغيير','class="btn btn-lg btn-primary btn-block font_input"');?> </p>
			      
			<?php echo form_close();?>
			<div id="infoMessage"><?php echo $message;?></div>
		</div>
	</div>





