<style>
.field:hover{
	  background-color: #1C6093;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #1C6093;
}

.field{
	  background-color: #428BCA;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}

</style>
<div id="content" class="container">
	<div id="main" class="row-fluid">
		<div class="row">
			<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <img src="<?php echo base_url(); ?>images/mypc_add.png"/>Register</h2>
		</div>
		<div class="panel-body">
			<h3>Please enter the users information below.</h3>
			<?php echo form_open("auth/register");?>
			<div class="row-fluid">
				<div class="span4">
					<h4 class="field">First Name</h4>
					<?php echo form_input($first_name,set_value('first name'),'class="form-control font_input" placeholder="first name" required=""');?>
				</div>
				<div class="span4">
					<h4 class="field">Last Name</h4>
					<?php echo form_input($last_name,set_value('last name'),'class="form-control font_input" placeholder="last name" required=""');?>
				</div>
				<div class="span4">
					<h4 class="field">Email</h4>
					<?php echo form_input($email,set_value('email'),'class="form-control font_input" placeholder="email" required=""');?>
				</div>
			</div>
			<div class="row-fluid">
				<h4 class="field">Phone</h4>
			</div>
			<div class="row-fluid">
				
				<div class="span4">
					<?php echo form_input($phone1,'','class="font_input" required=""');?>
				</div>
				<div class="span4">
					<?php echo form_input($phone2,'','class="font_input" required=""');?>
				</div>
				<div class="span4">
					<?php echo form_input($phone3,'','class="font_input" required=""');?>
				</div>
			</div>
		      <p class="field">
		            Password:   </p> <br />
		            <?php echo form_input($password,set_value('password'),'class="form-control font_input" placeholder="password" required=""');?>
		     
		
		      <p class="field">
		            Confirm Password: </p> <br />
		            <?php echo form_input($password_confirm,set_value('password confirm'),'class="form-control font_input" placeholder="password confirm" required=""');?>
		           
		      
		
			  <p><?php echo form_submit('submit', 'register','class="btn btn-lg btn-primary btn-block"');?></p>
		      
			  <div id="infoMessage"><?php echo $message;?></div>
		<?php echo form_close();?>
		</div>
	</div>
		</div>
		
	</div><!-- #main -->
</div><!-- #content -->
