<style>
.field:hover{
	  background-color: #1C6093;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  box-shadow: 0px 0px 8px #1C6093;
}

.field{
	  background-color: #428BCA;
	  -webkit-transition:all 0.8s linear 0s;
	  -moz-transition:all 0.8s linear 0s;
	  -o-transition:all 0.8s linear 0s;
	  transition:all 0.8s linear 0s;
	  color:#FFFFFF;
	  padding: 10px;
	  
}

</style>
<div id="content" class="container">
	<div id="main" class="row-fluid">
		<div class="row">
			<div class="panel panel-default">
		<div class="panel-heading">
			<h2> <img src="<?php echo base_url(); ?>images/mypc_add.png"/>التسجيل</h2>
		</div>
		<div class="panel-body">
			<?php echo form_open("auth/register");?>
			<div class="row-fluid">
				<div class="span4">
					<h4 class="field">الاسم الأول</h4>
					<?php echo form_input($first_name,set_value('first name'),'class="form-control font_input" placeholder="الاسم الأول" required=""');?>
				</div>
				<div class="span4">
					<h4 class="field">الاسم الاخير</h4>
					<?php echo form_input($last_name,set_value('last name'),'class="form-control font_input" placeholder="الاسم الأخير" required=""');?>
				</div>
				<div class="span4">
					<h4 class="field">الإيميل</h4>
					<?php echo form_input($email,set_value('email'),'class="form-control font_input" placeholder="الإيميل" required=""');?>
				</div>
			</div>
			<div class="row-fluid">
				<h4 class="field">رقم التلفون</h4>
			</div>
			<div class="row-fluid">
				
				<div class="span4">
					<?php echo form_input($phone1,'','class="font_input" required=""');?>
				</div>
				<div class="span4">
					<?php echo form_input($phone2,'','class="font_input" required=""');?>
				</div>
				<div class="span4">
					<?php echo form_input($phone3,'','class="font_input" required=""');?>
				</div>
			</div>
		      <p class="field">
		            كلمة السر: </p> <br />
		            <?php echo form_input($password,set_value('password'),'class="form-control font_input" placeholder="كلمة السر" required=""');?>
		      
		
		      <p class="field">
		            تأكيد كلمة السر:   </p> <br />
		            <?php echo form_input($password_confirm,set_value('password confirm'),'class="form-control font_input" placeholder=" تأكيد كلمة السر" required=""');?>
		           
		    
		
			  <p><?php echo form_submit('submit', 'تسجيل','class="btn btn-lg btn-primary btn-block"');?></p>
		      
			  <div id="infoMessage"><?php echo $message;?></div>
		<?php echo form_close();?>
		</div>
	</div>
		</div>
		
	</div><!-- #main -->
</div><!-- #content -->
